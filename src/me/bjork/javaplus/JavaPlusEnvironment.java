package me.bjork.javaplus;

import me.bjork.javaplus.communication.connection.IServerHandler;
import me.bjork.javaplus.core.ConfigurationData;
import me.bjork.javaplus.core.ConsoleCommands;
import me.bjork.javaplus.core.Logging;
import me.bjork.javaplus.core.figuredata.FigureDataManager;
import me.bjork.javaplus.core.language.LanguageManager;
import me.bjork.javaplus.core.settings.SettingsManager;
import me.bjork.javaplus.database.DatabaseManager;
import me.bjork.javaplus.database.SqlHelper;
import me.bjork.javaplus.habbohotel.Game;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created on 19/05/2017.
 */
public final class JavaPlusEnvironment {

    private static ConfigurationData config;
    private static LanguageManager langage;
    private static SettingsManager settings;
    private static IServerHandler server;
    private static Game game;

    private static String version = "0.0.1-SNAPSHOT";
    public static String SWFrevision = "";

    public static int serverStarted = 0;
    public static Boolean debugging = false;
    public static Boolean isReady = false;
    public static boolean isShuttingDown = false;

    public static String serverIP;
    public static String serverPort;

    public static void main(String[] args) throws Exception {
        try {
            ConfigurationData.setConfiguration(new ConfigurationData("config.ini"));
            SWFrevision = getConfig().get("game.revision");
            debugging = getConfig().get("debug.mode").equals("true");

            JavaPlusEnvironment.serverStarted = getIntUnixTimestamp();
            Logging.println("Javaplus " + version);

            if (debugging)
                Logging.printDebug("Mode DEBUG Enabled");

            DatabaseManager.getInstance().initialize();

            cleanDatabase();

            langage = new LanguageManager();
            langage.init();

            settings = new SettingsManager();
            settings.init();

            FigureDataManager.loadFigureData();

            serverIP = JavaPlusEnvironment.getConfig().get("network.host", "127.0.0.1");
            serverPort = JavaPlusEnvironment.getConfig().get("network.port", "30000");

            server = Class.forName(JavaPlusEnvironment.getServerClassPath()).asSubclass(IServerHandler.class).newInstance();
            server.setIp(serverIP);
            server.setPort(Integer.valueOf(serverPort));

            game = new Game();

            JavaPlusEnvironment.isReady = true;

            if (server.listenSocket()) {
                Logging.println("Server is listening on " + serverIP + ":" + serverPort);
            } else {
                Logging.println("Server could not listen on " + serverPort + ":" + serverPort + ", please double check everything is correct in icarus.properties");
            }

            ConsoleCommands.init();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void cleanDatabase() {

        try (Connection connection = SqlHelper.getConnection())
        {
            Statement statement = connection.createStatement();
            statement.execute("TRUNCATE `catalog_marketplace_data`");
            statement.execute("UPDATE `rooms` SET `users_now` = '0' WHERE `users_now` > '0'");
            statement.execute("UPDATE `users` SET `online` = '0' WHERE `online` = '1'");
            statement.execute("UPDATE `server_status` SET `users_online` = '0', `loaded_rooms` = '0' LIMIT 1");
        }
        catch (SQLException e)
        {
            Logging.exception(e);
        }
        Logging.println("Database -> Cleaned!");
    }

    public static void exit(String message) {
        Logging.printError("Server has shutdown. Reason: \"" + message + "\"");
        System.exit(0);
    }

    public static void shutdownServer()
    {
        JavaPlusEnvironment.isShuttingDown = true;
        JavaPlusEnvironment.isReady = false;
        Logging.println("Server shutting down...");

       if (getGame().getGameCycle() != null)
           getGame().getGameCycle().stop();

       if (JavaPlusEnvironment.getServer().getMessageHandler() != null)
           JavaPlusEnvironment.getServer().getMessageHandler().unregisterPackets();

       if (getGame().getGameClientManager() != null)
           getGame().getGameClientManager().disposeAll();

       if (getGame().getRoomManager() != null)
           getGame().getRoomManager().dispose();

       if (getGame().getRoomCycle() != null)
           getGame().getRoomCycle().stop();

       cleanDatabase();
       Logging.println("Server has successfully shutdown.");
       exit("Server Manual Shutdown");
    }

    public static long getUnixTimestamp()
    {
        return (System.currentTimeMillis() / 1000L);
    }

    public static int getIntUnixTimestamp()
    {
        return (int) (System.currentTimeMillis() / 1000);
    }

    public static Game getGame() {
        return game;
    }

    public static SettingsManager getSettings() {
        return settings;
    }
    public static ConfigurationData getConfig() {
        return ConfigurationData.currentConfig();
    }
    private static String getServerClassPath() {
        return "me.bjork.javaplus.communication.connection.netty.NettyServer";
    }
    public static IServerHandler getServer() { return server; }

    public static int getServerStartedTime() {
        return serverStarted;
    }
}
