package me.bjork.javaplus.communication.connection;

import me.bjork.javaplus.communication.packets.PacketManager;

/**
 * Created on 20/05/2017.
 */
public abstract class IServerHandler {

    private int port;
    private String ip;

    private PacketManager messages;

    public IServerHandler() {
        this.messages = new PacketManager();
    }

    public abstract boolean listenSocket();

    public int getPort() {
        return port;
    }

    public String getIp() {
        return ip;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public PacketManager getMessageHandler() {
        return messages;
    }
}