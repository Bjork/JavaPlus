package me.bjork.javaplus.communication.connection.netty;

import me.bjork.javaplus.communication.connection.IPlayerNetwork;
import me.bjork.javaplus.communication.packets.parsers.OutgoingMessageComposer;
import org.jboss.netty.channel.Channel;

/**
 * Created on 20/05/2017.
 */
public class NettyPlayerNetwork extends IPlayerNetwork {

    private Channel channel;

    public NettyPlayerNetwork(Channel channel, int connectionId) {
        super(connectionId);
        this.channel = channel;
    }

    @Override
    public void close() {
        channel.close();
    }

    @Override
    public void send(OutgoingMessageComposer response) {
        channel.write(response);
    }

    @Override
    public Channel getChannel() {
        return channel;
    }
}