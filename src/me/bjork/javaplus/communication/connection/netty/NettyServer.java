package me.bjork.javaplus.communication.connection.netty;

import me.bjork.javaplus.communication.connection.IServerHandler;
import me.bjork.javaplus.communication.connection.netty.codecs.NetworkDecoder;
import me.bjork.javaplus.communication.connection.netty.codecs.NetworkEncoder;
import me.bjork.javaplus.communication.connection.netty.connections.ConnectionHandler;
import me.bjork.javaplus.core.Logging;
import me.bjork.javaplus.habbohotel.gameclients.GameClientManager;
import org.jboss.netty.bootstrap.ServerBootstrap;
import org.jboss.netty.channel.ChannelException;
import org.jboss.netty.channel.ChannelPipeline;
import org.jboss.netty.channel.socket.nio.NioServerSocketChannelFactory;

import java.net.InetSocketAddress;
import java.util.concurrent.Executors;

/**
 * Created on 20/05/2017.
 */
public class NettyServer extends IServerHandler {

    private NioServerSocketChannelFactory factory;
    private ServerBootstrap bootstrap;
    private GameClientManager sessionManager;

    public NettyServer() {
        super();
        this.sessionManager = new GameClientManager();
    }

    @Override
    public boolean listenSocket() {

        this.factory = new NioServerSocketChannelFactory (
                Executors.newCachedThreadPool(),
                Executors.newCachedThreadPool()
        );


        this.bootstrap = new ServerBootstrap(this.factory);

        ChannelPipeline pipeline = this.bootstrap.getPipeline();

        pipeline.addLast("encoder", new NetworkEncoder());
        pipeline.addLast("decoder", new NetworkDecoder());
        pipeline.addLast("handler", new ConnectionHandler(this.sessionManager));

        try {
            this.bootstrap.bind(new InetSocketAddress(this.getIp(), this.getPort()));
        } catch (ChannelException ex) {
            Logging.exception(ex);
            return false;
        }
        return true;
    }
}
