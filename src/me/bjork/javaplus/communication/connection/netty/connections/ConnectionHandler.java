package me.bjork.javaplus.communication.connection.netty.connections;

import me.bjork.javaplus.JavaPlusEnvironment;
import me.bjork.javaplus.communication.connection.netty.streams.NettyRequest;
import me.bjork.javaplus.core.Logging;
import me.bjork.javaplus.habbohotel.gameclients.GameClient;
import me.bjork.javaplus.habbohotel.gameclients.GameClientManager;
import org.jboss.netty.channel.*;

/**
 * Created on 20/05/2017.
 */
public class ConnectionHandler extends SimpleChannelHandler {

    private GameClientManager sessionManager;
    private Boolean _deciphered = false;

    public ConnectionHandler(GameClientManager sessionManager) {
        this.sessionManager = sessionManager;
    }

    @Override
    public void channelOpen(ChannelHandlerContext ctx, ChannelStateEvent e) {
        if (JavaPlusEnvironment.isShuttingDown) {
            ctx.getChannel().close();
            return;
        }

        sessionManager.addSession(ctx.getChannel());
        GameClient player = (GameClient) ctx.getChannel().getAttachment();
        Logging.printDebug("[ConnID: " + player.getNetwork().getConnectionId() + "] Connection from " + ctx.getChannel().getRemoteAddress().toString().replace("/", "").split(":")[0]);
    }

    @Override
    public void channelClosed(ChannelHandlerContext ctx, ChannelStateEvent e) {
        sessionManager.removeSession(ctx.getChannel());
        GameClient client = (GameClient) ctx.getChannel().getAttachment();
        Logging.printDebug("[ConnID: " + client.getNetwork().getConnectionId() + "] Disconnection from " + ctx.getChannel().getRemoteAddress().toString().replace("/", "").split(":")[0]);

       if (client != null)
           client.disconnect();
    }

    @Override
    public void messageReceived(ChannelHandlerContext ctx, MessageEvent e) {

        try {
            GameClient client = (GameClient) ctx.getChannel().getAttachment();
            NettyRequest request = (NettyRequest) e.getMessage();

            if (request == null) {
                return;
            }

            if (client != null || !JavaPlusEnvironment.isShuttingDown){

                JavaPlusEnvironment.getServer().getMessageHandler().handleRequest(client, request);
            }

        } catch (Exception ex) {
            Logging.exception(ex);
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, ExceptionEvent e) {
        ctx.getChannel().close();
    }
}