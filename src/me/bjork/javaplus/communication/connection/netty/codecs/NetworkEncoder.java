package me.bjork.javaplus.communication.connection.netty.codecs;

import me.bjork.javaplus.communication.packets.parsers.OutgoingMessageComposer;
import org.apache.commons.io.Charsets;
import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.Channels;
import org.jboss.netty.channel.MessageEvent;
import org.jboss.netty.channel.SimpleChannelHandler;

/**
 * Created on 20/05/2017.
 */

public class NetworkEncoder extends SimpleChannelHandler {

    @Override
    @Deprecated
    public void writeRequested(ChannelHandlerContext ctx, MessageEvent e) {

        try {

            if (e.getMessage() instanceof String) {
                Channels.write(ctx, e.getFuture(), ChannelBuffers.copiedBuffer((String) e.getMessage(),  Charsets.ISO_8859_1));
                return;
            }

            if (e.getMessage() instanceof OutgoingMessageComposer) {

                OutgoingMessageComposer msg = (OutgoingMessageComposer) e.getMessage();
                if (!msg.getResponse().isFinalised()) {
                    msg.write();
                }
                //Logging.printDebug("SENT: " + msg.getResponse().getBodyString());
                Channels.write(ctx, e.getFuture(), ChannelBuffers.copiedBuffer(msg.getResponse().get()));
                return;
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}