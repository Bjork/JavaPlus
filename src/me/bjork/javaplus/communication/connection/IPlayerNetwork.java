package me.bjork.javaplus.communication.connection;

import me.bjork.javaplus.communication.packets.parsers.OutgoingMessageComposer;
import org.jboss.netty.channel.Channel;

/**
 * Created on 20/05/2017.
 */
public abstract class IPlayerNetwork {

    private int connectionId;

    public IPlayerNetwork(int connectionId) {
        this.connectionId = connectionId;
    }

    public abstract void send(OutgoingMessageComposer response);
    public abstract void close();

    public abstract Channel getChannel();

    public int getConnectionId() {
        return connectionId;
    }

    public void setConnectionId(int connectionId) {
        this.connectionId = connectionId;
    }
}