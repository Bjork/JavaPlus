package me.bjork.javaplus.communication.interfaces;

/**
 * Created on 20/05/2017.
 */
public interface ClientMessage {
    public int readShort();
    public Integer readInt();
    public boolean readIntAsBool();
    public boolean readBoolean();
    public String readString();
    public byte[] readBytes(int len);
    public String getMessageBody();
    public int getMessageId();
    public byte[] getRawMessage();
}