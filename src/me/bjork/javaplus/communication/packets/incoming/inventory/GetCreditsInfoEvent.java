package me.bjork.javaplus.communication.packets.incoming.inventory;

import me.bjork.javaplus.communication.interfaces.ClientMessage;
import me.bjork.javaplus.communication.packets.MessageEvent;
import me.bjork.javaplus.communication.packets.outgoing.inventory.ActivityPointsComposer;
import me.bjork.javaplus.communication.packets.outgoing.inventory.CreditBalanceComposer;
import me.bjork.javaplus.habbohotel.gameclients.GameClient;

/**
 * Created on 22/05/2017.
 */
public class GetCreditsInfoEvent implements MessageEvent {
    @Override
    public void parse(GameClient client, ClientMessage packet) {

        client.send(new CreditBalanceComposer(client.getHabbo().getCredits()));
        client.send(new ActivityPointsComposer(client.getHabbo().getCurrencies().getAllCurrencies()));
    }
}
