package me.bjork.javaplus.communication.packets.incoming.users;

import me.bjork.javaplus.communication.interfaces.ClientMessage;
import me.bjork.javaplus.communication.packets.MessageEvent;
import me.bjork.javaplus.communication.packets.outgoing.users.UserClubComposer;
import me.bjork.javaplus.habbohotel.gameclients.GameClient;

/**
 * Created on 22/05/2017.
 */
public class ScrGetUserInfoEvent implements MessageEvent {
    @Override
    public void parse(GameClient client, ClientMessage packet) {
        client.send(new UserClubComposer(client));
    }
}
