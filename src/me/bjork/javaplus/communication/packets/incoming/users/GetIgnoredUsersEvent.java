package me.bjork.javaplus.communication.packets.incoming.users;

import me.bjork.javaplus.communication.interfaces.ClientMessage;
import me.bjork.javaplus.communication.packets.MessageEvent;
import me.bjork.javaplus.communication.packets.outgoing.users.IgnoredUsersComposer;
import me.bjork.javaplus.habbohotel.gameclients.GameClient;

import java.util.ArrayList;

/**
 * Created on 22/05/2017.
 */
public class GetIgnoredUsersEvent implements MessageEvent {
    @Override
    public void parse(GameClient client, ClientMessage packet) {
        ArrayList<String> users = new ArrayList<String>();
        client.send(new IgnoredUsersComposer(users));
    }
}
