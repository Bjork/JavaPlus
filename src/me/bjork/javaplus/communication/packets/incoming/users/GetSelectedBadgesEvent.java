package me.bjork.javaplus.communication.packets.incoming.users;

import me.bjork.javaplus.JavaPlusEnvironment;
import me.bjork.javaplus.communication.interfaces.ClientMessage;
import me.bjork.javaplus.communication.packets.MessageEvent;
import me.bjork.javaplus.communication.packets.outgoing.users.HabboUserBadgesComposer;
import me.bjork.javaplus.habbohotel.gameclients.GameClient;
import me.bjork.javaplus.habbohotel.users.Habbo;
import me.bjork.javaplus.habbohotel.users.inventory.BadgeComponent;

/**
 * Created on 30/05/2017.
 */
public class GetSelectedBadgesEvent implements MessageEvent {
    @Override
    public void parse(GameClient client, ClientMessage packet) {
        int userId = packet.readInt();

        Habbo habbo = JavaPlusEnvironment.getGame().getGameClientManager().getHabboByUserId(userId);

        if (habbo != null)
            client.send(new HabboUserBadgesComposer(habbo.getInventory().getBadges().getBadgesSlot(), habbo.getId()));
        else
            client.send(new HabboUserBadgesComposer(BadgeComponent.getBadgesOfflineHabbo(userId), userId));

    }
}
