package me.bjork.javaplus.communication.packets.incoming.users;

import me.bjork.javaplus.communication.interfaces.ClientMessage;
import me.bjork.javaplus.communication.packets.MessageEvent;
import me.bjork.javaplus.communication.packets.outgoing.notifications.BubbleNotificationComposer;
import me.bjork.javaplus.communication.packets.outgoing.rooms.avatar.ActionComposer;
import me.bjork.javaplus.communication.packets.outgoing.rooms.avatar.RespectNotificationComposer;
import me.bjork.javaplus.habbohotel.gameclients.GameClient;
import me.bjork.javaplus.habbohotel.rooms.Room;
import me.bjork.javaplus.habbohotel.rooms.RoomUser;

/**
 * Created on 07/06/2017.
 */
public class RespectUserEvent implements MessageEvent {
    @Override
    public void parse(GameClient client, ClientMessage packet) {
        if (client == null || client.getHabbo() == null || !client.getHabbo().inRoom() || client.getHabbo().getStats().getDailyRespectPoints() <= 0)
            return;

        Room room = client.getHabbo().currentRoom();
        if (room == null)
            return;

        RoomUser roomUser = client.getHabbo().getRoomUser();
        if (roomUser == null)
            return;

        int receiverId = packet.readInt();

        RoomUser receiver = room.getRoomUserManager().getRoomUserByHabbo(receiverId);

        if (receiver == null)
            return;

        receiver.getHabbo().getStats().incrementRespectPoints(1);
        //user.getPlayer().getAchievements().progressAchievement(AchievementType.RESPECT_EARNED, 1); //TODO Achievements

        client.getHabbo().getStats().incrementGivenRespectPoints(1);
        client.getHabbo().getStats().decrementDailyRespects(1);

        room.send(new ActionComposer(client.getHabbo().getRoomUser().getVirtualId(), 7));
        //TODO Respect Notification Enabled
        room.send(new RespectNotificationComposer(receiver.getVirtualId(), receiver.getHabbo().getStats().getRespects()));
        receiver.getClient().send(new BubbleNotificationComposer("You have been respected.\rYou now have " + receiver.getHabbo().getStats().getRespects() + " respect points.", "${image.library.url}notifications/respects.gif"));

        //client.getPlayer().getQuests().progressQuest(QuestType.SOCIAL_RESPECT); //TODO Questing + ACH
        //client.getPlayer().getAchievements().progressAchievement(AchievementType.RESPECT_GIVEN, 1);
    }
}
