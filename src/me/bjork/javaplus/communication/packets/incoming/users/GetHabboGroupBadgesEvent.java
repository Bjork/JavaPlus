package me.bjork.javaplus.communication.packets.incoming.users;

import me.bjork.javaplus.communication.interfaces.ClientMessage;
import me.bjork.javaplus.communication.packets.MessageEvent;
import me.bjork.javaplus.communication.packets.outgoing.users.HabboGroupBadgesComposer;
import me.bjork.javaplus.habbohotel.gameclients.GameClient;
import me.bjork.javaplus.habbohotel.rooms.Room;

/**
 * Created on 24/05/2017.
 */
public class GetHabboGroupBadgesEvent implements MessageEvent {
    @Override
    public void parse(GameClient client, ClientMessage packet) {

        Room Room = client.getHabbo().currentRoom();
        if (Room == null)
            return;

        //TODO
        Room.send(new HabboGroupBadgesComposer());
        client.send(new HabboGroupBadgesComposer());

    }
}
