package me.bjork.javaplus.communication.packets.incoming.navigator;

import me.bjork.javaplus.communication.interfaces.ClientMessage;
import me.bjork.javaplus.communication.packets.MessageEvent;
import me.bjork.javaplus.communication.packets.outgoing.navigator.*;
import me.bjork.javaplus.habbohotel.gameclients.GameClient;

/**
 * Created on 22/05/2017.
 */
public class InitializeNewNavigatorEvent implements MessageEvent {
    @Override
    public void parse(GameClient client, ClientMessage packet) {

        client.send(new NavigatorPreferencesComposer());
        client.send(new NavigatorMetaDataParserComposer());
        client.send(new NavigatorLiftedRoomsComposer());
        client.send(new NavigatorSavedSearchesMessageComposer());
        client.send(new NavigatorCollapsedCategoriesComposer());
    }
}
