package me.bjork.javaplus.communication.packets.incoming.navigator;

import me.bjork.javaplus.JavaPlusEnvironment;
import me.bjork.javaplus.communication.interfaces.ClientMessage;
import me.bjork.javaplus.communication.packets.MessageEvent;
import me.bjork.javaplus.communication.packets.outgoing.navigator.GetGuestRoomResultComposer;
import me.bjork.javaplus.habbohotel.gameclients.GameClient;
import me.bjork.javaplus.habbohotel.rooms.RoomData;

/**
 * Created on 23/05/2017.
 */
public class GetGuestRoomEvent implements MessageEvent {
    @Override
    public void parse(GameClient client, ClientMessage packet) {
        int roomId = packet.readInt();

        RoomData roomData = JavaPlusEnvironment.getGame().getRoomManager().getRoomData(roomId);
        if (roomData == null)
            return;

        Boolean isLoading = packet.readInt() == 1;
        Boolean checkEntry = packet.readInt() == 1;

        client.send(new GetGuestRoomResultComposer(client, roomData, isLoading, checkEntry));
    }
}
