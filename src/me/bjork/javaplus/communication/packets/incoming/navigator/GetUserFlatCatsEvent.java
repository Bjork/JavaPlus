package me.bjork.javaplus.communication.packets.incoming.navigator;

import me.bjork.javaplus.communication.interfaces.ClientMessage;
import me.bjork.javaplus.communication.packets.MessageEvent;
import me.bjork.javaplus.communication.packets.outgoing.navigator.UserFlatCatsComposer;
import me.bjork.javaplus.habbohotel.gameclients.GameClient;

/**
 * Created on 22/05/2017.
 */
public class GetUserFlatCatsEvent implements MessageEvent {
    @Override
    public void parse(GameClient client, ClientMessage packet) {

        if (client == null)
            return;

        client.send(new UserFlatCatsComposer());
    }
}
