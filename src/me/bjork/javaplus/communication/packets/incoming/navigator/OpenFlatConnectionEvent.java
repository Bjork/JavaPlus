package me.bjork.javaplus.communication.packets.incoming.navigator;

import me.bjork.javaplus.communication.interfaces.ClientMessage;
import me.bjork.javaplus.communication.packets.MessageEvent;
import me.bjork.javaplus.habbohotel.gameclients.GameClient;

/**
 * Created on 24/05/2017.
 */
public class OpenFlatConnectionEvent implements MessageEvent {
    @Override
    public void parse(GameClient client, ClientMessage packet) {
        if (client == null || client.getHabbo() == null)
            return;

        int roomId = packet.readInt();
        String password = packet.readString();        

        if (System.currentTimeMillis() - client.getHabbo().getLastRoomRequest() < 500) {
            return;
        }

      //TODO FINISH
        /*if(client.getPlayer().getEntity() != null && !client.getPlayer().isSpectating(id) && !client.getPlayer().hasQueued(id)) {
            if(!client.getPlayer().getEntity().isFinalized()) {
                client.getPlayer().setEntity(null);
            }
        }*/
        
        client.getHabbo().setLastRoomRequest(System.currentTimeMillis());
        client.getHabbo().loadRoomForUser(roomId, password);
    }
}
