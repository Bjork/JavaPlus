package me.bjork.javaplus.communication.packets.incoming.navigator;

import com.google.common.collect.Lists;
import me.bjork.javaplus.JavaPlusEnvironment;
import me.bjork.javaplus.communication.interfaces.ClientMessage;
import me.bjork.javaplus.communication.packets.MessageEvent;
import me.bjork.javaplus.communication.packets.outgoing.navigator.NavigatorSearchResultSetComposer;
import me.bjork.javaplus.habbohotel.gameclients.GameClient;
import me.bjork.javaplus.habbohotel.navigator.types.Category;

import java.util.List;

/**
 * Created on 22/05/2017.
 */
public class NavigatorSearchEvent implements MessageEvent {
    @Override
    public void parse(GameClient client, ClientMessage packet) {

        String category = packet.readString();
        String search = packet.readString();

        List<Category> categories = Lists.newArrayList();

        if (!search.isEmpty())
        {
            Category cat = JavaPlusEnvironment.getGame().getNavigatorManager().getCategory(0);
            categories.add(cat);
        }
        else
        {
            for(Category navigatorCategory : JavaPlusEnvironment.getGame().getNavigatorManager().getCategories().values()) {
                if(navigatorCategory.getCategory().equals(category)) {
                    if(navigatorCategory.isVisible())
                        categories.add(navigatorCategory);
                }
            }

            if(categories.size() == 0) {
                for(Category navigatorCategory : JavaPlusEnvironment.getGame().getNavigatorManager().getCategories().values()) {
                    if(navigatorCategory.getCategoryType().toString().toLowerCase().equals(category) && navigatorCategory.isVisible()) {
                        categories.add(navigatorCategory);
                    }
                }
            }

            if(categories.size() == 0) {
                for(Category navigatorCategory : JavaPlusEnvironment.getGame().getNavigatorManager().getCategories().values()) {
                    if(navigatorCategory.getCategoryId().equals(category) && navigatorCategory.isVisible()) {
                        categories.add(navigatorCategory);
                    }
                }
            }
        }

        client.send(new NavigatorSearchResultSetComposer(category, search, categories, client, 12));
    }
}
