package me.bjork.javaplus.communication.packets.incoming.navigator;

import me.bjork.javaplus.communication.interfaces.ClientMessage;
import me.bjork.javaplus.communication.packets.MessageEvent;
import me.bjork.javaplus.communication.packets.outgoing.navigator.NavigatorFlatCatsComposer;
import me.bjork.javaplus.habbohotel.gameclients.GameClient;

/**
 * Created on 22/05/2017.
 */
public class GetNavigatorFlatsEvent implements MessageEvent {
    @Override
    public void parse(GameClient client, ClientMessage packet) {

        client.send(new NavigatorFlatCatsComposer());

        /*ICollection<SearchResultList> Categories = PlusEnvironment.GetGame().GetNavigator().GetEventCategories();

        Session.SendPacket(new NavigatorFlatCatsComposer(Categories, Session.GetHabbo().Rank));*/
    }
}
