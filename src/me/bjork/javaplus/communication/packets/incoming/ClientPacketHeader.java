package me.bjork.javaplus.communication.packets.incoming;

/**
 * Created on 20/05/2017.
 */
public class ClientPacketHeader {
    // Handshake
    public static final short GetClientVersionMessageEvent = 4000;//4000
    public static final short InitCryptoMessageEvent = 3392;//316
    public static final short GenerateSecretKeyMessageEvent = 3622;//3847
    public static final short UniqueIDMessageEvent = 3521;//1471
    public static final short SSOTicketMessageEvent = 1989;//1778
    public static final short InfoRetrieveMessageEvent = 2629;//186
    public static final short PingMessageEvent = 509;//2584

    // Avatar
    public static final short GetWardrobeMessageEvent = 3901;//765
    public static final short SaveWardrobeOutfitMessageEvent = 1777;//55

    // Catalog
    public static final short GetCatalogIndexMessageEvent = 3226;//1294
    public static final short GetCatalogPageMessageEvent = 60;//39
    public static final short PurchaseFromCatalogMessageEvent = 3492;//2830
    public static final short PurchaseFromCatalogAsGiftMessageEvent = 1555;//21
    public static final short CheckPetNameMessageEvent = 3733;//159
    public static final short CheckGnomeNameMessageEvent = 1179;//2281
    public static final short GetCatalogModeMessageEvent = 951;//2267
    public static final short GetCatalogOfferMessageEvent = 362;//2180
    public static final short GetCatalogRoomPromotionMessageEvent = 2757;//538
    public static final short GetRecyclerRewardsMessageEvent = 1595;//3258
    public static final short PurchaseRoomPromotionMessageEvent = 1542;//3078
    public static final short PurchaseGroupMessageEvent = 2959;//2546
    public static final short GetClubGiftsMessageEvent = 3127;//3302

    // Marketplace
    public static final short GetOffersMessageEvent = 2817;//442
    public static final short GetOwnOffersMessageEvent = 904;//3829
    public static final short GetMarketplaceCanMakeOfferMessageEvent = 1552;//1647
    public static final short GetMarketplaceItemStatsMessageEvent = 1561;//1203
    public static final short MakeOfferMessageEvent = 2308;//255
    public static final short CancelOfferMessageEvent = 100;//1862
    public static final short RedeemOfferCreditsMessageEvent = 195;//1207
    public static final short BuyOfferMessageEvent = 2879;//3699

    // Game Center
    public static final short InitializeGameCenterMessageEvent = 1825;//751
    public static final short GetPlayableGamesMessageEvent = 1418;//482
    public static final short Game2GetWeeklyLeaderboardMessageEvent = 285;//2106
    public static final short GetGameListingMessageEvent = 705;//2993
    public static final short JoinPlayerQueueMessageEvent = 167;//951

    // Navigator
    public static final short ToggleStaffPickEvent = 2282;

    // Messenger
    public static final short GetBuddyRequestsMessageEvent = 1646;//2485

    // Quests
    public static final short GetQuestListMessageEvent = 2198;//2305
    public static final short StartQuestMessageEvent = 2457;//1282
    public static final short GetCurrentQuestMessageEvent = 651;//90
    public static final short CancelQuestMessageEvent = 104;//3879

    // Room Avatar
    public static final short ActionMessageEvent = 3268;//3639
    public static final short ApplySignMessageEvent = 3555;//2966
    public static final short DanceMessageEvent = 1225;//645
    public static final short SitMessageEvent = 3735;//1565
    public static final short ChangeMottoMessageEvent = 674;//3515
    public static final short LookToMessageEvent = 1142;//3744
    public static final short DropHandItemMessageEvent = 3296;//1751

    // Room Connection
    public static final short OpenFlatConnectionMessageEvent = 189;//407
    public static final short GoToFlatMessageEvent = 2947;//1601

    // Room Chat
    public static final short ChatMessageEvent = 744;//670
    public static final short ShoutMessageEvent = 697;//2101
    public static final short WhisperMessageEvent = 3003;//878

    // Room Polls
    public static final short PollStartMessageEvent = 3970;
    public static final short PollAnswerMessageEvent = 2445;
    public static final short PollRejectMessageEvent = 281;

    // Room Engine
    public static final short MoveObjectMessageEvent = 3583;//1781
    public static final short ApplyDecorationMessageEvent = 2729;//728
    public static final short GetFurnitureAliasesMessageEvent = 3116;//2125

    // Room Furniture

    // Room Settings
    public static final short GetRoomBannedUsersMessageEvent = 2009;//581
    public static final short DeleteRoomMessageEvent = 439;//722
    public static final short GetRoomFilterListMessageEvent = 179;//1348
    public static final short GetRoomRightsMessageEvent = 3937;//2734
    public static final short GetRoomSettingsMessageEvent = 581;//1014
    public static final short ModifyRoomFilterListMessageEvent = 87;//256
    public static final short SaveEnforcedCategorySettingsMessageEvent = 531;//3413
    public static final short SaveRoomSettingsMessageEvent = 3023;//2074
    public static final short ToggleMuteToolMessageEvent = 1301;//2462
    public static final short UnbanUserFromRoomMessageEvent = 2050;//3060

    // Room Action
    public static final short GiveRoomScoreMessageEvent = 3261;//336
    public static final short UnIgnoreUserMessageEvent = 981;//3023
    public static final short KickUserMessageEvent = 1336;//3929
    public static final short RemoveMyRightsMessageEvent = 111;//879
    public static final short GiveHandItemMessageEvent = 2523;//3315
    public static final short BanUserMessageEvent = 3009;//3940
    public static final short AssignRightsMessageEvent = 3843;//3574
    public static final short LetUserInMessageEvent = 1781;//2356

    // Users
    public static final short GetIgnoredUsersMessageEvent = 198;

    // Moderation
    public static final short OpenHelpToolMessageEvent = 1282;//1839
    public static final short CallForHelpPendingCallsDeletedMessageEvent = 3643;
    public static final short ModeratorActionMessageEvent = 760;//781
    public static final short ModerationMsgMessageEvent = 2348;//2375
    public static final short ModerationMuteMessageEvent = 2474;//1940
    public static final short ModerationTradeLockMessageEvent = 3955;//1160
    public static final short GetModeratorUserRoomVisitsMessageEvent = 3848;//730
    public static final short ModerationKickMessageEvent = 1011;//3589
    public static final short GetModeratorRoomInfoMessageEvent = 1997;//182
    public static final short GetModeratorUserInfoMessageEvent = 2677;//2984
    public static final short GetModeratorRoomChatlogMessageEvent = 3216;//2312
    public static final short ModerateRoomMessageEvent = 500;//3458
    public static final short GetModeratorUserChatlogMessageEvent = 63;//695
    public static final short GetModeratorTicketChatlogsMessageEvent = 1449;//3484
    public static final short ModerationCautionMessageEvent = 2223;//505
    public static final short ModerationBanMessageEvent = 2473;//2595
    public static final short SubmitNewTicketMessageEvent = 1046;//963
    public static final short CloseIssueDefaultActionEvent = 1921;

    // Help
    public static final short SendBullyReportMessageEvent = 3540;//2973
    public static final short OnBullyClickMessageEvent = 254;//1932
    public static final short GetSanctionStatusMessageEvent = 3209;//2883
    public static final short SubmitBullyReportMessageEvent = 3971;//1803

    // Inventory
    public static final short GetCreditsInfoMessageEvent = 1051;//3697
    public static final short GetAchievementsMessageEvent = 2249;//2931
    public static final short GetBadgesMessageEvent = 2954;//166
    public static final short RequestFurniInventoryMessageEvent = 2395;//352
    public static final short SetActivatedBadgesMessageEvent = 2355;//2752
    public static final short AvatarEffectActivatedMessageEvent = 2658;//129
    public static final short AvatarEffectSelectedMessageEvent = 1816;//628

    public static final short InitTradeMessageEvent = 3399;//3313
    public static final short TradingCancelConfirmMessageEvent = 3738;//2264
    public static final short TradingModifyMessageEvent = 644;//1153
    public static final short TradingOfferItemMessageEvent = 842;//114
    public static final short TradingCancelMessageEvent = 2934;//2967
    public static final short TradingConfirmMessageEvent = 1394;//2399
    public static final short TradingOfferItemsMessageEvent = 1607;//2996
    public static final short TradingRemoveItemMessageEvent = 3313;//1033
    public static final short TradingAcceptMessageEvent = 247;//3374

    // Register
    public static final short UpdateFigureDataMessageEvent = 498;//2560

    // Groups
    public static final short GetBadgeEditorPartsMessageEvent = 3706;//1670
    public static final short GetGroupCreationWindowMessageEvent = 365;//468
    public static final short GetGroupFurniSettingsMessageEvent = 1062;//41
    public static final short DeclineGroupMembershipMessageEvent = 1571;//403
    public static final short AcceptGroupMembershipMessageEvent = 2996;//2259
    public static final short JoinGroupMessageEvent = 748;//2615
    public static final short UpdateGroupColoursMessageEvent = 3469;//1443
    public static final short SetGroupFavouriteMessageEvent = 77;//2625
    public static final short GetGroupMembersMessageEvent = 3181;//205
    public static final short TakeAdminRightsMessageEvent = 1661;//2725
    public static final short UpdateGroupSettingsMessageEvent = 2435;//3180
    public static final short UpdateGroupBadgeMessageEvent = 1589;//2959
    public static final short UpdateGroupIdentityMessageEvent = 1375;//1062

    // Group Forums
    public static final short PostGroupContentMessageEvent = 1499;//477
    public static final short GetForumStatsMessageEvent = 1126;//872
    public static final short DeleteGroupMessageEvent = 114;//747
    public static final short DeleteGroupPostMessageEvent = 1991;//317
    public static final short GetThreadsListDataMessageEvent = 2568;//1606
    public static final short GetThreadDataMessageEvent = 2324;//1559
    public static final short GetForumUserProfileMessageEvent = 3515;//2639
    public static final short GetForumsListDataMessageEvent = 3802;//3912
    public static final short UpdateThreadMessageEvent = 2980;//1522
    public static final short UpdateForumSettingsMessageEvent = 3295;//931

    // Sound

    public static final short GoToHotelViewMessageEvent = 1429;//3576
    public static final short GetPromoArticlesMessageEvent = 2782;//3895
    public static final short ModifyWhoCanRideHorseMessageEvent = 3604;//1993
    public static final short RemoveBuddyMessageEvent = 1636;//698
    public static final short RefreshCampaignMessageEvent = 3960;//3544
    public static final short AcceptBuddyMessageEvent = 2067;//45
    public static final short YouTubeVideoInformationMessageEvent = 1295;//2395
    public static final short FollowFriendMessageEvent = 848;//2280
    public static final short SaveBotActionMessageEvent = 2921;//678
    public static final short GetSellablePetBreedsMessageEvent = 599;//2505
    public static final short ForceOpenCalendarBoxMessageEvent = 1275;//2879
    public static final short SetFriendBarStateMessageEvent = 3841;//716
    public static final short SetSoundSettingsMessageEvent = 608;//3820
    public static final short FriendListUpdateMessageEvent = 1166;//2664
    public static final short ConfirmLoveLockMessageEvent = 3873;//2082
    public static final short UseHabboWheelMessageEvent = 2148;//2651
    public static final short ToggleMoodlightMessageEvent = 14;//1826
    public static final short GetDailyQuestMessageEvent = 3441;//484
    public static final short SetMannequinNameMessageEvent = 3262;//2406
    public static final short UseOneWayGateMessageEvent = 1970;//2816
    public static final short EventTrackerMessageEvent = 143;//2386
    public static final short FloorPlanEditorRoomPropertiesMessageEvent = 2478;//24
    public static final short PickUpPetMessageEvent = 3975;//2342
    public static final short GetPetInventoryMessageEvent = 3646;//263
    public static final short InitializeFloorPlanSessionMessageEvent = 3069;//2623
    public static final short SetUserFocusPreferenceEvent = 799;//526
    public static final short RemoveRightsMessageEvent = 877;//40
    public static final short SaveWiredEffectConfigMessageEvent = 2234;//3431
    public static final short GetRoomEntryDataMessageEvent = 1747;//2768
    public static final short CanCreateRoomMessageEvent = 2411;//361
    public static final short SetTonerMessageEvent = 1389;//1061
    public static final short SaveWiredTriggerConfigMessageEvent = 3877;//1897
    public static final short PlaceBotMessageEvent = 3770;//2321
    public static final short GetRelationshipsMessageEvent = 3046;//866
    public static final short SetMessengerInviteStatusMessageEvent = 1663;//1379
    public static final short UseFurnitureMessageEvent = 3249;//3846
    public static final short GetUserFlatCatsMessageEvent = 493;//3672
    public static final short ReleaseTicketMessageEvent = 3931;//3800
    public static final short OpenPlayerProfileMessageEvent = 3053;//3591
    public static final short CreditFurniRedeemMessageEvent = 3945;//1676
    public static final short DisconnectionMessageEvent = 1474;//2391
    public static final short PickupObjectMessageEvent = 1766;//636
    public static final short FindRandomFriendingRoomMessageEvent = 2189;//1874
    public static final short UseSellableClothingMessageEvent = 2849;//818
    public static final short MoodlightUpdateMessageEvent = 2913;//856
    public static final short GetPetTrainingPanelMessageEvent = 3915;//2088
    public static final short GetSongInfoMessageEvent = 3916;//3418
    public static final short UseWallItemMessageEvent = 3674;//3396
    public static final short GetTalentTrackMessageEvent = 680;//1284
    public static final short GiveAdminRightsMessageEvent = 404;//465
    public static final short SaveWiredConditionConfigMessageEvent = 2370;//488
    public static final short RedeemVoucherMessageEvent = 1384;//489
    public static final short ThrowDiceMessageEvent = 3427;//1182
    public static final short CraftSecretMessageEvent = 3623;//1622
    public static final short SetRelationshipMessageEvent = 1514;//2112
    public static final short RequestBuddyMessageEvent = 1706;//3775
    public static final short MemoryPerformanceMessageEvent = 124;//731
    public static final short ToggleYouTubeVideoMessageEvent = 1956;//890
    public static final short SetMannequinFigureMessageEvent = 1909;//3936
    public static final short GetEventCategoriesMessageEvent = 597;//1086
    public static final short DeleteGroupThreadMessageEvent = 50;//3299
    public static final short MessengerInitMessageEvent = 2825;//2151
    public static final short CancelTypingMessageEvent = 1329;//1114
    public static final short GetMoodlightConfigMessageEvent = 2906;//3472
    public static final short GetGroupInfoMessageEvent = 681;//3211
    public static final short CreateFlatMessageEvent = 92;//3077
    public static final short LatencyTestMessageEvent = 878;//1789
    public static final short GetSelectedBadgesMessageEvent = 2735;//2226
    public static final short AddStickyNoteMessageEvent = 3891;//425
    public static final short ChangeNameMessageEvent = 2709;//1067
    public static final short RideHorseMessageEvent = 3387;//1440
    public static final short InitializeNewNavigatorMessageEvent = 3375;//882
    public static final short SetChatPreferenceMessageEvent = 1045;//2006
    public static final short UpdateStickyNoteMessageEvent = 3120;//342
    public static final short OpenGiftMessageEvent = 349;//1515
    public static final short GetRecipeConfigMessageEvent = 2428;//3654
    public static final short ScrGetUserInfoMessageEvent = 2749;//12
    public static final short RemoveGroupMemberMessageEvent = 1590;//649
    public static final short DiceOffMessageEvent = 1124;//191
    public static final short YouTubeGetNextVideo = 2618;//1843
    public static final short DeleteFavouriteRoomMessageEvent = 3223;//855
    public static final short RespectUserMessageEvent = 3812;//1955
    public static final short AddFavouriteRoomMessageEvent = 3251;//3092
    public static final short DeclineBuddyMessageEvent = 3484;//835
    public static final short StartTypingMessageEvent = 2826;//3362
    public static final short GetGroupFurniConfigMessageEvent = 3902;//3046
    public static final short SendRoomInviteMessageEvent = 1806;//2694
    public static final short RemoveAllRightsMessageEvent = 884;//1404
    public static final short GetYouTubeTelevisionMessageEvent = 1326;//3517
    public static final short FindNewFriendsMessageEvent = 3889;//1264
    public static final short GetPromotableRoomsMessageEvent = 2306;//276
    public static final short GetBotInventoryMessageEvent = 775;//363
    public static final short GetRentableSpaceMessageEvent = 2035;//793
    public static final short BuyRentableSpaceMessageEvent = 268;//
    public static final short CancelRentableSpaceMessageEvent = 1750;//
    public static final short OpenBotActionMessageEvent = 3236;//2544
    public static final short OpenCalendarBoxMessageEvent = 1229;//724
    public static final short CheckValidNameMessageEvent = 2507;//8
    public static final short PlaceObjectMessageEvent = 1809;//579
    public static final short RemoveGroupFavouriteMessageEvent = 226;//1412
    public static final short UpdateNavigatorSettingsMessageEvent = 1824;//2501
    public static final short NavigatorSearchMessageEvent = 618;//2722
    public static final short GetPetInformationMessageEvent = 2986;//2853
    public static final short GetGuestRoomMessageEvent = 2247;//1164
    public static final short GetMarketplaceConfigurationMessageEvent = 2811;//1604
    public static final short RemoveSaddleFromHorseMessageEvent = 844;//1892
    public static final short GetHabboClubWindowMessageEvent = 3530;//715
    public static final short DeleteStickyNoteMessageEvent = 3885;//2777
    public static final short MuteUserMessageEvent = 2101;//2997
    public static final short ApplyHorseEffectMessageEvent = 3364;//870
    public static final short HabboSearchMessageEvent = 1194;//3375
    public static final short PickTicketMessageEvent = 1807;//3973
    public static final short GetGiftWrappingConfigurationMessageEvent = 1570;//1928
    public static final short GetCraftingRecipesAvailableMessageEvent = 1869;//1653
    public static final short ManageGroupMessageEvent = 737;//2547
    public static final short PlacePetMessageEvent = 1495;//223
    public static final short EditRoomPromotionMessageEvent = 816;//3707
    public static final short SaveFloorPlanModelMessageEvent = 1936;//1287
    public static final short MoveWallItemMessageEvent = 1778;//609
    public static final short ClientVariablesMessageEvent = 1220;//1600
    public static final short PickUpBotMessageEvent = 3058;//644
    public static final short GetHabboGroupBadgesMessageEvent = 3925;//301
    public static final short GetUserTagsMessageEvent = 84;//1722
    public static final short MoveAvatarMessageEvent = 2121;//1737
    public static final short SaveBrandingItemMessageEvent = 2208;//3156
    public static final short RespectPetMessageEvent = 1967;//1618
    public static final short UpdateMagicTileMessageEvent = 2997;//1248
    public static final short GetStickyNoteMessageEvent = 2469;//2796
    public static final short IgnoreUserMessageEvent = 2374;//2394
    public static final short SendMsgMessageEvent = 2409;//1981
    public static final short CloseTicketMesageEvent = 1080;//50
    public static final short ReloadRecyclerMessageEvent = 2152;
    public static final short RecycleItemEvent = 88;
    public static final short SoundSettingsMessageEvent = 3203;
    public static final short RequestCameraConfigurationEvent = 1132;
}
