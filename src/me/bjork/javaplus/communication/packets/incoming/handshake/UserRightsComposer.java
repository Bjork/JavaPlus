package me.bjork.javaplus.communication.packets.incoming.handshake;

import me.bjork.javaplus.communication.packets.outgoing.ServerPacketHeader;
import me.bjork.javaplus.communication.packets.parsers.OutgoingMessageComposer;
import me.bjork.javaplus.habbohotel.gameclients.GameClient;

/**
 * Created on 30/05/2017.
 */
public class UserRightsComposer extends OutgoingMessageComposer {
    private GameClient client;

    public UserRightsComposer(GameClient client) {
        this.client = client;
    }

    @Override
    public void write() {
        response.init(ServerPacketHeader.UserRightsMessageComposer);
        response.writeInt(2); //TODO UserHC Subscription
        response.writeInt(client.getHabbo().getRank());
        response.writeInt(client.getHabbo().getPermissions().hasRight("mod_tool"));
    }
}
