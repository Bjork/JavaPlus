package me.bjork.javaplus.communication.packets.incoming.handshake;

import me.bjork.javaplus.communication.interfaces.ClientMessage;
import me.bjork.javaplus.communication.packets.MessageEvent;
import me.bjork.javaplus.communication.packets.outgoing.handshake.UserObjectComposer;
import me.bjork.javaplus.communication.packets.outgoing.handshake.UserPerksComposer;
import me.bjork.javaplus.habbohotel.gameclients.GameClient;

/**
 * Created on 22/05/2017.
 */
public class InfoRetrieveEvent implements MessageEvent {
    @Override
    public void parse(GameClient client, ClientMessage packet) {
        client.send(new UserObjectComposer(client.getHabbo()));
        client.send(new UserPerksComposer());
    }
}
