package me.bjork.javaplus.communication.packets.incoming.handshake;

import me.bjork.javaplus.communication.interfaces.ClientMessage;
import me.bjork.javaplus.communication.packets.MessageEvent;
import me.bjork.javaplus.communication.packets.outgoing.handshake.SecretKeyComposer;
import me.bjork.javaplus.habbohotel.gameclients.GameClient;

/**
 * Created on 20/05/2017.
 */
public class GenerateSecretKeyEvent implements MessageEvent {
    @Override
    public void parse(GameClient client, ClientMessage packet) {
        String CipherPublickey = packet.readString();

        client.send(new SecretKeyComposer(CipherPublickey));
    }
}
