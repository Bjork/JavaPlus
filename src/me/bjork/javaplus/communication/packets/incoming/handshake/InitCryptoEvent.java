package me.bjork.javaplus.communication.packets.incoming.handshake;

import me.bjork.javaplus.communication.interfaces.ClientMessage;
import me.bjork.javaplus.communication.packets.MessageEvent;
import me.bjork.javaplus.communication.packets.outgoing.handshake.InitCryptoMessageComposer;
import me.bjork.javaplus.habbohotel.gameclients.GameClient;

/**
 * Created on 20/05/2017.
 */
public class InitCryptoEvent implements MessageEvent {
    @Override
    public void parse(GameClient client, ClientMessage reader) {
        client.send(new InitCryptoMessageComposer());
    }
}
