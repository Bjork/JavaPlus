package me.bjork.javaplus.communication.packets.incoming.handshake;

import com.mysql.cj.core.util.StringUtils;
import me.bjork.javaplus.communication.interfaces.ClientMessage;
import me.bjork.javaplus.communication.packets.MessageEvent;
import me.bjork.javaplus.habbohotel.gameclients.GameClient;

/**
 * Created on 21/05/2017.
 */
public class SSOTicketEvent implements MessageEvent {
    @Override
    public void parse(GameClient client, ClientMessage packet) {

        if (client == null)
            return;

        String ssoTicket = packet.readString();

        if (StringUtils.isNullOrEmpty(ssoTicket) || ssoTicket.length() < 15)
            client.getNetwork().close();

        if (!client.tryAuthenticate(ssoTicket))
            client.getNetwork().close();
    }
}
