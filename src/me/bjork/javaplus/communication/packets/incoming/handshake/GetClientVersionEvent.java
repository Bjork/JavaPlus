package me.bjork.javaplus.communication.packets.incoming.handshake;

import me.bjork.javaplus.JavaPlusEnvironment;
import me.bjork.javaplus.communication.interfaces.ClientMessage;
import me.bjork.javaplus.communication.packets.MessageEvent;
import me.bjork.javaplus.habbohotel.gameclients.GameClient;

/**
 * Created on 20/05/2017.
 */
public class GetClientVersionEvent implements MessageEvent {
    @Override
    public void parse(GameClient player, ClientMessage packet) {

        String build = packet.readString();

        if (!build.equals(JavaPlusEnvironment.SWFrevision)) {
            player.getNetwork().close();
        }
    }
}
