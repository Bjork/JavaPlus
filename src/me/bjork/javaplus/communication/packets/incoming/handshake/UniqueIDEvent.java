package me.bjork.javaplus.communication.packets.incoming.handshake;

import me.bjork.javaplus.communication.interfaces.ClientMessage;
import me.bjork.javaplus.communication.packets.MessageEvent;
import me.bjork.javaplus.communication.packets.outgoing.handshake.SetUniqueIdComposer;
import me.bjork.javaplus.habbohotel.gameclients.GameClient;

/**
 * Created on 21/05/2017.
 */
public class UniqueIDEvent implements MessageEvent {
    @Override
    public void parse(GameClient client, ClientMessage packet) {
        String useless = packet.readString();
        String machineId = packet.readString();

        client.setMachineId(machineId);
        client.send(new SetUniqueIdComposer(client));
    }
}
