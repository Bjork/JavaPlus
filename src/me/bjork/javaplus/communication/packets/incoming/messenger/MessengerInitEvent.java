package me.bjork.javaplus.communication.packets.incoming.messenger;

import me.bjork.javaplus.communication.interfaces.ClientMessage;
import me.bjork.javaplus.communication.packets.MessageEvent;
import me.bjork.javaplus.communication.packets.outgoing.messenger.BuddyListComposer;
import me.bjork.javaplus.communication.packets.outgoing.messenger.MessengerInitComposer;
import me.bjork.javaplus.habbohotel.gameclients.GameClient;

/**
 * Created on 22/05/2017.
 */
public class MessengerInitEvent implements MessageEvent {
    @Override
    public void parse(GameClient client, ClientMessage packet) {

        if (client == null || client.getHabbo() == null /*|| client.getHabbo().GetMessenger() == null*/)
            return;

        client.send(new MessengerInitComposer());
        client.send(new BuddyListComposer());
    }
}
