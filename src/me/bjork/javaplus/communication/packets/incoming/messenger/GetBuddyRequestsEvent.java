package me.bjork.javaplus.communication.packets.incoming.messenger;

import me.bjork.javaplus.communication.interfaces.ClientMessage;
import me.bjork.javaplus.communication.packets.MessageEvent;
import me.bjork.javaplus.communication.packets.outgoing.messenger.BuddyRequestsComposer;
import me.bjork.javaplus.habbohotel.gameclients.GameClient;

/**
 * Created on 22/05/2017.
 */
public class GetBuddyRequestsEvent implements MessageEvent {
    @Override
    public void parse(GameClient client, ClientMessage packet) {
        client.send(new BuddyRequestsComposer());

        /*
         ICollection<MessengerRequest> Requests = Session.GetHabbo().GetMessenger().GetRequests().ToList();

            Session.SendPacket(new BuddyRequestsComposer(Requests));
         */
    }
}
