package me.bjork.javaplus.communication.packets.incoming.rooms.chat;

import com.google.common.primitives.Ints;
import me.bjork.javaplus.JavaPlusEnvironment;
import me.bjork.javaplus.communication.interfaces.ClientMessage;
import me.bjork.javaplus.communication.packets.MessageEvent;
import me.bjork.javaplus.communication.packets.outgoing.moderation.MutedMessageComposer;
import me.bjork.javaplus.communication.packets.outgoing.rooms.chat.ChatComposer;
import me.bjork.javaplus.habbohotel.gameclients.GameClient;
import me.bjork.javaplus.habbohotel.rooms.Room;
import me.bjork.javaplus.habbohotel.rooms.RoomUser;
import me.bjork.javaplus.habbohotel.rooms.filter.FilterResult;

/**
 * Created on 29/05/2017.
 */
public class ChatEvent implements MessageEvent {
    @Override
    public void parse(GameClient client, ClientMessage packet) {
        if (client == null || client.getHabbo() == null || !client.getHabbo().inRoom())
            return;

        Room room = client.getHabbo().currentRoom();

        if (room == null)
            return;

        RoomUser roomUser = client.getHabbo().getRoomUser();

        if (roomUser == null)
            return;

        String message = packet.readString();
        int colour = packet.readInt();

        if (message.length() > 100)
            message = message.substring(0, 100);

        if (message.startsWith(":") && JavaPlusEnvironment.getGame().getChatManager().getCommands().handle(client, message))
            return;

        if (!roomUser.isVisible() && !client.getHabbo().isInvisible())
            return;

        final double timeMutedExpire = client.getHabbo().getTimeMuted() - JavaPlusEnvironment.getUnixTimestamp();

        if (client.getHabbo().getTimeMuted() != 0) {
            if (client.getHabbo().getTimeMuted() > JavaPlusEnvironment.getUnixTimestamp()) {
                client.send(new MutedMessageComposer(timeMutedExpire));
                return;
            }
        }

        if (client.getHabbo().getRoomUser().isRoomMuted() && !client.getHabbo().getPermissions().hasRight("room_ignore_mute") && room.getOwnerId() != client.getHabbo().getId()) {
            return;
        }

        if ((room.getRights().hasMute(client.getHabbo().getId()) || JavaPlusEnvironment.getGame().getMuteManager().isMuted(client.getHabbo().getId())) && !client.getHabbo().getPermissions().hasRight("room_ignore_mute")) {
            client.send(new MutedMessageComposer(room.getRights().getMuteTime(client.getHabbo().getId())));
            return;
        }

        if (!isValidColour(colour, client))
            colour = 0;

        //TODO Text Color

        String safeMessage = filterMessage(message);

        if (safeMessage == null)
            return;

        if (!client.getHabbo().getPermissions().hasRight("word_filter_override")) {
            FilterResult filterResult = JavaPlusEnvironment.getGame().getChatManager().getFilter().filter(safeMessage);

            if (filterResult.isBlocked()) {
                //client.send(new AdvancedAlertMessageComposer(LanguageManager.TryGetValue("game.message.blocked").replace("%s", filterResult.getMessage()))); //TODO
                return;
            } else if (filterResult.wasModified()) {
                safeMessage = filterResult.getMessage();
            }
        }

        if (roomUser.onChat(safeMessage)) {

            //TODO Chatlogs ?

            /*if (client.getPlayer().getEntity().getPrivateChatItemId() != 0) { //TODO Chat Tente
                // broadcast message only to players in the tent.
                RoomItemFloor floorItem = client.getPlayer().getEntity().getRoom().getItems().getFloorItem(client.getPlayer().getEntity().getPrivateChatItemId());

                if (floorItem != null) {
                    ((PrivateChatFloorItem) floorItem).broadcastMessage(new TalkMessageComposer(client.getPlayer().getEntity().getId(), filteredMessage, RoomManager.getInstance().getEmotions().getEmotion(filteredMessage), colour));
                }
            } else {*/
            //room.getRoomUserManager().broadcastChatMessage(new ChatComposer(roomUser.getVirtualId(), safeMessage, JavaPlusEnvironment.getGame().getRoomManager().getEmotions().getEmotion(safeMessage), colour), roomUser);
            /*}*/

            room.getRoomUserManager().broadcastChatMessage(new ChatComposer(roomUser.getVirtualId(), safeMessage, JavaPlusEnvironment.getGame().getChatManager().getEmotions().getEmotion(safeMessage), colour), roomUser);
        }
    }

    //TODO Utiliser room_chat_styles
    private static int[] allowedColours = new int[]{
            0, 3, 4, 5, 6, 7, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 29, 37
    };

    public static boolean isValidColour(int colour, GameClient client) {
        if (!Ints.contains(allowedColours, colour))
            return false;

        if ((colour == 23 || colour == 37) && !client.getHabbo().getPermissions().hasRight("mod_tool"))
            return false;

        return true;
    }

    public static String filterMessage(String message) {
        return message.replace((char) 13 + "", "");
    }
}
