package me.bjork.javaplus.communication.packets.incoming.rooms.chat;

import me.bjork.javaplus.JavaPlusEnvironment;
import me.bjork.javaplus.communication.interfaces.ClientMessage;
import me.bjork.javaplus.communication.packets.MessageEvent;
import me.bjork.javaplus.communication.packets.outgoing.moderation.MutedMessageComposer;
import me.bjork.javaplus.communication.packets.outgoing.rooms.chat.WhisperMessageComposer;
import me.bjork.javaplus.core.settings.SettingsManager;
import me.bjork.javaplus.habbohotel.gameclients.GameClient;
import me.bjork.javaplus.habbohotel.rooms.Room;
import me.bjork.javaplus.habbohotel.rooms.RoomUser;
import me.bjork.javaplus.habbohotel.rooms.filter.FilterResult;
import me.bjork.javaplus.habbohotel.rooms.types.misc.ChatEmotion;

/**
 * Created on 31/05/2017.
 */
public class WhisperEvent implements MessageEvent {
    @Override
    public void parse(GameClient client, ClientMessage packet) {
        if (client == null || client.getHabbo() == null || !client.getHabbo().inRoom())
            return;

        Room room = client.getHabbo().currentRoom();

        if (room == null)
            return;

        RoomUser roomUser = client.getHabbo().getRoomUser();

        if (roomUser == null)
            return;

        if (!roomUser.isVisible() && !client.getHabbo().isInvisible())
            return;

        String text = packet.readString();
        String user = text.split(" ")[0];
        String message = text.substring(user.length() + 1);
        int colour = packet.readInt();

        if (message.length() > 100)
            message = message.substring(0, 100);

        RoomUser userTo = room.getRoomUserManager().getRoomUserByUsername(user);

        if (userTo == null || userTo.isBot() || userTo.isPet() || user.equals(client.getHabbo().getUsername()))
            return;

        final double timeMutedExpire = client.getHabbo().getTimeMuted() - JavaPlusEnvironment.getUnixTimestamp();

        if (client.getHabbo().getTimeMuted() != 0) {
            if (client.getHabbo().getTimeMuted() > JavaPlusEnvironment.getUnixTimestamp()) {
                client.send(new MutedMessageComposer(timeMutedExpire));
                return;
            }
        }

        if (client.getHabbo().getRoomUser().isRoomMuted() && !client.getHabbo().getPermissions().hasRight("room_ignore_mute") && room.getOwnerId() != client.getHabbo().getId()) {
            return;
        }

        if ((room.getRights().hasMute(client.getHabbo().getId()) || JavaPlusEnvironment.getGame().getMuteManager().isMuted(client.getHabbo().getId())) && !client.getHabbo().getPermissions().hasRight("room_ignore_mute")) {
            client.send(new MutedMessageComposer(room.getRights().getMuteTime(client.getHabbo().getId())));
            return;
        }

        if (!userTo.getHabbo().getReceiveWhispers() && !client.getHabbo().getPermissions().hasRight("room_whisper_override")) {
            client.sendWhisper("Oops, this user has their whispers disabled!");
            return;
        }

        if (!ChatEvent.isValidColour(colour, client))
            colour = 0;

        //TODO Text Color

        String safeMessage = ChatEvent.filterMessage(message);

        if (safeMessage == null)
            return;

        if (!client.getHabbo().getPermissions().hasRight("word_filter_override")) {
            FilterResult filterResult = JavaPlusEnvironment.getGame().getChatManager().getFilter().filter(safeMessage);

            if (filterResult.isBlocked()) {
                //client.send(new AdvancedAlertMessageComposer(LanguageManager.TryGetValue("game.message.blocked").replace("%s", filterResult.getMessage()))); //TODO
                return;
            } else if (filterResult.wasModified()) {
                safeMessage = filterResult.getMessage();
            }
        }

        if (roomUser.onChat(safeMessage)) {
            //TODO Chatlogs ?

            if (!userTo.getHabbo().ignores(client.getHabbo().getId())) {
                userTo.getClient().send(new WhisperMessageComposer(roomUser.getVirtualId(), safeMessage, ChatEmotion.NONE, colour));
            }

            for (RoomUser entity : room.getRoomUserManager().getRoomUsersByRank(Integer.valueOf(SettingsManager.tryGetValue("minrank.see_whispers")))) {
                if (entity.getHabbo().getId() != client.getHabbo().getId() && !user.equals(entity.getHabbo().getUsername())) {
                    entity.getClient().send(new WhisperMessageComposer(roomUser.getVirtualId(), "<Whisper to " + user + ">: " + safeMessage));
                }

            }
        }
        client.send(new WhisperMessageComposer(roomUser.getVirtualId(), safeMessage, ChatEmotion.NONE, colour));
    }
}
