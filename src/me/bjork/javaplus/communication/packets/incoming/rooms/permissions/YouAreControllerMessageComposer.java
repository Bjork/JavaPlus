package me.bjork.javaplus.communication.packets.incoming.rooms.permissions;

import me.bjork.javaplus.communication.packets.outgoing.ServerPacketHeader;
import me.bjork.javaplus.communication.packets.parsers.OutgoingMessageComposer;

public class YouAreControllerMessageComposer extends OutgoingMessageComposer {
	private int accessLevel;
	public YouAreControllerMessageComposer(int accessLevel) {
		this.accessLevel = accessLevel;
	}

	@Override
	public void write() {
		response.init(ServerPacketHeader.YouAreControllerMessageComposer);
		response.writeInt(accessLevel);
	}

}
