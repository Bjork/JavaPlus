package me.bjork.javaplus.communication.packets.incoming.rooms.avatar;

import me.bjork.javaplus.communication.interfaces.ClientMessage;
import me.bjork.javaplus.communication.packets.MessageEvent;
import me.bjork.javaplus.communication.packets.outgoing.rooms.avatar.ActionComposer;
import me.bjork.javaplus.habbohotel.gameclients.GameClient;
import me.bjork.javaplus.habbohotel.rooms.Room;
import me.bjork.javaplus.habbohotel.rooms.RoomUser;

/**
 * Created on 01/06/2017.
 */
public class ActionEvent implements MessageEvent {
    @Override
    public void parse(GameClient client, ClientMessage packet) {
        if (client == null || client.getHabbo() == null || !client.getHabbo().inRoom())
            return;

        Room room = client.getHabbo().currentRoom();

        if (room == null)
            return;

        RoomUser roomUser = client.getHabbo().getRoomUser();

        if (roomUser == null)
            return;

        int actionId = packet.readInt();

        if (actionId == 5)
            roomUser.setIdle();
        else
            roomUser.unIdle();

        //if (actionId == 1)
            //TODO Quest Waving

        room.send(new ActionComposer(roomUser.getVirtualId(), actionId));
    }
}
