package me.bjork.javaplus.communication.packets.incoming.rooms.avatar;

import me.bjork.javaplus.communication.interfaces.ClientMessage;
import me.bjork.javaplus.communication.packets.MessageEvent;
import me.bjork.javaplus.habbohotel.gameclients.GameClient;
import me.bjork.javaplus.habbohotel.rooms.RoomUser;

/**
 * Created on 01/06/2017.
 */
public class SitEvent implements MessageEvent {
    @Override
    public void parse(GameClient client, ClientMessage packet) {
        if (client == null || client.getHabbo() == null || !client.getHabbo().inRoom())
            return;

        RoomUser roomUser = client.getHabbo().getRoomUser();

        if (roomUser == null)
            return;

        if (roomUser.isWalking())
            roomUser.stopWalking();

        roomUser.unIdle();
        roomUser.getRoom().sitUser(client);
    }
}
