package me.bjork.javaplus.communication.packets.incoming.rooms.avatar;

import me.bjork.javaplus.communication.interfaces.ClientMessage;
import me.bjork.javaplus.communication.packets.MessageEvent;
import me.bjork.javaplus.communication.packets.outgoing.rooms.avatar.DanceComposer;
import me.bjork.javaplus.habbohotel.gameclients.GameClient;
import me.bjork.javaplus.habbohotel.rooms.Room;
import me.bjork.javaplus.habbohotel.rooms.RoomUser;

/**
 * Created on 01/06/2017.
 */
public class DanceEvent implements MessageEvent {
    @Override
    public void parse(GameClient client, ClientMessage packet) {
        if (client == null || client.getHabbo() == null || !client.getHabbo().inRoom())
            return;

        Room room = client.getHabbo().currentRoom();
        if (room == null)
            return;

        RoomUser roomUser = client.getHabbo().getRoomUser();
        if (roomUser == null)
            return;

        roomUser.unIdle();

        int danceId = packet.readInt();

        if (roomUser.getDanceId() == danceId)
            return;

        if (danceId < 0 && danceId > 4)
            danceId = 0;

        if (danceId > 0 && roomUser.getHandItem() > 0)
            roomUser.carryItem(0);

        //TODO Effect checking ?

        roomUser.setDanceId(danceId);

        room.send(new DanceComposer(roomUser.getVirtualId(), danceId));
        //TODO dance & massdance questing
    }
}
