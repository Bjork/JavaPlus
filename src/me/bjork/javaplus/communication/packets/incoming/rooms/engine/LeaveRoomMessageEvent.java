package me.bjork.javaplus.communication.packets.incoming.rooms.engine;

import me.bjork.javaplus.communication.interfaces.ClientMessage;
import me.bjork.javaplus.communication.packets.MessageEvent;
import me.bjork.javaplus.communication.packets.outgoing.rooms.session.HotelViewMessageComposer;
import me.bjork.javaplus.habbohotel.gameclients.GameClient;

/**
 * Created on 25/05/2017.
 */
public class LeaveRoomMessageEvent implements MessageEvent {
    @Override
    public void parse(GameClient client, ClientMessage packet) {
        if (client == null)
            return;

        if (client.getHabbo() == null || client.getHabbo().getRoomUser() == null || client.getHabbo().getRoomUser().getRoom() == null) {
            client.send(new HotelViewMessageComposer());
            if (client.getHabbo() != null){
                client.getHabbo().getRoomUser().setDoorbellAnswered(false);
                client.getHabbo().setRequestedRoomId(0);
                client.getHabbo().setCurrentRoomId(0);
            }
            return;
        }
        client.getHabbo().getRoomUser().removeUserFromRoom(false, true, false);
    }
}
