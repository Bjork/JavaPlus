package me.bjork.javaplus.communication.packets.incoming.rooms.engine;

import me.bjork.javaplus.communication.interfaces.ClientMessage;
import me.bjork.javaplus.communication.packets.MessageEvent;
import me.bjork.javaplus.communication.packets.incoming.rooms.permissions.YouAreControllerMessageComposer;
import me.bjork.javaplus.communication.packets.outgoing.rooms.avatar.ApplyEffectComposer;
import me.bjork.javaplus.communication.packets.outgoing.rooms.avatar.CarryObjectComposer;
import me.bjork.javaplus.communication.packets.outgoing.rooms.avatar.DanceComposer;
import me.bjork.javaplus.communication.packets.outgoing.rooms.avatar.SleepComposer;
import me.bjork.javaplus.communication.packets.outgoing.rooms.chat.FloodFilterMessageComposer;
import me.bjork.javaplus.communication.packets.outgoing.rooms.engine.*;
import me.bjork.javaplus.communication.packets.outgoing.rooms.permissions.YouAreOwnerMessageComposer;
import me.bjork.javaplus.core.utils.Util;
import me.bjork.javaplus.habbohotel.gameclients.GameClient;
import me.bjork.javaplus.habbohotel.rooms.Room;
import me.bjork.javaplus.habbohotel.rooms.RoomUser;
import me.bjork.javaplus.habbohotel.rooms.objects.users.RoomUserStatus;

/**
 * Created on 24/05/2017.
 */
public class GetRoomEntryDataEvent implements MessageEvent {
    @Override
    public void parse(GameClient client, ClientMessage packet) {

        if (client == null || client.getHabbo() == null)
            return;
        
        Room room = client.getHabbo().currentRoom();

        if (room == null)
            return;

        client.send(new HeightMapComposer(room));
        client.send(new FloorHeightMapComposer(room));        
        client.send(new RoomEntryInfoComposer(room.getId(), room.getRights().hasRights(client)));
        client.send(new RoomVisualizationSettingsComposer(room.getWallThickness(), room.getFloorThickness(), Util.enumToBool(String.valueOf(room.getHidewall()))));

        client.getHabbo().getRoomUser().setId(client.getHabbo().getId());
        client.getHabbo().getRoomUser().setRoom(room);
        client.getHabbo().getRoomUser().setVirtualId(room.getRoomUserManager().getFreeVirtualId());
        client.getHabbo().getRoomUser().getStatuses().clear();

        RoomUser user = client.getHabbo().getRoomUser();

        if (!room.getProcess().isActive()) {
            room.getProcess().start();
        }

        if (user == null || user.getClient() == null)
        	return;

        int accessLevel = 0;        
        if (room.getOwnerId() == client.getHabbo().getId() || client.getHabbo().getPermissions().hasRight("room_any_owner") ) {
            user.addStatus(RoomUserStatus.CONTROLLER, "useradmin");
            client.send(new YouAreOwnerMessageComposer());
            accessLevel = 4;
        } else if (room.getRights().hasRights(client)) {
        	user.addStatus(RoomUserStatus.CONTROLLER, "1");
            accessLevel = 1;
        }
        client.send(new YouAreControllerMessageComposer(accessLevel));
        
        if (!room.getPetMorphsAllowed() && client.getHabbo().getPetId() != 0)
        	client.getHabbo().setPetId(0);

        if (!client.getHabbo().getIsTeleporting() && !client.getHabbo().getIsHopping())
        {
            user.setLocation(room.getModel().getTile((short)room.getModel().getDoorX(), (short)room.getModel().getDoorY()));
            user.setZ(room.getModel().getDoorZ());
            user.setHeadRotation(room.getModel().getDoorDirection());
            user.setBodyRotation(room.getModel().getDoorDirection());
        }
        else if (!user.isBot() && (client.getHabbo().getIsTeleporting() || client.getHabbo().getIsHopping()))
    	{
    		//TODO faire le teleport quand item implémenté
    	}

    	room.send(new UserDisplayMessageComposer(user));
        room.send(new UserStatusMessageComposer(user));

        if (!room.getRoomUserManager().getRoomUserList().contains(user)) {
            room.getRoomUserManager().addRoomUser(user);
            room.setUsersNow(room.getRoomUserManager().getRoomUserList().size());
        }

        client.send(new UserDisplayMessageComposer(room.getRoomUserManager().getRoomUserList()));
        client.send(new UserStatusMessageComposer(room.getRoomUserManager().getRoomUserList()));

        for (RoomUser roomUser : room.getRoomUserManager().getRoomUsers()) {
            if (!roomUser.isBot() && !roomUser.isPet() && roomUser.getDanceId() > 0) {
                client.send(new DanceComposer(roomUser.getVirtualId(), roomUser.getDanceId()));
            }
            /*else if(roomUser.isBot() && roomUser.botData.getDanceId > 0) {
                client.send(new DanceMessageComposer(roomUser.getVirtualId(), roomUser.botData.getDanceId()));
            }*/

            if (roomUser.getHandItem() > 0) {
                client.send(new CarryObjectComposer(roomUser.getVirtualId(), roomUser.getHandItem()));
            }

            if (roomUser.isIdle())
                client.send(new SleepComposer(roomUser.getVirtualId(), true));

            if (roomUser.getCurrentEffect() != null) {
                client.send(new ApplyEffectComposer(roomUser.getVirtualId(), roomUser.getCurrentEffect().getEffectId()));
            }
        }
        
        if (client.getHabbo().getRoomFloodTime() >= 1) {
            client.send(new FloodFilterMessageComposer((int)client.getHabbo().getRoomFloodTime()));
        }
    }
}
