package me.bjork.javaplus.communication.packets.incoming.rooms.avatar;

import me.bjork.javaplus.communication.interfaces.ClientMessage;
import me.bjork.javaplus.communication.packets.MessageEvent;
import me.bjork.javaplus.communication.packets.outgoing.rooms.avatar.DanceComposer;
import me.bjork.javaplus.habbohotel.gameclients.GameClient;
import me.bjork.javaplus.habbohotel.rooms.Room;
import me.bjork.javaplus.habbohotel.rooms.RoomUser;

/**
 * Created on 01/06/2017.
 */
public class GiveHandItemEvent implements MessageEvent {
    @Override
    public void parse(GameClient client, ClientMessage packet) {
        if (client == null || client.getHabbo() == null || !client.getHabbo().inRoom())
            return;

        Room room = client.getHabbo().currentRoom();
        if (room == null)
            return;

        RoomUser roomUser = client.getHabbo().getRoomUser();
        if (roomUser == null)
            return;

        int targetUser = packet.readInt();

        RoomUser targetRoomUser = room.getRoomUserManager().getRoomUserByHabbo(targetUser);
        if (targetRoomUser == null)
            return;

        int itemId = roomUser.getHandItem();

        if (itemId > 0)
        {
            if (targetRoomUser.getDanceId() > 0)
                room.send(new DanceComposer(targetRoomUser.getVirtualId(), 0));

            targetRoomUser.carryItem(itemId);
            roomUser.carryItem(0);
        }
    }
}
