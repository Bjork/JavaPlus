package me.bjork.javaplus.communication.packets.incoming.rooms.chat;

import me.bjork.javaplus.communication.interfaces.ClientMessage;
import me.bjork.javaplus.communication.packets.MessageEvent;
import me.bjork.javaplus.communication.packets.outgoing.rooms.chat.UserTypingComposer;
import me.bjork.javaplus.habbohotel.gameclients.GameClient;
import me.bjork.javaplus.habbohotel.rooms.Room;
import me.bjork.javaplus.habbohotel.rooms.RoomUser;

/**
 * Created on 29/05/2017.
 */
public class CancelTypingEvent implements MessageEvent {
    @Override
    public void parse(GameClient client, ClientMessage packet) {

        if (client == null || client.getHabbo() == null || !client.getHabbo().inRoom())
            return;

        Room room = client.getHabbo().currentRoom();

        if (room == null)
            return;

        RoomUser roomUser = client.getHabbo().getRoomUser();

        if (roomUser == null)
            return;

        room.send(new UserTypingComposer(roomUser.getVirtualId(), false));
    }
}
