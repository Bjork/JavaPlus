package me.bjork.javaplus.communication.packets.incoming.rooms.avatar;

import me.bjork.javaplus.communication.interfaces.ClientMessage;
import me.bjork.javaplus.communication.packets.MessageEvent;
import me.bjork.javaplus.habbohotel.gameclients.GameClient;
import me.bjork.javaplus.habbohotel.rooms.Room;
import me.bjork.javaplus.habbohotel.rooms.RoomUser;
import me.bjork.javaplus.habbohotel.rooms.models.types.mapping.Rotation;
import me.bjork.javaplus.habbohotel.rooms.objects.users.RoomUserStatus;

/**
 * Created on 30/05/2017.
 */
public class LookToEvent implements MessageEvent {
    @Override
    public void parse(GameClient client, ClientMessage packet) {

        if (client == null || client.getHabbo() == null)
            return;

        Room room = client.getHabbo().currentRoom();

        if (room == null)
            return;

        RoomUser roomUser = client.getHabbo().getRoomUser();

        if (roomUser == null)
            return;

        int x = packet.readInt();
        int y = packet.readInt();

        if (x == roomUser.getX() && y == roomUser.getY() || roomUser.isWalking() || roomUser.hasStatus(RoomUserStatus.MOVE) ||
                roomUser.hasStatus(RoomUserStatus.LAY) || roomUser.getMountedEntity() != null)
            return;

        int rotation = Rotation.calculate(roomUser.getX(), roomUser.getY(), x, y);

        roomUser.setBodyRotation(rotation);
        roomUser.setHeadRotation(rotation);
        roomUser.markNeedsUpdate();

        roomUser.unIdle();

        if (roomUser.getMountedEntity() != null) {
            RoomUser horse = room.getRoomUserManager().getRoomUserByVirtualId(roomUser.getHorseId());
            if (horse != null) {
                horse.setBodyRotation(rotation);
                horse.setHeadRotation(rotation);
                roomUser.markNeedsUpdate();
            }
        }
    }
}
