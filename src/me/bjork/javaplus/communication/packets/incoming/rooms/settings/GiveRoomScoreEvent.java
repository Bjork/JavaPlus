package me.bjork.javaplus.communication.packets.incoming.rooms.settings;

import me.bjork.javaplus.communication.interfaces.ClientMessage;
import me.bjork.javaplus.communication.packets.MessageEvent;
import me.bjork.javaplus.database.queries.rooms.RoomDao;
import me.bjork.javaplus.habbohotel.gameclients.GameClient;
import me.bjork.javaplus.habbohotel.rooms.Room;

/**
 * Created on 03/06/2017.
 */
public class GiveRoomScoreEvent implements MessageEvent {
    @Override
    public void parse(GameClient client, ClientMessage packet) {
        if (client == null || client.getHabbo() == null || !client.getHabbo().inRoom())
            return;

        Room room = client.getHabbo().currentRoom();

        if (room == null)
            return;

        if (!client.getHabbo().canRateRoom() && room.getOwnerId() == client.getHabbo().getId())
            return;

        client.getHabbo().getRatedRooms().add(client.getHabbo().getId());

        int direction = packet.readInt();
        int score = room.getScore();

        if (direction == 1)
            score++;
        else
            score--;

        room.setScore(score);
        room.getRoomUserManager().refreshScore();
        RoomDao.saveScore(score, room.getId());
    }
}
