package me.bjork.javaplus.communication.packets.incoming.rooms.engine;

import me.bjork.javaplus.communication.interfaces.ClientMessage;
import me.bjork.javaplus.communication.packets.MessageEvent;
import me.bjork.javaplus.habbohotel.gameclients.GameClient;
import me.bjork.javaplus.habbohotel.rooms.Room;
import me.bjork.javaplus.habbohotel.rooms.RoomUser;

/**
 * Created on 27/05/2017.
 */
public class MoveAvatarEvent implements MessageEvent {
    @Override
    public void parse(GameClient client, ClientMessage packet) {

        int goalX = packet.readInt();
        int goalY = packet.readInt();

        Room room = client.getHabbo().currentRoom();

        if (room == null)
            return;

        RoomUser roomUser = room.getRoomUserManager().getRoomUserByHabbo(client.getHabbo().getId());

        if (roomUser == null || !roomUser.canWalk())
            return;

        roomUser.walkTo(goalX, goalY);
    }
}
