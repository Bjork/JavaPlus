package me.bjork.javaplus.communication.packets.incoming.rooms.engine;

import me.bjork.javaplus.communication.interfaces.ClientMessage;
import me.bjork.javaplus.communication.packets.MessageEvent;
import me.bjork.javaplus.communication.packets.outgoing.rooms.engine.FurnitureAliasesComposer;
import me.bjork.javaplus.habbohotel.gameclients.GameClient;

/**
 * Created on 24/05/2017.
 */
public class GetFurnitureAliasesEvent implements MessageEvent {
    @Override
    public void parse(GameClient client, ClientMessage packet) {
        client.send(new FurnitureAliasesComposer());
    }
}
