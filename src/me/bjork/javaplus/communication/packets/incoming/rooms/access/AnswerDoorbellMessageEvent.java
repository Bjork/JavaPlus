package me.bjork.javaplus.communication.packets.incoming.rooms.access;

import me.bjork.javaplus.JavaPlusEnvironment;
import me.bjork.javaplus.communication.interfaces.ClientMessage;
import me.bjork.javaplus.communication.packets.MessageEvent;
import me.bjork.javaplus.communication.packets.outgoing.rooms.access.DoorbellAcceptedComposer;
import me.bjork.javaplus.communication.packets.outgoing.rooms.access.DoorbellNoAnswerComposer;
import me.bjork.javaplus.communication.packets.outgoing.rooms.session.HotelViewMessageComposer;
import me.bjork.javaplus.habbohotel.gameclients.GameClient;

/**
 * Created on 01/06/2017.
 */
public class AnswerDoorbellMessageEvent implements MessageEvent {
    @Override
    public void parse(GameClient client, ClientMessage packet) {
        if (client.getHabbo().getRoomUser() == null) {
            return;
        }

        if (client.getHabbo().getRoomUser().getRoom() == null) {
            return;
        }

        String username = packet.readString();
        boolean answered = packet.readBoolean();

        if (username.equals("")) {
            return;
        }

        GameClient requestingClient = JavaPlusEnvironment.getGame().getGameClientManager().getClientByUsername(username);

        // player could of d/c
        if (requestingClient == null) {
            return;
        }

        if (requestingClient.getHabbo() == null || requestingClient.getHabbo().getRoomUser() == null) {
            return;
        }

        // already answered ?
        if (requestingClient.getHabbo().getRoomUser().isDoorbellAnswered()) {
            return;
        }

        // still ringing for this room ?
        if (requestingClient.getHabbo().getRequestedRoomId() != client.getHabbo().getRoomUser().getRoom().getId()) {
            return;
        }

        if (!client.getHabbo().getRoomUser().getRoom().getRights().hasRights(client) && !client.getHabbo().getPermissions().hasRight("room_any_rights")) {
            return;
        }

        if (answered) {
            requestingClient.getHabbo().getRoomUser().setDoorbellAnswered(true);
            requestingClient.send(new DoorbellAcceptedComposer());
        } else {
            requestingClient.send(new DoorbellNoAnswerComposer());
            requestingClient.send(new HotelViewMessageComposer());
            requestingClient.getHabbo().setCurrentRoomId(0);
        }

        requestingClient.getHabbo().setRequestedRoomId(0);
    }
}
