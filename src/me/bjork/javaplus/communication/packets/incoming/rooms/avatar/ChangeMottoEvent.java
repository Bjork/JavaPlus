package me.bjork.javaplus.communication.packets.incoming.rooms.avatar;

import me.bjork.javaplus.JavaPlusEnvironment;
import me.bjork.javaplus.communication.interfaces.ClientMessage;
import me.bjork.javaplus.communication.packets.MessageEvent;
import me.bjork.javaplus.communication.packets.outgoing.rooms.engine.UserChangeComposer;
import me.bjork.javaplus.habbohotel.gameclients.GameClient;
import me.bjork.javaplus.habbohotel.rooms.Room;
import me.bjork.javaplus.habbohotel.rooms.RoomUser;
import me.bjork.javaplus.habbohotel.rooms.filter.FilterResult;
import org.apache.commons.lang3.StringUtils;

/**
 * Created on 03/06/2017.
 */
public class ChangeMottoEvent implements MessageEvent {
    @Override
    public void parse(GameClient client, ClientMessage packet) {
        if (client == null || client.getHabbo() == null || !client.getHabbo().inRoom())
            return;

        Room room = client.getHabbo().currentRoom();
        if (room == null)
            return;

        RoomUser roomUser = client.getHabbo().getRoomUser();
        if (roomUser == null)
            return;

        if (client.getHabbo().getTimeMuted() > 0) {
            final int timeMutedExpire = (int)client.getHabbo().getTimeMuted() - JavaPlusEnvironment.getIntUnixTimestamp();
            client.sendNotification("Oops, you're currently muted (Time left: %timeleft% seconds)! You cannot change your motto right now.".replace("%timeleft%", timeMutedExpire + ""));
            return;
        }

        if (JavaPlusEnvironment.getIntUnixTimestamp() - client.getHabbo().getLastMottoUpdateTime() <= 2) {
            int mottoUpdateWarnings = client.getHabbo().getMottoUpdateWarnings() + 1;
            client.getHabbo().setMottoUpdateWarnings(mottoUpdateWarnings);
            if (mottoUpdateWarnings >= 25)
                client.getHabbo().setSessionMottoBlocked(true);
            return;
        }

        if (client.getHabbo().getSessionMottoBlocked())
            return;

        client.getHabbo().setLastMottoUpdateTime(JavaPlusEnvironment.getIntUnixTimestamp());

        String motto = packet.readString();

        if (motto.equals(client.getHabbo().getMotto()))
            return;

        if (!client.getHabbo().getPermissions().hasRight("word_filter_override")) {
            FilterResult filterResult = JavaPlusEnvironment.getGame().getChatManager().getFilter().filter(motto);

            if (filterResult.isBlocked()) {
                //client.send(new AdvancedAlertMessageComposer(LanguageManager.TryGetValue("game.message.blocked").replace("%s", filterResult.getMessage()))); //TODO
                return;
            } else if (filterResult.wasModified()) {
                motto = filterResult.getMessage();
            }
        }

        client.getHabbo().setMotto(StringUtils.abbreviate(motto, 38));
        client.getHabbo().saveUser();

        room.send(new UserChangeComposer(roomUser, false));

        //TODO Quest & achievement unlocking
        //PlusEnvironment.GetGame().GetQuestManager().ProgressUserQuest(Session, QuestType.PROFILE_CHANGE_MOTTO);
        //PlusEnvironment.GetGame().GetAchievementManager().ProgressAchievement(Session, "ACH_Motto", 1);
    }
}
