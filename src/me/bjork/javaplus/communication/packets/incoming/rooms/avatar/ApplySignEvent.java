package me.bjork.javaplus.communication.packets.incoming.rooms.avatar;

import me.bjork.javaplus.communication.interfaces.ClientMessage;
import me.bjork.javaplus.communication.packets.MessageEvent;
import me.bjork.javaplus.habbohotel.gameclients.GameClient;
import me.bjork.javaplus.habbohotel.rooms.Room;
import me.bjork.javaplus.habbohotel.rooms.RoomUser;
import me.bjork.javaplus.habbohotel.rooms.objects.users.RoomUserStatus;

/**
 * Created on 01/06/2017.
 */
public class ApplySignEvent implements MessageEvent {
    @Override
    public void parse(GameClient client, ClientMessage packet) {
        if (client == null || client.getHabbo() == null || !client.getHabbo().inRoom())
            return;

        Room room = client.getHabbo().currentRoom();
        if (room == null)
            return;

        RoomUser roomUser = client.getHabbo().getRoomUser();
        if (roomUser == null)
            return;

        int signId = packet.readInt();

        roomUser.unIdle();

        roomUser.addStatus(RoomUserStatus.SIGN, String.valueOf(signId));
        roomUser.markDisplayingSign();
        roomUser.markNeedsUpdate();
    }
}
