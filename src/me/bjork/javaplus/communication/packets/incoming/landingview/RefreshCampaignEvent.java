package me.bjork.javaplus.communication.packets.incoming.landingview;

import com.mysql.cj.core.util.StringUtils;
import me.bjork.javaplus.communication.interfaces.ClientMessage;
import me.bjork.javaplus.communication.packets.MessageEvent;
import me.bjork.javaplus.communication.packets.outgoing.landingview.CampaignComposer;
import me.bjork.javaplus.core.Logging;
import me.bjork.javaplus.habbohotel.gameclients.GameClient;

/**
 * Created on 22/05/2017.
 */
public class RefreshCampaignEvent implements MessageEvent {
    @Override
    public void parse(GameClient client, ClientMessage packet) {
        try {

            String parseCampaings = packet.readString();
            if (parseCampaings.contains("gamesmaker"))
                return;

            String campaingName = "";
            String[] parser = parseCampaings.split(";");

            for (int i = 0; i < parser.length; i++) {
                if (StringUtils.isNullOrEmpty(parser[i]) || parser[i].endsWith(","))
                    continue;

                String[] data = parser[i].split(",");
                campaingName = data[1];
            }
            client.send(new CampaignComposer(parseCampaings, campaingName));
        }
        catch (Exception e)
        {
            Logging.printDebug("Error: " + e);
        }
    }
}
