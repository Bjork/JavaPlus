package me.bjork.javaplus.communication.packets;

import me.bjork.javaplus.communication.interfaces.ClientMessage;
import me.bjork.javaplus.habbohotel.gameclients.GameClient;

/**
 * Created on 20/05/2017.
 */
public interface MessageEvent {
    public void parse(GameClient client, ClientMessage packet);
}
