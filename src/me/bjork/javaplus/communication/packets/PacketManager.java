package me.bjork.javaplus.communication.packets;

import me.bjork.javaplus.communication.interfaces.ClientMessage;
import me.bjork.javaplus.communication.packets.incoming.ClientPacketHeader;
import me.bjork.javaplus.communication.packets.incoming.gamecenter.GetGameListingEvent;
import me.bjork.javaplus.communication.packets.incoming.gamecenter.InitializeGameCenterEvent;
import me.bjork.javaplus.communication.packets.incoming.handshake.*;
import me.bjork.javaplus.communication.packets.incoming.inventory.GetCreditsInfoEvent;
import me.bjork.javaplus.communication.packets.incoming.landingview.RefreshCampaignEvent;
import me.bjork.javaplus.communication.packets.incoming.messenger.FriendListUpdateEvent;
import me.bjork.javaplus.communication.packets.incoming.messenger.GetBuddyRequestsEvent;
import me.bjork.javaplus.communication.packets.incoming.messenger.MessengerInitEvent;
import me.bjork.javaplus.communication.packets.incoming.misc.ClientVariablesEvent;
import me.bjork.javaplus.communication.packets.incoming.misc.EventTrackerEvent;
import me.bjork.javaplus.communication.packets.incoming.misc.LatencyTestEvent;
import me.bjork.javaplus.communication.packets.incoming.misc.PingEvent;
import me.bjork.javaplus.communication.packets.incoming.navigator.*;
import me.bjork.javaplus.communication.packets.incoming.rooms.access.AnswerDoorbellMessageEvent;
import me.bjork.javaplus.communication.packets.incoming.rooms.access.LoadRoomByDoorBellMessageEvent;
import me.bjork.javaplus.communication.packets.incoming.rooms.avatar.*;
import me.bjork.javaplus.communication.packets.incoming.rooms.chat.*;
import me.bjork.javaplus.communication.packets.incoming.rooms.engine.GetFurnitureAliasesEvent;
import me.bjork.javaplus.communication.packets.incoming.rooms.engine.GetRoomEntryDataEvent;
import me.bjork.javaplus.communication.packets.incoming.rooms.engine.LeaveRoomMessageEvent;
import me.bjork.javaplus.communication.packets.incoming.rooms.engine.MoveAvatarEvent;
import me.bjork.javaplus.communication.packets.incoming.rooms.settings.GiveRoomScoreEvent;
import me.bjork.javaplus.communication.packets.incoming.users.*;
import me.bjork.javaplus.core.Logging;
import me.bjork.javaplus.habbohotel.gameclients.GameClient;

import java.util.HashMap;

/**
 * Created on 20/05/2017.
 */
public class PacketManager {
    private HashMap<Integer, MessageEvent> messages;

    public PacketManager() {
        this.messages = new HashMap<Integer, MessageEvent>();
        this.register();
    }

    public void register() {
        this.messages.clear();

        registerHandshakePackets();
        registerMiscPackets();
        registerLandingView();
        registerGamecenter();
        registerMessenger();
        registerNavigator();
        registerInventory();
        registerUsers();
        registerRooms();
        registerRoomChat();
        registerRoomAvatar();
    }



    private void registerHandshakePackets() {
        this.messages.put((int)ClientPacketHeader.GetClientVersionMessageEvent, new GetClientVersionEvent());
        this.messages.put((int)ClientPacketHeader.InitCryptoMessageEvent, new InitCryptoEvent());
        this.messages.put((int)ClientPacketHeader.GenerateSecretKeyMessageEvent, new GenerateSecretKeyEvent());
        this.messages.put((int)ClientPacketHeader.UniqueIDMessageEvent, new UniqueIDEvent());
        this.messages.put((int)ClientPacketHeader.SSOTicketMessageEvent, new SSOTicketEvent());
        this.messages.put((int)ClientPacketHeader.InfoRetrieveMessageEvent, new InfoRetrieveEvent());
    }

    private void registerMiscPackets() {
        this.messages.put((int)ClientPacketHeader.ClientVariablesMessageEvent, new ClientVariablesEvent());
        this.messages.put((int)ClientPacketHeader.EventTrackerMessageEvent, new EventTrackerEvent());
        this.messages.put((int)ClientPacketHeader.LatencyTestMessageEvent, new LatencyTestEvent());
        this.messages.put((int)ClientPacketHeader.PingMessageEvent, new PingEvent());
    }

    private void registerLandingView() {
        this.messages.put((int)ClientPacketHeader.RefreshCampaignMessageEvent, new RefreshCampaignEvent());
    }

    private void registerGamecenter() {
        this.messages.put((int)ClientPacketHeader.GetGameListingMessageEvent, new GetGameListingEvent());
        this.messages.put((int)ClientPacketHeader.InitializeGameCenterMessageEvent, new InitializeGameCenterEvent());
    }

    private void registerMessenger() {
        this.messages.put((int)ClientPacketHeader.MessengerInitMessageEvent, new MessengerInitEvent());
        this.messages.put((int)ClientPacketHeader.GetBuddyRequestsMessageEvent, new GetBuddyRequestsEvent());
        this.messages.put((int)ClientPacketHeader.FriendListUpdateMessageEvent, new FriendListUpdateEvent());
    }

    private void registerNavigator() {
        this.messages.put((int)ClientPacketHeader.InitializeNewNavigatorMessageEvent, new InitializeNewNavigatorEvent());
        this.messages.put((int)ClientPacketHeader.GetUserFlatCatsMessageEvent, new GetUserFlatCatsEvent());
        this.messages.put((int)ClientPacketHeader.GetEventCategoriesMessageEvent, new GetNavigatorFlatsEvent());
        this.messages.put((int)ClientPacketHeader.NavigatorSearchMessageEvent, new NavigatorSearchEvent());
        this.messages.put((int)ClientPacketHeader.GetGuestRoomMessageEvent, new GetGuestRoomEvent());
        this.messages.put((int)ClientPacketHeader.OpenFlatConnectionMessageEvent, new OpenFlatConnectionEvent());
        this.messages.put((int)ClientPacketHeader.GoToFlatMessageEvent, new LoadRoomByDoorBellMessageEvent());
        this.messages.put((int)ClientPacketHeader.LetUserInMessageEvent, new AnswerDoorbellMessageEvent());
    }

    private void registerRooms() {
        this.messages.put((int)ClientPacketHeader.GetRoomEntryDataMessageEvent, new GetRoomEntryDataEvent());
        this.messages.put((int)ClientPacketHeader.GetFurnitureAliasesMessageEvent, new GetFurnitureAliasesEvent());
        this.messages.put((int)ClientPacketHeader.GoToHotelViewMessageEvent, new LeaveRoomMessageEvent());
        this.messages.put((int)ClientPacketHeader.MoveAvatarMessageEvent, new MoveAvatarEvent());
        //this.messages.put((int)ClientPacketHeader.RequestCameraConfigurationEvent, new ); //TODO
    }

    private void registerRoomChat() {
        this.messages.put((int)ClientPacketHeader.StartTypingMessageEvent, new StartTypingEvent());
        this.messages.put((int)ClientPacketHeader.CancelTypingMessageEvent, new CancelTypingEvent());
        this.messages.put((int)ClientPacketHeader.ChatMessageEvent, new ChatEvent());
        this.messages.put((int)ClientPacketHeader.ShoutMessageEvent, new ShoutEvent());
        this.messages.put((int)ClientPacketHeader.WhisperMessageEvent, new WhisperEvent());
    }
    private void registerInventory() {
        this.messages.put((int)ClientPacketHeader.GetCreditsInfoMessageEvent, new GetCreditsInfoEvent());
        //this.messages.put((int)ClientPacketHeader.GetAchievementsMessageEvent, new ); //TODO
    }

    private void registerUsers() {
        this.messages.put((int)ClientPacketHeader.ScrGetUserInfoMessageEvent, new ScrGetUserInfoEvent());
        this.messages.put((int)ClientPacketHeader.SoundSettingsMessageEvent, new SoundSettingsEvent());
        this.messages.put((int)ClientPacketHeader.GetIgnoredUsersMessageEvent, new GetIgnoredUsersEvent());
        this.messages.put((int)ClientPacketHeader.GetHabboGroupBadgesMessageEvent, new GetHabboGroupBadgesEvent());
        this.messages.put((int)ClientPacketHeader.GetSelectedBadgesMessageEvent, new GetSelectedBadgesEvent());
        this.messages.put((int)ClientPacketHeader.RespectUserMessageEvent, new RespectUserEvent());
    }

    private void registerRoomAvatar() {
        this.messages.put((int)ClientPacketHeader.LookToMessageEvent, new LookToEvent());
        this.messages.put((int)ClientPacketHeader.SitMessageEvent, new SitEvent());
        this.messages.put((int)ClientPacketHeader.ActionMessageEvent, new ActionEvent());
        this.messages.put((int)ClientPacketHeader.DanceMessageEvent, new DanceEvent());
        this.messages.put((int)ClientPacketHeader.ApplySignMessageEvent, new ApplySignEvent());
        this.messages.put((int)ClientPacketHeader.GiveHandItemMessageEvent, new GiveHandItemEvent());
        this.messages.put((int)ClientPacketHeader.DropHandItemMessageEvent, new DropHandItemEvent());
        this.messages.put((int)ClientPacketHeader.ChangeMottoMessageEvent, new ChangeMottoEvent());
        this.messages.put((int)ClientPacketHeader.GiveRoomScoreMessageEvent, new GiveRoomScoreEvent());


    }

    public void handleRequest(GameClient player, ClientMessage message) {
        if (messages.containsKey(message.getMessageId())) {
            Logging.printDebug("[INCOMING]: " + message.getMessageId() + " / " + message.getMessageBody());
            messages.get(message.getMessageId()).parse(player, message);
        }
        else
            Logging.printDebug("[UNHANDLED INCOMING]: " + message.getMessageId());
    }

    public void unregisterPackets(){
        this.messages.clear();
    }

    public HashMap<Integer, MessageEvent> getMessages() {
        return messages;
    }
}