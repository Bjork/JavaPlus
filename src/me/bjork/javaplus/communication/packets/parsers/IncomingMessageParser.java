package me.bjork.javaplus.communication.packets.parsers;

import me.bjork.javaplus.communication.interfaces.ClientMessage;

/**
 * Created on 20/05/2017.
 */
public interface IncomingMessageParser {
    public void read(ClientMessage reader);
}
