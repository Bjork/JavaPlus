package me.bjork.javaplus.communication.packets.parsers;

import me.bjork.javaplus.communication.connection.netty.streams.NettyResponse;

/**
 * Created on 20/05/2017.
 */
public abstract class OutgoingMessageComposer {
    protected NettyResponse response;

    public OutgoingMessageComposer() {
        this.response = new NettyResponse();
    }

    public abstract void write();

    public NettyResponse getResponse() {
        return response;
    }

    public void setResponse(NettyResponse response) {
        this.response = response;
    }
}
