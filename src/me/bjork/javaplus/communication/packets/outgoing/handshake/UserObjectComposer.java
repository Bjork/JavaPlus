package me.bjork.javaplus.communication.packets.outgoing.handshake;

import me.bjork.javaplus.communication.packets.outgoing.ServerPacketHeader;
import me.bjork.javaplus.communication.packets.parsers.OutgoingMessageComposer;
import me.bjork.javaplus.habbohotel.users.Habbo;

/**
 * Created on 22/05/2017.
 */
public class UserObjectComposer extends OutgoingMessageComposer {

    private int id;
    private String username;
    private String look;
    private String gender;
    private String motto;
    private int respects;
    private int dailyRespectsPoints;
    private int dailyPetRespectPoints;
    private String lastOnline;
    private Boolean canChangeName;

    public UserObjectComposer(Habbo habbo) {

        this.id = habbo.getId();
        this.username = habbo.getUsername();
        this.look = habbo.getLook();
        this.gender = habbo.getGender();
        this.motto = habbo.getMotto();
        this.respects = habbo.getStats().getRespects();
        this.dailyRespectsPoints = habbo.getStats().getDailyRespectPoints();
        this.dailyPetRespectPoints = habbo.getStats().getDailyPetRespectPoints();
        this.lastOnline = String.valueOf(habbo.getLastonline());
        this.canChangeName = habbo.getChangingName();
    }

    @Override
    public void write() {
        response.init(ServerPacketHeader.UserObjectMessageComposer);
        response.writeInt(id);
        response.writeString(username);
        response.writeString(look);
        response.writeString(gender.toUpperCase());
        response.writeString(motto);
        response.writeString("");
        response.writeBool(false);
        response.writeInt(respects);
        response.writeInt(dailyRespectsPoints);
        response.writeInt(dailyPetRespectPoints);
        response.writeBool(false);
        response.writeString(lastOnline);
        response.writeBool(canChangeName);
        response.writeBool(false);
    }
}
