package me.bjork.javaplus.communication.packets.outgoing.handshake;

import me.bjork.javaplus.communication.packets.outgoing.ServerPacketHeader;
import me.bjork.javaplus.communication.packets.parsers.OutgoingMessageComposer;

/**
 * Created on 20/05/2017.
 */
public class InitCryptoMessageComposer extends OutgoingMessageComposer {

    @Override
    public void write() {
        response.init(ServerPacketHeader.InitCryptoMessageComposer);
        response.writeString("");
        response.writeString("");
    }
}
