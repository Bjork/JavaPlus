package me.bjork.javaplus.communication.packets.outgoing.handshake;

import me.bjork.javaplus.communication.packets.outgoing.ServerPacketHeader;
import me.bjork.javaplus.communication.packets.parsers.OutgoingMessageComposer;
import me.bjork.javaplus.habbohotel.gameclients.GameClient;

/**
 * Created on 21/05/2017.
 */
public class SetUniqueIdComposer extends OutgoingMessageComposer {
    private GameClient client;

    public SetUniqueIdComposer(GameClient client) {
        this.client = client;
    }

    @Override
    public void write() {
        response.init(ServerPacketHeader.SetUniqueIdMessageComposer);
        response.writeString(this.client.getMachineId());
    }
}
