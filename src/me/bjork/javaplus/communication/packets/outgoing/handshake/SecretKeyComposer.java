package me.bjork.javaplus.communication.packets.outgoing.handshake;

import me.bjork.javaplus.communication.packets.outgoing.ServerPacketHeader;
import me.bjork.javaplus.communication.packets.parsers.OutgoingMessageComposer;

/**
 * Created on 20/05/2017.
 */
public class SecretKeyComposer extends OutgoingMessageComposer {
    private final String publicKey;

    public SecretKeyComposer(final String publicKey) {
        this.publicKey = publicKey;
    }

    @Override
    public void write() {
        response.init(ServerPacketHeader.SecretKeyMessageComposer);
        response.writeString(publicKey);
    }
}
