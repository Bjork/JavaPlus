package me.bjork.javaplus.communication.packets.outgoing.notifications;

import me.bjork.javaplus.communication.packets.outgoing.ServerPacketHeader;
import me.bjork.javaplus.communication.packets.parsers.OutgoingMessageComposer;

/**
 * Created on 31/05/2017.
 */
public class MOTDNotificationComposer extends OutgoingMessageComposer {
    private String message;

    public MOTDNotificationComposer(String message) {
        this.message = message;
    }

    @Override
    public void write() {
        response.init(ServerPacketHeader.MOTDNotificationMessageComposer);
        response.writeInt(1);
        response.writeString(message);
    }
}
