package me.bjork.javaplus.communication.packets.outgoing.notifications;

import me.bjork.javaplus.communication.packets.outgoing.ServerPacketHeader;
import me.bjork.javaplus.communication.packets.parsers.OutgoingMessageComposer;

/**
 * Created on 31/05/2017.
 */
public class BubbleNotificationComposer extends OutgoingMessageComposer {
    private String message;
    private String image;

    public BubbleNotificationComposer(String message, String image) {
        this.message = message;
        this.image = image;
    }

    @Override
    public void write() {
        response.init(ServerPacketHeader.RoomNotificationMessageComposer);
        response.writeString("generic");
        response.writeInt(3);
        response.writeString("display");
        response.writeString("BUBBLE");
        response.writeString("image");
        response.writeString(image);
        response.writeString("message");
        response.writeString(message);
    }
}
