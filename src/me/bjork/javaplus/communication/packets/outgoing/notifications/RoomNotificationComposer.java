package me.bjork.javaplus.communication.packets.outgoing.notifications;

import com.mysql.cj.core.util.StringUtils;
import me.bjork.javaplus.communication.packets.outgoing.ServerPacketHeader;
import me.bjork.javaplus.communication.packets.parsers.OutgoingMessageComposer;

/**
 * Created on 31/05/2017.
 */
public class RoomNotificationComposer extends OutgoingMessageComposer {
    private String title;
    private String message;
    private String image;
    private String hotelName;
    private String hotelURL;

    public RoomNotificationComposer(String title, String message, String image, String hotelName, String hotelURL) {
        this.title = title;
        this.message = message;
        this.image = image;
        this.hotelName = hotelName;
        this.hotelURL = hotelURL;
    }

    @Override
    public void write() {
        response.init(ServerPacketHeader.RoomNotificationMessageComposer);
        response.writeString(image);
        response.writeInt(StringUtils.isNullOrEmpty(hotelName) ? 2 : 4);
        response.writeString("title");
        response.writeString(title);
        response.writeString("message");
        response.writeString(message);

        if (!StringUtils.isNullOrEmpty(hotelName))
        {
            response.writeString("linkUrl");
            response.writeString(hotelURL);
            response.writeString("linkTitle");
            response.writeString(hotelName);
        }
    }
}
