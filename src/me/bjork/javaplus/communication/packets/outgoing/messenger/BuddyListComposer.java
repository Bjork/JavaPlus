package me.bjork.javaplus.communication.packets.outgoing.messenger;

import me.bjork.javaplus.communication.packets.outgoing.ServerPacketHeader;
import me.bjork.javaplus.communication.packets.parsers.OutgoingMessageComposer;

/**
 * Created on 22/05/2017.
 */
public class BuddyListComposer extends OutgoingMessageComposer {
    @Override
    public void write() {
        response.init(ServerPacketHeader.BuddyListMessageComposer);
        response.writeInt(0);
        response.writeInt(0);
        response.writeInt(0);
    }
}
