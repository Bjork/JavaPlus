package me.bjork.javaplus.communication.packets.outgoing.messenger;

import me.bjork.javaplus.communication.packets.outgoing.ServerPacketHeader;
import me.bjork.javaplus.communication.packets.parsers.OutgoingMessageComposer;

/**
 * Created on 22/05/2017.
 */
public class BuddyRequestsComposer extends OutgoingMessageComposer {
    @Override
    public void write() {

        response.init(ServerPacketHeader.BuddyRequestsMessageComposer);
        response.writeInt(0); // Count
        response.writeInt(0); // Count

        /*foreach (MessengerRequest Request in requests)
        {
            base.WriteInteger(Request.From);
            base.WriteString(Request.Username);

            UserCache User = PlusEnvironment.GetGame().GetCacheManager().GenerateUser(Request.From);
            base.WriteString(User != null ? User.Look : "");
        }*/
    }
}
