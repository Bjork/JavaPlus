package me.bjork.javaplus.communication.packets.outgoing.messenger;

import me.bjork.javaplus.communication.packets.outgoing.ServerPacketHeader;
import me.bjork.javaplus.communication.packets.parsers.OutgoingMessageComposer;
import me.bjork.javaplus.core.settings.SettingsManager;

/**
 * Created on 22/05/2017.
 */
public class MessengerInitComposer extends OutgoingMessageComposer {
    @Override
    public void write() {
        response.init(ServerPacketHeader.MessengerInitMessageComposer);
        response.writeInt(Integer.valueOf(SettingsManager.tryGetValue("messenger.buddy_limit"))); //Friend Max
        response.writeInt(300);
        response.writeInt(800);
        response.writeInt(0);
    }
}
