package me.bjork.javaplus.communication.packets.outgoing;

/**
 * Created on 20/05/2017.
 */
public class ServerPacketHeader {
    // Handshake
    public static final short InitCryptoMessageComposer = 3531;//675
    public static final short SecretKeyMessageComposer = 696;//3179
    public static final short AuthenticationOKMessageComposer = 1079;//1442
    public static final short UserObjectMessageComposer = 845;//1823
    public static final short UserPerksMessageComposer = 1790;//2807
    public static final short UserRightsMessageComposer = 3315;//1862
    public static final short GenericErrorMessageComposer = 905;//169
    public static final short SetUniqueIdMessageComposer = 3731;//2935
    public static final short AvailabilityStatusMessageComposer = 3690;//2468

    // Avatar
    public static final short WardrobeMessageComposer = 2959;//2760

    // Catalog
    public static final short CatalogIndexMessageComposer = 2140;//2018
    public static final short CatalogItemDiscountMessageComposer = 796;//3322
    public static final short PurchaseOKMessageComposer = 1450;//2843
    public static final short CatalogOfferMessageComposer = 1757;//3848
    public static final short CatalogPageMessageComposer = 3277;//3477
    public static final short CatalogUpdatedMessageComposer = 1411;//885
    public static final short SellablePetBreedsMessageComposer = 2333;//1871
    public static final short GroupFurniConfigMessageComposer = 3388;//418
    public static final short PresentDeliverErrorMessageComposer = 1971;//934

    // Marketplace
    public static final short MarketplaceCancelOfferResultMessageComposer = 1980;//202
    public static final short MarketPlaceOffersMessageComposer = 291;//2985
    public static final short MarketPlaceOwnOffersMessageComposer = 1892;//2806
    public static final short MarketplaceItemStatsMessageComposer = 2201;//2909
    public static final short MarketplaceConfigurationMessageComposer = 1817;//3702
    public static final short MarketplaceCanMakeOfferResultMessageComposer = 2452;//1874
    public static final short MarketplaceMakeOfferResultMessageComposer = 480;//3960

    // Quests
    public static final short QuestListMessageComposer = 3436;//664
    public static final short QuestCompletedMessageComposer = 3715;//3692
    public static final short QuestAbortedMessageComposer = 182;//3581
    public static final short QuestStartedMessageComposer = 3281;//1477

    // Room Avatar
    public static final short ActionMessageComposer = 3349;//179
    public static final short SleepMessageComposer = 2306;//3852
    public static final short DanceMessageComposer = 130;//845
    public static final short CarryObjectMessageComposer = 2106;//2623
    public static final short AvatarEffectMessageComposer = 2062;//2662

    // Room Chat
    public static final short ChatMessageComposer = 2785;//3821
    public static final short ShoutMessageComposer = 2888;//909
    public static final short WhisperMessageComposer = 1400;//2280
    public static final short FloodControlMessageComposer = 803;//1197
    public static final short UserTypingMessageComposer = 1727;//2854

    // Room Engine
    public static final short UsersMessageComposer = 3857;//2422
    public static final short FurnitureAliasesMessageComposer = 2159;//81
    public static final short ObjectAddMessageComposer = 2076;//505
    public static final short ObjectsMessageComposer = 2783;//3521
    public static final short ObjectUpdateMessageComposer = 1104;//273
    public static final short ObjectRemoveMessageComposer = 2362;//85
    public static final short SlideObjectBundleMessageComposer = 330;//11437
    public static final short ItemsMessageComposer = 580;//2335
    public static final short ItemAddMessageComposer = 2236;//1841
    public static final short ItemUpdateMessageComposer = 3408;//2933
    public static final short ItemRemoveMessageComposer = 209;//762

    // Room Session
    public static final short RoomForwardMessageComposer = 3289;//1963
    public static final short RoomReadyMessageComposer = 768;//2029
    public static final short OpenConnectionMessageComposer = 3566;//1329
    public static final short CloseConnectionMessageComposer = 726;//1898
    public static final short FlatAccessibleMessageComposer = 735;//1179
    public static final short CantConnectMessageComposer = 200;//1864

    // Room Permissions
    public static final short YouAreControllerMessageComposer = 680;//1425
    public static final short YouAreNotControllerMessageComposer = 1068;//1202
    public static final short YouAreOwnerMessageComposer = 1932;//495

    // Room Settings
    public static final short RoomSettingsDataMessageComposer = 3361;//633
    public static final short RoomSettingsSavedMessageComposer = 3865;//3737
    public static final short FlatControllerRemovedMessageComposer = 1501;//1205
    public static final short FlatControllerAddedMessageComposer = 3493;//1056
    public static final short RoomRightsListMessageComposer = 225;//2410

    // Room Polls
    public static final short PollOfferMessageComposer = 2350;
    public static final short PollContentsMessageComposer = 813;

    // Room Furniture
    public static final short HideWiredConfigMessageComposer = 2430;//3715
    public static final short WiredEffectConfigMessageComposer = 1428;//1469
    public static final short WiredConditionConfigMessageComposer = 1775;//1456
    public static final short WiredTriggerConfigMessageComposer = 21;//1618
    public static final short MoodlightConfigMessageComposer = 1540;//1964
    public static final short GroupFurniSettingsMessageComposer = 3755;//613
    public static final short OpenGiftMessageComposer = 862;//1375

    // Navigator
    public static final short UpdateFavouriteRoomMessageComposer = 3016;//854
    public static final short NavigatorLiftedRoomsMessageComposer = 1568;//761
    public static final short NavigatorPreferencesMessageComposer = 3617;//1430
    public static final short NavigatorFlatCatsMessageComposer = 1265;//1109
    public static final short NavigatorMetaDataParserMessageComposer = 1071;//371
    public static final short NavigatorCollapsedCategoriesMessageComposer = 232;//1263
    public static final short NavigatorSettingsMessageComposer = 2477;//3175
    public static final short NavigatorSearchResultSetMessageComposer = 1089;//815

    // Messenger
    public static final short BuddyListMessageComposer = 2900;//3394
    public static final short BuddyRequestsMessageComposer = 177;//2757
    public static final short NewBuddyRequestMessageComposer = 1525;//2981

    // Moderation
    public static final short ModeratorInitMessageComposer = 2120;//2545
    public static final short ModeratorUserRoomVisitsMessageComposer = 1282;//1101
    public static final short ModeratorRoomChatlogMessageComposer = 3561;//1362
    public static final short ModeratorUserInfoMessageComposer = 3234;//289
    public static final short ModeratorSupportTicketResponseMessageComposer = 2651;//3927
    public static final short ModeratorUserChatlogMessageComposer = 2812;//3308
    public static final short ModeratorRoomInfoMessageComposer = 2318;//13
    public static final short ModeratorSupportTicketMessageComposer = 1258;//1275
    public static final short ModeratorTicketChatlogMessageComposer = 3637;//766
    public static final short CallForHelpPendingCallsMessageComposer = 2460;
    public static final short CfhTopicsInitMessageComposer = 1094;

    // Inventory
    public static final short CreditBalanceMessageComposer = 1958;//3604
    public static final short BadgesMessageComposer = 2943;//154
    public static final short FurniListAddMessageComposer = 2020;//176
    public static final short FurniListNotificationMessageComposer = 439;//2725
    public static final short FurniListRemoveMessageComposer = 3968;//1903
    public static final short FurniListMessageComposer = 3640;//2183
    public static final short FurniListUpdateMessageComposer = 1619;//506
    public static final short AvatarEffectsMessageComposer = 1684;//3310
    public static final short AvatarEffectActivatedMessageComposer = 545;//1710
    public static final short AvatarEffectExpiredMessageComposer = 2673;//68
    public static final short AvatarEffectAddedMessageComposer = 1507;//2760
    public static final short TradingErrorMessageComposer = 2484;//2876
    public static final short TradingAcceptMessageComposer = 969;//1367
    public static final short TradingStartMessageComposer = 2527;//2290
    public static final short TradingUpdateMessageComposer = 2088;//2277
    public static final short TradingClosedMessageComposer = 1436;//2068
    public static final short TradingCompleteMessageComposer = 2288;//1959
    public static final short TradingConfirmedMessageComposer = 969;//1367
    public static final short TradingFinishMessageComposer = 3443;//2369

    // Inventory Achievements
    public static final short AchievementsMessageComposer = 1801;//509
    public static final short AchievementScoreMessageComposer = 1115;//3710
    public static final short AchievementUnlockedMessageComposer = 3385;//1887
    public static final short AchievementProgressedMessageComposer = 2749;//305

    // Notifications
    public static final short ActivityPointsMessageComposer = 3318;//1911
    public static final short HabboActivityPointNotificationMessageComposer = 543;//606

    // Users
    public static final short ScrSendUserInfoMessageComposer = 826;//2811
    public static final short IgnoredUsersMessageComposer = 2157;

    // Groups
    public static final short UnknownGroupMessageComposer = 1136;//1T981
    public static final short GroupMembershipRequestedMessageComposer = 2472;//423
    public static final short ManageGroupMessageComposer = 230;//2653
    public static final short HabboGroupBadgesMessageComposer = 711;//2487
    public static final short NewGroupInfoMessageComposer = 815;//1095
    public static final short GroupInfoMessageComposer = 3712;//3160
    public static final short GroupCreationWindowMessageComposer = 1062;//1232
    public static final short SetGroupIdMessageComposer = 364;//3197
    public static final short GroupMembersMessageComposer = 1401;//2297
    public static final short UpdateFavouriteGroupMessageComposer = 2000;//3685
    public static final short GroupMemberUpdatedMessageComposer = 3911;//2954
    public static final short RefreshFavouriteGroupMessageComposer = 149;//382

    // Group Forums
    public static final short ForumsListDataMessageComposer = 1539;//3596
    public static final short ForumDataMessageComposer = 91;//254
    public static final short ThreadCreatedMessageComposer = 2675;//3683
    public static final short ThreadDataMessageComposer = 2526;//879
    public static final short ThreadsListDataMessageComposer = 1056;//1538
    public static final short ThreadUpdatedMessageComposer = 951;//3226
    public static final short ThreadReplyMessageComposer = 1003;//1936

    // Sound
    public static final short SoundSettingsMessageComposer = 1949;//2921

    public static final short QuestionParserMessageComposer = 1163;//1719
    public static final short AvatarAspectUpdateMessageComposer = 1141;
    public static final short HelperToolMessageComposer = 3610;//224
    public static final short RoomErrorNotifMessageComposer = 2355;//444
    public static final short FollowFriendFailedMessageComposer = 3469;//1170

    public static final short FindFriendsProcessResultMessageComposer = 2921;//3763
    public static final short UserChangeMessageComposer = 2248;//32
    public static final short FloorHeightMapMessageComposer = 1819;//1112
    public static final short RoomInfoUpdatedMessageComposer = 3743;//3833
    public static final short MessengerErrorMessageComposer = 880;//915
    public static final short GameAccountStatusMessageComposer = 3750;//139
    public static final short GuestRoomSearchResultMessageComposer = 1634;//43
    public static final short NewUserExperienceGiftOfferMessageComposer = 2029;//1904
    public static final short UpdateUsernameMessageComposer = 3461;//3801
    public static final short VoucherRedeemOkMessageComposer = 2809;//3432
    public static final short FigureSetIdsMessageComposer = 1811;//3469
    public static final short StickyNoteMessageComposer = 344;//2338
    public static final short UserRemoveMessageComposer = 3839;//2841
    public static final short GetGuestRoomResultMessageComposer = 306;//2224
    public static final short DoorbellMessageComposer = 2068;//162

    public static final short GiftWrappingConfigurationMessageComposer = 766;//3348
    public static final short GetRelationshipsMessageComposer = 112;//1589
    public static final short FriendNotificationMessageComposer = 3024;//1211
    public static final short BadgeEditorPartsMessageComposer = 2839;//2519
    public static final short TraxSongInfoMessageComposer = 1159;//523
    public static final short PostUpdatedMessageComposer = 1180;//1752
    public static final short UserUpdateMessageComposer = 3559;//3153
    public static final short MutedMessageComposer = 2246;//229
    public static final short CheckGnomeNameMessageComposer = 3228;//2491
    public static final short OpenBotActionMessageComposer = 464;//895
    public static final short FavouritesMessageComposer = 3267;//604
    public static final short TalentLevelUpMessageComposer = 3150;//3538

    public static final short BCBorrowedItemsMessageComposer = 1043;//3424
    public static final short UserTagsMessageComposer = 940;//774
    public static final short CampaignMessageComposer = 2394;//3234
    public static final short RoomEventMessageComposer = 1587;//2274
    public static final short HabboSearchResultMessageComposer = 2823;//214
    public static final short PetHorseFigureInformationMessageComposer = 2926;//560
    public static final short PetInventoryMessageComposer = 1988;//3528
    public static final short PongMessageComposer = 1240;//624
    public static final short RentableSpaceMessageComposer = 2323;//2660
    public static final short GetYouTubePlaylistMessageComposer = 1354;//763
    public static final short RespectNotificationMessageComposer = 1818;//474
    public static final short RecyclerRewardsMessageComposer = 2442;//2457
    public static final short ReloadRecyclerComposer = 1604;
    public static final short RecyclerStateComposer = 2769;
    public static final short GetRoomBannedUsersMessageComposer = 1810;//3580
    public static final short RoomRatingMessageComposer = 2454;//3464
    public static final short PlayableGamesMessageComposer = 3076;//549
    public static final short TalentTrackLevelMessageComposer = 700;//2382
    public static final short JoinQueueMessageComposer = 3139;//749
    public static final short PetBreedingMessageComposer = 528;//616
    public static final short SubmitBullyReportMessageComposer = 47;//453
    public static final short UserNameChangeMessageComposer = 574;//2587
    public static final short LoveLockDialogueMessageComposer = 1157;//173
    public static final short SendBullyReportMessageComposer = 39;//2094
    public static final short VoucherRedeemErrorMessageComposer = 2279;//3670
    public static final short PurchaseErrorMessageComposer = 1331;//3016
    public static final short UnknownCalendarMessageComposer = 128;//1799
    public static final short FriendListUpdateMessageComposer = 1190;//1611

    public static final short UserFlatCatsMessageComposer = 3379;//377
    public static final short UpdateFreezeLivesMessageComposer = 2998;//1395
    public static final short UnbanUserFromRoomMessageComposer = 3710;//3472
    public static final short PetTrainingPanelMessageComposer = 546;//1067
    public static final short LoveLockDialogueCloseMessageComposer = 1767;//1534
    public static final short BuildersClubMembershipMessageComposer = 820;//2357
    public static final short FlatAccessDeniedMessageComposer = 797;//1582
    public static final short LatencyResponseMessageComposer = 942;//3014
    public static final short HabboUserBadgesMessageComposer = 3269;//1123
    public static final short HeightMapMessageComposer = 1232;//207

    public static final short CanCreateRoomMessageComposer = 3568;//1237
    public static final short InstantMessageErrorMessageComposer = 945;//2964
    public static final short GnomeBoxMessageComposer = 1694;//1778
    public static final short IgnoreStatusMessageComposer = 2485;//3882
    public static final short PetInformationMessageComposer = 3380;//3913
    public static final short ConcurrentUsersGoalProgressMessageComposer = 3782;//2955
    public static final short VideoOffersRewardsMessageComposer = 1806;//1896
    public static final short SanctionStatusMessageComposer = 3525;//193
    public static final short GetYouTubeVideoMessageComposer = 1022;//2374
    public static final short CheckPetNameMessageComposer = 1760;//3019
    public static final short RespectPetNotificationMessageComposer = 540;//3637
    public static final short EnforceCategoryUpdateMessageComposer = 3714;//315
    public static final short CommunityGoalHallOfFameMessageComposer = 2629;//690
    public static final short FloorPlanFloorMapMessageComposer = 1855;//2337
    public static final short SendGameInvitationMessageComposer = 2071;//1165
    public static final short GiftWrappingErrorMessageComposer = 1385;//2534
    public static final short PromoArticlesMessageComposer = 3015;//3565
    public static final short Game1WeeklyLeaderboardMessageComposer = 57;//3124
    public static final short RentableSpacesErrorMessageComposer = 1255;//838
    public static final short AddExperiencePointsMessageComposer = 3791;//3779
    public static final short OpenHelpToolMessageComposer = 2460;//3831
    public static final short GetRoomFilterListMessageComposer = 1100;//2169
    public static final short GameAchievementListMessageComposer = 2141;//1264
    public static final short PromotableRoomsMessageComposer = 442;//2166
    public static final short FloorPlanSendDoorMessageComposer = 1685;//2180
    public static final short RoomEntryInfoMessageComposer = 3675;//3378
    public static final short RoomNotificationMessageComposer = 3152;//2419
    public static final short ClubGiftsMessageComposer = 2992;//1549
    public static final short MOTDNotificationMessageComposer = 1368;//1829
    public static final short PopularRoomTagsResultMessageComposer = 1002;//234
    public static final short NewConsoleMessageMessageComposer = 984;//2121
    public static final short RoomPropertyMessageComposer = 1897;//1328
    public static final short TalentTrackMessageComposer = 382;//3614
    public static final short ProfileInformationMessageComposer = 3263;//3872
    public static final short BadgeDefinitionsMessageComposer = 1827;//2066
    public static final short Game2WeeklyLeaderboardMessageComposer = 275;//1127
    public static final short NameChangeUpdateMessageComposer = 1226;//2698
    public static final short RoomVisualizationSettingsMessageComposer = 3003;//3786
    public static final short FlatCreatedMessageComposer = 3001;//1621
    public static final short BotInventoryMessageComposer = 3692;//2620
    public static final short LoadGameMessageComposer = 652;//1403
    public static final short UpdateMagicTileMessageComposer = 2811;//2641
    public static final short CampaignCalendarDataMessageComposer = 2276;//1480
    public static final short MaintenanceStatusMessageComposer = 3465;//3198
    public static final short Game3WeeklyLeaderboardMessageComposer = 1326;//2194
    public static final short GameListMessageComposer = 1220;//2481
    public static final short RoomMuteSettingsMessageComposer = 1117;//257
    public static final short RoomInviteMessageComposer = 2138;//3942
    public static final short LoveLockDialogueSetLockedMessageComposer = 1767;//1534
    public static final short BroadcastMessageAlertMessageComposer = 1751;//1279
    public static final short MessengerInitMessageComposer = 1329;//391
    public static final short WelcomeAlertComposer = 2954;
    public static final short HallOfFameMessageComposer = 2208;//3565
    public static final short ClubDataWindowComposer = 1319;
    public static final short ClubDataComposer = 1570;
    public static final short NavigatorSavedSearchesMessageComposer = 2908;
}
