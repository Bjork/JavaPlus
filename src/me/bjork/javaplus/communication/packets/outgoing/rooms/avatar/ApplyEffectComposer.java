package me.bjork.javaplus.communication.packets.outgoing.rooms.avatar;

import me.bjork.javaplus.communication.packets.outgoing.ServerPacketHeader;
import me.bjork.javaplus.communication.packets.parsers.OutgoingMessageComposer;

/**
 * Created on 07/06/2017.
 */
public class ApplyEffectComposer extends OutgoingMessageComposer {
    private int virtualId;
    private int effectId;

    public ApplyEffectComposer(int virtualId, int effectId) {
        this.virtualId = virtualId;
        this.effectId = effectId;
    }

    @Override
    public void write() {
        response.init(ServerPacketHeader.AvatarEffectMessageComposer);
        response.writeInt(virtualId);
        response.writeInt(effectId);
        response.writeInt(0);
    }
}
