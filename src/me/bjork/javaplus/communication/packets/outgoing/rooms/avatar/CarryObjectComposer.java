package me.bjork.javaplus.communication.packets.outgoing.rooms.avatar;

import me.bjork.javaplus.communication.packets.outgoing.ServerPacketHeader;
import me.bjork.javaplus.communication.packets.parsers.OutgoingMessageComposer;

/**
 * Created on 26/05/2017.
 */
public class CarryObjectComposer extends OutgoingMessageComposer {
    private int virtualId;
    private int handItem;

    public CarryObjectComposer(int virtualId, int handItem) {
        this.virtualId = virtualId;
        this.handItem = handItem;
    }

    @Override
    public void write() {
        response.init(ServerPacketHeader.CarryObjectMessageComposer);
        response.writeInt(virtualId);
        response.writeInt(handItem);
    }
}
