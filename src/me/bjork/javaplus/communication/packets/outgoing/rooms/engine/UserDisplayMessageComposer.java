package me.bjork.javaplus.communication.packets.outgoing.rooms.engine;

import me.bjork.javaplus.communication.packets.outgoing.ServerPacketHeader;
import me.bjork.javaplus.communication.packets.parsers.OutgoingMessageComposer;
import me.bjork.javaplus.habbohotel.groups.Group;
import me.bjork.javaplus.habbohotel.rooms.RoomUser;
import me.bjork.javaplus.habbohotel.users.Habbo;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * Created on 25/05/2017.
 */
public class UserDisplayMessageComposer extends OutgoingMessageComposer {

    private List<RoomUser> roomUsers;

    public UserDisplayMessageComposer(RoomUser user) {
        this(Arrays.asList(new RoomUser[] { user }));
    }

    public UserDisplayMessageComposer(List<RoomUser> roomUsers) {
        this.roomUsers = roomUsers;
    }

    @Override
    public void write() {
        response.init(ServerPacketHeader.UsersMessageComposer);

        synchronized (this.roomUsers) {
            response.writeInt(this.roomUsers.size());
            for (RoomUser user : this.roomUsers) {
                writeUser(user);
            }
        }
    }

    private void writeUser(RoomUser user) {
        Habbo habbo = user.getClient().getHabbo();
        Group group = null;

        //TODO Group Checking et Ajout pets et bots

        if (habbo.getPetId() == 0) {
            response.writeInt(habbo.getId());
            response.writeString(habbo.getUsername());
            response.writeString(habbo.getMotto());
            response.writeString(habbo.getPetId() > 0 && habbo.getPetId() != 100 ? PetFigureForType(habbo.getPetId()) : habbo.getLook());
            response.writeInt(user.getVirtualId());
            response.writeInt(user.getX());
            response.writeInt(user.getY());
            response.writeString(Double.toString(user.getZ()));
            response.writeInt(0); // 2 for real user, 4 for bot
            response.writeInt(1); // 1 for real user, 2 for pet, 3 for bot
            response.writeString(habbo.getGender().toLowerCase());

            if (group != null) {
                //response.writeInt(group.getId());
                //response.writeInt(0);
                //response.writeString(group.getName());
            } else {
                response.writeInt(0);
                response.writeInt(0);
                response.writeString("");
            }

            response.writeString("");
            response.writeInt(habbo.getStats().getAchievementPoints());
            response.writeBool(false);
        }
        else if (habbo.getPetId() > 0 && habbo.getPetId() != 100) {
            response.writeInt(habbo.getId());
            response.writeString(habbo.getUsername());
            response.writeString(habbo.getMotto());
            response.writeString(PetFigureForType(habbo.getPetId()));
            response.writeInt(user.getVirtualId());
            response.writeInt(user.getX());
            response.writeInt(user.getY());
            response.writeString(Double.toString(user.getZ()));
            response.writeInt(0); // 2 for real user, 4 for bot
            response.writeInt(2); // 1 for real user, 2 for pet, 3 for bot
            response.writeInt(habbo.getPetId());
            response.writeInt(habbo.getId());
            response.writeString(habbo.getUsername());
            response.writeInt(1);
            response.writeBool(false);
            response.writeBool(false);
            response.writeInt(0);
            response.writeInt(0);
            response.writeString("");
        }
    }

    public String PetFigureForType(int Type)
    {
        Random random = new Random();

        switch (Type)
        {
            default:
            case 60:
            {
                int randomNumber = random.nextInt(4 - 1 + 1) + 1;
                switch (randomNumber)
                {
                    default:
                    case 1:
                        return "0 0 f08b90 2 2 -1 1 3 -1 1";
                    case 2:
                        return "0 15 ffffff 2 2 -1 0 3 -1 0";
                    case 3:
                        return "0 20 d98961 2 2 -1 0 3 -1 0";
                    case 4:
                        return "0 21 da9dbd 2 2 -1 0 3 -1 0";
                }
            }

            case 1:
            {
                int randomNumber = random.nextInt(4 - 1 + 1) + 1;
                switch (randomNumber)
                {
                    default:
                    case 1:
                        return "1 18 d5b35f 2 2 -1 0 3 -1 0";
                    case 2:
                        return "1 0 ff7b3a 2 2 -1 0 3 -1 0";
                    case 3:
                        return "1 18 d98961 2 2 -1 0 3 -1 0";
                    case 4:
                        return "1 0 ff7b3a 2 2 -1 0 3 -1 1";
                    case 5:
                        return "1 24 d5b35f 2 2 -1 0 3 -1 0";
                }
            }

            case 2:
            {
                int randomNumber = random.nextInt(6 - 1 + 1) + 1;
                switch (randomNumber)
                {
                    default:
                    case 1:
                        return "3 3 eeeeee 2 2 -1 0 3 -1 0";
                    case 2:
                        return "3 0 ffffff 2 2 -1 0 3 -1 0";
                    case 3:
                        return "3 5 eeeeee 2 2 -1 0 3 -1 0";
                    case 4:
                        return "3 6 eeeeee 2 2 -1 0 3 -1 0";
                    case 5:
                        return "3 4 dddddd 2 2 -1 0 3 -1 0";
                    case 6:
                        return "3 5 dddddd 2 2 -1 0 3 -1 0";
                }
            }

            case 3:
            {
                int randomNumber = random.nextInt(5 - 1 + 1) + 1;
                switch (randomNumber)
                {
                    default:
                    case 1:
                        return "2 10 84ce84 2 2 -1 0 3 -1 0";
                    case 2:
                        return "2 8 838851 2 2 0 0 3 -1 0";
                    case 3:
                        return "2 11 b99105 2 2 -1 0 3 -1 0";
                    case 4:
                        return "2 3 e8ce25 2 2 -1 0 3 -1 0";
                    case 5:
                        return "2 2 fcfad3 2 2 -1 0 3 -1 0";
                }
            }

            case 4:
            {
                int randomNumber = random.nextInt(4 - 1 + 1) + 1;
                switch (randomNumber)
                {
                    default:
                    case 1:
                        return "4 2 e4feff 2 2 -1 0 3 -1 0";
                    case 2:
                        return "4 3 e4feff 2 2 -1 0 3 -1 0";
                    case 3:
                        return "4 1 eaeddf 2 2 -1 0 3 -1 0";
                    case 4:
                        return "4 0 ffffff 2 2 -1 0 3 -1 0";
                }
            }

            case 5:
            {
                int randomNumber = random.nextInt(7 - 1 + 1) + 1;
                switch (randomNumber)
                {
                    default:
                    case 1:
                        return "5 2 ffffff 2 2 -1 0 3 -1 0";
                    case 2:
                        return "5 0 ffffff 2 2 -1 0 3 -1 0";
                    case 3:
                        return "5 3 ffffff 2 2 -1 0 3 -1 0";
                    case 4:
                        return "5 5 ffffff 2 2 -1 0 3 -1 0";
                    case 5:
                        return "5 7 ffffff 2 2 -1 0 3 -1 0";
                    case 6:
                        return "5 1 ffffff 2 2 -1 0 3 -1 0";
                    case 7:
                        return "5 8 ffffff 2 2 -1 0 3 -1 0";
                }
            }

            case 6:
            {
                int randomNumber = random.nextInt(11 - 1 + 1) + 1;
                switch (randomNumber)
                {
                    default:
                    case 1:
                        return "6 0 ffffff 2 2 -1 0 3 -1 0";
                    case 2:
                        return "6 1 ffffff 2 2 -1 0 3 -1 0";
                    case 3:
                        return "6 2 ffffff 2 2 -1 0 3 -1 0";
                    case 4:
                        return "6 3 ffffff 2 2 -1 0 3 -1 0";
                    case 5:
                        return "6 4 ffffff 2 2 -1 0 3 -1 0";
                    case 6:
                        return "6 0 ffd8c9 2 2 -1 0 3 -1 0";
                    case 7:
                        return "6 5 ffffff 2 2 -1 0 3 -1 0";
                    case 8:
                        return "6 11 ffffff 2 2 -1 0 3 -1 0";
                    case 9:
                        return "6 2 ffe49d 2 2 -1 0 3 -1 0";
                    case 10:
                        return "6 11 ff9ae 2 2 -1 0 3 -1 0";
                    case 11:
                        return "6 2 ff9ae 2 2 -1 0 3 -1 0";
                }
            }

            case 7:
            {
                int randomNumber = random.nextInt(7 - 1 + 1) + 1;
                switch (randomNumber)
                {
                    default:
                    case 1:
                        return "7 5 aeaeae 2 2 -1 0 3 -1 0";
                    case 2:
                        return "7 7 ffc99a 2 2 -1 0 3 -1 0";
                    case 3:
                        return "7 5 cccccc 2 2 -1 0 3 -1 0";
                    case 4:
                        return "7 5 9adcff 2 2 -1 0 3 -1 0";
                    case 5:
                        return "7 5 ff7d6a 2 2 -1 0 3 -1 0";
                    case 6:
                        return "7 6 cccccc 2 2 -1 0 3 -1 0";
                    case 7:
                        return "7 0 cccccc 2 2 -1 0 3 -1 0";
                }
            }

            case 8:
            {
                int randomNumber = random.nextInt(13 - 1 + 1) + 1;
                switch (randomNumber)
                {
                    default:
                    case 1:
                        return "8 0 ffffff 2 2 -1 0 3 -1 0";
                    case 2:
                        return "8 1 ffffff 2 2 -1 0 3 -1 0";
                    case 3:
                        return "8 2 ffffff 2 2 -1 0 3 -1 0";
                    case 4:
                        return "8 3 ffffff 2 2 -1 0 3 -1 0";
                    case 5:
                        return "8 4 ffffff 2 2 -1 0 3 -1 0";
                    case 6:
                        return "8 14 ffffff 2 2 -1 0 3 -1 0";
                    case 7:
                        return "8 11 ffffff 2 2 -1 0 3 -1 0";
                    case 8:
                        return "8 8 ffffff 2 2 -1 0 3 -1 0";
                    case 9:
                        return "8 6 ffffff 2 2 -1 0 3 -1 0";
                    case 10:
                        return "8 5 ffffff 2 2 -1 0 3 -1 0";
                    case 11:
                        return "8 9 ffffff 2 2 -1 0 3 -1 0";
                    case 12:
                        return "8 10 ffffff 2 2 -1 0 3 -1 0";
                    case 13:
                        return "8 7 ffffff 2 2 -1 0 3 -1 0";
                }
            }

            case 9:
            {
                int randomNumber = random.nextInt(9 - 1 + 1) + 1;
                switch (randomNumber)
                {
                    default:
                    case 1:
                        return "9 0 ffffff 2 2 -1 0 3 -1 0";
                    case 2:
                        return "9 1 ffffff 2 2 -1 0 3 -1 0";
                    case 3:
                        return "9 2 ffffff 2 2 -1 0 3 -1 0";
                    case 4:
                        return "9 3 ffffff 2 2 -1 0 3 -1 0";
                    case 5:
                        return "9 4 ffffff 2 2 -1 0 3 -1 0";
                    case 6:
                        return "9 5 ffffff 2 2 -1 0 3 -1 0";
                    case 7:
                        return "9 6 ffffff 2 2 -1 0 3 -1 0";
                    case 8:
                        return "9 7 ffffff 2 2 -1 0 3 -1 0";
                    case 9:
                        return "9 8 ffffff 2 2 -1 0 3 -1 0";
                }
            }

            case 10:
            {
                return "10 0 ffffff 2 2 -1 0 3 -1 0";

            }

            case 11:
            {
                int randomNumber = random.nextInt(13 - 1 + 1) + 1;
                switch (randomNumber)
                {
                    default:
                    case 1:
                        return "11 1 ffffff 2 2 -1 0 3 -1 0";
                    case 2:
                        return "11 2 ffffff 2 2 -1 0 3 -1 0";
                    case 3:
                        return "11 3 ffffff 2 2 -1 0 3 -1 0";
                    case 4:
                        return "11 4 ffffff 2 2 -1 0 3 -1 0";
                    case 5:
                        return "11 5 ffffff 2 2 -1 0 3 -1 0";
                    case 6:
                        return "11 9 ffffff 2 2 -1 0 3 -1 0";
                    case 7:
                        return "11 10 ffffff 2 2 -1 0 3 -1 0";
                    case 8:
                        return "11 6 ffffff 2 2 -1 0 3 -1 0";
                    case 9:
                        return "11 12 ffffff 2 2 -1 0 3 -1 0";
                    case 10:
                        return "11 11 ffffff 2 2 -1 0 3 -1 0";
                    case 11:
                        return "11 15 ffffff 2 2 -1 0 3 -1 0";
                    case 12:
                        return "11 13 ffffff 2 2 -1 0 3 -1 0";
                    case 13:
                        return "11 18 ffffff 2 2 -1 0 3 -1 0";
                }
            }

            case 12:
            {
                int randomNumber = random.nextInt(6 - 1 + 1) + 1;
                switch (randomNumber)
                {
                    default:
                    case 1:
                        return "12 0 ffffff 2 2 -1 0 3 -1 0";
                    case 2:
                        return "12 1 ffffff 2 2 -1 0 3 -1 0";
                    case 3:
                        return "12 2 ffffff 2 2 -1 0 3 -1 0";
                    case 4:
                        return "12 3 ffffff 2 2 -1 0 3 -1 0";
                    case 5:
                        return "12 4 ffffff 2 2 -1 0 3 -1 0";
                    case 6:
                        return "12 5 ffffff 2 2 -1 0 3 -1 0";
                }
            }

            case 14:
            {
                int randomNumber = random.nextInt(14 - 1 + 1) + 1;
                switch (randomNumber)
                {
                    default:
                    case 1:
                        return "14 0 ffffff 2 2 -1 0 3 -1 0";
                    case 2:
                        return "14 1 ffffff 2 2 -1 0 3 -1 0";
                    case 3:
                        return "14 2 ffffff 2 2 -1 0 3 -1 0";
                    case 4:
                        return "14 3 ffffff 2 2 -1 0 3 -1 0";
                    case 5:
                        return "14 6 ffffff 2 2 -1 0 3 -1 0";
                    case 6:
                        return "14 4 ffffff 2 2 -1 0 3 -1 0";
                    case 7:
                        return "14 5 ffffff 2 2 -1 0 3 -1 0";
                    case 8:
                        return "14 7 ffffff 2 2 -1 0 3 -1 0";
                    case 9:
                        return "14 8 ffffff 2 2 -1 0 3 -1 0";
                    case 10:
                        return "14 9 ffffff 2 2 -1 0 3 -1 0";
                    case 11:
                        return "14 10 ffffff 2 2 -1 0 3 -1 0";
                    case 12:
                        return "14 11 ffffff 2 2 -1 0 3 -1 0";
                    case 13:
                        return "14 12 ffffff 2 2 -1 0 3 -1 0";
                    case 14:
                        return "14 13 ffffff 2 2 -1 0 3 -1 0";
                }
            }

            case 15:
            {
                int randomNumber = random.nextInt(20 - 1 + 1) + 1;
                switch (randomNumber)
                {
                    default:
                    case 1:
                        return "15 2 ffffff 2 2 -1 0 3 -1 0";
                    case 2:
                        return "15 3 ffffff 2 2 -1 0 3 -1 0";
                    case 3:
                        return "15 4 ffffff 2 2 -1 0 3 -1 0";
                    case 4:
                        return "15 5 ffffff 2 2 -1 0 3 -1 0";
                    case 5:
                        return "15 6 ffffff 2 2 -1 0 3 -1 0";
                    case 6:
                        return "15 7 ffffff 2 2 -1 0 3 -1 0";
                    case 7:
                        return "15 8 ffffff 2 2 -1 0 3 -1 0";
                    case 8:
                        return "15 9 ffffff 2 2 -1 0 3 -1 0";
                    case 9:
                        return "15 10 ffffff 2 2 -1 0 3 -1 0";
                    case 10:
                        return "15 11 ffffff 2 2 -1 0 3 -1 0";
                    case 11:
                        return "15 12 ffffff 2 2 -1 0 3 -1 0";
                    case 12:
                        return "15 13 ffffff 2 2 -1 0 3 -1 0";
                    case 13:
                        return "15 14 ffffff 2 2 -1 0 3 -1 0";
                    case 14:
                        return "15 15 ffffff 2 2 -1 0 3 -1 0";
                    case 15:
                        return "15 16 ffffff 2 2 -1 0 3 -1 0";
                    case 16:
                        return "15 17 ffffff 2 2 -1 0 3 -1 0";
                    case 17:
                        return "15 78 ffffff 2 2 -1 0 3 -1 0";
                    case 18:
                        return "15 77 ffffff 2 2 -1 0 3 -1 0";
                    case 19:
                        return "15 79 ffffff 2 2 -1 0 3 -1 0";
                    case 20:
                        return "15 80 ffffff 2 2 -1 0 3 -1 0";
                }
            }

            case 17:
            {
                int randomNumber = random.nextInt(8 - 1 + 1) + 1;
                switch (randomNumber)
                {
                    default:
                    case 1:
                        return "17 1 ffffff";
                    case 2:
                        return "17 2 ffffff";
                    case 3:
                        return "17 3 ffffff";
                    case 4:
                        return "17 4 ffffff";
                    case 5:
                        return "17 5 ffffff";
                    case 6:
                        return "18 0 ffffff";
                    case 7:
                        return "19 0 ffffff";
                    case 8:
                        return "20 0 ffffff";
                }
            }

            case 21:
            {
                int randomNumber = random.nextInt(3 - 1 + 1) + 1;
                switch (randomNumber)
                {
                    default:
                    case 1:
                        return "21 0 ffffff";
                    case 2:
                        return "22 0 ffffff";
                }
            }

            case 23:
            {
                int randomNumber = random.nextInt(3 - 1 + 1) + 1;
                switch (randomNumber)
                {
                    default:
                    case 1:
                        return "23 0 ffffff";
                    case 2:
                        return "23 1 ffffff";
                    case 3:
                        return "23 3 ffffff";
                }
            }

            case 26:
            {
                int randomNumber = random.nextInt(4 - 1 + 1) + 1;
                switch (randomNumber)
                {
                    default:
                    case 1:
                        return "26 1 ffffff 5 0 -1 0 4 402 5 3 301 4 1 101 2 2 201 3";
                    case 2:
                        return "26 1 ffffff 5 0 -1 0 1 102 13 3 301 4 4 401 5 2 201 3";
                    case 3:
                        return "26 6 ffffff 5 1 102 8 2 201 16 4 401 9 3 303 4 0 -1 6";
                    case 4:
                        return "26 30 ffffff 5 0 -1 0 3 303 4 4 401 5 1 101 2 2 201 3";
                }
            }
        }
    }
}
