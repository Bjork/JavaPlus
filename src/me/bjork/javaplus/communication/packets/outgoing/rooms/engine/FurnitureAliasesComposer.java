package me.bjork.javaplus.communication.packets.outgoing.rooms.engine;

import me.bjork.javaplus.communication.packets.outgoing.ServerPacketHeader;
import me.bjork.javaplus.communication.packets.parsers.OutgoingMessageComposer;

/**
 * Created on 24/05/2017.
 */
public class FurnitureAliasesComposer extends OutgoingMessageComposer {
    @Override
    public void write() {
        response.init(ServerPacketHeader.FurnitureAliasesMessageComposer);
        response.writeInt(0);
    }
}
