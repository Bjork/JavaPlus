package me.bjork.javaplus.communication.packets.outgoing.rooms.session;

import me.bjork.javaplus.communication.packets.outgoing.ServerPacketHeader;
import me.bjork.javaplus.communication.packets.parsers.OutgoingMessageComposer;

/**
 * Created on 24/05/2017.
 */
public class CantConnectComposer extends OutgoingMessageComposer {
    private int error;

    public CantConnectComposer(int error) {
        this.error = error;
    }

    @Override
    public void write() {
        response.init(ServerPacketHeader.CantConnectMessageComposer);
        response.writeInt(error);
    }
}
