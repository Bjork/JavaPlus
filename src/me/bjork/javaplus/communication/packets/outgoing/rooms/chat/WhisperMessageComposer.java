package me.bjork.javaplus.communication.packets.outgoing.rooms.chat;

import me.bjork.javaplus.communication.packets.outgoing.ServerPacketHeader;
import me.bjork.javaplus.communication.packets.parsers.OutgoingMessageComposer;
import me.bjork.javaplus.habbohotel.rooms.types.misc.ChatEmotion;

/**
 * Created on 31/05/2017.
 */
public class WhisperMessageComposer extends OutgoingMessageComposer {
    private int virtualId;
    private String message;
    private ChatEmotion chatEmotion;
    private int colour;

    public WhisperMessageComposer(int virtualId, String safeMessage, ChatEmotion emotion, int colour) {
        this.virtualId = virtualId;
        this.message = safeMessage;
        this.chatEmotion = emotion;
        this.colour = colour;
    }

    public WhisperMessageComposer(int virtualId, String message) {
        this(virtualId, message, ChatEmotion.NONE, 0);
    }

    @Override
    public void write() {
        response.init(ServerPacketHeader.WhisperMessageComposer);
        response.writeInt(virtualId);
        response.writeString(message);
        response.writeInt(chatEmotion.getEmotionId());
        response.writeInt(colour);
        response.writeInt(0);
        response.writeInt(-1);
    }
}
