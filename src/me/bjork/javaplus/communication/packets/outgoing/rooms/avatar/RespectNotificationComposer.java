package me.bjork.javaplus.communication.packets.outgoing.rooms.avatar;

import me.bjork.javaplus.communication.packets.outgoing.ServerPacketHeader;
import me.bjork.javaplus.communication.packets.parsers.OutgoingMessageComposer;

/**
 * Created on 07/06/2017.
 */
public class RespectNotificationComposer extends OutgoingMessageComposer {
    private int virtualId;
    private int respects;

    public RespectNotificationComposer(int virtualId, int respects) {
        this.virtualId = virtualId;
        this.respects = respects;
    }

    @Override
    public void write() {
        response.init(ServerPacketHeader.RespectNotificationMessageComposer);
        response.writeInt(virtualId);
        response.writeInt(respects);
    }
}
