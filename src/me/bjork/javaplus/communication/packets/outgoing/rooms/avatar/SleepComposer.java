package me.bjork.javaplus.communication.packets.outgoing.rooms.avatar;

import me.bjork.javaplus.communication.packets.outgoing.ServerPacketHeader;
import me.bjork.javaplus.communication.packets.parsers.OutgoingMessageComposer;

/**
 * Created on 26/05/2017.
 */
public class SleepComposer extends OutgoingMessageComposer {
    private int virtualId;
    private boolean isSleeping;

    public SleepComposer(int virtualId, boolean isSleeping) {
        this.virtualId = virtualId;
        this.isSleeping = isSleeping;
    }

    @Override
    public void write() {
        response.init(ServerPacketHeader.SleepMessageComposer);
        response.writeInt(virtualId);
        response.writeBool(isSleeping);
    }
}
