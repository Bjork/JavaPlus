package me.bjork.javaplus.communication.packets.outgoing.rooms.avatar;

import me.bjork.javaplus.communication.packets.outgoing.ServerPacketHeader;
import me.bjork.javaplus.communication.packets.parsers.OutgoingMessageComposer;

/**
 * Created on 01/06/2017.
 */
public class DanceComposer extends OutgoingMessageComposer {
    private int virtualId;
    private int danceId;

    public DanceComposer(int virtualId, int danceId) {
        this.virtualId = virtualId;
        this.danceId = danceId;
    }

    @Override
    public void write() {
        response.init(ServerPacketHeader.DanceMessageComposer);
        response.writeInt(virtualId);
        response.writeInt(danceId);
    }
}
