package me.bjork.javaplus.communication.packets.outgoing.rooms.engine;

import me.bjork.javaplus.communication.packets.outgoing.ServerPacketHeader;
import me.bjork.javaplus.communication.packets.parsers.OutgoingMessageComposer;

/**
 * Created on 24/05/2017.
 */
public class RoomPropertyComposer extends OutgoingMessageComposer {
    private String name;
    private String value;

    public RoomPropertyComposer(String name, String value) {
        this.name = name;
        this.value = value;
    }

    @Override
    public void write() {
        response.init(ServerPacketHeader.RoomPropertyMessageComposer);
        response.writeString(name);
        response.writeString(value);
    }
}
