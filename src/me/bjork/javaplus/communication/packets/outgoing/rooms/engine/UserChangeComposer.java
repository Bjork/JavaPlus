package me.bjork.javaplus.communication.packets.outgoing.rooms.engine;

import me.bjork.javaplus.communication.packets.outgoing.ServerPacketHeader;
import me.bjork.javaplus.communication.packets.parsers.OutgoingMessageComposer;
import me.bjork.javaplus.habbohotel.rooms.RoomUser;

/**
 * Created on 03/06/2017.
 */
public class UserChangeComposer extends OutgoingMessageComposer {
    private RoomUser roomUser;
    private boolean self;

    public UserChangeComposer(RoomUser roomUser, boolean self) {
        this.roomUser = roomUser;
        this.self = self;
    }

    @Override
    public void write() {
        response.init(ServerPacketHeader.UserChangeMessageComposer);
        response.writeInt((self) ? -1 : roomUser.getVirtualId());
        response.writeString(roomUser.getClient().getHabbo().getLook());
        response.writeString(roomUser.getClient().getHabbo().getGender());
        response.writeString(roomUser.getClient().getHabbo().getMotto());
        response.writeInt(roomUser.getClient().getHabbo().getStats().getAchievementPoints());
    }
}
