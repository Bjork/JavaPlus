package me.bjork.javaplus.communication.packets.outgoing.rooms.permissions;

import me.bjork.javaplus.communication.packets.outgoing.ServerPacketHeader;
import me.bjork.javaplus.communication.packets.parsers.OutgoingMessageComposer;

public class YouAreOwnerMessageComposer extends OutgoingMessageComposer {
	@Override
	public void write() {
		response.init(ServerPacketHeader.YouAreOwnerMessageComposer);
	}
}
