package me.bjork.javaplus.communication.packets.outgoing.rooms.engine;

import me.bjork.javaplus.communication.packets.outgoing.ServerPacketHeader;
import me.bjork.javaplus.communication.packets.parsers.OutgoingMessageComposer;
import me.bjork.javaplus.habbohotel.rooms.Room;

/**
 * Created on 24/05/2017.
 */
public class FloorHeightMapComposer extends OutgoingMessageComposer {
    private Room room;

    public FloorHeightMapComposer(Room room) {
        this.room = room;
    }

    @Override
    public void write() {
        response.init(ServerPacketHeader.FloorHeightMapMessageComposer);
        response.writeBool(true);
        response.writeInt(this.room.getModel().getWallHeight());
        response.writeString(this.room.getModel().getRelativeMap());
    }
}
