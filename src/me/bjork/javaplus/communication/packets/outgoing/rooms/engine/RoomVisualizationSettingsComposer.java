package me.bjork.javaplus.communication.packets.outgoing.rooms.engine;

import me.bjork.javaplus.communication.packets.outgoing.ServerPacketHeader;
import me.bjork.javaplus.communication.packets.parsers.OutgoingMessageComposer;

/**
 * Created on 24/05/2017.
 */
public class RoomVisualizationSettingsComposer extends OutgoingMessageComposer {
    private boolean hideWalls;
    private int wallThickness;
    private int floorThickness;

    public RoomVisualizationSettingsComposer(int wallThickness, int floorThickness, Boolean hideWalls) {
        this.hideWalls = hideWalls;
        this.wallThickness = wallThickness;
        this.floorThickness = floorThickness;
    }

    @Override
    public void write() {
        response.init(ServerPacketHeader.RoomVisualizationSettingsMessageComposer);
        response.writeBool(hideWalls);
        response.writeInt(wallThickness);
        response.writeInt(floorThickness);
    }
}
