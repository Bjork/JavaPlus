package me.bjork.javaplus.communication.packets.outgoing.rooms.settings;

import me.bjork.javaplus.communication.packets.outgoing.ServerPacketHeader;
import me.bjork.javaplus.communication.packets.parsers.OutgoingMessageComposer;

/**
 * Created on 03/06/2017.
 */
public class RoomRatingMessageComposer extends OutgoingMessageComposer {
    private int score;
    private boolean canRateRoom;

    public RoomRatingMessageComposer(int score, boolean canRateRoom) {
        this.score = score;
        this.canRateRoom = canRateRoom;
    }

    @Override
    public void write() {
        response.init(ServerPacketHeader.RoomRatingMessageComposer);
        response.writeInt(score);
        response.writeBool(canRateRoom);
    }
}
