package me.bjork.javaplus.communication.packets.outgoing.rooms.settings;

import me.bjork.javaplus.communication.packets.outgoing.ServerPacketHeader;
import me.bjork.javaplus.communication.packets.parsers.OutgoingMessageComposer;

/**
 * Created on 24/05/2017.
 */
public class RoomRatingComposer extends OutgoingMessageComposer {
    private int score;
    private boolean canVote;

    public RoomRatingComposer(int score, boolean canVote) {
        this.score = score;
        this.canVote = canVote;
    }

    @Override
    public void write() {
        response.init(ServerPacketHeader.RoomRatingMessageComposer);
        response.writeInt(score);
        response.writeBool(canVote);
    }
}
