package me.bjork.javaplus.communication.packets.outgoing.rooms.avatar;

import me.bjork.javaplus.communication.packets.outgoing.ServerPacketHeader;
import me.bjork.javaplus.communication.packets.parsers.OutgoingMessageComposer;

/**
 * Created on 01/06/2017.
 */
public class ActionComposer extends OutgoingMessageComposer {
    private int virtualId;
    private int actionId;

    public ActionComposer(int virtualId, int actionId) {
        this.virtualId = virtualId;
        this.actionId = actionId;
    }

    @Override
    public void write() {
        response.init(ServerPacketHeader.ActionMessageComposer);
        response.writeInt(virtualId);
        response.writeInt(actionId);
    }
}
