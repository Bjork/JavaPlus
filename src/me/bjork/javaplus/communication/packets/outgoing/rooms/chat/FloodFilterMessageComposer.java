package me.bjork.javaplus.communication.packets.outgoing.rooms.chat;

import me.bjork.javaplus.communication.packets.outgoing.ServerPacketHeader;
import me.bjork.javaplus.communication.packets.parsers.OutgoingMessageComposer;

public class FloodFilterMessageComposer extends OutgoingMessageComposer {	
	private int floodTime;

	public FloodFilterMessageComposer(int floodTime) {
		this.floodTime = floodTime;
	}

	@Override
	public void write() {
		response.init(ServerPacketHeader.FloodControlMessageComposer);
		response.writeInt(floodTime);
	}
}
