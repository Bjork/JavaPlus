package me.bjork.javaplus.communication.packets.outgoing.rooms.access;

import me.bjork.javaplus.communication.packets.outgoing.ServerPacketHeader;
import me.bjork.javaplus.communication.packets.parsers.OutgoingMessageComposer;

/**
 * Created on 01/06/2017.
 */
public class DoorbellAcceptedComposer extends OutgoingMessageComposer {
    @Override
    public void write() {
        response.init(ServerPacketHeader.FlatAccessibleMessageComposer);
        response.writeString("");
    }
}
