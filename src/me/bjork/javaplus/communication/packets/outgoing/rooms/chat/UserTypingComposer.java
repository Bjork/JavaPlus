package me.bjork.javaplus.communication.packets.outgoing.rooms.chat;

import me.bjork.javaplus.communication.packets.outgoing.ServerPacketHeader;
import me.bjork.javaplus.communication.packets.parsers.OutgoingMessageComposer;

/**
 * Created on 29/05/2017.
 */
public class UserTypingComposer extends OutgoingMessageComposer {
    private int virtualId;
    private boolean isTyping;

    public UserTypingComposer(int virtualId, boolean isTyping) {
        this.virtualId = virtualId;
        this.isTyping = isTyping;
    }

    @Override
    public void write() {
        response.init(ServerPacketHeader.UserTypingMessageComposer);
        response.writeInt(virtualId);
        response.writeInt(isTyping ? 1 : 0);
    }
}
