package me.bjork.javaplus.communication.packets.outgoing.rooms.engine;

import me.bjork.javaplus.communication.packets.outgoing.ServerPacketHeader;
import me.bjork.javaplus.communication.packets.parsers.OutgoingMessageComposer;
import me.bjork.javaplus.habbohotel.rooms.RoomUser;
import me.bjork.javaplus.habbohotel.rooms.objects.users.RoomUserStatus;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Created on 25/05/2017.
 */
public class UserStatusMessageComposer extends OutgoingMessageComposer {
    private List<RoomUser> roomUsers;

    public UserStatusMessageComposer(RoomUser roomUser) {
        this(Arrays.asList(new RoomUser[] { roomUser }));
    }

    public UserStatusMessageComposer(List<RoomUser> roomUsers) {
        this.roomUsers = roomUsers;
    }

    @Override
    public void write() {
        response.init(ServerPacketHeader.UserUpdateMessageComposer);
        synchronized (this.roomUsers) {
            response.writeInt(this.roomUsers.size());
            for (RoomUser user : this.roomUsers) {
                response.writeInt(user.getVirtualId());
                response.writeInt(user.getPreviousLocation().x);
                response.writeInt(user.getPreviousLocation().y);
                response.writeString(user.getPreviousLocation().getStackHeight() + "");
                response.writeInt(user.getHeadRotation());
                response.writeInt(user.getBodyRotation());
                String status = "/";
                for (Map.Entry<RoomUserStatus, String> set : user.getStatuses().entrySet()) {
                    status += set.getKey().getStatusCode() + " " + set.getValue() + "//";
                }
                response.writeString(status);
                user.setPreviousLocation(user.getCurrentLocation());
                user.getPreviousLocation().setStackHeight(user.getZ());
            }
        }
    }
}
