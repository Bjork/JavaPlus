package me.bjork.javaplus.communication.packets.outgoing.rooms.session;

import me.bjork.javaplus.communication.packets.outgoing.ServerPacketHeader;
import me.bjork.javaplus.communication.packets.parsers.OutgoingMessageComposer;

/**
 * Created on 24/05/2017.
 */
public class RoomReadyComposer extends OutgoingMessageComposer {
    private int roomId;
    private String modelName;

    public RoomReadyComposer(int roomId, String modelName) {
        this.roomId = roomId;
        this.modelName = modelName;
    }

    @Override
    public void write() {
        response.init(ServerPacketHeader.RoomReadyMessageComposer);
        response.writeString(modelName);
        response.writeInt(roomId);
    }
}
