package me.bjork.javaplus.communication.packets.outgoing.rooms.engine;

import me.bjork.javaplus.communication.packets.outgoing.ServerPacketHeader;
import me.bjork.javaplus.communication.packets.parsers.OutgoingMessageComposer;

/**
 * Created on 24/05/2017.
 */
public class RoomEntryInfoComposer extends OutgoingMessageComposer {
    private int roomId;
    private boolean isOwner;

    public RoomEntryInfoComposer(int roomId, boolean isOwner) {
        this.roomId = roomId;
        this.isOwner = isOwner;
    }

    @Override
    public void write() {
        response.init(ServerPacketHeader.RoomEntryInfoMessageComposer);
        response.writeInt(roomId);
        response.writeBool(isOwner);
    }
}
