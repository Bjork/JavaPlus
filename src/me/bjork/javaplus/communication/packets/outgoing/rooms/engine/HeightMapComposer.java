package me.bjork.javaplus.communication.packets.outgoing.rooms.engine;

import me.bjork.javaplus.communication.packets.outgoing.ServerPacketHeader;
import me.bjork.javaplus.communication.packets.parsers.OutgoingMessageComposer;
import me.bjork.javaplus.habbohotel.rooms.Room;
import me.bjork.javaplus.habbohotel.rooms.models.types.tiles.RoomTile;

/**
 * Created on 24/05/2017.
 */
public class HeightMapComposer extends OutgoingMessageComposer {
    private Room room;

    public HeightMapComposer(Room room) {
        this.room = room;
    }

    @Override
    public void write() {

        response.init(ServerPacketHeader.HeightMapMessageComposer);
        response.writeInt(room.getModel().getMapSizeX());
        response.writeInt(room.getModel().getMapSizeX() * room.getModel().getMapSizeY());
        for (short y = 0; y < room.getModel().getMapSizeY(); y++)
        {
            for (short x = 0; x < room.getModel().getMapSizeX(); x++)
            {
                RoomTile t = room.getModel().getTile(x, y);

                if (t != null)
                    response.writeShort(t.relativeHeight());
                else
                    response.writeShort(Short.MAX_VALUE);

            }
        }
    }
}
