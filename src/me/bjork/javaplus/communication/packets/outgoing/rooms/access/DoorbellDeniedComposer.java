package me.bjork.javaplus.communication.packets.outgoing.rooms.access;

import me.bjork.javaplus.communication.packets.outgoing.ServerPacketHeader;
import me.bjork.javaplus.communication.packets.parsers.OutgoingMessageComposer;

/**
 * Created on 24/05/2017.
 */
public class DoorbellDeniedComposer extends OutgoingMessageComposer {
    private String username;

    public DoorbellDeniedComposer(String username) {
        this.username = username;
    }

    @Override
    public void write() {
        response.init(ServerPacketHeader.FlatAccessDeniedMessageComposer);
        response.writeString(username);
    }
}
