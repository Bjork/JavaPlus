package me.bjork.javaplus.communication.packets.outgoing.generic;

import me.bjork.javaplus.communication.packets.outgoing.ServerPacketHeader;
import me.bjork.javaplus.communication.packets.parsers.OutgoingMessageComposer;

/**
 * Created on 24/05/2017.
 */
public class GenericErrorComposer extends OutgoingMessageComposer {

    public static final int AUTHENTICATION_FAILED = -3;
    public static final int CONNECTING_TO_THE_SERVER_FAILED = -400;
    public static final int KICKED_OUT_OF_THE_ROOM = 4008;
    public static final int NEED_TO_BE_VIP = 4009;
    public static final int ROOM_NAME_UNACCEPTABLE = 4010;
    public static final int CANNOT_BAN_GROUP_MEMBER = 4011;
    public static final int PASSWORD_INCORRECT = -100002;
    private final int errorCode;

    public GenericErrorComposer(int errorCode) {
        this.errorCode = errorCode;
    }

    @Override
    public void write() {
        response.init(ServerPacketHeader.GenericErrorMessageComposer);
        response.writeInt(errorCode);
    }
}
