package me.bjork.javaplus.communication.packets.outgoing.gamecenter;

import me.bjork.javaplus.communication.packets.outgoing.ServerPacketHeader;
import me.bjork.javaplus.communication.packets.parsers.OutgoingMessageComposer;

/**
 * Created on 22/05/2017.
 */
public class GameListComposer extends OutgoingMessageComposer {
    @Override
    public void write() {
        response.init(ServerPacketHeader.GameListMessageComposer);
        response.writeInt(0);
    }
}
