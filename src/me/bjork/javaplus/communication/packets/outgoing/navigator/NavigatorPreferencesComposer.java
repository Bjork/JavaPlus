package me.bjork.javaplus.communication.packets.outgoing.navigator;

import me.bjork.javaplus.communication.packets.outgoing.ServerPacketHeader;
import me.bjork.javaplus.communication.packets.parsers.OutgoingMessageComposer;

/**
 * Created on 22/05/2017.
 */
public class NavigatorPreferencesComposer extends OutgoingMessageComposer {
    @Override
    public void write() {
        response.init(ServerPacketHeader.NavigatorPreferencesMessageComposer);
        response.writeInt(68);
        response.writeInt(42);
        response.writeInt(425);
        response.writeInt(592);
        response.writeBool(false);
        response.writeInt(0);
    }
}
