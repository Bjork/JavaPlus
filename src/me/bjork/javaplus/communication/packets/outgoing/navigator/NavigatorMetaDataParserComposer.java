package me.bjork.javaplus.communication.packets.outgoing.navigator;

import me.bjork.javaplus.communication.packets.outgoing.ServerPacketHeader;
import me.bjork.javaplus.communication.packets.parsers.OutgoingMessageComposer;

/**
 * Created on 22/05/2017.
 */
public class NavigatorMetaDataParserComposer extends OutgoingMessageComposer {
    @Override
    public void write() {
        response.init(ServerPacketHeader.NavigatorMetaDataParserMessageComposer);
        response.writeInt(4);
        response.writeString("official_view");
        response.writeInt(0);
        response.writeString("hotel_view");
        response.writeInt(0);
        response.writeString("roomads_view");
        response.writeInt(0);
        response.writeString("myworld_view");
        response.writeInt(0);
    }
}
