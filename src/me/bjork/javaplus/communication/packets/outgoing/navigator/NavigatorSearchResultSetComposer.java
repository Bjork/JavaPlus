package me.bjork.javaplus.communication.packets.outgoing.navigator;

import me.bjork.javaplus.communication.packets.outgoing.ServerPacketHeader;
import me.bjork.javaplus.communication.packets.parsers.OutgoingMessageComposer;
import me.bjork.javaplus.habbohotel.gameclients.GameClient;
import me.bjork.javaplus.habbohotel.navigator.types.Category;
import me.bjork.javaplus.habbohotel.navigator.types.categories.NavigatorSearchAllowance;
import me.bjork.javaplus.habbohotel.navigator.types.categories.NavigatorViewMode;
import me.bjork.javaplus.habbohotel.navigator.types.search.NavigatorHandler;
import me.bjork.javaplus.habbohotel.rooms.RoomData;
import me.bjork.javaplus.habbohotel.rooms.types.RoomWriter;

import java.util.List;

/**
 * Created on 22/05/2017.
 */
public class NavigatorSearchResultSetComposer extends OutgoingMessageComposer {
    private String category;
    private String search;
    private List<Category> categoryList;
    private GameClient client;
    private int fetchLimit;

    public NavigatorSearchResultSetComposer(String category, String search, List<Category> categoryList, GameClient client, int fetchLimit) {
        this.category = category;
        this.search = search;
        this.categoryList = categoryList;
        this.client = client;
        this.fetchLimit = fetchLimit;
    }

    @Override
    public void write() {

        response.init(ServerPacketHeader.NavigatorSearchResultSetMessageComposer);
        response.writeString(category);
        response.writeString(search);

        response.writeInt(categoryList.size());
        for (Category category : this.categoryList) {
            response.writeString(category.getCategoryId());
            response.writeString(category.getPublicName());
            response.writeInt(NavigatorSearchAllowance.getIntValue(category.getSearchAllowance()));
            response.writeBool(false);
            response.writeInt(category.getViewMode() == NavigatorViewMode.REGULAR ? 0 : category.getViewMode() == NavigatorViewMode.THUMBNAIL ? 1 : 0);

            List<RoomData> rooms = NavigatorHandler.search(category, search, client, this.categoryList.size() == 1, fetchLimit);
            response.writeInt(rooms.size());
            for (RoomData roomData : rooms) {
                RoomWriter.write(roomData, response);
            }
            rooms.clear();
        }
        categoryList.clear();
        categoryList = null;
    }
}
