package me.bjork.javaplus.communication.packets.outgoing.navigator;

import me.bjork.javaplus.communication.packets.outgoing.ServerPacketHeader;
import me.bjork.javaplus.communication.packets.parsers.OutgoingMessageComposer;

/**
 * Created on 22/05/2017.
 */
public class UserFlatCatsComposer extends OutgoingMessageComposer {
    @Override
    public void write() {
        response.init(ServerPacketHeader.UserFlatCatsMessageComposer);
        response.writeInt(0); // Count
        /*
        foreach (SearchResultList Cat in Categories)
            {
                base.WriteInteger(Cat.Id);
                base.WriteString(Cat.PublicName);
                base.WriteBoolean(Cat.RequiredRank <= Rank);
                base.WriteBoolean(false);
                base.WriteString("");
                base.WriteString("");
                base.WriteBoolean(false);
            }
         */
    }
}
