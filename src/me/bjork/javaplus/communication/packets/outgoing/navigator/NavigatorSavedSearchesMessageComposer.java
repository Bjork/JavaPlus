package me.bjork.javaplus.communication.packets.outgoing.navigator;

import me.bjork.javaplus.communication.packets.outgoing.ServerPacketHeader;
import me.bjork.javaplus.communication.packets.parsers.OutgoingMessageComposer;

/**
 * Created on 22/05/2017.
 */
public class NavigatorSavedSearchesMessageComposer extends OutgoingMessageComposer {
    @Override
    public void write() {
        response.init(ServerPacketHeader.NavigatorSavedSearchesMessageComposer);
        response.writeInt(0); // Count
    }
}
