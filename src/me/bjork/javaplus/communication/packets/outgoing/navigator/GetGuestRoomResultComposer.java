package me.bjork.javaplus.communication.packets.outgoing.navigator;

import me.bjork.javaplus.communication.packets.outgoing.ServerPacketHeader;
import me.bjork.javaplus.communication.packets.parsers.OutgoingMessageComposer;
import me.bjork.javaplus.habbohotel.gameclients.GameClient;
import me.bjork.javaplus.habbohotel.rooms.RoomData;
import me.bjork.javaplus.habbohotel.rooms.types.RoomWriter;

/**
 * Created on 23/05/2017.
 */
public class GetGuestRoomResultComposer extends OutgoingMessageComposer {

    private GameClient client;
    private RoomData data;
    private Boolean isLoading;
    private Boolean checkEntry;

    public GetGuestRoomResultComposer(GameClient client, RoomData data, Boolean isLoading, Boolean checkEntry) {
        this.client = client;
        this.data = data;
        this.isLoading = isLoading;
        this.checkEntry = checkEntry;
    }

    @Override
    public void write() {
        response.init(ServerPacketHeader.GetGuestRoomResultMessageComposer);
        RoomWriter.entryData(data, response, isLoading, checkEntry);
    }
}
