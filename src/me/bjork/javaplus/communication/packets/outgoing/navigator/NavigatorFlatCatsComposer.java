package me.bjork.javaplus.communication.packets.outgoing.navigator;

import me.bjork.javaplus.communication.packets.outgoing.ServerPacketHeader;
import me.bjork.javaplus.communication.packets.parsers.OutgoingMessageComposer;

/**
 * Created on 22/05/2017.
 */
public class NavigatorFlatCatsComposer extends OutgoingMessageComposer {
    @Override
    public void write() {
        response.init(ServerPacketHeader.NavigatorFlatCatsMessageComposer);
        response.writeInt(0); //Count

        /*
        foreach (SearchResultList Category in Categories.ToList())
            {
                base.WriteInteger(Category.Id);
                base.WriteString(Category.PublicName);
                base.WriteBoolean(true);
            }
         */
    }
}
