package me.bjork.javaplus.communication.packets.outgoing.navigator;

import me.bjork.javaplus.communication.packets.outgoing.ServerPacketHeader;
import me.bjork.javaplus.communication.packets.parsers.OutgoingMessageComposer;

/**
 * Created on 22/05/2017.
 */
public class NavigatorSettingsComposer extends OutgoingMessageComposer {
    private final Integer roomId;

    public NavigatorSettingsComposer(final Integer roomId) {
        this.roomId = roomId;
    }
    @Override
    public void write() {
        response.init(ServerPacketHeader.NavigatorSettingsMessageComposer);
        response.writeInt(roomId);
        response.writeInt(0);
    }
}
