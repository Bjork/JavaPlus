package me.bjork.javaplus.communication.packets.outgoing.moderation;

import me.bjork.javaplus.communication.packets.outgoing.ServerPacketHeader;
import me.bjork.javaplus.communication.packets.parsers.OutgoingMessageComposer;

/**
 * Created on 29/05/2017.
 */
public class MutedMessageComposer extends OutgoingMessageComposer {
    private double timeMutedExpire;

    public MutedMessageComposer(double timeMutedExpire) {
        this.timeMutedExpire = timeMutedExpire;
    }

    @Override
    public void write() {
        response.init(ServerPacketHeader.MutedMessageComposer);
        response.writeInt((int)timeMutedExpire);
    }
}
