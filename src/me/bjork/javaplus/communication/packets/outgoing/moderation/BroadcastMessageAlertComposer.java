package me.bjork.javaplus.communication.packets.outgoing.moderation;

import me.bjork.javaplus.communication.packets.outgoing.ServerPacketHeader;
import me.bjork.javaplus.communication.packets.parsers.OutgoingMessageComposer;

/**
 * Created on 24/05/2017.
 */
public class BroadcastMessageAlertComposer extends OutgoingMessageComposer {
    private String message;
    private String url;

    public BroadcastMessageAlertComposer(String message, String url) {
        this.message = message;
        this.url = url;
    }

    @Override
    public void write() {
        response.init(ServerPacketHeader.BroadcastMessageAlertMessageComposer);
        response.writeString(message);
        response.writeString(url);
    }
}
