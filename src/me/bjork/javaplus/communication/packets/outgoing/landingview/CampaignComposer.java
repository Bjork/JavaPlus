package me.bjork.javaplus.communication.packets.outgoing.landingview;

import me.bjork.javaplus.communication.packets.outgoing.ServerPacketHeader;
import me.bjork.javaplus.communication.packets.parsers.OutgoingMessageComposer;

/**
 * Created on 22/05/2017.
 */
public class CampaignComposer extends OutgoingMessageComposer {
    private String parseCampaings;
    private String campaingName;

    public CampaignComposer(String parseCampaings, String campaingName) {
    this.parseCampaings = parseCampaings;
    this.campaingName = campaingName;
    }

    @Override
    public void write() {
        response.init(ServerPacketHeader.CampaignMessageComposer);
        response.writeString(parseCampaings);
        response.writeString(campaingName);
    }
}
