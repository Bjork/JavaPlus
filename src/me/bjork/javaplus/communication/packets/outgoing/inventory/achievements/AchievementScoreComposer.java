package me.bjork.javaplus.communication.packets.outgoing.inventory.achievements;

import me.bjork.javaplus.communication.packets.outgoing.ServerPacketHeader;
import me.bjork.javaplus.communication.packets.parsers.OutgoingMessageComposer;

/**
 * Created on 30/05/2017.
 */
public class AchievementScoreComposer extends OutgoingMessageComposer {
    private int achievementScore;

    public AchievementScoreComposer(int achievementScore) {
        this.achievementScore = achievementScore;
    }

    @Override
    public void write() {
        response.init(ServerPacketHeader.AchievementScoreMessageComposer);
        response.writeInt(achievementScore);
    }
}
