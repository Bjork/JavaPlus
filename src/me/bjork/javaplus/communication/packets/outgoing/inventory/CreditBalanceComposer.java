package me.bjork.javaplus.communication.packets.outgoing.inventory;

import me.bjork.javaplus.communication.packets.outgoing.ServerPacketHeader;
import me.bjork.javaplus.communication.packets.parsers.OutgoingMessageComposer;

/**
 * Created on 22/05/2017.
 */
public class CreditBalanceComposer extends OutgoingMessageComposer {
    private int credits;
    public CreditBalanceComposer(int credits) {
        this.credits = credits;
    }

    @Override
    public void write() {
        response.init(ServerPacketHeader.CreditBalanceMessageComposer);
        response.writeString(credits + ".0");
    }
}
