package me.bjork.javaplus.communication.packets.outgoing.inventory.purse;

import me.bjork.javaplus.communication.packets.outgoing.ServerPacketHeader;
import me.bjork.javaplus.communication.packets.parsers.OutgoingMessageComposer;

/**
 * Created on 08/06/2017.
 */
public class HabboActivityPointNotificationComposer extends OutgoingMessageComposer {
    private int amount;
    private int reward;
    private int type;

    public HabboActivityPointNotificationComposer(int amount, int reward, int type) {
        this.amount = amount;
        this.reward = reward;
        this.type = type;
    }

    @Override
    public void write() {
        response.init(ServerPacketHeader.HabboActivityPointNotificationMessageComposer);
        response.writeInt(amount);
        response.writeInt(reward);
        response.writeInt(type);
    }
}
