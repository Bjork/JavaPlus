package me.bjork.javaplus.communication.packets.outgoing.inventory;

import me.bjork.javaplus.communication.packets.outgoing.ServerPacketHeader;
import me.bjork.javaplus.communication.packets.parsers.OutgoingMessageComposer;
import me.bjork.javaplus.habbohotel.users.currencies.type.CurrencyType;

import java.util.Collection;

/**
 * Created on 22/05/2017.
 */
public class ActivityPointsComposer extends OutgoingMessageComposer {
    private Collection<CurrencyType> currencies;

    public ActivityPointsComposer(Collection<CurrencyType> currencies) {
        this.currencies = currencies;
    }

    @Override
    public void write() {
        response.init(ServerPacketHeader.ActivityPointsMessageComposer);
        response.writeInt(currencies.size());
        for (CurrencyType type : currencies) {
            response.writeInt(type.getType());
            response.writeInt(type.getAmount());
        }
    }
}
