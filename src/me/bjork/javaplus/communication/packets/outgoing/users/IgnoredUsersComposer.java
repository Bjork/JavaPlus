package me.bjork.javaplus.communication.packets.outgoing.users;

import me.bjork.javaplus.communication.packets.outgoing.ServerPacketHeader;
import me.bjork.javaplus.communication.packets.parsers.OutgoingMessageComposer;

import java.util.ArrayList;

/**
 * Created on 22/05/2017.
 */
public class IgnoredUsersComposer extends OutgoingMessageComposer {
    private ArrayList<String> users;
    public IgnoredUsersComposer(ArrayList<String> users) {
        this.users = users;
    }

    @Override
    public void write() {
        response.init(ServerPacketHeader.IgnoredUsersMessageComposer);
        response.writeInt(0); //Count
        // for avec response.writeString("username");
    }
}
