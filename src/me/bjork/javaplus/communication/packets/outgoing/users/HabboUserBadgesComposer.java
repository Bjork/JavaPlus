package me.bjork.javaplus.communication.packets.outgoing.users;

import me.bjork.javaplus.communication.packets.outgoing.ServerPacketHeader;
import me.bjork.javaplus.communication.packets.parsers.OutgoingMessageComposer;
import me.bjork.javaplus.habbohotel.users.inventory.Badge;

import java.util.ArrayList;

/**
 * Created on 30/05/2017.
 */
public class HabboUserBadgesComposer extends OutgoingMessageComposer {
    private final ArrayList<Badge> badges;
    private final int userId;

    public HabboUserBadgesComposer(ArrayList<Badge> badges, int userId) {
        this.badges = badges;
        this.userId = userId;
    }

    @Override
    public void write() {
        response.init(ServerPacketHeader.HabboUserBadgesMessageComposer);
        response.writeInt(this.userId);
        synchronized (this.badges) {
            response.writeInt(this.badges.size());

            for (Badge badge : this.badges) {
                response.writeInt(badge.getSlot());
                response.writeString(badge.getCode());
            }
        }
    }
}
