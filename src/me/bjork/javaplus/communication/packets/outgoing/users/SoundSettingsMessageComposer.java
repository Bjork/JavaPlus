package me.bjork.javaplus.communication.packets.outgoing.users;

import me.bjork.javaplus.communication.packets.outgoing.ServerPacketHeader;
import me.bjork.javaplus.communication.packets.parsers.OutgoingMessageComposer;
import me.bjork.javaplus.habbohotel.users.Habbo;
import me.bjork.javaplus.habbohotel.users.messenger.friendbar.FriendBarStateUtility;

/**
 * Created on 22/05/2017.
 */
public class SoundSettingsMessageComposer extends OutgoingMessageComposer {
    private Habbo habbo;

    public SoundSettingsMessageComposer(Habbo habbo) {
        this.habbo = habbo;
    }

    @Override
    public void write() {

        response.init(ServerPacketHeader.SoundSettingsMessageComposer);//TODO Sound Settings Composer
        for (int volume : habbo.getVolumes())
            response.writeInt(volume);
        response.writeBool(habbo.getChatpreference());
        response.writeBool(habbo.getIgnoreinvites());
        response.writeBool(habbo.getFocuspreference());
        response.writeInt(FriendBarStateUtility.getInt(habbo.getFriendBarState()));
        response.writeInt(0);
        response.writeInt(0);
    }
}
