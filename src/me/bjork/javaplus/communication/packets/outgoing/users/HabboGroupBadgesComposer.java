package me.bjork.javaplus.communication.packets.outgoing.users;

import me.bjork.javaplus.communication.packets.outgoing.ServerPacketHeader;
import me.bjork.javaplus.communication.packets.parsers.OutgoingMessageComposer;

/**
 * Created on 24/05/2017.
 */
public class HabboGroupBadgesComposer extends OutgoingMessageComposer {
    @Override
    public void write() {
        response.init(ServerPacketHeader.HabboGroupBadgesMessageComposer);
        response.writeInt(0);
    }
}
