package me.bjork.javaplus.communication.packets.outgoing.users;

import me.bjork.javaplus.communication.packets.outgoing.ServerPacketHeader;
import me.bjork.javaplus.communication.packets.parsers.OutgoingMessageComposer;
import me.bjork.javaplus.habbohotel.gameclients.GameClient;

/**
 * Created on 22/05/2017.
 */
public class UserClubComposer extends OutgoingMessageComposer {

    private GameClient client;

    public UserClubComposer(GameClient client) {
        this.client = client;
    }

    @Override
    public void write() {
        response.init(ServerPacketHeader.ScrSendUserInfoMessageComposer);
        response.writeString("habbo_club");
        response.writeInt(0);
        response.writeInt(1);
        response.writeInt(0);
        response.writeInt(-1);
        response.writeBool(true);
        response.writeBool(true);
        response.writeInt(0);
        response.writeInt(0);
        response.writeInt(0);
    }
}
