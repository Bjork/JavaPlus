package me.bjork.javaplus.habbohotel.gameclients;

import me.bjork.javaplus.JavaPlusEnvironment;
import me.bjork.javaplus.communication.connection.IPlayerNetwork;
import me.bjork.javaplus.communication.packets.incoming.handshake.UserRightsComposer;
import me.bjork.javaplus.communication.packets.outgoing.handshake.AuthenticationOKComposer;
import me.bjork.javaplus.communication.packets.outgoing.handshake.AvailabilityStatusComposer;
import me.bjork.javaplus.communication.packets.outgoing.inventory.achievements.AchievementScoreComposer;
import me.bjork.javaplus.communication.packets.outgoing.moderation.BroadcastMessageAlertComposer;
import me.bjork.javaplus.communication.packets.outgoing.navigator.NavigatorSettingsComposer;
import me.bjork.javaplus.communication.packets.outgoing.rooms.chat.WhisperMessageComposer;
import me.bjork.javaplus.communication.packets.parsers.OutgoingMessageComposer;
import me.bjork.javaplus.core.Logging;
import me.bjork.javaplus.core.settings.SettingsManager;
import me.bjork.javaplus.database.queries.players.HabboDao;
import me.bjork.javaplus.habbohotel.rooms.RoomUser;
import me.bjork.javaplus.habbohotel.users.Habbo;

/**
 * Created on 19/05/2017.
 */
public class GameClient {

    private IPlayerNetwork network;
    private String machineId = "";
    private Habbo habbo;
    private int lastPing;

    public GameClient(IPlayerNetwork network) {
        this.network = network;
    }

    public boolean tryAuthenticate(String ssoTicket) {
        try
        {
            habbo = HabboDao.getHabbo(ssoTicket);

            if (habbo != null) {
                GameClient cloneSession = JavaPlusEnvironment.getGame().getGameClientManager().getClientByUserID(habbo.getId());

                if (cloneSession != null) {
                    JavaPlusEnvironment.getGame().getGameClientManager().getClientByUserID(habbo.getId()).disconnect();
                    cloneSession.getNetwork().close();
                }

                JavaPlusEnvironment.getGame().getGameClientManager().put(this, habbo.getId(), habbo.getUsername());
                habbo.setClient(this);
                habbo.init();

                send(new AuthenticationOKComposer());
                //send(new AvatarEffectsComposer(_habbo.Effects().GetAllEffects));
                send(new NavigatorSettingsComposer(habbo.getHomeroom()));
                //send(new FavouritesComposer(userData.user.FavoriteRooms));
                //send(new FigureSetIdsComposer(_habbo.GetClothing().GetClothingParts));
                send(new UserRightsComposer(this));
                send(new AvailabilityStatusComposer());
                send(new AchievementScoreComposer(habbo.getStats().getAchievementPoints()));
                //send(new BuildersClubMembershipComposer());
                //send(new CfhTopicsInitComposer(PlusEnvironment.GetGame().GetModerationManager().UserActionPresets));
                //send(new BadgeDefinitionsComposer(PlusEnvironment.GetGame().GetAchievementManager()._achievements));
                //send(new SoundSettingsComposer(_habbo.ClientVolume, _habbo.ChatPreference, _habbo.AllowMessengerInvites, _habbo.FocusPreference, FriendBarStateUtility.GetInt(_habbo.FriendbarState)));
                //SendMessage(new TalentTrackLevelComposer());

                /*if (GetHabbo().GetMessenger() != null)
                    GetHabbo().GetMessenger().OnStatusChanged(true);

                if (!string.IsNullOrEmpty(MachineId))
                {
                    if (this._habbo.MachineId != MachineId)
                    {
                        using (IQueryAdapter dbClient = PlusEnvironment.GetDatabaseManager().GetQueryReactor())
                        {
                            dbClient.SetQuery("UPDATE `users` SET `machine_id` = @MachineId WHERE `id` = @id LIMIT 1");
                            dbClient.AddParameter("MachineId", MachineId);
                            dbClient.AddParameter("id", _habbo.Id);
                            dbClient.RunQuery();
                        }
                    }

                    _habbo.MachineId = MachineId;
                }

                PermissionGroup PermissionGroup = null;
                if (PlusEnvironment.GetGame().GetPermissionManager().TryGetGroup(_habbo.Rank, out PermissionGroup))
                {
                    if (!String.IsNullOrEmpty(PermissionGroup.Badge))
                        if (!_habbo.GetBadgeComponent().HasBadge(PermissionGroup.Badge))
                            _habbo.GetBadgeComponent().GiveBadge(PermissionGroup.Badge, true, this);
                }

                SubscriptionData SubData = null;
                if (PlusEnvironment.GetGame().GetSubscriptionManager().TryGetSubscriptionData(this._habbo.VIPRank, out SubData))
                {
                    if (!String.IsNullOrEmpty(SubData.Badge))
                    {
                        if (!_habbo.GetBadgeComponent().HasBadge(SubData.Badge))
                            _habbo.GetBadgeComponent().GiveBadge(SubData.Badge, true, this);
                    }
                }

                if (!PlusEnvironment.GetGame().GetCacheManager().ContainsUser(_habbo.Id))
                    PlusEnvironment.GetGame().GetCacheManager().GenerateUser(_habbo.Id);

                if (userData.user.GetPermissions().HasRight("mod_tickets"))
                {
                    SendPacket(new ModeratorInitComposer(
                            PlusEnvironment.GetGame().GetModerationManager().UserMessagePresets,
                            PlusEnvironment.GetGame().GetModerationManager().RoomMessagePresets,
                            PlusEnvironment.GetGame().GetModerationManager().GetTickets));
                }

                if (PlusEnvironment.GetSettingsManager().TryGetValue("user.login.message.enabled") == "1")
                    SendPacket(new MOTDNotificationComposer(PlusEnvironment.GetLanguageManager().TryGetValue("user.login.message")));

                if (PlusEnvironment.GetSettingsManager().TryGetValue("welcome.window.login.enabled") == "1")
                    SendPacket(new WelcomeAlertComposer(PlusEnvironment.GetSettingsManager().TryGetValue("welcome.window.location").ToString()));

                // Tried to understand how this works, follow the SanctionStatusComposer definition for more info
                //SendPacket(new SanctionStatusComposer());

                PlusEnvironment.GetGame().GetRewardManager().CheckRewards(this);
                RentableSpace.CheckSpace(this);*/
                return true;
            }
        }
        catch (Exception e)
        {
            Logging.exception(e);
        }
        return false;
    }

    public void sendNotification(String message) {
        send(new BroadcastMessageAlertComposer(message, ""));
    }

    public IPlayerNetwork getNetwork() {
        return network;
    }

    public void send(OutgoingMessageComposer response) {
        this.network.send(response);
    }

    public void disconnect() {
        this.dispose();
    }

    public void dispose() {
        if (this.getHabbo() != null)
            this.getHabbo().dispose();

        this.habbo = null;
        this.machineId = null;
        this.network = null;
    }

    public Habbo getHabbo() {
        return habbo;
    }

    public String getMachineId()
    {
        return this.machineId;
    }

    public void setMachineId(String machineId)
    {
        if (machineId == null)
        {
            throw new NullPointerException("Cannot set machineID to NULL");
        }
        this.machineId = machineId;
    }

    public String getIpAddress() {
        String ipAddress = "0.0.0.0";

        if (!SettingsManager.tryGetValue("use.ip_last").equals("true")) {
            return this.network.getChannel().getRemoteAddress().toString();
        } else {
            if (this.getHabbo() != null) {
                ipAddress = HabboDao.getIpAddress(this.getHabbo().getId());
            }
            return ipAddress;
        }
    }

    public void sendWhisper(String message) {
        if (this.getHabbo() == null || this.getHabbo().currentRoom() == null)
            return;

        RoomUser user = getHabbo().currentRoom().getRoomUserManager().getRoomUserByUsername(getHabbo().getUsername());
        if (user == null)
            return;

        this.send(new WhisperMessageComposer(user.getVirtualId(), message));
    }

    public int getLastPing() {
        return lastPing;
    }

    public void setLastPing(int lastPing) {
        this.lastPing = lastPing;
    }
}
