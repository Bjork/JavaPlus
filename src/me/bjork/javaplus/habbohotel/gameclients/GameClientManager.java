package me.bjork.javaplus.habbohotel.gameclients;

import me.bjork.javaplus.communication.connection.netty.NettyPlayerNetwork;
import me.bjork.javaplus.communication.packets.parsers.OutgoingMessageComposer;
import me.bjork.javaplus.core.Logging;
import me.bjork.javaplus.habbohotel.users.Habbo;
import org.jboss.netty.channel.Channel;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * Created on 20/05/2017.
 */
public class GameClientManager {
    private ConcurrentMap<Integer, GameClient> sessions;
    private ConcurrentMap<Integer, GameClient> userIDRegister;
    private ConcurrentMap<String, GameClient> usernameRegister;

    public GameClientManager() {
        this.sessions = new ConcurrentHashMap<Integer, GameClient>();
        this.userIDRegister = new ConcurrentHashMap<Integer, GameClient>();
        this.usernameRegister = new ConcurrentHashMap<String, GameClient>();
    }

    public void addSession(Channel channel) {
        GameClient client = new GameClient(new NettyPlayerNetwork(channel, channel.getId()));
        channel.setAttachment(client);
        this.sessions.putIfAbsent(channel.getId(), client);
    }

    public void removeSession(Channel channel) {
        try {
            this.sessions.remove(channel.getId());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void put(GameClient client, int userId, String username) {
        if (this.usernameRegister.containsKey(username.toLowerCase()))
            this.usernameRegister.remove(username.toLowerCase());

        if (this.userIDRegister.containsKey(userId))
            this.userIDRegister.remove(userId);

        this.userIDRegister.put(userId, client);
        this.usernameRegister.put(username.toLowerCase(), client);
    }

    public void remove(int userid, String username) {
        GameClient client = null;
        this.userIDRegister.remove(userid);
        this.usernameRegister.remove(username.toLowerCase());
    }

    public GameClient getClientByUserID(int userID){
        if (userIDRegister.containsKey(userID))
            return userIDRegister.get(userID);
        return null;
    }

    public GameClient getClientByUsername(String username) {
        if (usernameRegister.containsKey(username.toLowerCase()))
            return usernameRegister.get(username.toLowerCase());
        return null;
    }

    public Habbo getHabboByUserId(int id) {
        for(GameClient client : this.getSessions().values())
        {
            if(client.getHabbo() == null)
                continue;

            if(client.getHabbo().getId() == id)
                return client.getHabbo();
        }
        return null;
    }

    public boolean isOnline(int playerId) {
        return this.userIDRegister.containsKey(playerId);
    }

    public boolean isOnline(String username) {
        return this.usernameRegister.containsKey(username.toLowerCase());
    }

    public Map<Integer, GameClient> getSessions() {
        return this.userIDRegister;
    }

    public void broadcastToModerators(OutgoingMessageComposer messageComposer) {
        for (GameClient session : this.getSessions().values()) {
            if (session.getHabbo() != null && session.getHabbo().getPermissions() != null && session.getHabbo().getPermissions().hasRight("mod_tool")) {
                session.send(messageComposer);
            }
        }
    }

    public void broadcast(OutgoingMessageComposer messageComposer) {
        for (GameClient session : this.getSessions().values()) {
            if (session != null && session.getHabbo() != null) {
                session.send(messageComposer);
            }
        }
    }

    public void disposeAll() {
        Logging.println("Saving users & dispose them...");
        for (GameClient client : this.getSessions().values()) {
            if (client == null)
                continue;
            try {
                client.disconnect();
                client.getNetwork().close();
            } catch (Exception e) {
                Logging.exception(e);
            }
        }
        Logging.println("Done saving & dispose users!");
            if (this.sessions.size() > 0)
                this.sessions.clear();
        Logging.println("Connections closed!");
    }
}

