package me.bjork.javaplus.habbohotel.commands;

import com.mysql.cj.core.util.StringUtils;
import me.bjork.javaplus.communication.packets.outgoing.notifications.MOTDNotificationComposer;
import me.bjork.javaplus.habbohotel.commands.user.AboutCommand;
import me.bjork.javaplus.habbohotel.commands.user.CarryItemCommand;
import me.bjork.javaplus.habbohotel.commands.user.EnableCommand;
import me.bjork.javaplus.habbohotel.commands.user.UnloadRoomCommand;
import me.bjork.javaplus.habbohotel.gameclients.GameClient;

import java.util.AbstractMap;
import java.util.HashMap;
import java.util.Map;

/**
 * Created on 31/05/2017.
 */
public class CommandManager {
    private String prefix = ":";
    private Map<String, IChatCommand> commands;

    public CommandManager()
    {
        this.prefix = ":";
        this.commands = new HashMap<String, IChatCommand>();

        this.registerAdministrator();
        this.registerModerator();
        this.registerEvents();
        this.registerVIP();
        this.registerUser();


    }

    private void registerAdministrator() {

    }

    private void registerModerator() {
    }

    private void registerEvents() {

    }

    private void registerVIP() {

    }

    private void registerUser() {
        this.registerCommand("info", new AboutCommand());
        this.registerCommand("about", new AboutCommand());
        this.registerCommand("carry", new CarryItemCommand());
        this.registerCommand("unload", new UnloadRoomCommand());
        this.registerCommand("enable", new EnableCommand());
    }

    public boolean handle(GameClient client, String message)
    {
        if (client == null || client.getHabbo() == null || client.getHabbo().currentRoom() == null)
            return false;

        if (!message.startsWith(this.prefix))
            return false;

        if (message.equals(prefix + "commands"))
        {
            StringBuilder list = new StringBuilder();
            list.append("This is the list of commands you have available:\n\n");

            for (AbstractMap.Entry<String, IChatCommand> command : commands.entrySet())
            {
                if (!StringUtils.isNullOrEmpty(command.getValue().getPermissionRequired()))
                {
                    if (!client.getHabbo().getPermissions().hasCommand(command.getValue().getPermissionRequired()))
                        continue;
                }
                list.append("- :" + command.getKey() + " " + command.getValue().getParameters() + " - " + command.getValue().getDescription() + "\n");
            }
            client.send(new MOTDNotificationComposer(list.toString()));
            return true;
        }

        message = message.substring(1);
        String[] command = message.split(" ");

        if (command.length == 0)
            return false;

        IChatCommand cmd = this.commands.get(command[0].toLowerCase());

        if (cmd != null) {
            //if (client.getHabbo().getPermissions().hasRight("mod_tool"))
            //this.logCommand(client.getHabbo().getId(), message, client.getMachineId()); TODO Command Logging

            if (StringUtils.isNullOrEmpty(cmd.getPermissionRequired())) {
                if (client.getHabbo().getPermissions().hasCommand(cmd.getPermissionRequired()))
                    return false;
            }

            cmd.handle(client, client.getHabbo().currentRoom(), command);
            return true;
        }
        return false;
    }

    public void registerCommand(String commandText, IChatCommand command)
    {
        this.commands.put(commandText, command);
    }
}
