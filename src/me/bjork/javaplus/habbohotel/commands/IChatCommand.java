package me.bjork.javaplus.habbohotel.commands;

import me.bjork.javaplus.habbohotel.gameclients.GameClient;
import me.bjork.javaplus.habbohotel.rooms.Room;

/**
 * Created on 31/05/2017.
 */
public interface IChatCommand {
    public String getPermissionRequired();
    public String getParameters();
    public String getDescription();
    public void handle(GameClient client, Room room, String[] params);
}
