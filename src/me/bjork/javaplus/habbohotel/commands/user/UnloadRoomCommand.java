package me.bjork.javaplus.habbohotel.commands.user;

import me.bjork.javaplus.habbohotel.commands.IChatCommand;
import me.bjork.javaplus.habbohotel.gameclients.GameClient;
import me.bjork.javaplus.habbohotel.rooms.Room;

/**
 * Created on 07/06/2017.
 */
public class UnloadRoomCommand implements IChatCommand {
    @Override
    public String getPermissionRequired() {
        return "command_unload";
    }

    @Override
    public String getParameters() {
        return "";
    }

    @Override
    public String getDescription() {
        return "Unload the current room";
    }

    @Override
    public void handle(GameClient client, Room room, String[] params) {

        if (client.getHabbo().getPermissions().hasRight("room_unload_any"))
            room.setIdleNow();
        else
            if (room.getRights().hasRights(client))
                room.setIdleNow();
    }
}
