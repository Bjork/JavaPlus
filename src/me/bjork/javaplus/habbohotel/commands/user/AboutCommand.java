package me.bjork.javaplus.habbohotel.commands.user;

import me.bjork.javaplus.JavaPlusEnvironment;
import me.bjork.javaplus.communication.packets.outgoing.notifications.RoomNotificationComposer;
import me.bjork.javaplus.habbohotel.commands.IChatCommand;
import me.bjork.javaplus.habbohotel.gameclients.GameClient;
import me.bjork.javaplus.habbohotel.rooms.Room;

import java.util.concurrent.TimeUnit;

/**
 * Created on 31/05/2017.
 */
public class AboutCommand implements IChatCommand {
    @Override
    public String getPermissionRequired() {
        return "command_info";
    }

    @Override
    public String getParameters() {
        return "";
    }

    @Override
    public String getDescription() {
        return "Displays generic informations that everybody loves to see.";
    }

    @Override
    public void handle(GameClient client, Room room, String[] params) {
        int seconds = (JavaPlusEnvironment.getIntUnixTimestamp() - JavaPlusEnvironment.getServerStartedTime());
        int day = (int) TimeUnit.SECONDS.toDays(seconds);
        long hours = TimeUnit.SECONDS.toHours(seconds) - (day * 24);
        long minute = TimeUnit.SECONDS.toMinutes(seconds) - (TimeUnit.SECONDS.toHours(seconds)* 60);
        long second = TimeUnit.SECONDS.toSeconds(seconds) - (TimeUnit.SECONDS.toMinutes(seconds) * 60);

        int onlineUsers = JavaPlusEnvironment.getGame().getGameClientManager().getSessions().size();
        int roomCount = JavaPlusEnvironment.getGame().getRoomManager().getActiveRoomsCount();
        Runtime instance = Runtime.getRuntime();
        instance.gc();

        String title = "Powered by JavaPlus (Java port of PlusEmulator C#)";

        StringBuilder list = new StringBuilder();
        list.append("<b>Credits</b>:\n- Sledmore (Plus Emulator)\n- Leon (Comet)\n- The General (Arcturus)\n- Quackster (Icarus)\n\n");
        list.append("Ported By Bjork\n\n");
        list.append("<b>Current Emulator Informations:</b>\n");
        list.append("Online users: " + onlineUsers + "\n");
        list.append("Rooms loaded: " + roomCount + "\n");
        list.append("Uptime: " + day + (day > 1 ? " days, " : " day, ") + hours + (hours > 1 ? " hours, " : " hour, ") + minute + (minute > 1 ? " minutes, " : " minute, ") + second + (second > 1 ? " seconds!" : " second!") + "\n");
        list.append("RAM Usage: " + (instance.totalMemory() - instance.freeMemory()) / (1024 * 1024) + "/" + (instance.freeMemory()) / (1024 * 1024) + "MB\n");
        list.append("CPU Cores: " + instance.availableProcessors() + "\n");
        list.append("Total Memory: " + instance.maxMemory() / (1024 * 1024) + "MB" + "\n");

        client.send(new RoomNotificationComposer(title, list.toString(), "plus", "", ""));
    }
}
