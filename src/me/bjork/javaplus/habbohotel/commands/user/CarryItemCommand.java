package me.bjork.javaplus.habbohotel.commands.user;

import me.bjork.javaplus.core.utils.Util;
import me.bjork.javaplus.habbohotel.commands.IChatCommand;
import me.bjork.javaplus.habbohotel.gameclients.GameClient;
import me.bjork.javaplus.habbohotel.rooms.Room;
import me.bjork.javaplus.habbohotel.rooms.RoomUser;

/**
 * Created on 01/06/2017.
 */
public class CarryItemCommand implements IChatCommand {
    @Override
    public String getPermissionRequired() {
        return "command_carry";
    }

    @Override
    public String getParameters() {
        return "%itemid%";
    }

    @Override
    public String getDescription() {
        return "Allows you to carry a hand item";
    }

    @Override
    public void handle(GameClient client, Room room, String[] params) {

        if (!Util.isNumeric(params[1]))
        {
            client.sendWhisper("Please enter a valid integer!");
            return;
        }

        int itemId = Integer.valueOf(params[1]);

        RoomUser roomUser = client.getHabbo().getRoomUser();
        if (roomUser == null)
            return;

        roomUser.carryItem(itemId);
        return;
    }
}
