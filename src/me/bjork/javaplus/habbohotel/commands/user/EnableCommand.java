package me.bjork.javaplus.habbohotel.commands.user;

import me.bjork.javaplus.core.utils.Util;
import me.bjork.javaplus.habbohotel.commands.IChatCommand;
import me.bjork.javaplus.habbohotel.gameclients.GameClient;
import me.bjork.javaplus.habbohotel.rooms.Room;
import me.bjork.javaplus.habbohotel.rooms.RoomUser;
import me.bjork.javaplus.habbohotel.users.effects.AvatarEffect;

/**
 * Created on 07/06/2017.
 */
public class EnableCommand implements IChatCommand {
    @Override
    public String getPermissionRequired() {
        return "command_enable";
    }

    @Override
    public String getParameters() {
        return "%effectId%";
    }

    @Override
    public String getDescription() {
        return "Gives you the ability to set an effect on your user!";
    }

    @Override
    public void handle(GameClient client, Room room, String[] params) {
        if (client == null || client.getHabbo() == null)
            return;

        /*if (!Room.EnablesEnabled && !Session.GetHabbo().GetPermissions().HasRight("mod_tool"))//TODO Special RoomPermissions
        {
            Session.SendWhisper("Oops, it appears that the room owner has disabled the ability to use the enable command in here.");
            return;
        }*/

        RoomUser roomUser = client.getHabbo().getRoomUser();
        if (roomUser == null)
            return;

        if (roomUser.getMountedEntity() != null) {
            client.sendWhisper("You cannot enable effects whilst riding a horse!");
            return;
        }
        //TODO user isLying

        if (!Util.isNumeric(params[1])) {
            client.sendWhisper("Please enter a valid integer!");
            return;
        }

        int effectId = Integer.parseInt(params[1]);

        if (effectId > Integer.MAX_VALUE || effectId < Integer.MIN_VALUE)
            return;

        if ((effectId == 102 || effectId == 187) && !client.getHabbo().getPermissions().hasRight("mod_tool")) {
            client.sendWhisper("Sorry, only staff members can use this effects.");
            return;
        }

        if (effectId == 178 && (!client.getHabbo().getPermissions().hasRight("gold_vip") && !client.getHabbo().getPermissions().hasRight("events_staff"))) {
            client.sendWhisper("Sorry, only staff members can use this effects.");
            return;
        }

        if (params.length == 2) {
            roomUser.applyEffect(new AvatarEffect(effectId, 0));
        } else if (params.length == 3) {
            roomUser.applyEffect(new AvatarEffect(effectId, Integer.parseInt(params[2]) * 2));
        } else {
            client.sendWhisper("To get a effect type :enable %number% OR :enable %number% %time% in seconds");
            return;
        }
    }
}
