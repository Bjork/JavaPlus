package me.bjork.javaplus.habbohotel.permissions.types;

/**
 * Created on 24/05/2017.
 */
public class Permission {
    private final int id;
    private final String permission;
    private final String description;

    public Permission(int id, String permission, String description) {
        this.id = id;
        this.permission = permission;
        this.description = description;
    }

    public String getName() {
        return permission;
    }
}
