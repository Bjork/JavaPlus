package me.bjork.javaplus.habbohotel.permissions.types;

/**
 * Created on 24/05/2017.
 */
public class PermissionCommand {
    private final String command;
    private final int groupId;
    private final int subscriptionId;

    public PermissionCommand(String command, int groupId, int subscriptionId) {
        this.command = command;
        this.groupId = groupId;
        this.subscriptionId = subscriptionId;
    }

    public int getGroupid() {
        return groupId;
    }

    public int getSubscriptionid() {
        return subscriptionId;
    }

    public String getCommand() {
        return command;
    }
}
