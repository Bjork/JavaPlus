package me.bjork.javaplus.habbohotel.permissions.types;

/**
 * Created on 24/05/2017.
 */
public class PermissionGroup {
    private final int id;
    private final String name;
    private final String description;
    private final String badge;
    private final int floodtime;

    public PermissionGroup(int id, String name, String description, String badge, int floodtime) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.badge = badge;
        this.floodtime = floodtime;
    }


    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public int getFloodtime() { return floodtime; }
}
