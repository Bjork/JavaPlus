package me.bjork.javaplus.habbohotel.permissions;

import me.bjork.javaplus.JavaPlusEnvironment;
import me.bjork.javaplus.habbohotel.users.Habbo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 24/05/2017.
 */
public class PermissionComponent {

    private List<String> permissions = new ArrayList<String>();
    private List<String> commands = new ArrayList<String>();

    public PermissionComponent(Habbo habbo)
    {
        if (this.permissions.size() > 0)
            this.permissions.clear();

        if (this.commands.size() > 0)
            this.commands.size();

        this.permissions.addAll(JavaPlusEnvironment.getGame().getPermissionManager().getPermissionsForPlayer(habbo));
        this.commands.addAll(JavaPlusEnvironment.getGame().getPermissionManager().getCommandsForPlayer(habbo));
    }

    public Boolean hasRight(String Right)
    {
        return this.permissions.contains(Right);
    }

    public Boolean hasCommand(String Command)
    {
        return this.commands.contains(Command);
    }

    public void dispose()
    {
        this.permissions.clear();
        this.commands.clear();
    }
}
