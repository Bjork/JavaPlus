package me.bjork.javaplus.habbohotel.permissions;

import me.bjork.javaplus.core.Logging;
import me.bjork.javaplus.database.SqlHelper;
import me.bjork.javaplus.database.queries.permissions.PermissionDao;
import me.bjork.javaplus.habbohotel.permissions.types.Permission;
import me.bjork.javaplus.habbohotel.permissions.types.PermissionCommand;
import me.bjork.javaplus.habbohotel.permissions.types.PermissionGroup;
import me.bjork.javaplus.habbohotel.users.Habbo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * Created on 24/05/2017.
 */
public class PermissionManager {

    private Map<Integer, Permission> permissions = new HashMap<Integer, Permission>();
    private Map<String, PermissionCommand> commands = new HashMap<String, PermissionCommand>();
    private Map<Integer, PermissionGroup> permissionGroups = new LinkedHashMap<Integer, PermissionGroup>();
    private Map<Integer, List<String>> permissionGroupRights = new LinkedHashMap<Integer, List<String>>();
    private Map<Integer, List<String>> permissionSubscriptionRights = new LinkedHashMap<Integer, List<String>>();

    public PermissionManager() { }

    public void initialize()
    {
        permissions.clear();
        commands.clear();
        permissionGroups.clear();
        permissionGroupRights.clear();

        loadPermissions();
        loadCommands();
        loadPermissionGroups();
        permissionGroupRights();
        Logging.println("Permission Manager -> LOADED");
    }

    private void loadPermissions() {
        try {
            this.permissions = PermissionDao.getPermissions();
            Logging.println("Loaded " + this.permissions.size() + " permissions.");
        }
        catch (Exception e)
        {
            Logging.exception(e);
        }
    }

    private void loadCommands() {
        try {
            this.commands = PermissionDao.getCommands();
            Logging.println("Loaded " + this.commands.size() + " permissions groups.");
        }
        catch (Exception e)
        {
            Logging.exception(e);
        }
    }

    private void loadPermissionGroups() {
        try {
            this.permissionGroups = PermissionDao.getPermissionGroups();
            Logging.println("Loaded " + this.permissionGroups.size() + " permissions groups.");
        }
        catch (Exception e)
        {
            Logging.exception(e);
        }
    }

    private void permissionGroupRights() {
        try {

            Connection sqlConnection = null;
            PreparedStatement preparedStatement = null;
            ResultSet resultSet = null;

            try {
                sqlConnection = SqlHelper.getConnection();
                preparedStatement = SqlHelper.prepare("SELECT * FROM `permissions_rights`", sqlConnection);
                resultSet = preparedStatement.executeQuery();

                while (resultSet.next()) {
                    int groupId = resultSet.getInt("group_id");
                    int permissionId = resultSet.getInt("permission_id");

                    if (!permissionGroups.containsKey(groupId))
                        continue;

                    Permission permission = null;
                    permission = permissions.get(permissionId);

                    if (permission == null)
                        continue;

                    if (permissionGroupRights.containsKey(groupId)) {
                        permissionGroupRights.get(groupId).add(permission.getName());
                    }
                    else
                    {
                        List<String> rightsSet = new ArrayList<String>();
                        rightsSet.add(permission.getName());
                        permissionGroupRights.put(groupId, rightsSet);
                    }
                }
            } catch (SQLException e) {
                SqlHelper.handleSqlException(e);
            } finally {
                SqlHelper.closeSilently(resultSet);
                SqlHelper.closeSilently(preparedStatement);
                SqlHelper.closeSilently(sqlConnection);
            }

            try {
                sqlConnection = SqlHelper.getConnection();
                preparedStatement = SqlHelper.prepare("SELECT * FROM `permissions_subscriptions`", sqlConnection);
                resultSet = preparedStatement.executeQuery();

                while (resultSet.next()) {
                    int permissionId = resultSet.getInt("permission_id");
                    int subscriptionId = resultSet.getInt("subscription_id");

                    Permission permission = null;
                    permission = permissions.get(permissionId);

                    if (permission == null)
                        continue;

                    if (permissionSubscriptionRights.containsKey(subscriptionId)) {
                        permissionSubscriptionRights.get(subscriptionId).add(permission.getName());
                    }
                    else
                    {
                        List<String> rightsSet = new ArrayList<String>();
                        rightsSet.add(permission.getName());
                        permissionSubscriptionRights.put(subscriptionId, rightsSet);
                    }
                }
            } catch (SQLException e) {
                SqlHelper.handleSqlException(e);
            } finally {
                SqlHelper.closeSilently(resultSet);
                SqlHelper.closeSilently(preparedStatement);
                SqlHelper.closeSilently(sqlConnection);
            }

            Logging.println("Loaded " + this.permissionGroupRights.size() + " permissions groups rights.");
            Logging.println("Loaded " + this.permissionSubscriptionRights.size() + " permissions subscription rights.");
        }
        catch (Exception e)
        {
           Logging.exception(e);
        }
    }

    public List<String> getPermissionsForPlayer(Habbo habbo)
    {
        List<String> permissionSet = new ArrayList<String>();

        if (tryGetPermissionGroupRights(habbo.getRank())) {
            List<String> permRights = getPermissionGroupRightsByRank(habbo.getRank());
            permissionSet.addAll(permRights);
        }

        if (tryGetPermissionSubscriptionRights(habbo.getViprank())) {
            List<String> subscriptionRights = getPermissionSubscriptionRightsByRank(habbo.getViprank());
            permissionSet.addAll(subscriptionRights);
        }

        return permissionSet;
    }

    public List<String> getCommandsForPlayer(Habbo habbo) {
        List<String> commands = new ArrayList<String>();

        for (PermissionCommand command : this.commands.values())
            if (habbo.getRank() >= command.getGroupid() && habbo.getViprank() >= command.getSubscriptionid())
                commands.add(command.getCommand());
        return commands;
    }

    public PermissionGroup getRankGroup(int rankId)
    {
        if (this.permissionGroups.containsKey(rankId))
            return this.permissionGroups.get(rankId);

        return null;
    }

    public Boolean tryGetPermissionGroupRights(int rankId) { return this.permissionGroupRights.containsKey(rankId); }
    public List<String> getPermissionGroupRightsByRank(int rankId) {return this.permissionGroupRights.get(rankId); }
    public Boolean tryGetPermissionSubscriptionRights(int vipRankId) { return this.permissionGroupRights.containsKey(vipRankId); }
    public List<String> getPermissionSubscriptionRightsByRank(int vipRankId) {return this.permissionGroupRights.get(vipRankId); }
}
