package me.bjork.javaplus.habbohotel.users;

import com.google.common.collect.Lists;
import me.bjork.javaplus.JavaPlusEnvironment;
import me.bjork.javaplus.communication.packets.outgoing.generic.GenericErrorComposer;
import me.bjork.javaplus.communication.packets.outgoing.rooms.access.DoorbellComposer;
import me.bjork.javaplus.communication.packets.outgoing.rooms.access.DoorbellDeniedComposer;
import me.bjork.javaplus.communication.packets.outgoing.rooms.engine.RoomPropertyComposer;
import me.bjork.javaplus.communication.packets.outgoing.rooms.session.CantConnectComposer;
import me.bjork.javaplus.communication.packets.outgoing.rooms.session.HotelViewMessageComposer;
import me.bjork.javaplus.communication.packets.outgoing.rooms.session.OpenConnectionComposer;
import me.bjork.javaplus.communication.packets.outgoing.rooms.session.RoomReadyComposer;
import me.bjork.javaplus.communication.packets.outgoing.rooms.settings.RoomRatingComposer;
import me.bjork.javaplus.core.Logging;
import me.bjork.javaplus.core.figuredata.FigureDataManager;
import me.bjork.javaplus.core.utils.Util;
import me.bjork.javaplus.database.SqlHelper;
import me.bjork.javaplus.database.queries.groups.GroupDao;
import me.bjork.javaplus.database.queries.moderation.TicketDao;
import me.bjork.javaplus.database.queries.players.HabboDao;
import me.bjork.javaplus.database.queries.players.HabboStatsDao;
import me.bjork.javaplus.habbohotel.gameclients.GameClient;
import me.bjork.javaplus.habbohotel.permissions.PermissionComponent;
import me.bjork.javaplus.habbohotel.rooms.Room;
import me.bjork.javaplus.habbohotel.rooms.RoomUser;
import me.bjork.javaplus.habbohotel.rooms.RoomUserType;
import me.bjork.javaplus.habbohotel.rooms.settings.RoomState;
import me.bjork.javaplus.habbohotel.users.currencies.CurrencyComponent;
import me.bjork.javaplus.habbohotel.users.inventory.InventoryComponent;
import me.bjork.javaplus.habbohotel.users.messenger.friendbar.FriendBarState;
import me.bjork.javaplus.habbohotel.users.messenger.friendbar.FriendBarStateUtility;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created on 20/05/2017.
 */
public class Habbo {
    private int id = -1;
    private String username = "";
    private int rank;
    private String motto;
    private String look;
    private String gender;
    private int credits;
    private int homeroom;
    private Boolean hasfriendrequestsdisabled;
    private int lastonline;
    private Boolean appearoffline;
    private Boolean hideinroom;
    private double createdate;
    private String machineid;
    private ArrayList<Integer> volume;
    private Boolean chatpreference;
    private Boolean focuspreference;
    private Boolean petsmuted;
    private Boolean botsmuted;
    private double lastnamechange;
    private Boolean ignoreinvites;
    private double timemuted;
    private double tradinglock;
    private Boolean allowgifts;
    private Boolean disableforcedeffects;
    private Boolean allowmimic;
    private int viprank;

    private Boolean allowTradingRequests;
    private Boolean allowConsoleMessages;
    private Boolean receiveWhispers;
    private Boolean ignorePublicWhispers;
    private Boolean allowUserFollowing;
    private Boolean disconnected;
    private Boolean habboSaved;
    private Boolean changingName;
    private int friendCount;
    private int bannedPhraseCount;
    private double sessionStart;
    private int messengerSpamCount;
    private int messengerSpamTime;
    private int tentId;
    private int hopperId;
    private Boolean isHopping;
    private int teleportId;
    private Boolean isTeleporting;
    private int teleportingRoomId;
    private Boolean roomAuthOk;
    private int requestedRoomId;
    private int currentRoomId;
    private Boolean hasSpoken;
    private int lastAdvertiseReport;
    private Boolean advertisingReported;
    private int questLastCompleted;
    private int customBubbleId;
    private int petId;
    private int lastGiftPurchaseTime;
    private int lastMottoUpdateTime;
    private int lastClothingUpdateTime;
    private int lastForumMessageUpdateTime;
    private int giftPurchasingWarnings;
    private int mottoUpdateWarnings;
    private int clothingUpdateWarnings;
    private long lastRoomRequest;
    private Boolean sessionGiftBlocked;
    private Boolean sessionMottoBlocked;
    private Boolean sessionClothingBlocked;
    private boolean authenticated;
    private boolean invisible = false;
    private double roomFloodTime = 0;

    public static final String DEFAULT_FIGURE = "hr-100-61.hd-180-2.sh-290-91.ch-210-66.lg-270-82";

    private GameClient client;
    private PermissionComponent permissions;
    private RoomUser roomUser;
    private HabboStats habboStats;
    private InventoryComponent inventoryComponent;
    private CurrencyComponent currencies;

    private List<Integer> ignoredPlayers = new ArrayList<>();
    private List<Integer> ratedRooms;
    private List<Integer> groups = new ArrayList<>();
    private List<Integer> rooms = new ArrayList<>();
    public boolean isDisposed = false;
    private long roomLastMessageTime = 0;
    private int roomFloodFlag = 0;
    private String lastMessage = "";
    private FriendBarState friendBarState;
    private long lastRewardTime;

    public Habbo(int id, String username, int rank, String motto, String look, String gender, int credits, int homeRoom, Boolean hasFriendRequestsDisabled,
                     int lastOnline, Boolean appearOffline, Boolean hideInRoom, double createDate, String machineID, String clientVolume, Boolean chatPreference,
                     Boolean focusPreference, Boolean petsMuted, Boolean botsMuted, double lastNameChange, Boolean ignoreInvites,
                     double timeMuted, double tradingLock, Boolean allowGifts, int friendBarState, Boolean disableForcedEffects, Boolean allowMimic, int vipRank)
    {
        this.id = id;
        this.username = username;
        this.rank = rank;
        this.motto = motto;
        this.look = look;
        this.gender = gender;
        this.credits = credits;
        this.homeroom = homeRoom;
        this.hasfriendrequestsdisabled = hasFriendRequestsDisabled;
        this.lastonline = lastOnline;
        this.appearoffline = appearOffline;
        this.hideinroom = hideInRoom;
        this.createdate = createDate;
        this.machineid = machineID;
        this.volume = new ArrayList<Integer>();
        for (String volume : clientVolume.split(",")) {
            if (Util.isParsable(volume))
                this.volume.add(Integer.valueOf(volume));
            else
                this.volume.add(100);
        }
        this.chatpreference = chatPreference;
        this.focuspreference = focusPreference;
        this.petsmuted = petsMuted;
        this.botsmuted = botsMuted;
        this.lastnamechange = lastNameChange;
        this.ignoreinvites = ignoreInvites;
        this.timemuted = timeMuted;
        this.tradinglock = tradingLock;
        if (tradinglock > 0 && JavaPlusEnvironment.getUnixTimestamp() > this.tradinglock)
        {
            this.tradinglock = 0;
            try (Connection connection = SqlHelper.getConnection())
            {
                Statement statement = connection.createStatement();
                statement.execute("UPDATE `user_info` SET `trading_locked` = '0' WHERE `user_id` = '" + this.getId() + "' LIMIT 1");
            }
            catch (SQLException e)
            {
                Logging.exception(e);
            }
        }
        this.allowgifts = allowGifts;
        this.disableforcedeffects = disableForcedEffects;
        this.allowmimic = allowMimic;
        this.viprank = vipRank;

        this.allowTradingRequests = true;//TODO
        this.allowUserFollowing = true;//TODO
        this.allowConsoleMessages = true;
        this.receiveWhispers = true;
        this.ignorePublicWhispers = false;
        this.disconnected = false;
        this.habboSaved = false;
        this.changingName = false;
        this.friendBarState = FriendBarStateUtility.getEnum(friendBarState);

        this.friendCount = 0;

        this.bannedPhraseCount = 0;
        this.sessionStart = JavaPlusEnvironment.getUnixTimestamp();
        this.lastRewardTime = JavaPlusEnvironment.getIntUnixTimestamp();
        this.messengerSpamCount = 0;
        this.messengerSpamTime = 0;

        this.tentId = 0;
        this.hopperId = 0;
        this.setIsHopping(false);
        this.teleportId = 0;
        this.setIsTeleporting(false);
        this.teleportingRoomId = 0;
        this.roomAuthOk = false;
        this.requestedRoomId = 0;
        this.currentRoomId = 0;

        this.hasSpoken = false;
        this.lastAdvertiseReport = 0;
        this.advertisingReported = false;

        this.questLastCompleted = 0;
        this.customBubbleId = 0;
        this.setPetId(0);

        this.lastGiftPurchaseTime = JavaPlusEnvironment.getIntUnixTimestamp();
        this.lastMottoUpdateTime = JavaPlusEnvironment.getIntUnixTimestamp();
        this.lastClothingUpdateTime =  JavaPlusEnvironment.getIntUnixTimestamp();
        this.lastForumMessageUpdateTime = JavaPlusEnvironment.getIntUnixTimestamp();

        this.giftPurchasingWarnings = 0;
        this.mottoUpdateWarnings = 0;
        this.clothingUpdateWarnings = 0;
        this.lastRoomRequest = 0;

        this.sessionGiftBlocked = false;
        this.sessionMottoBlocked = false;
        this.sessionClothingBlocked = false;

        if(this.look != null) {
            if (!FigureDataManager.isValidFigureCode(this.look, this.gender)) {
                this.look = DEFAULT_FIGURE;
            }
        }
        else
            this.look = DEFAULT_FIGURE;

        this.setRatedRooms(new ArrayList<Integer>());

        this.groups = GroupDao.getIdsByPlayerId(this.id);
        this.authenticated = true;
    }

    public void init()
    {
        this.permissions = new PermissionComponent(this);
        this.initHabboStats();
        this.roomUser = new RoomUser(this);
        this.roomUser.setRoomUserType(RoomUserType.USER);
        this.inventoryComponent = new InventoryComponent(this);
        this.currencies = new CurrencyComponent(this);

        JavaPlusEnvironment.getGame().getRoomManager().loadRoomsForUser(this);

        /*this.Achievements = data.achievements;

        this.FavoriteRooms = new ArrayList();
        foreach (int id in data.favouritedRooms)
        {
            FavoriteRooms.Add(id);
        }*/

        /*quests = data.quests;

        Messenger = new HabboMessenger(Id);
        Messenger.Init(data.friends, data.requests);
        this._friendCount = Convert.ToInt32(data.friends.Count);
        Relationships = data.relations;

        this.InitSearches();
        this.InitFX();
        this.InitClothing();
        this.InitIgnores();
        this.InitPolls();*/
    }

    private void initHabboStats()
    {
        this.habboStats = HabboStatsDao.getStats(this.getId());

        if (this.habboStats == null) {
            HabboStatsDao.createUserStats(this.getId());
            this.habboStats = HabboStatsDao.getStats(this.getId());
        }

        if (!this.getStats().getRespectTimestamp().equals(Util.statsDate()))
        {
            this.getStats().setRespectTimestamp(Util.statsDate());
            int dailyRespects = this.getStats().getDailyRespectPoints() + 5;
            int dailyPetRespects = this.getStats().getDailyPetRespectPoints() + 5;

            if (this.getPermissions().hasRight("mod_tool")) {
                dailyRespects = this.getStats().getDailyRespectPoints() + 20;
                dailyPetRespects = this.getStats().getDailyPetRespectPoints() + 5;
            }
            /*else if (PlusEnvironment.GetGame().GetSubscriptionManager().TryGetSubscriptionData(VIPRank, out SubData))
                DailyRespects = SubData.Respects;*/ //TODO subcription respect settings

            if (dailyRespects > 100)
                dailyRespects = 100;

            if (dailyPetRespects > 100)
                dailyPetRespects = 100;

            this.getStats().setDailyRespectPoints(dailyRespects);
            this.getStats().setDailyPetRespectPoints(dailyPetRespects);

            HabboStatsDao.updateDailyRespects(this);
        }
    }

    public void loadRoomForUser(int roomId, String password) {
        if (this.getClient() == null || this.getClient().getHabbo() == null)
            return;

        if (this.getClient().getHabbo().inRoom() && !this.getRoomUser().isDoorbellAnswered())
        {
            Room oldRoom = JavaPlusEnvironment.getGame().getRoomManager().getRoom(this.getClient().getHabbo().currentRoomId);

            if (oldRoom == null)
                return;

            if (oldRoom.getRoomUserManager() != null) {
                this.getClient().getHabbo().getRoomUser().removeUserFromRoom(false, false, false);
            }
        }

        if (this.getClient().getHabbo().getIsTeleporting() && this.getClient().getHabbo().teleportingRoomId != roomId)
        {
            this.getClient().send(new HotelViewMessageComposer());
            return;
        }

        Room room = JavaPlusEnvironment.getGame().getRoomManager().loadRoom(roomId);

        if (room == null)
        {
            this.getClient().send(new HotelViewMessageComposer());
            return;
        }

        if (room.isCrashed)
        {
            this.getClient().sendNotification("This room has crashed! :(");
            return;
        }

        if (room.getUsersNow() >= room.getUsersMax() && !this.getClient().getHabbo().getPermissions().hasRight("room_enter_full") && this.getClient().getHabbo().getId() != room.getOwnerId())
        {
            this.getClient().send(new CantConnectComposer(1));
            this.getClient().send(new HotelViewMessageComposer());
            return;
        }

        if (!this.getClient().getHabbo().getPermissions().hasRight("room_ban_override") && room.getRights().hasBan(this.getClient().getHabbo().getId()))
        {
            setRoomAuthOk(false);
            this.getClient().send(new CantConnectComposer(4));
            this.getClient().send(new HotelViewMessageComposer());
            return;
        }

        this.getClient().getHabbo().setCurrentRoomId(roomId);

        boolean isOwner = (room.getOwnerId() == this.getClient().getHabbo().getId());
        boolean isTeleporting = this.getClient().getHabbo().getIsTeleporting() && (this.getClient().getHabbo().teleportingRoomId == room.getId() && this.getClient().getHabbo().getIsHopping());

        if (room.getAccess() != RoomState.OPEN) {
            if (!room.getRights().hasRights(getClient()) && !isTeleporting) {
                if (!isOwner && !this.getRoomUser().isDoorbellAnswered()) {
                    if (room.getAccess() == RoomState.DOORBELL && !this.getClient().getHabbo().getPermissions().hasRight("room_enter_locked")) {
                        if (room.userCount() > 0) {
                            room.send(new DoorbellComposer(getClient().getHabbo().getUsername()), true);
                            this.getClient().send(new DoorbellComposer(""));
                            this.getClient().getHabbo().setRequestedRoomId(room.getId());
                        } else {
                            this.getClient().send(new DoorbellDeniedComposer(""));
                            this.getClient().send(new HotelViewMessageComposer());
                        }
                        return;
                    }
                }

                if (room.getAccess() == RoomState.PASSWORD && !this.getClient().getHabbo().getPermissions().hasRight("room_enter_locked"))
                {
                    if (!password.equals(room.getPassword())) {
                        this.getClient().send(new GenericErrorComposer(GenericErrorComposer.PASSWORD_INCORRECT));
                        this.getClient().send(new HotelViewMessageComposer());
                        return;
                    }
                }
            }
        }
        this.getClient().send(new OpenConnectionComposer());

        this.getClient().getHabbo().getRoomUser().setDoorbellAnswered(false);
        if (this.getClient().getHabbo().getRequestedRoomId() > 0) {
            this.getClient().getHabbo().setRequestedRoomId(0);
        }

        this.getClient().send(new RoomReadyComposer(room.getId(), room.getModelName()));

        if (!room.getWallpaper().equals("0.0"))
            this.getClient().send(new RoomPropertyComposer("wallpaper", room.getWallpaper()));
        if (!room.getFloor().equals("0.0"))
            this.getClient().send(new RoomPropertyComposer("floor", room.getFloor()));

        this.getClient().send(new RoomPropertyComposer("landscape", room.getLandscape()));
        this.getClient().send(new RoomRatingComposer(room.getScore(), !(getClient().getHabbo().ratedRooms.contains(room.getId()) || room.getOwnerId() == getClient().getHabbo().getId())));
        //TODO Voir Comet pour les extras
    }

    public void ignorePlayer(int playerId) {
        if (this.ignoredPlayers == null) {
            this.ignoredPlayers = new ArrayList<>();
        }

        this.ignoredPlayers.add(playerId);
    }

    public void unignorePlayer(int playerId) {
        this.ignoredPlayers.remove((Integer) playerId);
    }

    public boolean ignores(int playerId) {
        return this.ignoredPlayers != null && this.ignoredPlayers.contains(playerId);
    }

    public void saveUser() {
        HabboDao.updateHabboData(id, username, motto, look, credits, gender);
    }

    public boolean canRateRoom() {
        return !this.getRatedRooms().contains(this.getId());
    }

    public void dispose()
    {
        if (!this.habboSaved)
        {
            this.habboSaved = true;
            HabboDao.saveHabbo(this);
            HabboStatsDao.saveHabboStats(this.getStats());

            if (getPermissions().hasRight("mod_tickets"))
                TicketDao.updateTickets(this.getId());

            this.currencies.saveUserCurrencies();
        }

        if (this.isAuthenticated()) {
            if (this.inRoom() && this.currentRoom() != null) {
                this.roomUser.removeUserFromRoom(true, false, false);
            }
        }

        this.inventoryComponent.dispose();
        //this.messenger.dispose();
        this.ignoredPlayers.clear();
        this.roomUser.dispose();
        this.permissions.dispose();
        this.rooms.clear();
        this.getRatedRooms().clear();
        this.currencies.dispose();

        this.inventoryComponent = null;
        //this.messenger = null;
        this.ignoredPlayers = null;
        this.roomUser = null;
        this.permissions = null;
        this.rooms = null;
        this.ratedRooms = null;

        this.habboStats = null;

        JavaPlusEnvironment.getGame().getGameClientManager().remove(this.getId(), this.getUsername());

        this.client = null;
        this.isDisposed = true;
    }

    public boolean isAuthenticated() {
        return authenticated;
    }

    public GameClient getClient()
    {
        return this.client;
    }

    public void setClient(GameClient client) {
        this.client = client;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public String getMotto() {
        return motto;
    }

    public void setMotto(String motto) {
        this.motto = motto;
    }

    public String getLook() {
        return look;
    }

    public void setLook(String look) {
        this.look = look;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getCredits() {
        return credits;
    }

    public void setCredits(int credits) {
        this.credits = credits;
    }

    public int getHomeroom() {
        return homeroom;
    }

    public void setHomeroom(int homeroom) {
        this.homeroom = homeroom;
    }

    public int getViprank() {
        return viprank;
    }

    public void setViprank(int viprank) {
        this.viprank = viprank;
    }

    public List<Integer> getVolumes() {
        return this.volume;
    }

    public Boolean inRoom() { return getCurrentRoomId() >= 1 && currentRoom() != null; } //TODO remake avec RoomUser

    public Room currentRoom()
    {
        if (getCurrentRoomId() <= 0)
                return null;
            Room room = JavaPlusEnvironment.getGame().getRoomManager().getRoom(getCurrentRoomId());
            if (room != null) {
                return room;
            }
            return null;
    }

    public List<Integer> getRooms() {
        return rooms;
    }

    public HabboStats getStats()
    {
        return this.habboStats;
    }

    public InventoryComponent getInventory() { return this.inventoryComponent; }

    public int getLastonline() {
        return lastonline;
    }

    public void setLastonline(int lastonline) {
        this.lastonline = lastonline;
    }

    public Boolean getChangingName() {
        return changingName;
    }

    public void setChangingName(Boolean changingName) {
        this.changingName = changingName;
    }

    public List<Integer> getGroups() {
        return groups == null ? Lists.newArrayList() : groups;
    }

    public int getCurrentRoomId() {
        return currentRoomId;
    }

    public void setCurrentRoomId(int currentRoomId) {
        this.currentRoomId = currentRoomId;
    }

    public void setRoomAuthOk(Boolean roomAuthOk) {
        this.roomAuthOk = roomAuthOk;
    }

    public Boolean getRoomAuthOk() {
        return roomAuthOk;
    }

    public PermissionComponent getPermissions()
    {
        return this.permissions;
    }

	public long getLastRoomRequest() {
		return lastRoomRequest;
	}

	public void setLastRoomRequest(long lastRoomRequest) {
		this.lastRoomRequest = lastRoomRequest;		
	}

	public List<Integer> getRatedRooms() {
		return ratedRooms;
	}

	public void setRatedRooms(List<Integer> ratedRooms) {
		this.ratedRooms = ratedRooms;
	}

	public int getPetId() {
		return petId;
	}

	public void setPetId(int petId) {
		this.petId = petId;
	}

	public Boolean getIsTeleporting() {
		return isTeleporting;
	}

	public void setIsTeleporting(Boolean isTeleporting) {
		this.isTeleporting = isTeleporting;
	}

	public Boolean getIsHopping() {
		return isHopping;
	}

	public void setIsHopping(Boolean isHopping) {
		this.isHopping = isHopping;
	}

	public RoomUser getRoomUser() { return roomUser; }

    public boolean isInvisible() {
        return invisible;
    }

    public void setInvisible(boolean invisible) {
        this.invisible = invisible;
    }

    public double getTimeMuted() {
        return timemuted;
    }

    public void setTimeMuted(double timeMuted) {
        this.timemuted = timeMuted;
    }

    public double getRoomFloodTime() {
        return roomFloodTime;
    }

    public void setRoomFloodTime(double roomFloodTime) {
        this.roomFloodTime = roomFloodTime;
    }

    public long getRoomLastMessageTime() {
        return roomLastMessageTime;
    }

    public void setRoomLastMessageTime(long roomLastMessageTime) {
        this.roomLastMessageTime = roomLastMessageTime;
    }

    public int getRoomFloodFlag() {
        return roomFloodFlag;
    }

    public void setRoomFloodFlag(int roomFloodFlag) {
        this.roomFloodFlag = roomFloodFlag;
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    public Boolean getReceiveWhispers() {
        return receiveWhispers;
    }

    public void setReceiveWhispers(Boolean receiveWhispers) {
        this.receiveWhispers = receiveWhispers;
    }

    public int getRequestedRoomId() {return this.requestedRoomId; }

    public void setRequestedRoomId(int requestedRoomId) {
        this.requestedRoomId = requestedRoomId;
    }

    public int getLastGiftPurchaseTime() {
        return lastGiftPurchaseTime;
    }

    public void setLastGiftPurchaseTime(int lastGiftPurchaseTime) {
        this.lastGiftPurchaseTime = lastGiftPurchaseTime;
    }

    public int getLastMottoUpdateTime() {
        return lastMottoUpdateTime;
    }

    public void setLastMottoUpdateTime(int lastMottoUpdateTime) {
        this.lastMottoUpdateTime = lastMottoUpdateTime;
    }

    public int getLastClothingUpdateTime() {
        return lastClothingUpdateTime;
    }

    public void setLastClothingUpdateTime(int lastClothingUpdateTime) {
        this.lastClothingUpdateTime = lastClothingUpdateTime;
    }

    public int getLastForumMessageUpdateTime() {
        return lastForumMessageUpdateTime;
    }

    public void setLastForumMessageUpdateTime(int lastForumMessageUpdateTime) {
        this.lastForumMessageUpdateTime = lastForumMessageUpdateTime;
    }

    public int getGiftPurchasingWarnings() {
        return giftPurchasingWarnings;
    }

    public void setGiftPurchasingWarnings(int giftPurchasingWarnings) {
        this.giftPurchasingWarnings = giftPurchasingWarnings;
    }

    public int getMottoUpdateWarnings() {
        return mottoUpdateWarnings;
    }

    public void setMottoUpdateWarnings(int mottoUpdateWarnings) {
        this.mottoUpdateWarnings = mottoUpdateWarnings;
    }

    public int getClothingUpdateWarnings() {
        return clothingUpdateWarnings;
    }

    public void setClothingUpdateWarnings(int clothingUpdateWarnings) {
        this.clothingUpdateWarnings = clothingUpdateWarnings;
    }

    public Boolean getSessionGiftBlocked() {
        return sessionGiftBlocked;
    }

    public void setSessionGiftBlocked(Boolean sessionGiftBlocked) {
        this.sessionGiftBlocked = sessionGiftBlocked;
    }

    public Boolean getSessionMottoBlocked() {
        return sessionMottoBlocked;
    }

    public void setSessionMottoBlocked(Boolean sessionMottoBlocked) {
        this.sessionMottoBlocked = sessionMottoBlocked;
    }

    public Boolean getSessionClothingBlocked() {
        return sessionClothingBlocked;
    }

    public void setSessionClothingBlocked(Boolean sessionClothingBlocked) {
        this.sessionClothingBlocked = sessionClothingBlocked;
    }

    public FriendBarState getFriendBarState() {
        return friendBarState;
    }

    public CurrencyComponent getCurrencies() { return this.currencies; }

    public long getLastRewardTime() {
        return lastRewardTime;
    }

    public void setLastRewardTime(long lastRewardTime) {
        this.lastRewardTime = lastRewardTime;
    }

    public void decreaseCredits(int amount) {
        this.credits -= amount;
    }

    public void increaseCredits(int amount) {
        this.credits += amount;
    }

    public int getMessengerSpamTime() {
        return messengerSpamTime;
    }

    public void decreaseMessengerSpamTime(int time) {
        this.messengerSpamTime -= time;
    }

    public int getMessengerSpamCount() {
        return messengerSpamCount;
    }

    public void setMessengerSpamCount(int messengerSpamCount) {
        this.messengerSpamCount = messengerSpamCount;
    }

    public Boolean getChatpreference() {
        return chatpreference;
    }

    public void setChatpreference(Boolean chatpreference) {
        this.chatpreference = chatpreference;
    }

    public Boolean getFocuspreference() {
        return focuspreference;
    }

    public void setFocuspreference(Boolean focuspreference) {
        this.focuspreference = focuspreference;
    }

    public Boolean getIgnoreinvites() {
        return ignoreinvites;
    }

    public void setIgnoreinvites(Boolean ignoreinvites) {
        this.ignoreinvites = ignoreinvites;
    }
}
