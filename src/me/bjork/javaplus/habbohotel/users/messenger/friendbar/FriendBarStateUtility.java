package me.bjork.javaplus.habbohotel.users.messenger.friendbar;

/**
 * Created on 08/06/2017.
 */
public class FriendBarStateUtility {
    public static FriendBarState getEnum(int State) {
        switch (State) {
            default:
            case 0:
                return FriendBarState.Closed;

            case 1:
                return FriendBarState.Open;
        }
    }

    public static int getInt(FriendBarState State) {
        switch (State) {
            default:
            case Closed:
                return 0;

            case Open:
                return 1;
        }
    }
}
