package me.bjork.javaplus.habbohotel.users.messenger.friendbar;

/**
 * Created on 08/06/2017.
 */
public enum FriendBarState
{
    Open,
    Closed
}
