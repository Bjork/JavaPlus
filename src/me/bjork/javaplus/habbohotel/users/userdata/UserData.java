package me.bjork.javaplus.habbohotel.users.userdata;

import me.bjork.javaplus.habbohotel.users.Habbo;

/**
 * Created on 21/05/2017.
 */
public class UserData {
    public int userId;
    public Habbo user;

    public UserData(int userId, Habbo user)
    {
        this.userId = userId;
        this.user = user;
    }
}
