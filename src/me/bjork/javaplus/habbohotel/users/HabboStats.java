package me.bjork.javaplus.habbohotel.users;

import me.bjork.javaplus.JavaPlusEnvironment;
import me.bjork.javaplus.database.queries.players.HabboStatsDao;

/**
 * Created on 30/05/2017.
 */
public class HabboStats {
    private int userId;
    private int roomVisits;
    private double onlineTime;
    private double sessionStart;
    private int respects;
    private int respectGiven;
    private int giftsGiven;
    private int giftsReceived;
    private int dailyRespectPoints;
    private int dailyPetRespectPoints;
    private int achievementPoints;
    private int questId;
    private int questProgress;
    private int groupId;
    private String respectTimestamp;
    private int forumPosts;
    private int rentedItemId;
    private int rentExpire;

    public HabboStats(int userId, int roomVisits, double onlineTime, int respects, int respectGiven, int giftsGiven, int giftsReceived, int dailyRespectPoints, int dailyPetRespectPoints, int achievementPoints, int questId, int questProgress, int groupId, String respectTimestamp, int forumPosts, int rentedItemId, int rentExpire)
    {
        this.userId = userId;
        this.roomVisits = roomVisits;
        this.onlineTime = onlineTime;
        this.sessionStart = JavaPlusEnvironment.getUnixTimestamp();
        this.respects = respects;
        this.respectGiven = respectGiven;
        this.giftsGiven = giftsGiven;
        this.giftsReceived = giftsReceived;
        this.dailyRespectPoints = dailyRespectPoints;
        this.dailyPetRespectPoints = dailyPetRespectPoints;
        this.achievementPoints = achievementPoints;
        this.questId = questId;
        this.questProgress = questProgress;
        this.groupId = groupId;
        this.respectTimestamp = respectTimestamp;
        this.forumPosts = forumPosts;
        this.rentedItemId = rentedItemId;
        this.rentExpire = rentExpire;
    }

    public int getUserId() {
        return userId;
    }

    public int getDailyPetRespectPoints() {
        return dailyPetRespectPoints;
    }

    public void setDailyPetRespectPoints(int dailyPetRespectPoints) {
        this.dailyPetRespectPoints = dailyPetRespectPoints;
    }

    public int getDailyRespectPoints() {
        return dailyRespectPoints;
    }

    public void setDailyRespectPoints(int dailyRespectPoints) {
        this.dailyRespectPoints = dailyRespectPoints;
    }

    public int getRespects() {
        return respects;
    }

    public void setRespects(int respects) {
        this.respects = respects;
    }

    public int getRespectGiven() {
        return respectGiven;
    }

    public void setRespectGiven(int respectGiven) {
        this.respectGiven = respectGiven;
    }

    public int getRoomVisits() {
        return roomVisits;
    }

    public void setRoomVisits(int roomVisits) {
        this.roomVisits = roomVisits;
    }

    public double getOnlineTime() {
        return onlineTime;
    }

    public void setOnlineTime(double onlineTime) {
        this.onlineTime = onlineTime;
    }

    public int getGiftsReceived() {
        return giftsReceived;
    }

    public void setGiftsReceived(int giftsReceived) {
        this.giftsReceived = giftsReceived;
    }

    public int getAchievementPoints() {
        return achievementPoints;
    }

    public void setAchievementPoints(int achievementPoints) {
        this.achievementPoints = achievementPoints;
    }

    public int getGiftsGiven() {
        return giftsGiven;
    }

    public void setGiftsGiven(int giftsGiven) {
        this.giftsGiven = giftsGiven;
    }

    public int getQuestId() {
        return questId;
    }

    public void setQuestId(int questId) {
        this.questId = questId;
    }

    public int getQuestProgress() {
        return questProgress;
    }

    public void setQuestProgress(int questProgress) {
        this.questProgress = questProgress;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public String getRespectTimestamp() {
        return respectTimestamp;
    }

    public void setRespectTimestamp(String respectTimestamp) {
        this.respectTimestamp = respectTimestamp;
    }

    public int getForumPosts() {
        return forumPosts;
    }

    public void setForumPosts(int forumPosts) {
        this.forumPosts = forumPosts;
    }

    public int getRentedItemId() {
        return rentedItemId;
    }

    public void setRentedItemId(int rentedItemId) {
        this.rentedItemId = rentedItemId;
    }

    public int getRentExpire() {
        return rentExpire;
    }

    public void setRentExpire(int rentExpire) {
        this.rentExpire = rentExpire;
    }

    public void incrementAchievementPoints(int amount) {
        this.achievementPoints += amount;
        this.save();
    }

    public void incrementRespectPoints(int amount) {
        this.respects += amount;
        this.save();
    }

    public void incrementGivenRespectPoints(int amount) {
        this.respectGiven += amount;
        this.save();
    }

    public void decrementDailyRespects(int amount) {
        this.dailyRespectPoints -= amount;
        this.save();
    }

    public void decrementDailyPetRespects(int amount) {
        this.dailyPetRespectPoints -= amount;
        this.save();
    }

    public void save() {
        HabboStatsDao.updatePlayerStatistics(this);
    }

    public double getSessionStart() {
        return sessionStart;
    }
}
