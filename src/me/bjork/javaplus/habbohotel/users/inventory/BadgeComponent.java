package me.bjork.javaplus.habbohotel.users.inventory;

import me.bjork.javaplus.database.queries.players.HabboDao;
import me.bjork.javaplus.habbohotel.users.Habbo;

import java.util.*;

/**
 * Created on 30/05/2017.
 */
public class BadgeComponent {
    private Habbo habbo;
    private final Map<String, Badge> badges;

    public BadgeComponent(Habbo habbo)
    {
        this.habbo = habbo;
        this.badges = new HashMap<String, Badge>();

        for (Badge badge : this.badges.values())
        {
            if (!this.badges.containsKey(badge))
                this.badges.put(badge.getCode(), badge);
        }
    }

    public ArrayList<Badge> getBadgesSlot()
    {
        ArrayList<Badge> badges = new ArrayList<Badge>();

        for(Badge badge : this.badges.values())
        {
            if (badge.getSlot() <= 0)
                continue;
            badges.add(badge);
        }

        Collections.sort(badges, new Comparator<Badge>()
        {
            @Override
            public int compare(Badge o1, Badge o2)
            {
                return o1.getSlot() - o2.getSlot();
            }
        });
        return badges;
    }

    public static ArrayList<Badge> getBadgesOfflineHabbo(int userId) {
        ArrayList<Badge> badgesList = new ArrayList<Badge>();
        badgesList.addAll(HabboDao.getUserBadges(userId));
        return badgesList;
    }

    public void dispose() {
        synchronized (this.badges)
        {
            this.badges.clear();
        }
    }
}
