package me.bjork.javaplus.habbohotel.users.inventory;

import me.bjork.javaplus.habbohotel.users.Habbo;

/**
 * Created on 30/05/2017.
 */
public class InventoryComponent {

    private BadgeComponent badgesComponent;

    public InventoryComponent(Habbo habbo)
    {
        this.badgesComponent = new BadgeComponent(habbo);
    }

    public BadgeComponent getBadges() { return this.badgesComponent; }

    public void dispose() {
        this.badgesComponent.dispose();
        this.badgesComponent = null;
    }
}
