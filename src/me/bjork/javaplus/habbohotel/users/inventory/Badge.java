package me.bjork.javaplus.habbohotel.users.inventory;

/**
 * Created on 30/05/2017.
 */
public class Badge {
    private String Code;
    private int Slot;

    public Badge(String Code, int Slot)
    {
        this.Code = Code;
        this.Slot = Slot;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }

    public int getSlot() {
        return Slot;
    }

    public void setSlot(int slot) {
        Slot = slot;
    }
}
