package me.bjork.javaplus.habbohotel.users.currencies.type;

/**
 * Created on 08/06/2017.
 */
public class CurrencyType {
    private int type;
    private int amount;

    public CurrencyType(int type, int amount) {
        this.type = type;
        this.amount = amount;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
