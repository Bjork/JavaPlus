package me.bjork.javaplus.habbohotel.users.currencies;

import me.bjork.javaplus.JavaPlusEnvironment;
import me.bjork.javaplus.communication.packets.outgoing.inventory.purse.HabboActivityPointNotificationComposer;
import me.bjork.javaplus.database.queries.players.CurrenciesDao;
import me.bjork.javaplus.habbohotel.currencies.CurrencyDefinition;
import me.bjork.javaplus.habbohotel.users.Habbo;
import me.bjork.javaplus.habbohotel.users.currencies.type.CurrencyType;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Created on 08/06/2017.
 */
public class CurrencyComponent {
    private Habbo habbo;
    private Map<Integer, CurrencyType> currencies;

    public CurrencyComponent(Habbo habbo) {
        this.habbo = habbo;
        this.currencies = new HashMap<Integer, CurrencyType>();
        this.init();
    }

    private void init() {
        if (this.currencies.size() > 0)
            this.currencies.clear();

        this.currencies = CurrenciesDao.getUserCurrencies(habbo.getId());

        if (this.currencies == null) {
            CurrenciesDao.createDefaultCurrency(habbo.getId());
            this.currencies = CurrenciesDao.getUserCurrencies(habbo.getId());
        }
    }

    public void checkCurrencyTimer()
    {
        for (CurrencyType type : this.getCurrencies().values())
        {
            CurrencyDefinition definition = JavaPlusEnvironment.getGame().getCurrencyManager().getCurrencyByType(type.getType());
            if (definition == null)
                continue;

            type.setAmount(type.getAmount() + definition.getReward());

            if (habbo != null)
                habbo.getClient().send(new HabboActivityPointNotificationComposer(type.getAmount(), definition.getReward(), type.getType()));
        }
    }

    public CurrencyType getCurrency(int type, boolean addToDatabase) {
        if (!this.currencies.containsKey(type) && addToDatabase) {
            CurrenciesDao.createCurrency(type, habbo.getId());
            this.currencies.put(type, new CurrencyType(type, 0));
        }
        return this.currencies.get(type);
    }

    public void saveUserCurrencies()
    {
        for (CurrencyType type : this.getCurrencies().values()) {
            CurrenciesDao.save(type.getAmount(), type.getType(), habbo.getId());
        }
    }

    public void dispose()
    {
        this.currencies.clear();
    }

    public Map<Integer, CurrencyType> getCurrencies() { return this.currencies; }
    public Collection<CurrencyType> getAllCurrencies() { return this.currencies.values(); }
}
