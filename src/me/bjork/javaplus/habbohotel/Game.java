package me.bjork.javaplus.habbohotel;

import me.bjork.javaplus.habbohotel.currencies.CurrencyManager;
import me.bjork.javaplus.habbohotel.gameclients.GameClientManager;
import me.bjork.javaplus.habbohotel.groups.GroupManager;
import me.bjork.javaplus.habbohotel.moderation.BanManager;
import me.bjork.javaplus.habbohotel.moderation.MuteManager;
import me.bjork.javaplus.habbohotel.navigator.NavigatorManager;
import me.bjork.javaplus.habbohotel.permissions.PermissionManager;
import me.bjork.javaplus.habbohotel.rooms.RoomManager;
import me.bjork.javaplus.habbohotel.rooms.chat.ChatManager;
import me.bjork.javaplus.habbohotel.rooms.cycle.RoomCycle;

/**
 * Created on 19/05/2017.
 */
public class Game {
    private GameClientManager gameClientManager;
    private NavigatorManager navigatorManager;
    private RoomManager roomManager;
    private GroupManager groupManager;
    private PermissionManager permissionManager;
    private GameCycle gameCycle;
    private RoomCycle roomCycle;
    private MuteManager muteManager;
    private BanManager banManager;
    private ChatManager chatManager;
    private CurrencyManager currencyManager;

    public Game() {
        this.gameClientManager = new GameClientManager();
        this.navigatorManager = new NavigatorManager();
        this.roomManager = new RoomManager();
        this.groupManager = new GroupManager();
        this.permissionManager = new PermissionManager();
        this.permissionManager.initialize();
        this.chatManager = new ChatManager();
        this.currencyManager = new CurrencyManager();

        this.gameCycle = new GameCycle();
        this.gameCycle.start();
        this.roomCycle = new RoomCycle();
        this.roomCycle.start();

        this.muteManager = new MuteManager();
        this.banManager = new BanManager();
    }

    public GameClientManager getGameClientManager() { return gameClientManager; }
    public GroupManager getGroupManager() {
        return groupManager;
    }
    public RoomManager getRoomManager() {
        return roomManager;
    }
    public NavigatorManager getNavigatorManager() {
        return navigatorManager;
    }
    public PermissionManager getPermissionManager() {
        return permissionManager;
    }
    public GameCycle getGameCycle() { return gameCycle; }
    public RoomCycle getRoomCycle() { return roomCycle; }
    public MuteManager getMuteManager() { return muteManager; }
    public BanManager getBanManager() { return banManager; }
    public ChatManager getChatManager() {
        return chatManager;
    }
    public CurrencyManager getCurrencyManager() { return currencyManager; }
}
