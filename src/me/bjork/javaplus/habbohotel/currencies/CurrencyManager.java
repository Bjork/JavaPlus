package me.bjork.javaplus.habbohotel.currencies;

import me.bjork.javaplus.core.Logging;
import me.bjork.javaplus.database.queries.players.CurrenciesDao;

import java.util.HashMap;
import java.util.Map;

/**
 * Created on 08/06/2017.
 */
public class CurrencyManager {
    private Map<Integer, CurrencyDefinition> currencies;

    public CurrencyManager() {
        this.currencies = new HashMap<Integer, CurrencyDefinition>();
        this.init();
    }

    private void init() {
        if (this.currencies.size() > 0)
            this.currencies.clear();

        this.currencies = CurrenciesDao.getCurrenciesDefinitions();

        Logging.println("Loaded " + this.currencies.size() + " Currency Definitions.");
    }

    public CurrencyDefinition tryGetCurrency(String currencyName) {
        if (this.currencies.containsKey(currencyName.toUpperCase()))
            return this.currencies.get(currencyName.toUpperCase());
        return null;
    }

    public CurrencyDefinition getCurrencyByType(int currencyType)
    {
        for (CurrencyDefinition definition : this.currencies.values()) {
            if (definition.getType() == currencyType)
                return definition;
        }
        return null;
    }
}
