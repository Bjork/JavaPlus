package me.bjork.javaplus.habbohotel.currencies;

/**
 * Created on 08/06/2017.
 */
public class CurrencyDefinition {
    private String name;
    private int type;
    private int reward;

    public CurrencyDefinition(String name, int type, int reward) {
        this.name = name;
        this.type = type;
        this.reward = reward;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getReward() {
        return reward;
    }

    public void setReward(int reward) {
        this.reward = reward;
    }
}
