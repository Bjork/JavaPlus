package me.bjork.javaplus.habbohotel;

import me.bjork.javaplus.JavaPlusEnvironment;
import me.bjork.javaplus.communication.packets.outgoing.handshake.UserObjectComposer;
import me.bjork.javaplus.communication.packets.outgoing.inventory.CreditBalanceComposer;
import me.bjork.javaplus.core.Logging;
import me.bjork.javaplus.core.settings.SettingsManager;
import me.bjork.javaplus.core.utils.Util;
import me.bjork.javaplus.database.queries.players.HabboStatsDao;
import me.bjork.javaplus.database.queries.system.StatisticsDao;
import me.bjork.javaplus.habbohotel.gameclients.GameClient;
import me.bjork.javaplus.habbohotel.rooms.RoomManager;

import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 * Created on 29/05/2017.
 */
public class GameCycle implements Runnable {
    private static final int interval = 1;
    private ScheduledFuture gameFuture;
    private boolean active = false;

    public GameCycle() {
    }

    public void start() {
        this.gameFuture = RoomManager.getScheduler().scheduleAtFixedRate(this, interval, interval, TimeUnit.MINUTES);
        this.active = true;
    }

    @Override
    public void run() {
        try {
            if (!this.active) {
                return;
            }

            JavaPlusEnvironment.getGame().getMuteManager().processMute();
            processUsers();
            StatisticsDao.saveStatistics(JavaPlusEnvironment.getGame().getGameClientManager().getSessions().size(), JavaPlusEnvironment.getGame().getRoomManager().getActiveRooms().size());

        } catch (Exception e) {
            Logging.printError("Error during game thread", e);
        }
    }

    private void processUsers() throws Exception {
        for (GameClient client : JavaPlusEnvironment.getGame().getGameClientManager().getSessions().values()) {
            try {
                if (client == null || client.getHabbo() == null) {
                    continue;
                }

                if ((JavaPlusEnvironment.getIntUnixTimestamp() - client.getLastPing()) >= 300) {
                    client.disconnect();
                    continue;
                }

                if (!client.getHabbo().getStats().getRespectTimestamp().equals(Util.statsDate())) {
                    client.getHabbo().getStats().setRespectTimestamp(Util.statsDate());

                    int dailyRespects = client.getHabbo().getStats().getDailyRespectPoints() + 5;
                    int dailyPetRespects = client.getHabbo().getStats().getDailyPetRespectPoints() + 5;

                    if (client.getHabbo().getPermissions().hasRight("mod_tool")) {
                        dailyRespects = client.getHabbo().getStats().getDailyRespectPoints() + 20;
                        dailyPetRespects = client.getHabbo().getStats().getDailyPetRespectPoints() + 5;
                    }
                    /*else if (PlusEnvironment.GetGame().GetSubscriptionManager().TryGetSubscriptionData(VIPRank, out SubData))
                                    DailyRespects = SubData.Respects;*/ //TODO subcription respect settings

                    if (dailyRespects > 100)
                        dailyRespects = 100;

                    if (dailyPetRespects > 100)
                        dailyPetRespects = 100;

                    client.getHabbo().getStats().setDailyRespectPoints(dailyRespects);
                    client.getHabbo().getStats().setDailyPetRespectPoints(dailyPetRespects);

                    HabboStatsDao.updateDailyRespects(client.getHabbo());
                    client.send(new UserObjectComposer(client.getHabbo()));
                }

                //((GameClient) client).getHabbo().getAchievements().progressAchievement(AchievementType.ONLINE_TIME, 1); //TODO Acheivement OnlineTime
                final boolean needsReward = (JavaPlusEnvironment.getIntUnixTimestamp() - client.getHabbo().getLastRewardTime()) >= (60 * Integer.valueOf(SettingsManager.tryGetValue("user.currency_scheduler.tick")));
                if (needsReward) {
                    client.getHabbo().setLastRewardTime(JavaPlusEnvironment.getIntUnixTimestamp());
                    int creditUpdate = Integer.valueOf(SettingsManager.tryGetValue("user.currency_scheduler.credit_reward"));

                    /*if (PlusEnvironment.GetGame().GetSubscriptionManager().TryGetSubscriptionData(this._vipRank, out SubscriptionData SubData)) //TODO Subscriptions settings
                        CreditUpdate += SubData.Credits;*/

                    if (creditUpdate > 0) {
                        client.getHabbo().increaseCredits(creditUpdate);
                    }

                    client.send(new CreditBalanceComposer(client.getHabbo().getCredits()));
                    client.getHabbo().saveUser();

                    client.getHabbo().getCurrencies().checkCurrencyTimer();
                    client.getHabbo().getCurrencies().saveUserCurrencies();
                }

                if (client.getHabbo().getMessengerSpamTime() > 0)
                    client.getHabbo().decreaseMessengerSpamTime(60);
                if (client.getHabbo().getMessengerSpamTime() <= 0)
                    client.getHabbo().setMessengerSpamCount(0);

                if (client.getHabbo().getGiftPurchasingWarnings() < 15)
                    client.getHabbo().setGiftPurchasingWarnings(0);
                if (client.getHabbo().getMottoUpdateWarnings() < 15)
                    client.getHabbo().setMottoUpdateWarnings(0);
                if (client.getHabbo().getClothingUpdateWarnings() < 15)
                    client.getHabbo().setClothingUpdateWarnings(0);

            } catch (Exception e) {
                Logging.printError("Error while cycling rewards", e);
            }
        }
    }

    public void stop() {
        this.active = false;
        this.gameFuture.cancel(false);
    }
}
