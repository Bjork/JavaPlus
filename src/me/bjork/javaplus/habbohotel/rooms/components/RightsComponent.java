package me.bjork.javaplus.habbohotel.rooms.components;

import me.bjork.javaplus.JavaPlusEnvironment;
import me.bjork.javaplus.core.Logging;
import me.bjork.javaplus.database.queries.rooms.RightsDao;
import me.bjork.javaplus.habbohotel.gameclients.GameClient;
import me.bjork.javaplus.habbohotel.rooms.Room;
import me.bjork.javaplus.habbohotel.rooms.components.types.RoomBan;
import me.bjork.javaplus.habbohotel.rooms.components.types.RoomMute;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created on 24/05/2017.
 */
public class RightsComponent {
    private Room room;

    private List<Integer> rights;
    private Map<Integer, RoomBan> bannedPlayers;
    private Map<Integer, RoomMute> mutedPlayers;

    public RightsComponent(Room room) {
        this.room = room;

        try {
            this.rights = RightsDao.getRightsByRoomId(room.getId());
        } catch (Exception e) {
            this.rights = new CopyOnWriteArrayList<>();
            Logging.exception(e);
        }

        this.bannedPlayers = RightsDao.getRoomBansByRoomId(this.room.getId());
        this.mutedPlayers = new ConcurrentHashMap<>();
    }

    public void dispose() {
        this.rights.clear();
        this.bannedPlayers.clear();
        this.room = null;
    }

    public boolean hasRights(GameClient session) {

            /*final Group group = this.getRoom().getGroup();

            if (group != null && group.getData() != null && group.getMembershipComponent() != null && group.getMembershipComponent().getMembers() != null) {
                if (group.getData().canMembersDecorate() && group.getMembershipComponent().getMembers().containsKey(session.getHabbo().getId())) {
                    return true;
                }

                if (group.getMembershipComponent().getAdministrators().contains(session.getHabbo().getId())) {
                    return true;
                }
            }*/ //Todo group check rights

        if (session.getHabbo().getPermissions().hasRight("room_any_owner"))
            return true;

        if (this.room.getOwnerId() == session.getHabbo().getId())
            return true;

        if (this.rights.contains(session.getHabbo().getId()) || session.getHabbo().getPermissions().hasRight("room_any_rights"))
            return true;

        return false;
    }

    /*public boolean canPlaceFurniture(final int playerId) {
        final Group group = this.getRoom().getGroup();

        if (group != null && group.getData() != null && group.getMembershipComponent() != null && group.getMembershipComponent().getMembers() != null) {
            if (group.getData().canMembersDecorate() && group.getMembershipComponent().getMembers().containsKey(playerId)) {
                return true;
            }

            if (group.getMembershipComponent().getAdministrators().contains(playerId)) {
                return true;
            }
        }

        if(this.hasRights(playerId, false) && CometSettings.playerRightsItemPlacement) {
            return true;
        }

        return this.room.getData().getOwnerId() == playerId;
    }*/

    /*public void removeRights(int playerId) {
        if (this.rights.contains(playerId)) {
            this.rights.remove(rights.indexOf(playerId));
            RightsDao.delete(playerId, room.getId());
        }
    }*/

    /*public void addRights(int playerId) {
        this.rights.add(playerId);
        RightsDao.add(playerId, this.room.getId());
    }*/

    /*public void addBan(int playerId, String playerName, int expireTimestamp) {
        this.bannedPlayers.put(playerId, new RoomBan(playerId, playerName, expireTimestamp));
        RightsDao.addRoomBan(playerId, this.room.getId(), expireTimestamp);
    }*/

    public void addMute(int playerId, int minutes) {
        this.mutedPlayers.put(playerId, new RoomMute(playerId, (minutes * 60) * 2));
    }

    public boolean hasBan(int userId) {
        return this.bannedPlayers.containsKey(userId);
    }

    /*public void removeBan(int playerId) {
        this.bannedPlayers.remove(playerId);

        // delete it from the db.
        RightsDao.deleteRoomBan(playerId, this.room.getId());
    }*/

    public boolean hasMute(int playerId) {
        return this.mutedPlayers.containsKey(playerId);
    }

    public int getMuteTime(int playerId) {
        if(this.hasMute(playerId)) {
            return this.mutedPlayers.get(playerId).getTicksLeft() / 2;
        }

        return 0;
    }

    public void tick() {
        List<RoomBan> bansToRemove = new ArrayList<>();
        List<RoomMute> mutesToRemove = new ArrayList<>();

        for (RoomBan ban : this.bannedPlayers.values()) {
            if (ban.getExpireTimestamp() <= (int) JavaPlusEnvironment.getUnixTimestamp() && !ban.isPermanent()) {
                bansToRemove.add(ban);
            }
        }

        for (RoomMute mute : this.mutedPlayers.values()) {
            if (mute.getTicksLeft() <= 0) {
                mutesToRemove.add(mute);
            }

            mute.decreaseTicks();
        }


        for (RoomBan ban : bansToRemove) {
            this.bannedPlayers.remove(ban.getPlayerId());
        }

        for (RoomMute mute : mutesToRemove) {
            this.mutedPlayers.remove(mute.getPlayerId());
        }

        bansToRemove.clear();
        mutesToRemove.clear();
    }

    public Map<Integer, RoomBan> getBannedPlayers() {
        return this.bannedPlayers;
    }

    public List<Integer> getAll() {
        return this.rights;
    }

    public Room getRoom() {
        return this.room;
    }
}
