package me.bjork.javaplus.habbohotel.rooms.components;

import me.bjork.javaplus.communication.packets.outgoing.rooms.engine.UserStatusMessageComposer;
import me.bjork.javaplus.core.Logging;
import me.bjork.javaplus.core.settings.SettingsManager;
import me.bjork.javaplus.core.utils.TimeSpan;
import me.bjork.javaplus.habbohotel.rooms.Room;
import me.bjork.javaplus.habbohotel.rooms.RoomManager;
import me.bjork.javaplus.habbohotel.rooms.RoomUser;
import me.bjork.javaplus.habbohotel.rooms.models.types.mapping.Rotation;
import me.bjork.javaplus.habbohotel.rooms.models.types.tiles.RoomTile;
import me.bjork.javaplus.habbohotel.rooms.objects.users.RoomUserStatus;

import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 * Created on 26/05/2017.
 */
public class ProcessComponent implements Runnable {

    private ScheduledFuture processFuture;
    private Room room;
    private boolean active = false;

    private List<Long> processTimes;

    private long lastProcess = 0;

    private boolean isProcessing = false;

    private List<RoomUser> playersToRemove;
    private List<RoomUser> playersToUpdate;

    public ProcessComponent(Room room) {
        this.room = room;
    }

    public void start() {
        if (this.active) {
            stop();
        }

        this.processFuture = RoomManager.getScheduler().scheduleAtFixedRate(this, 500, 500, TimeUnit.MILLISECONDS);
        this.active = true;

        Logging.printDebug("Room Processing started");
    }

    public void stop() {

        if (this.getProcessTimes() != null) {
            this.getProcessTimes().clear();
        }

        if (this.processFuture != null) {
            this.active = false;

            Logging.printDebug("Room Processing stopped");
        }
    }

    @Override
    public void run() {
        this.tick();
    }

    private void tick() {
        if (!this.active) {
            return;
        }

        if (this.isProcessing) return;

        this.isProcessing = true;

        long timeSinceLastProcess = this.lastProcess == 0 ? 0 : (System.currentTimeMillis() - this.lastProcess);
        this.lastProcess = System.currentTimeMillis();

        if (this.getProcessTimes() != null && this.getProcessTimes().size() < 30) {
            Logging.printDebug("Time since last process: " + timeSinceLastProcess + "ms");
        }

        long timeStart = System.currentTimeMillis();

        try {
            this.getRoom().tick();
        } catch (Exception e) {
            Logging.printError("Error while cycling room: " + room.getId() + ", " + room.getCaption(), e);
            Logging.exception(e);
        }

        try {
           List<RoomUser> entities = this.room.getRoomUserManager().getRoomUsers();

            playersToRemove = new ArrayList<>();
            playersToUpdate = new ArrayList<>();

            for (RoomUser entity : entities) {
                this.startProcessing(entity);
            }

            // only send the updates if we need to
            if (playersToUpdate.size() > 0) {
                this.getRoom().send(new UserStatusMessageComposer(playersToUpdate));
            }

            for (RoomUser entity : playersToUpdate) {
                if (entity.updatePhase == 1) continue;

                if (this.updateEntityStuff(entity) && !entity.isBot() && !entity.isPet()) {
                    playersToRemove.add(entity);
                }
            }

            for (RoomUser entity : playersToRemove) {
                entity.removeUserFromRoom(entity.getHabbo() == null, true, false);
            }

            playersToRemove.clear();
            playersToUpdate.clear();

            playersToRemove = null;
            playersToUpdate = null;

        } catch (Exception e) {
            Logging.printError("Error during room entity processing", e);
        }

        TimeSpan span = new TimeSpan(timeStart, System.currentTimeMillis());

        if (this.getProcessTimes() != null) {
            if (this.getProcessTimes().size() < 30)
                this.getProcessTimes().add(span.toMilliseconds());
        }
        this.isProcessing = false;
    }

    private void startProcessing(RoomUser user) {
        if (!user.isBot() && !user.isPet()) {

            try {
                if (user.getClient() == null || user.getClient().getHabbo() == null || user.getClient().getHabbo().isDisposed) {
                    playersToRemove.add(user);
                    return;
                }
            } catch (Exception e) {
                Logging.printError("Failed to remove null player from room - user data was null");
                return;
            }

            boolean playerNeedsRemove = processEntity(user);
            if (playerNeedsRemove) {
                playersToRemove.add(user);
            }
        } else if (user.isBot() && !user.isPet()) {
            // do anything special here for bots?
            processEntity(user);
        } else if (!user.isBot() && user.isPet()) {
            if (user.getHorseId() == 0) {
                // do anything special here for pets?
                processEntity(user);
            }
        }

        if ((user.needsUpdate() && !user.needsUpdateCancel() || user.needsForcedUpdate) && user.isVisible()) {
            if (user.needsForcedUpdate && user.updatePhase == 1) {
                user.needsForcedUpdate = false;
                user.updatePhase = 0;
                playersToUpdate.add(user);
            } else if (user.needsForcedUpdate) {
                if (user.hasStatus(RoomUserStatus.MOVE)) {
                    user.removeStatus(RoomUserStatus.MOVE);
                }
                user.updatePhase = 1;
                playersToUpdate.add(user);
            } else {
                if (user.isPet()) {
                    if (user.getHorseId() > 0) {
                        processEntity(user);
                        user.getMountedEntity().markUpdateComplete();
                        playersToUpdate.add(user.getMountedEntity());
                    }
                }
                user.markUpdateComplete();
                playersToUpdate.add(user);
            }
        }
    }

    private boolean updateEntityStuff(RoomUser user) { //TODO
        return false;
    }

    public boolean isActive() {
        return this.active;
    }

    private boolean processEntity(RoomUser user) {
        return this.processEntity(user, false);
    }

    private boolean processEntity(RoomUser user, boolean isRetry) {
        boolean isPlayer = (!user.isBot() && !user.isPet());

        if (isPlayer && user.getClient().getHabbo() == null || user.getRoom() == null) {
            return true; // adds it to the to remove list automatically..
        }

        if (!isRetry) {
            if (isPlayer) {
                if (user.getClient().getHabbo().getRoomFloodTime() >= 0.5) {
                    user.getClient().getHabbo().setRoomFloodTime(user.getClient().getHabbo().getRoomFloodTime() - 1);

                    if (user.getClient().getHabbo().getRoomFloodTime() < 0) {
                        user.getClient().getHabbo().setRoomFloodTime(0);
                    }
                }
            } else {
                if (user.isBot() && user.getBotAI() != null) {
                    //user.BotAI.onTick(); TODO Bot AI onTick
                }
            }

            if (user.handItemNeedsRemove() && user.getHandItem() != 0) {
                user.carryItem(0);
                user.setHandItemTimer(0);
            }

            // Handle signs
            if (user.hasStatus(RoomUserStatus.SIGN) && !user.isDisplayingSign()) {
                user.removeStatus(RoomUserStatus.SIGN);
                user.markNeedsUpdate();
            }

            if (!user.isBot() && user.isIdleAndIncrement() && user.isVisible()) {
                if (user.getIdleTime() >= 60 * Integer.valueOf(SettingsManager.tryGetValue("room.idle_time")) * 2) {
                    if (this.getRoom().getOwnerId() != user.getId())
                        return true;
                }
            }
        }

        try
        {
            if (user.hasStatus(RoomUserStatus.MOVE)) {
                user.removeStatus(RoomUserStatus.MOVE);
                user.markNeedsUpdate();
            }

            if (user.isWalking()) { //TODO Fastwalk ?
                if (user.getPath() == null || user.getPath().isEmpty())
                    return false;

                if (user.getPath().size() > 0) {

                    RoomTile next = user.getPath().poll();

                    if (user.getPath().isEmpty())
                    {
                        //this.sitUpdate = true;
                    }

                    Deque<RoomTile> peekPath = room.getModel().findPath(room, user.getCurrentLocation(), user.getPath().peek());
                    if (peekPath.size() >= 3)
                    {
                        peekPath.pop(); //Start
                        peekPath.removeLast(); //End

                        if (peekPath.peek() != next)
                        {
                            next = peekPath.poll();
                            for (int i = 0; i < peekPath.size(); i++)
                            {
                                user.getPath().addFirst(peekPath.removeLast());
                            }
                        }
                    }

                    if (next == null) {
                        return false;
                    }

                    user.removeStatus(RoomUserStatus.SIT);
                    user.removeStatus(RoomUserStatus.LAY);

                    if (!room.tileWalkable(next.x, next.y) /*&& !(item instanceof InteractionTeleport)*/) //TODO
                    {
                        Deque<RoomTile> path = new LinkedList<>(user.getPath());
                        user.getPath().clear();
                        RoomTile newGoal = next;
                        if (!path.isEmpty())
                        {
                            newGoal = path.pop();
                        }
                        RoomTile oldGoal = user.getGoal();
                        user.setGoalLocation(room.getModel().getTile(newGoal.x, newGoal.y));
                        user.findPath();
                        if (user.getPath().size() > 0) {
                            for (int i = 0; i < user.getPath().size(); i++) {
                                path.addFirst(user.getPath().pollLast());
                            }
                        }
                        user.setGoalLocation(oldGoal);
                        user.getPath().clear();
                        user.getPath().addAll(path);
                        if (user.getPath().isEmpty())
                        {
                            room.send(new UserStatusMessageComposer(user));
                            return false;
                        }

                        next = user.getPath().poll();
                    }

                    double zHeight = 0.0; //TODO Hauteur Items
                    zHeight += room.getModel().getHeightAtSquare(next.x, next.y);

                    user.setPreviousLocation(user.getCurrentLocation());
                    user.addStatus(RoomUserStatus.MOVE, next.x + "," + next.y + "," + zHeight);

                    if (user.getMountedEntity() != null) {
                        //TODO Chercher le cheval pour update sa position
                        user.addStatus(RoomUserStatus.MOVE, next.x + "," + next.y + "," + (zHeight - 1.0));
                    }

                    int newRot = Rotation.calculate(user.getCurrentLocation().x, user.getCurrentLocation().y, next.x, next.y);
                    user.setBodyRotation(newRot);
                    user.setHeadRotation(newRot);
                    user.setCurrentLocation(room.getModel().getTile((short) next.x, (short) next.y));
                    user.setZ(zHeight);
                    user.markNeedsUpdate();

                    if ((next.x == room.getModel().getDoorX()) && (next.y == room.getModel().getDoorY())) {
                        return true;
                    }
                }
            }
        }
        catch (Exception e)
        {
            Logging.exception(e);
        }

        if (user.getCurrentEffect() != null) {
            user.getCurrentEffect().decrementDuration();

            if (user.getCurrentEffect().getDuration() == 0 && user.getCurrentEffect().expires()) {
                user.applyEffect(user.getLastEffect() != null ? user.getLastEffect() : null);

                if (user.getLastEffect() != null)
                    user.setLastEffect(null);
            }
        }
        return false;
    }

    public Room getRoom() {
        return this.room;
    }

    public List<Long> getProcessTimes() {
        return processTimes;
    }

    public void setProcessTimes(List<Long> processTimes) {
        this.processTimes = processTimes;
    }
}
