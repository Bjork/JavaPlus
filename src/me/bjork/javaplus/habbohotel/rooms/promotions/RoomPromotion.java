package me.bjork.javaplus.habbohotel.rooms.promotions;

import me.bjork.javaplus.JavaPlusEnvironment;

/**
 * Created on 23/05/2017.
 */
public class RoomPromotion {
    public static final int DEFAULT_PROMO_LENGTH = 120;
    private int roomId;
    private String promotionName;
    private String promotionDescription;
    private long timestampStart;
    private long timestampFinish;
    private int categoryId;

    public RoomPromotion(int roomId, String name, String description, int categoryId) {
        this.roomId = roomId;
        this.promotionName = name;
        this.promotionDescription = description;
        this.timestampStart = JavaPlusEnvironment.getUnixTimestamp();
        this.timestampFinish = this.timestampStart + (DEFAULT_PROMO_LENGTH * 60);
        this.categoryId = categoryId;
    }

    public RoomPromotion(int roomId, String name, String description, long start, long finish, int categoryId) {
        this.roomId = roomId;
        this.promotionName = name;
        this.promotionDescription = description;
        this.timestampStart = start;
        this.timestampFinish = finish;
        this.categoryId = categoryId;
    }

    public int getRoomId() {
        return this.roomId;
    }

    public String getPromotionName() {
        return this.promotionName;
    }

    public String getPromotionDescription() {
        return this.promotionDescription;
    }

    public boolean isExpired() {
        return JavaPlusEnvironment.getUnixTimestamp() >= this.timestampFinish;
    }

    public long getTimestampStart() {
        return this.timestampStart;
    }

    public long getTimestampFinish() {
        return this.timestampFinish;
    }

    public void setTimestampFinish(long timestampFinish) {
        this.timestampFinish = timestampFinish;
    }

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }

    public void setPromotionName(String promotionName) {
        this.promotionName = promotionName;
    }

    public void setPromotionDescription(String promotionDescription) {
        this.promotionDescription = promotionDescription;
    }

    public void setTimestampStart(long timestampStart) {
        this.timestampStart = timestampStart;
    }

    public int minutesLeft() {
        return (int) Math.floor(this.getTimestampFinish() - JavaPlusEnvironment.getUnixTimestamp()) / 60;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }
}