package me.bjork.javaplus.habbohotel.rooms.cycle;

import com.google.common.collect.Lists;
import me.bjork.javaplus.JavaPlusEnvironment;
import me.bjork.javaplus.core.Logging;
import me.bjork.javaplus.core.utils.TimeSpan;
import me.bjork.javaplus.habbohotel.rooms.RoomManager;

import java.util.List;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 * Created on 26/05/2017.
 */
public class RoomCycle implements Runnable {

    private final static int PERIOD = 500;
    private final static int FLAG = 2000;

    private ScheduledFuture myFuture;

    public RoomCycle() {
    }

    public void start() {
        this.myFuture = RoomManager.getScheduler().scheduleAtFixedRate(this, PERIOD, PERIOD, TimeUnit.MILLISECONDS);
    }

    public void stop() {
        this.myFuture.cancel(false);
    }

    @Override
    public void run() {
        try {
            long start = System.currentTimeMillis();

            JavaPlusEnvironment.getGame().getRoomManager().unloadIdleRooms();

            List<Integer> expiredPromotedRooms = Lists.newArrayList();

            /*for (RoomPromotion roomPromotion : RoomManager.getInstance().getRoomPromotions().values()) {
                if (roomPromotion.isExpired()) {
                    expiredPromotedRooms.add(roomPromotion.getRoomId());
                }
            }*/

            /*if (expiredPromotedRooms.size() != 0) {
                for (int roomId : expiredPromotedRooms) {
                    RoomManager.getInstance().getRoomPromotions().remove(roomId);
                }

                expiredPromotedRooms.clear();
            }*/

            TimeSpan span = new TimeSpan(start, System.currentTimeMillis());

            if (span.toMilliseconds() > FLAG) {
                Logging.printError("Global room processing (" + JavaPlusEnvironment.getGame().getRoomManager().getRoomInstances().size() + " rooms) took: " + span.toMilliseconds() + "ms to execute.");
            }
        } catch (Exception e) {
            Logging.exception(e);
        }
    }
}
