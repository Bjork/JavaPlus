package me.bjork.javaplus.habbohotel.rooms.ai;

import me.bjork.javaplus.habbohotel.rooms.ai.types.BotAIType;

/**
 * Created on 24/05/2017.
 */
public class RoomBot {
    private BotAIType AiType;

    public RoomBot(String aitype) {
        this.AiType = getAIFromString(aitype);
    }

    public boolean isPet() {
        return AiType.equals(BotAIType.PET);
    }

    public static BotAIType getAIFromString(String type)
    {
        switch (type)
        {
            case "pet":
                return BotAIType.PET;
            case "generic":
                return BotAIType.BOT;
            case "bartender":
                return BotAIType.BARTENDER;
            default:
                return BotAIType.BOT;
        }
    }
}
