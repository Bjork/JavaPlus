package me.bjork.javaplus.habbohotel.rooms.ai.types;

/**
 * Created on 26/05/2017.
 */
public enum BotAIType {
    BOT,
    PET,
    BARTENDER
}
