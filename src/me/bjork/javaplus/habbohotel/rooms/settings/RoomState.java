package me.bjork.javaplus.habbohotel.rooms.settings;

/**
 * Created on 22/05/2017.
 */
public enum RoomState {
    OPEN,
    DOORBELL,
    PASSWORD,
    INVISIBLE;

    public static int getRoomAccessPacketNum(RoomState Access)
    {
        switch (Access)
        {
            default:
            case OPEN:
                return 0;
            case DOORBELL:
                return 1;
            case PASSWORD:
                return 2;
            case INVISIBLE:
                return 3;
        }
    }

    public static RoomState toRoomAccess(String Id)
    {
        switch (Id)
        {
            default:
            case "open":
                return RoomState.OPEN;
            case "locked":
                return RoomState.DOORBELL;
            case "password":
                return RoomState.PASSWORD;
            case "invisible":
                return RoomState.INVISIBLE;
        }
    }

    public static RoomState toRoomAccess(int Id)
    {
        switch (Id)
        {
            default:
            case 0:
                return RoomState.OPEN;
            case 1:
                return RoomState.DOORBELL;
            case 2:
                return RoomState.PASSWORD;
            case 3:
                return RoomState.INVISIBLE;
        }
    }
}
