package me.bjork.javaplus.habbohotel.rooms;

import me.bjork.javaplus.JavaPlusEnvironment;
import me.bjork.javaplus.communication.packets.parsers.OutgoingMessageComposer;
import me.bjork.javaplus.core.Logging;
import me.bjork.javaplus.habbohotel.gameclients.GameClient;
import me.bjork.javaplus.habbohotel.rooms.components.ProcessComponent;
import me.bjork.javaplus.habbohotel.rooms.components.RightsComponent;
import me.bjork.javaplus.habbohotel.rooms.models.RoomModel;
import me.bjork.javaplus.habbohotel.rooms.objects.users.RoomUserStatus;

import java.util.List;

/**
 * Created on 22/05/2017.
 */
public class Room extends RoomData {

    private RoomUserManager roomUserManager;
    private RightsComponent rights;
    private RoomModel model;
    private ProcessComponent process;

    private boolean isDisposed = false;
    public boolean isCrashed;
    private int idleTicks = 0;
    private boolean roomMuted = false;

    public Room(RoomData data) {
        super(data);

        this.roomUserManager = new RoomUserManager(this);
        this.model = JavaPlusEnvironment.getGame().getRoomManager().getModel(this.getModelName());
        this.rights = new RightsComponent(this);
        this.process = new ProcessComponent(this);
    }

    public int userCount()
    {
        return roomUserManager.getRoomUsers().size();
    }

    public void send(OutgoingMessageComposer response, boolean UsersWithRightsOnly) {
        List<RoomUser> Users = roomUserManager.getRoomUsers();

        for (RoomUser user : Users)
        {
            if (user == null || user.isBot())
                continue;

            if (user.getClient() == null || user.getClient().getNetwork() == null)
                continue;

            if (UsersWithRightsOnly && !this.getRights().hasRights(user.getClient()))
                continue;

            user.getClient().send(response);
        }
    }


    public void send(OutgoingMessageComposer response) {
        List<RoomUser> Users = roomUserManager.getRoomUsers();

        for (RoomUser user : Users)
        {
            if (user == null || user.isBot())
                continue;

            user.getClient().send(response);
        }
    }

    public void send(OutgoingMessageComposer response, Boolean UsersWithRightsOnly)
    {
        if (response == null)
            return;

        try
        {
            List<RoomUser> Users = roomUserManager.getRoomUsers();

            if (this == null || roomUserManager == null || Users == null)
                return;

            for (RoomUser user : Users)
            {
                if (user == null || user.isBot())
                    continue;

                if (user.getClient() == null || user.getClient().getNetwork() == null)
                    continue;

                if (UsersWithRightsOnly && !this.getRights().hasRights(user.getClient()))
                    continue;

                user.getClient().send(response);
            }
        }
        catch (Exception e)
        {
            Logging.exception(e);
        }
    }

    public boolean canSitOrLayAt(short x, short y) {
        /*if (hasHabbosAt(x, y))
            return false; //TODO

        THashSet<HabboItem> items = getItemsAt(x, y);

        return canSitAt(items) || canLayAt(items);*/
        return false;
    }

    public boolean canSitOrLayAt(int x, int y)
    {
        /*if(hasHabbosAt(x, y))
            return false;

        THashSet<HabboItem> items = getItemsAt(x, y);

        return canSitAt(items) || canLayAt(items);*/
        return true;
    }

    public boolean hasHabbosAt(int x, int y)
    {
        synchronized (this.getRoomUserManager().getRoomUsers())
        {
            List<RoomUser> roomUsers = this.getRoomUserManager().getRoomUsers();

            for (RoomUser user : roomUsers)
            {
                if(user.getX() == x && user.getY() == y)
                    return true;
            }
        }
        return false;
    }

    public boolean hasRoomMute() {
        return roomMuted;
    }

    public void setRoomMute(boolean value) {
        this.roomMuted = value;
    }

    public RoomModel getModel() { return this.model; }
    public RoomUserManager getRoomUserManager()
    {
        return this.roomUserManager;
    }
    public RightsComponent getRights() {
        return this.rights;
    }
    public ProcessComponent getProcess() {
        return this.process;
    }

    public void dispose() { //TODO ajotuer des trucs à dispose ? Troc, filter, update messenger ...
        if (this.isDisposed) {
            return;
        }

        long currentTime = System.currentTimeMillis();

        //this.getItems().commit();

        this.isDisposed = true;
        this.isCrashed = false;

        this.process.stop();
        //this.itemProcess.stop();

        //this.game.stop();
        //this.game.dispose();

        if(this.roomUserManager != null)
            this.roomUserManager.dispose();

        //this.items.dispose();
        //this.bots.dispose();

        if(this.rights != null)
            this.rights.dispose();

        /*if (this.data.getType() == RoomType.PUBLIC) {
            RoomQueue.getInstance().removeQueue(this.getId());
        }*/

       if (this.forcedUnload) {
            JavaPlusEnvironment.getGame().getRoomManager().removeData(this.getId());
        }

        /*if (this.yesVotes != null) {
            this.yesVotes.clear();
        }*/

       /* if (this.noVotes != null) {
            this.noVotes.clear();
        }*/

        long timeTaken = System.currentTimeMillis() - currentTime;

        if (timeTaken >= 250) {
            Logging.printError("Room [" + this.getId() + "][" + this.getCaption() + "] took " + timeTaken + "ms to dispose");
        }

       Logging.printDebug("Room [ID: " + this.getId() + "] has been disposed");
    }

    public boolean isIdle() {
        if (this.idleTicks < 600 && this.getRoomUserManager().realPlayerCount() > 0) {
            this.idleTicks = 0;
        } else {
            if (this.idleTicks >= 600) {
                return true;
            } else {
                this.idleTicks += 10;
            }
        }
        return false;
    }

    private boolean forcedUnload = false;

    public void setIdleNow() {
        this.idleTicks = 600;
        this.forcedUnload = true;
    }

    public void tick() {
        if (this.rights != null) {
            this.rights.tick();
        }

        /*if (this.mapping != null) {
            this.mapping.tick();
        }*/
    }

    public void sitUser(GameClient client) {
        if (client.getHabbo().getRoomUser().getStatuses().containsKey(RoomUserStatus.SIT))
            return;

        double height = 0.5;
        int rotation = client.getHabbo().getRoomUser().getBodyRotation();

        switch (rotation) {
            case 1: {
                rotation++;
                break;
            }
            case 3: {
                rotation++;
                break;
            }
            case 5: {
                rotation++;
            }
        }

        client.getHabbo().getRoomUser().addStatus(RoomUserStatus.SIT, String.valueOf(height));
        client.getHabbo().getRoomUser().setBodyRotation(rotation);
        client.getHabbo().getRoomUser().markNeedsUpdate();
    }

    public boolean tileWalkable(int x, int y)
    {
        boolean walkable = this.model.tileWalkable(x, y);

        if (walkable)
        {
            if (this.hasHabbosAt(x, y) && !this.getAllowWalkthrough())
            {
                walkable = false;
            }
        }
        return walkable;
    }
}
