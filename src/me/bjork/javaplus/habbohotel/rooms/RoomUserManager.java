package me.bjork.javaplus.habbohotel.rooms;

import com.google.common.collect.Lists;
import me.bjork.javaplus.communication.packets.outgoing.rooms.settings.RoomRatingMessageComposer;
import me.bjork.javaplus.communication.packets.parsers.OutgoingMessageComposer;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created on 24/05/2017.
 */
public class RoomUserManager {
    private Room room;
    private ConcurrentHashMap<Integer, RoomUser> roomUsers;
    private AtomicInteger entityIdGenerator;

    private final Map<Integer, Integer> playerIdToEntity = new ConcurrentHashMap<>();

    public RoomUserManager(Room room) {
        this.room = room;
        this.roomUsers = new ConcurrentHashMap<Integer, RoomUser>();
        this.entityIdGenerator = new AtomicInteger();
    }

    public int getFreeVirtualId() {
        return this.entityIdGenerator.incrementAndGet();
    }

    public RoomUser getRoomUserByVirtualId(int virtualId) {
        if (!roomUsers.containsKey(virtualId))
            return null;

        return roomUsers.get(virtualId);
    }

    public RoomUser getRoomUserByHabbo(int id) {
        for (RoomUser user : this.getRoomUserList()) {
            if (user.getClient() != null && user.getClient().getHabbo() != null && user.getClient().getHabbo().getId() == id)
                return user;
        }
        return null;
    }

    public RoomUser getRoomUserByUsername(String userName) {
        for (RoomUser user : this.getRoomUserList()) {
            if (user.getClient() != null && user.getClient().getHabbo() != null && user.getClient().getHabbo().getUsername().equals(userName))
                return user;
        }
        return null;
    }

    public List<RoomUser> getRoomUsers() {
        List<RoomUser> list = new ArrayList<RoomUser>();

        for (RoomUser user : this.getRoomUserList()) {
            if (user.getClient() != null && !user.isBot())
                list.add(user);
        }
        return list;
    }

    public List<RoomUser> getRoomUsersByRank(int rank) {
        List<RoomUser> roomUsers = new ArrayList<>();
        for (RoomUser user : this.getRoomUserList()) {
            if (user.getClient() != null && user.getClient().getHabbo() != null && !user.isBot() && !user.isPet() && user.getClient().getHabbo().getRank() >= rank)
                roomUsers.add(user);
        }
        return roomUsers;
    }

    public void broadcastChatMessage(OutgoingMessageComposer msg, RoomUser sender) {
        for (RoomUser user : this.getRoomUserList()) {
            if (!user.isBot()) {
                if (user.getClient() == null)
                    continue;

                if (!user.getClient().getHabbo().ignores(sender.getId()))
                    user.getClient().send(msg);
            }
        }
    }

    public void addRoomUser(RoomUser user) {
        if (!this.roomUsers.containsKey(user.getVirtualId())) {
            this.roomUsers.put(user.getVirtualId(), user);
            this.playerIdToEntity.put(user.getVirtualId(), user.getId());
        }
    }

    public void removeRoomUser(RoomUser user) {
        if (this.roomUsers.containsKey(user.getVirtualId())) {
            this.roomUsers.remove(user.getVirtualId());
            this.playerIdToEntity.remove(user.getVirtualId());
        }
    }

    public ArrayList<RoomUser> getRoomUserList() {
        return new ArrayList<RoomUser>(this.roomUsers.values());
    }

    public Room getRoom() {
        return this.room;
    }

    public void dispose() {
        for (RoomUser user : this.getRoomUsers()) {
            user.getStatuses().clear();
            user.removeUserFromRoom(false, true, true);
        }

        this.room.setUsersNow(0);
        this.roomUsers.clear();
        this.playerIdToEntity.clear();

        this.roomUsers = null;
        this.room = null;
        this.entityIdGenerator = null;
    }

    public int playerCount() {
        List<Integer> countedEntities = Lists.newArrayList();

        try {
            for (RoomUser entity : this.getRoomUsers()) {
                if (!entity.isBot() && !entity.isPet() && entity.isVisible()) {
                    if(!countedEntities.contains(((RoomUser) entity).getId())) {
                        countedEntities.add(((RoomUser) entity).getId());
                    }
                }
            }
            return countedEntities.size();
        } catch(Exception e) {
            return 0;
        } finally {
            countedEntities.clear();
        }
    }

    public int realPlayerCount() {
        return this.playerIdToEntity.size();
    }

    public void refreshScore() {
        for (RoomUser entity : this.getRoomUsers()) {
            entity.getClient().send(new RoomRatingMessageComposer(room.getScore(), entity.getClient().getHabbo().canRateRoom()));
        }
    }
}
