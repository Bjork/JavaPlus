package me.bjork.javaplus.habbohotel.rooms;

/**
 * Created on 07/06/2017.
 */
public enum RoomUserType {
    USER(1),
    BOT(4),
    PET(2),
    UNKNOWN(3);

    private final int typeId;

    RoomUserType(int typeId)
    {
        this.typeId = typeId;
    }

    public int getTypeId()
    {
        return this.typeId;
    }
}
