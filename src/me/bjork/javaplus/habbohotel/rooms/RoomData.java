package me.bjork.javaplus.habbohotel.rooms;

import me.bjork.javaplus.database.queries.players.HabboDao;
import me.bjork.javaplus.database.queries.rooms.RoomDao;
import me.bjork.javaplus.habbohotel.rooms.settings.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created on 22/05/2017.
 */
public class RoomData {
    private final int id;
    private String caption;
    private String model;
    private String ownerName;
    private int ownerId;
    private String password;
    private int score;
    private String type;
    private RoomState access;
    private int usersNow;
    private int usersMax;
    private int category;
    private String description;
    private List<String> tags;
    private String floor;
    private String wallpaper;
    private String landscape;
    private int allowPets;
    private int allowPetsEating;
    private int roomBlockingEnabled;
    private int hidewall;
    private int wallThickness;
    private int floorThickness;
    private RoomMuteState muteSettings;
    private RoomBanState banSettings;
    private RoomKickState kickSettings;
    private int chatmode;
    private int chatSize;
    private int chatSpeed;
    private int extraFlood;
    private int chatDistance;
    private RoomTradeState tradeSettings;
    private Boolean pushEnabled;
    private Boolean pullEnabled;
    private Boolean superPushEnabled;
    private Boolean superPullEnabled;
    private Boolean respectNotifEnabled;
    private Boolean petMorphsAllowed;
    private int groupId;
    private Boolean wallHeight;

    public RoomData(int id, String caption, String model, String ownerName, int ownerId, String password, int score, String type, String access, int usersNow, int usersMax, int category,
                    String description, String tags, String floor, String wallpaper, String landscape, int allowPets, int allowPetsEating, int roomBlockingEnabled, int hidewall,
                    int wallThickness, int floorThickness, RoomMuteState muteSettings, RoomBanState banSettings, RoomKickState kickSettings, int chatmode, int chatSize, int chatSpeed, int extraFlood,
                    int chatDistance, int tradeSettings, Boolean pushEnabled, Boolean pullEnabled, Boolean superPushEnabled, Boolean superPullEnabled, Boolean respectNotifEnabled,
                    Boolean petMorphsAllowed, int groupId)
    {

        this.id = id;
        this.caption = caption;
        this.model = model;
        this.ownerName = ownerName;
        this.ownerId = ownerId;
        this.password = password;
        this.score = score;
        this.type = type;
        this.access = RoomState.toRoomAccess(access);
        this.usersNow = usersNow;
        this.usersMax = usersMax;
        this.category = category;
        this.description = description;
        this.tags = new ArrayList<>();
        for (String tag : tags.toString().split(","))
            this.tags.add(tag);
        this.floor = floor;
        this.wallpaper = wallpaper;
        this.landscape = landscape;
        this.allowPets = allowPets;
        this.allowPetsEating = allowPetsEating;
        this.roomBlockingEnabled = roomBlockingEnabled;
        this.hidewall = hidewall;
        this.wallThickness = wallThickness;
        this.floorThickness = floorThickness;
        this.muteSettings = muteSettings;
        this.banSettings = banSettings;
        this.kickSettings = kickSettings;
        this.chatmode = chatmode;
        this.chatSize = chatSize;
        this.chatSpeed = chatSpeed;
        this.extraFlood = extraFlood;
        this.chatDistance = chatDistance;
        this.tradeSettings = RoomTradeState.valueOf(tradeSettings);
        this.pushEnabled = pushEnabled;
        this.pullEnabled = pullEnabled;
        this.superPushEnabled = superPushEnabled;
        this.superPullEnabled = superPullEnabled;
        this.respectNotifEnabled = respectNotifEnabled;
        this.petMorphsAllowed = petMorphsAllowed;
        this.groupId = groupId;
    }

    public RoomData(ResultSet data) throws SQLException
    {
        this.id = data.getInt("id");
        this.caption = data.getString("caption");
        this.model = data.getString("model_name");
        this.ownerId = data.getInt("owner");
        this.ownerName = HabboDao.getUsernameById(this.ownerId);
        this.password = data.getString("password");
        this.score = data.getInt("score");
        this.type = data.getString("roomtype");
        String accessType = data.getString("state");
        if (!accessType.equals("open") && !accessType.equals("locked") && !accessType.equals("password")) {
            accessType = "open";
        }
        this.access = RoomState.toRoomAccess(accessType);
        this.usersNow = data.getInt("users_now");
        this.usersMax = data.getInt("users_max");
        this.category = data.getInt("category");
        this.description = data.getString("caption");
        this.tags = new ArrayList<>();
        String tags = data.getString("tags");
        if (!tags.isEmpty())
            for (String tag : tags.split(","))
                this.tags.add(tag);
        this.floor = data.getString("floor");
        this.wallpaper = data.getString("wallpaper");
        this.landscape = data.getString("landscape");
        this.allowPets = data.getInt("allow_pets");
        this.allowPetsEating = data.getInt("allow_pets_eat");
        this.roomBlockingEnabled = data.getInt("room_blocking_disabled");
        this.hidewall = data.getInt("allow_hidewall");
        this.wallThickness = data.getInt("wallthick");
        this.floorThickness = data.getInt("floorthick");
        this.muteSettings =  RoomMuteState.valueOf(data.getInt("mute_settings"));
        this.banSettings =  RoomBanState.valueOf(data.getInt("ban_settings"));
        this.kickSettings =  RoomKickState.valueOf(data.getInt("kick_settings"));
        this.chatmode = data.getInt("chat_mode");
        this.chatSize = data.getInt("chat_size");
        this.chatSpeed = data.getInt("chat_speed");
        this.extraFlood = data.getInt("chat_extra_flood");
        this.chatDistance = data.getInt("chat_hearing_distance");
        this.tradeSettings = RoomTradeState.valueOf(data.getInt("trade_settings"));
        this.pushEnabled = data.getString("push_enabled") == "1";
        this.pullEnabled = data.getString("pull_enabled") == "1";
        this.superPushEnabled = data.getString("spush_enabled") == "1";
        this.superPullEnabled = data.getString("spull_enabled") == "1";
        this.respectNotifEnabled = data.getString("respect_notifications_enabled") == "1";
        this.petMorphsAllowed = data.getString("pet_morphs_allowed") == "1";
        this.groupId = data.getInt("group_id");
    }

    public RoomData(RoomData data)
    {
        this.id = data.id;
        this.caption = data.caption;
        this.model = data.model;
        this.ownerName = data.ownerName;
        this.ownerId = data.ownerId;
        this.password = data.password;
        this.score = data.score;
        this.type = data.type;
        this.access = data.access;
        this.usersNow = data.usersNow;
        this.usersMax = data.usersMax;
        this.category = data.category;
        this.description = data.description;
        this.tags = data.tags;
        this.floor = data.floor;
        this.wallpaper = data.wallpaper;
        this.landscape = data.landscape;
        this.allowPets = data.allowPets;
        this.allowPetsEating = data.allowPetsEating;
        this.roomBlockingEnabled = data.roomBlockingEnabled;
        this.hidewall = data.hidewall;
        this.wallThickness = data.wallThickness;
        this.floorThickness = data.floorThickness;
        this.muteSettings = data.muteSettings;
        this.banSettings = data.banSettings;
        this.kickSettings = data.kickSettings;
        this.chatmode = data.chatmode;
        this.chatSize = data.chatSize;
        this.chatSpeed = data.chatSpeed;
        this.extraFlood = data.extraFlood;
        this.chatDistance = data.chatDistance;
        this.tradeSettings = data.tradeSettings;
        this.pushEnabled = data.pushEnabled;
        this.pullEnabled = data.pullEnabled;
        this.superPushEnabled = data.superPushEnabled;
        this.superPullEnabled = data.superPullEnabled;
        this.respectNotifEnabled = data.respectNotifEnabled;
        this.petMorphsAllowed = data.getPetMorphsAllowed();
        this.groupId = data.groupId;
    }

    public int getId() {
        return id;
    }

    public int getOwnerId() {
        return ownerId;
    }

    public String getType() { return type; }

    public String getModelName() { return model; }

    public int getUsersNow() {
        return usersNow;
    }

    public void setUsersNow(int usersNow) {
        this.usersNow = usersNow;
        RoomDao.updateUserCount(this.id, usersNow);
    }

    public int getCategoryId() {
        return category;
    }

    public RoomTradeState getTradeState() {
        return tradeSettings;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public int getScore() {
        return score;
    }

    public int getUsersMax() {
        return usersMax;
    }

    public String getDescription() {
        return description;
    }
    public List<String> getTags()
    {
        return tags;
    }

    public int getAllowPets() {
        return allowPets;
    }

    public RoomState getAccess() {
        return access;
    }

    public String getCaption() {
        return caption;
    }

    public RoomMuteState getMuteSettings() {
        return muteSettings;
    }

    public RoomBanState getBanSettings() {
        return banSettings;
    }

    public RoomKickState getKickSettings() {
        return kickSettings;
    }

    public int getChatmode() {
        return chatmode;
    }

    public int getChatSize() {
        return chatSize;
    }

    public int getChatSpeed() {
        return chatSpeed;
    }

    public int getExtraFlood() {
        return extraFlood;
    }

    public int getChatDistance() {
        return chatDistance;
    }

    public String getWallpaper() {
        return wallpaper;
    }

    public String getFloor() {
        return floor;
    }

    public String getLandscape() {
        return landscape;
    }

    public Boolean getWallHeight() {
        return wallHeight;
    }

    public int getWallThickness() {
        return wallThickness;
    }
    public int getFloorThickness() {
        return floorThickness;
    }

    public int getHidewall() {
        return hidewall;
    }

    public String getPassword() {
        return password;
    }
    
    public Boolean getAllowWalkthrough() {
        return roomBlockingEnabled == 1;
    }

	public Boolean getPetMorphsAllowed() {
		return petMorphsAllowed;
	}

    public void setScore(int score) {
        this.score = score;
    }
}
