package me.bjork.javaplus.habbohotel.rooms.chat;

import me.bjork.javaplus.core.Logging;
import me.bjork.javaplus.habbohotel.commands.CommandManager;
import me.bjork.javaplus.habbohotel.rooms.filter.WordFilter;
import me.bjork.javaplus.habbohotel.rooms.types.misc.ChatEmotionsManager;

/**
 * Created on 31/05/2017.
 */
public class ChatManager {
    private WordFilter filterManager;
    private ChatEmotionsManager emotions;
    private CommandManager commands;

    public ChatManager() //TODO Complete
    {
        //this.logs = new ChatlogManager();

        this.filterManager = new WordFilter();
        this.emotions = new ChatEmotionsManager();

        this.commands = new CommandManager();
        //this.petCommands = new PetCommandManager();
        //this.petLocale = new PetLocale();

        //this.chatStyles = new ChatStyleManager();
        //this.chatStyles.Init();
        Logging.println("Chat Manager -> LOADED");
    }

    public final WordFilter getFilter() {
        return filterManager;
    }
    public final ChatEmotionsManager getEmotions() {
        return this.emotions;
    }
    public final CommandManager getCommands() { return this.commands; }
}
