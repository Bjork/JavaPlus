package me.bjork.javaplus.habbohotel.rooms;

import me.bjork.javaplus.JavaPlusEnvironment;
import me.bjork.javaplus.core.Logging;
import me.bjork.javaplus.database.queries.rooms.RoomDao;
import me.bjork.javaplus.habbohotel.rooms.models.RoomModel;
import me.bjork.javaplus.habbohotel.rooms.promotions.RoomPromotion;
import me.bjork.javaplus.habbohotel.users.Habbo;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

/**
 * Created on 22/05/2017.
 */
public class RoomManager {
    private static ConcurrentHashMap<Integer, Room> activeRooms;
    private Map<Integer, Room> unloadingRoomInstances;
    private ConcurrentHashMap<Integer, RoomData> roomDataInstances;

    private static ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(8);
    private Map<String, RoomModel> models;
    private Map<Integer, RoomPromotion> roomPromotions;

    public RoomManager()
    {
        this.activeRooms = new ConcurrentHashMap<Integer, Room>();
        this.unloadingRoomInstances = new ConcurrentHashMap<>();
        this.roomDataInstances = new ConcurrentHashMap<Integer, RoomData>();
        this.scheduler = Executors.newScheduledThreadPool(8);
        this.models = new ConcurrentHashMap<>();
        this.roomPromotions = new ConcurrentHashMap<>();
        this.loadPromotedRooms();
        this.loadModels();
        Logging.println("Room Manager -> LOADED");
    }

    public void loadModels() {
        if (this.models != null && this.getModels().size() != 0) {
            this.getModels().clear();
        }

        this.models = RoomDao.getModels();

        Logging.println("Loaded " + this.getModels().size() + " room models");
    }

    public RoomModel getModel(String id) {
        if (this.models.containsKey(id))
            return this.models.get(id);

        Logging.printError("Couldn't find model: " + id);

        return null;
    }

    public void loadPromotedRooms() {
        RoomDao.deleteExpiredRoomPromotions();
        RoomDao.getActivePromotions(this.roomPromotions);

        Logging.println("Loaded " + this.getRoomPromotions().size() + " room promotions");
    }

    public List<RoomData> getRoomsByCategory(int category) {
        List<RoomData> rooms = new ArrayList<>();

        for (Room room : this.activeRooms.values()) {
            if (category != -1 && room.getCategoryId() != category) {
                continue;
            }

            if (room.getUsersNow() <= 0)
                continue;

            rooms.add(room);
        }
        return rooms;
    }

    public Room getRoom(int roomId)
    {
        return this.activeRooms.get(roomId);
    }

    public Room loadRoom(int roomId)
    {
        if (roomId == 0)
            return null;

        Room room = null;

        if (JavaPlusEnvironment.getGame().getRoomManager().getActiveRooms().containsKey(roomId)) {
            room = JavaPlusEnvironment.getGame().getRoomManager().getRoom(roomId);

            if (room != null) {
                return room;
            }
        }

        if (room == null) {
            RoomData data = JavaPlusEnvironment.getGame().getRoomManager().getRoomData(roomId);

            if (data == null) {
                return null;
            }
            room = new Room(data);
            this.activeRooms.put(roomId, room);
        }
        return room;
    }

    public Boolean tryGetModel(String id)
    {
        Boolean result = false;
        if (this.models.containsKey(id))
            result = true;
        return result;
    }

    public void unloadIdleRooms() {
        for (Room room : this.unloadingRoomInstances.values()) {
                room.dispose();
        }

        this.unloadingRoomInstances.clear();

        List<Room> idleRooms = new ArrayList<>();

        for (Room room : this.activeRooms.values()) {
            if (room.isIdle()) {
                idleRooms.add(room);
            }
        }

        for (Room room : idleRooms) {
            this.activeRooms.remove(room.getId());
            this.unloadingRoomInstances.put(room.getId(), room);
        }
    }

    public final Map<Integer, Room> getRoomInstances() {
        return this.activeRooms;
    }

    private ConcurrentHashMap<Integer, RoomData> getRoomDataInstances() {
        return this.roomDataInstances;
    }

    public RoomData getRoomData(int id) {
        if (this.getRoomDataInstances().containsKey(id))
            return this.getRoomDataInstances().get(id);

        RoomData roomData = RoomDao.getRoomDataByRoomId(id);
        if (roomData != null) {
            this.getRoomDataInstances().put(id, roomData);
        }
        return roomData;
    }

    public void loadRoomsForUser(Habbo habbo) {
        habbo.getRooms().clear();
        Map<Integer, RoomData> rooms = RoomDao.getRoomsByOwnerId(habbo.getId());

        for (Map.Entry<Integer, RoomData> roomEntry : rooms.entrySet()) {
            if (!habbo.getRooms().contains(roomEntry.getKey()))
                habbo.getRooms().add(roomEntry.getKey());

            if (!this.getRoomDataInstances().containsKey(roomEntry.getKey())) {
                this.getRoomDataInstances().put(roomEntry.getKey(), roomEntry.getValue());
            }
        }
    }

    public void removeData(int roomId) {
        if (this.getRoomDataInstances().get(roomId) == null) {
            return;
        }
        this.getRoomDataInstances().remove(roomId);
    }

    public void dispose()
    {
        int length = this.getActiveRooms().size();
        int i = 0;
        for (Room room : this.activeRooms.values())
        {
            if (room == null)
                continue;
            room.setIdleNow();

            Logging.println("<<- SERVER SHUTDOWN ->> ROOM ITEM SAVE: " + String.format("{0:0.##}", ((double)i / length) * 100) + "%");
            i++;
        }
        Logging.println("Done disposing rooms!");
    }

    public boolean hasPromotion(int roomId) { return this.roomPromotions.containsKey(roomId) && !this.roomPromotions.get(roomId).isExpired(); }
    public ConcurrentHashMap<Integer, Room> getActiveRooms() { return this.activeRooms; }
    public final Map<String, RoomModel> getModels() {
        return this.models;
    }
    public Map<Integer, RoomPromotion> getRoomPromotions() { return roomPromotions; }
    public boolean isActive(int id) { return this.activeRooms.containsKey(id); }
    public static ScheduledExecutorService getScheduler() {
        return scheduler;
    }

    public int getActiveRoomsCount() {
        return this.activeRooms.size();
    }
}
