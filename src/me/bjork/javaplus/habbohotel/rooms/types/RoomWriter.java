package me.bjork.javaplus.habbohotel.rooms.types;

import me.bjork.javaplus.JavaPlusEnvironment;
import me.bjork.javaplus.communication.connection.netty.streams.NettyResponse;
import me.bjork.javaplus.habbohotel.groups.Group;
import me.bjork.javaplus.habbohotel.navigator.types.publics.PublicRoom;
import me.bjork.javaplus.habbohotel.rooms.RoomData;
import me.bjork.javaplus.habbohotel.rooms.promotions.RoomPromotion;
import me.bjork.javaplus.habbohotel.rooms.settings.RoomState;

/**
 * Created on 23/05/2017.
 */
public class RoomWriter {
        public static void entryData(RoomData room, NettyResponse response, boolean isLoading, boolean checkEntry) {
        response.writeBool(isLoading); // is loading

        write(room, response);

        response.writeBool(checkEntry); // check entry??
        response.writeBool(JavaPlusEnvironment.getGame().getNavigatorManager().isStaffPicked(room.getId()));
        response.writeBool(false); // ??
        response.writeBool(false /*JavaPlusEnvironment.getGame().getRoomManager().isActive(room.getId()) && RoomManager.getInstance().get(room.getId()).hasRoomMute()*/); //TODO get roommuted when enter after roommute

        response.writeInt(room.getMuteSettings().getState());
        response.writeInt(room.getKickSettings().getState());
        response.writeInt(room.getBanSettings().getState());

        response.writeBool(false); // room muting TODO

        response.writeInt(room.getChatmode());
        response.writeInt(room.getChatSize());
        response.writeInt(room.getChatSpeed());
        response.writeInt(room.getChatDistance());
        response.writeInt(room.getExtraFlood());
    }

    public static void write(RoomData roomData, NettyResponse response) {

        boolean isActive = JavaPlusEnvironment.getGame().getRoomManager().isActive(roomData.getId());
        PublicRoom publicRoom = JavaPlusEnvironment.getGame().getNavigatorManager().getPublicRoom(roomData.getId());

        response.writeInt(roomData.getId());
        response.writeString(publicRoom != null ? publicRoom.getCaption() : roomData.getCaption());
        response.writeInt(roomData.getOwnerId());
        response.writeString(roomData.getOwnerName());
        response.writeInt(RoomWriter.roomAccessToNumber(roomData.getAccess()));
        response.writeInt(!isActive ? 0 : JavaPlusEnvironment.getGame().getRoomManager().getRoom(roomData.getId()).getRoomUserManager().realPlayerCount());
        response.writeInt(roomData.getUsersMax());
        response.writeString(publicRoom != null ? publicRoom.getDescription() : roomData.getDescription());
        response.writeInt(roomData.getTradeState().getState());
        response.writeInt(roomData.getScore());
        response.writeInt(0);
        response.writeInt(roomData.getCategoryId());

        response.writeInt(roomData.getTags().size());
        for (String tag : roomData.getTags()) {
            response.writeString(tag);
        }

        RoomPromotion promotion = JavaPlusEnvironment.getGame().getRoomManager().getRoomPromotions().get(roomData.getId());
        //Group group = GroupManager.getInstance().get(room.getGroupId());

        composeRoomSpecials(response, roomData, promotion, null);
    }


    public static void composeRoomSpecials(NettyResponse response, RoomData roomData, RoomPromotion promotion, Group group) {

        boolean composeGroup = false; //group != null && group.getData() != null;
        boolean composePromo = promotion != null;

        int specialsType = 0;

        if(group != null)
            specialsType += 2;

        if(promotion != null)
            specialsType += 4;

        if(roomData.getAllowPets() == 1) {
            specialsType += 16;
        }

        PublicRoom publicRoom = JavaPlusEnvironment.getGame().getNavigatorManager().getPublicRoom(roomData.getId());

        if(publicRoom != null)
            specialsType += 1;
        else
            specialsType += 8;

        response.writeInt(specialsType);

        if(publicRoom != null) {
            response.writeString(publicRoom.getImageUrl());
        }

        if (composeGroup) {
            composeGroup(group, response);
        }

        if (composePromo) {
            composePromotion(promotion, response);
        }
    }

    private static void composePromotion(RoomPromotion promotion, NettyResponse response) {
        response.writeString(promotion.getPromotionName()); // promo name
        response.writeString(promotion.getPromotionDescription()); // promo description
        response.writeInt(promotion.minutesLeft()); // promo minutes left
    }

    private static void composeGroup(Group group, NettyResponse response) {
        //response.writeInt(group.getId());
        //response.writeString(group.getData().getTitle());
        //response.writeString(group.getData().getBadge());
    }

    public static int roomAccessToNumber(RoomState access) {
        if (access == RoomState.DOORBELL) {
            return 1;
        } else if (access == RoomState.PASSWORD) {
            return 2;
        } else if (access == RoomState.INVISIBLE) {
            return 3;
        }
        return 0;
    }
}
