package me.bjork.javaplus.habbohotel.rooms.models.types.tiles;

/**
 * Created on 27/05/2017.
 */
public enum RoomTileState
{
    OPEN,
    BLOCKED
}
