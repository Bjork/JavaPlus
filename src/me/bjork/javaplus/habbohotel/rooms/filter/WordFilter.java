package me.bjork.javaplus.habbohotel.rooms.filter;

import me.bjork.javaplus.core.Logging;
import me.bjork.javaplus.core.settings.SettingsManager;
import me.bjork.javaplus.core.utils.FilterUtil;
import me.bjork.javaplus.database.queries.rooms.FilterDao;

import java.util.Map;

/**
 * Created on 29/05/2017.
 */
public class WordFilter {
    private Map<String, String> wordfilter;

    public WordFilter() {
        this.loadFilter();
    }

    public void loadFilter() {
        if (this.wordfilter != null) {
            this.wordfilter.clear();
        }
        this.wordfilter = FilterDao.loadWordfilter();

        Logging.println("Loaded " + wordfilter.size() + " filtered words");
    }

    public FilterResult filter(String message) {
        String filteredMessage = message;

        String filterMode = SettingsManager.tryGetValue("word.filter_mode");

        if (FilterMode.valueOf(filterMode.toUpperCase()) == FilterMode.STRICT) {
            message = FilterUtil.process(message.toLowerCase());
        }

        for (Map.Entry<String, String> word : wordfilter.entrySet()) {
            if (message.toLowerCase().contains(word.getKey())) {
                if (FilterMode.valueOf(filterMode.toUpperCase()) == FilterMode.STRICT)
                    return new FilterResult(true, word.getKey());

                filteredMessage = filteredMessage.replace("(?i)" + word.getKey(), word.getValue());
            }
        }

        return new FilterResult(filteredMessage, !message.equals(filteredMessage));
    }

    public void save() {
        FilterDao.save(this.wordfilter);
    }
}
