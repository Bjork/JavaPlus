package me.bjork.javaplus.habbohotel.rooms;

import gnu.trove.map.hash.THashMap;
import me.bjork.javaplus.JavaPlusEnvironment;
import me.bjork.javaplus.communication.packets.outgoing.generic.GenericErrorComposer;
import me.bjork.javaplus.communication.packets.outgoing.rooms.avatar.ApplyEffectComposer;
import me.bjork.javaplus.communication.packets.outgoing.rooms.avatar.CarryObjectComposer;
import me.bjork.javaplus.communication.packets.outgoing.rooms.avatar.DanceComposer;
import me.bjork.javaplus.communication.packets.outgoing.rooms.avatar.SleepComposer;
import me.bjork.javaplus.communication.packets.outgoing.rooms.chat.FloodFilterMessageComposer;
import me.bjork.javaplus.communication.packets.outgoing.rooms.engine.UserRemoveComposer;
import me.bjork.javaplus.communication.packets.outgoing.rooms.session.HotelViewMessageComposer;
import me.bjork.javaplus.core.Logging;
import me.bjork.javaplus.database.queries.rooms.RoomDao;
import me.bjork.javaplus.habbohotel.gameclients.GameClient;
import me.bjork.javaplus.habbohotel.rooms.ai.BotAI;
import me.bjork.javaplus.habbohotel.rooms.models.types.tiles.RoomTile;
import me.bjork.javaplus.habbohotel.rooms.objects.users.RoomUserStatus;
import me.bjork.javaplus.habbohotel.users.Habbo;
import me.bjork.javaplus.habbohotel.users.effects.AvatarEffect;

import java.util.Deque;
import java.util.LinkedList;
import java.util.Map;

/**
 * Created on 24/05/2017.
 */
public class RoomUser {
    private Habbo habbo;
    private Room room;
    private int habboId;
    private int virtualId;

    private GameClient client;

    private Boolean ridingHorse;
    private int horseId;
    private final THashMap<RoomUserStatus, String> status;
    private RoomUserType roomUserType;
    private BotAI botAI;

    private int X;
    private int Y;
    private double Z;
    private int rotBody;
    private int rotHead;
    private RoomUser mountedEntity;
    private boolean needsUpdate;
    public int updatePhase = 0;
    private boolean cancelNextUpdate;
    public boolean needsForcedUpdate = false;

    private RoomTile startLocation;
    private RoomTile previousLocation;
    private RoomTile currentLocation;
    private RoomTile goalLocation;
    private Deque<RoomTile> path = new LinkedList<>();

    private boolean overriden;
    private boolean isMoonwalking;
    private boolean isWalking;

    public boolean isKicked = false;
    private int kickWalkStage = 0;
    private boolean walkCancelled = false;
    private boolean canWalk = true;

    private boolean isVisible;

    private int handItem;
    private int handItemTimer;
    private int idleTime;
    private int signTime;

    private int danceId;
    private boolean isIdle = false;
    private boolean isRoomMuted = false;

    private AvatarEffect lastEffect;
    private AvatarEffect effect;
    private boolean doorbellAnswered;

    public RoomUser(Habbo habbo) {
        this.habbo = habbo;
        this.room = null;
        this.habboId = -1;
        this.virtualId = -1;

        this.roomUserType = RoomUserType.UNKNOWN;
        this.horseId = 0;
        this.ridingHorse = false;

        this.status = new THashMap<RoomUserStatus, String>();
        
        this.X = 0;
        this.Y = 0;
        this.Z = 0;
        this.rotHead = 0;
        this.rotBody = 0;
        this.isWalking = false;
        this.canWalk = true;

        this.cancelNextUpdate = false;
        this.isVisible = true; //TODO

        this.idleTime = 0;
        this.signTime = 0;
        this.handItem = 0;
        this.handItemTimer = 0;
        this.doorbellAnswered = false;

        if (room != null && room.hasRoomMute()) { //TODO A verifier
            this.isRoomMuted = true;
        }

        this.danceId = 0;
        this.overriden = false;
    }

    public void addStatus(RoomUserStatus key, String value) {
    	if (this.status.containsKey(key)) {
            this.status.replace(key, value);
        } else {
            this.status.put(key, value);
        }
	}
    
    public void removeStatus(RoomUserStatus status) {
        if (!this.status.containsKey(status)) {
            return;
        }

        this.status.remove(status);
    }

    public boolean hasStatus(RoomUserStatus key) {
        return this.status.containsKey(key);
    }

    public boolean handItemNeedsRemove() {
        if (this.handItemTimer == -999)
            return false;
        this.handItemTimer--;
        return this.handItemTimer <= 0;
    }

    public int getSignTime() {
        return this.signTime;
    }

    public void markDisplayingSign() {
        this.signTime = 6;
    }

    public boolean isDisplayingSign() {
        this.signTime--;
        if (this.signTime <= 0) {
            if (this.signTime < 0) {
                this.signTime = 0;
            }
            return false;
        } else {
            return true;
        }
    }

    public boolean isIdleAndIncrement() {
        this.idleTime++;
        if (this.idleTime >= 600) {
            if(!this.isIdle) {
                this.isIdle = true;
                this.getRoom().send(new SleepComposer(this.getVirtualId(), true));
            }
            return true;
        }
        return false;
    }

    public void setIdle() {
        this.idleTime = 600;
        this.getRoom().send(new SleepComposer(this.getVirtualId(), true));
    }

    public void unIdle() {
        final boolean sendUpdate = this.isIdle;
        this.isIdle = false;
        this.resetIdleTime();

        if (this.isBot()) {
            return;
        }

        if(sendUpdate) {
            this.getRoom().send(new SleepComposer(this.getVirtualId(), false));
        }
    }

    public void resetIdleTime() {
        this.idleTime = 0;
    }

    public void dispose() {
        this.status.clear();
        this.habbo = null;
        this.room = null;
        this.client = null;
        this.habboId = -1;
        this.virtualId = -1;
    }

    public GameClient getClient() {
        if (isBot())
            return null;

        if (this.client == null)
            return JavaPlusEnvironment.getGame().getGameClientManager().getClientByUserID(this.getId());

        return this.client;
    }

    public RoomUser getMountedEntity() {
        if (this.mountedEntity == null) return null;

        if (this.horseId == 0) {
            return null;
        }

        return mountedEntity;
    }

    public void walkTo(int goalX, int goalY) {

        if (goalX == this.getX() && goalY == this.getY())
            return;

        RoomTile tile = this.getRoom().getModel().getTile(goalX, goalY);
        if (tile.isWalkable() || this.getRoom().canSitOrLayAt(tile.x, tile.y)) {
            this.isWalking = true;
            setGoalLocation(tile);
            this.unIdle();
        }
    }

    public void findPath()
    {
        if (this.room != null && this.goalLocation != null && (this.goalLocation.isWalkable() || this.room.canSitOrLayAt(this.goalLocation.x, this.goalLocation.y)))
        {
            this.path = this.room.getModel().findPath(room, this.currentLocation, this.goalLocation);
        }
    }

    public void removeUserFromRoom(boolean isOffline, boolean NotifyClient, boolean NotifyKick) {
        try {
            /*
            // Bot Action on leave
            for (BotEntity entity : this.getRoom().getEntities().getBotEntities()) {
                if (entity.getAI().onPlayerLeave(this)) break;
            }

             // Item action on leave
            for (Map.Entry<Long, RoomItemFloor> floorItem : this.getRoom().getItems().getFloorItems().entrySet()) {
                if (floorItem.getValue() == null) continue;

                floorItem.getValue().onEntityLeaveRoom(this);
            }*/

            /*// Check and cancel any active trades
            Trade trade = this.getRoom().getTrade().get(this);

            if (trade != null) {
                trade.cancel(this.getPlayerId());
            }*/

            /*//Step off item
            for (RoomItemFloor item : this.getRoom().getItems().getItemsOnSquare(this.getPosition().getX(), this.getPosition().getY())) {
                if (item == null) continue;
                item.onEntityStepOff(this);
            }*/

            if (this.getRidingHorse()) {
                this.setRidingHorse(false);
                RoomUser userRiding = room.getRoomUserManager().getRoomUserByVirtualId(this.getHorseId());
                if (userRiding != null) {
                    userRiding.setRidingHorse(false);
                    userRiding.setHorseId(0);
                }
            }

            room.send(new UserRemoveComposer(this.getVirtualId()), false);

            if (NotifyKick && !isOffline && this.getClient() != null && this.getClient().getHabbo() != null) {
                this.getHabbo().getClient().send(new GenericErrorComposer(GenericErrorComposer.KICKED_OUT_OF_THE_ROOM));
            }

            if (!isOffline && NotifyClient && this.getClient() != null && this.getClient().getHabbo() != null) {
                this.getHabbo().getClient().send(new HotelViewMessageComposer());

                /*if (this.getPlayer().getData() != null) {
                    this.getPlayer().getMessenger().sendStatus(true, false);
                }*/
            }

            /*if (this.hasPlacedPet && this.getRoom().getData().getOwnerId() != this.playerId) {
                for (PetEntity petEntity : this.getRoom().getEntities().getPetEntities()) {
                    if (petEntity.getData().getOwnerId() == this.getPlayerId()) {
                        RoomPetDao.updatePet(0, 0, 0, petEntity.getData().getId());
                        petEntity.leaveRoom(false);

                        this.getPlayer().getPets().addPet(petEntity.getData());
                        this.getPlayer().getSession().send(new PetInventoryMessageComposer(this.getPlayer().getPets().getPets()));

                    }
                }
            }

            for(RoomEntity follower : this.getFollowingEntities()) {
                if(follower instanceof BotEntity) {
                    final BotEntity botEntity = ((BotEntity) follower);

                    if(botEntity.getData() != null) {
                        if (botEntity.getData().getMode() == BotMode.RELAXED) {
                            botEntity.getData().setMode(BotMode.DEFAULT);
                        }
                    }
                }
            }*/

            if (room.getRoomUserManager().getRoomUserList().contains(this)) {
                room.getRoomUserManager().removeRoomUser(this);
                room.setUsersNow(room.getRoomUserManager().getRoomUserList().size());
            }

            if (!isOffline && this.getClient() != null && this.getClient().getHabbo() != null) {
                this.getHabbo().getRoomUser().setDoorbellAnswered(false);
                this.getHabbo().setRequestedRoomId(0);
                this.getHabbo().setCurrentRoomId(0);
            }

            RoomDao.updateSettings(this.getClient(), room);
        } catch (Exception e) {
            Logging.exception(e);
        }
    }

    private int lastMessageCounter = 0;
    private String lastMessage = "";

    public boolean onChat(String message) {
        final long time = System.currentTimeMillis();

        final boolean isPlayerOnline = JavaPlusEnvironment.getGame().getGameClientManager().isOnline(this.getId());

        if(!isPlayerOnline) {
            this.removeUserFromRoom(true, false, false);
            return false;
        }

        if (!this.getClient().getHabbo().getPermissions().hasRight("room_bypass_flood")) {
            if (this.lastMessage.equals(message)) {
                this.lastMessageCounter++;

                if (this.lastMessageCounter >= 3) {
                    this.getClient().getHabbo().setRoomFloodTime(JavaPlusEnvironment.getGame().getPermissionManager().getRankGroup(this.getClient().getHabbo().getRank()).getFloodtime());
                }
            } else {
                this.lastMessage = message;
                this.lastMessageCounter = 0;
            }

            if (time - this.getClient().getHabbo().getRoomLastMessageTime() < 750) {
                this.getClient().getHabbo().setRoomFloodFlag(this.getClient().getHabbo().getRoomFloodFlag() + 1);

                if (this.getClient().getHabbo().getRoomFloodFlag() >= 3) {
                    this.getClient().getHabbo().setRoomFloodTime(JavaPlusEnvironment.getGame().getPermissionManager().getRankGroup(this.getClient().getHabbo().getRank()).getFloodtime());
                    this.getClient().getHabbo().setRoomFloodFlag(0);

                    this.getClient().send(new FloodFilterMessageComposer((int)this.getClient().getHabbo().getRoomFloodTime()));
                }
            } else {
                this.getClient().getHabbo().setRoomFloodFlag(0);
            }

            if (this.getClient().getHabbo().getRoomFloodTime() >= 1) {
                return false;
            }

            this.getClient().getHabbo().setRoomLastMessageTime(time);
            this.getClient().getHabbo().setLastMessage(message);
        }

        if (message.isEmpty() || message.length() > 100)
            return false;

        if (this.isRoomMuted() && !this.getClient().getHabbo().getPermissions().hasRight("room_ignore_mute") && this.getRoom().getOwnerId() != this.getId()) {
            return false;
        }

        if (this.getRoom().getRoomUserManager().playerCount() > 1) {
            //this.getPlayer().getQuests().progressQuest(QuestType.SOCIAL_CHAT); //TODO QUESTING!
        }

        this.unIdle();
        return true;
    }

    public Habbo getHabbo() { return habbo; }

    public boolean isAtGoal()
    {
        return this.currentLocation.equals(this.goalLocation);
    }

    public boolean isWalking() {
        return !isAtGoal() && this.canWalk;
    }

    public boolean canWalk() {
        return canWalk;
    }

    public void setCanWalk(boolean canWalk) {
        this.canWalk = canWalk;
    }

    public void setMountedEntity(RoomUser mountedEntity) {
        this.mountedEntity = mountedEntity;
    }

    public boolean needsUpdateCancel() {
        if (this.cancelNextUpdate) {
            this.cancelNextUpdate = false;
            return true;
        } else {
            return false;
        }
    }

    public void cancelNextUpdate() {
        this.cancelNextUpdate = true;
    }

    public void markNeedsUpdate() {
        this.needsUpdate = true;
    }

    public void markUpdateComplete() {
        this.needsUpdate = false;
    }

    public boolean needsUpdate() {
        return this.needsUpdate;
    }

    public Room getRoom() {
        return room;
    }

    public Room setRoom(Room room) { return this.room = room; }

    public Boolean getRidingHorse() {
        return ridingHorse;
    }

    public void setRidingHorse(Boolean ridingHorse) {
        this.ridingHorse = ridingHorse;
    }

    public int getId() { return habboId; }

    public void setId(int habboId) {
        this.habboId = habboId;
    }

    public int getVirtualId() {
        return virtualId;
    }

    public void setVirtualId(int virtualId) {
        this.virtualId = virtualId;
    }

    public Boolean isBot() {
        return this.roomUserType.equals(roomUserType.BOT);
    }

    public Boolean isPet() {
        return this.roomUserType.equals(roomUserType.PET);
    }

    public int getHorseId() {
        return horseId;
    }

    public void setHorseId(int horseId) {
        this.horseId = horseId;
    }

    public int getX() {
        return this.currentLocation.x;
    }

    public int getY() {
        return this.currentLocation.y;
    }

    public double getZ() {
        return Z;
    }

    public Map<RoomUserStatus, String> getStatuses() {
        return status;
    }

    public boolean isVisible() {
        return isVisible;
    }

    public void carryItem(int id) {
        this.carryItem(id, 240);
    }

    public void carryItem(int id, int timer) {
        this.handItem = id;
        this.handItemTimer = timer;
        this.getRoom().send(new CarryObjectComposer(this.getVirtualId(), handItem));
    }

    public int getHandItem() {
        return this.handItem;
    }

    public int getHandItemTimer() {
        return this.handItemTimer;
    }

    public void setHandItemTimer(int handItemTimer) {
        this.handItemTimer = handItemTimer;
    }

    public int getIdleTime() {
        return idleTime;
    }

    public boolean isIdle() {
        return isIdle;
    }

    public void applyEffect(AvatarEffect effect) {
        if(this.danceId > 0)
            this.getRoom().send(new DanceComposer(this.getVirtualId(), 0));

        if (effect == null) {
            this.getRoom().send(new ApplyEffectComposer(this.getVirtualId(), 0));
        } else {
            this.getRoom().send(new ApplyEffectComposer(this.getVirtualId(), effect.getEffectId()));
        }
        if (effect != null && effect.expires()) {
            this.lastEffect = this.effect;
        }
        this.effect = effect;
    }

    public AvatarEffect getCurrentEffect() {
        return this.effect;
    }

    public AvatarEffect getLastEffect() {
        return lastEffect;
    }

    public void setLastEffect(AvatarEffect lastEffect) {
        this.lastEffect = lastEffect;
    }

    public boolean isMoonwalking() {
        return this.isMoonwalking;
    }

    public void setIsMoonwalking(boolean isMoonwalking) {
        this.isMoonwalking = isMoonwalking;
    }

    public int getBodyRotation() {
        return this.rotBody;
    }

    public void setBodyRotation(int rotation) {
        this.rotBody = rotation;
    }

    public int getHeadRotation() {
        return this.rotHead;
    }

    public void setHeadRotation(int rotation) {
        this.rotHead = rotation;
    }

    public RoomTile getGoal()
    {
        return this.goalLocation;
    }

    public void setGoalLocation(RoomTile goalLocation)
    {
        this.setGoalLocation(goalLocation, false);
    }

    public void setGoalLocation(RoomTile goalLocation, boolean noReset)
    {
        this.startLocation = this.currentLocation;

        if (goalLocation != null && !noReset)
        {
            this.goalLocation = goalLocation;
            //this.tilesWalked = 0;
            this.findPath();
            //this.cmdSit = false;
        }
    }

    public void setLocation(RoomTile location)
    {
        if (location != null)
        {
            this.startLocation    = location;
            this.previousLocation = location;
            this.currentLocation  = location;
            this.goalLocation     = location;
        }
    }

    public RoomTile getCurrentLocation()
    {
        return this.currentLocation;
    }

    public void setCurrentLocation(RoomTile location)
    {
        if (location != null)
        {
            this.currentLocation = location;
        }
    }

    public RoomTile getPreviousLocation()
    {
        return this.previousLocation;
    }

    public void setPreviousLocation(RoomTile previousLocation)
    {
        this.previousLocation = previousLocation;
    }

    public void setZ(double z) {
        this.Z = z;
    }

    public Deque<RoomTile> getPath() {
        return path;
    }

    public boolean isRoomMuted() {
        return isRoomMuted;
    }

    public void setRoomMuted(boolean isRoomMuted) {
        this.isRoomMuted = isRoomMuted;
    }

    public boolean isDoorbellAnswered() {
        return this.doorbellAnswered;
    }

    public void setDoorbellAnswered(boolean value) {
        this.doorbellAnswered = value;
    }

    public void stopWalking() {

        synchronized (this.getStatuses())
        {
            this.getStatuses().remove(RoomUserStatus.MOVE);
            this.setGoalLocation(this.currentLocation);
        }
    }

    public int getDanceId() {
        return danceId;
    }

    public void setDanceId(int danceId) {
        this.danceId = danceId;
    }

    public RoomUserType getRoomUnitType() {
        return roomUserType;
    }

    public synchronized void setRoomUserType(RoomUserType roomUnitType) {
        this.roomUserType = roomUnitType;
    }

    public BotAI getBotAI() {
        return botAI;
    }
}
