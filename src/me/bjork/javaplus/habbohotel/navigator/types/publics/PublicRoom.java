package me.bjork.javaplus.habbohotel.navigator.types.publics;

/**
 * Created on 22/05/2017.
 */
public class PublicRoom {
    private final int roomId;
    private final String caption;
    private final String description;
    private final String imageUrl;
    private final int categoryId;

    public PublicRoom(int roomId, String caption, String description, String imageUrl, int categoryId) {
        this.roomId = roomId;
        this.caption = caption;
        this.description = description;
        this.imageUrl = imageUrl;
        this.categoryId = categoryId;
    }

    public int getRoomId() {
        return roomId;
    }

    public String getCaption() {
        return caption;
    }

    public String getDescription() {
        return description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public int getCategoryId() {
        return categoryId;
    }
}
