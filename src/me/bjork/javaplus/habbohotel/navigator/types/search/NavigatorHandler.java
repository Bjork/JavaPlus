package me.bjork.javaplus.habbohotel.navigator.types.search;

import com.google.common.collect.Lists;
import me.bjork.javaplus.JavaPlusEnvironment;
import me.bjork.javaplus.database.queries.rooms.RoomDao;
import me.bjork.javaplus.habbohotel.gameclients.GameClient;
import me.bjork.javaplus.habbohotel.navigator.types.Category;
import me.bjork.javaplus.habbohotel.navigator.types.publics.PublicRoom;
import me.bjork.javaplus.habbohotel.navigator.types.staffpicks.StaffPick;
import me.bjork.javaplus.habbohotel.rooms.RoomData;
import me.bjork.javaplus.habbohotel.rooms.promotions.RoomPromotion;

import java.util.*;

/**
 * Created on 22/05/2017.
 */
public class NavigatorHandler {

    public static List<RoomData> search(Category category, String search, GameClient client, boolean expanded, int fetchLimit)
    {
        List<RoomData> rooms = Lists.newCopyOnWriteArrayList();

        switch (category.getCategoryType()) {
            case QUERY:
                Collection<RoomData> query = NavigatorHandler.order(NavigatorHandler.getRoomsByQuery(search), fetchLimit);
                if(query == null)
                    break;
                rooms.addAll(query);
                break;
            case MY_ROOMS: //TODO Cache Room on Loading ?
                if(client.getHabbo().getRooms() == null) {
                    break;
                }

                for (Integer roomId : new LinkedList<>(client.getHabbo().getRooms())) {
                    if (JavaPlusEnvironment.getGame().getRoomManager().getRoomData(roomId) == null)
                        continue;

                    rooms.add(JavaPlusEnvironment.getGame().getRoomManager().getRoomData(roomId));
                }
                break;
            case POPULAR:
                rooms.addAll(order(JavaPlusEnvironment.getGame().getRoomManager().getRoomsByCategory(-1), expanded ? category.getRoomCountExpanded() : category.getRoomCount()));
                break;
            case CATEGORY:
                rooms.addAll(order(JavaPlusEnvironment.getGame().getRoomManager().getRoomsByCategory(category.getId()), expanded ? category.getRoomCountExpanded() : category.getRoomCount()));
                break;
            case TOP_PROMOTIONS:
                List<RoomData> promotedRooms = Lists.newArrayList();
                for (RoomPromotion roomPromotion : JavaPlusEnvironment.getGame().getRoomManager().getRoomPromotions().values()) {
                    if (roomPromotion != null) {
                        RoomData roomData = JavaPlusEnvironment.getGame().getRoomManager().getRoomData(roomPromotion.getRoomId());
                        if (roomData != null) {
                            promotedRooms.add(roomData);
                        }
                    }
                }
                rooms.addAll(order(promotedRooms, expanded ? category.getRoomCountExpanded() : category.getRoomCount()));
                promotedRooms.clear();
                break;

            case FEATURED:
                for (PublicRoom publicRoom : JavaPlusEnvironment.getGame().getNavigatorManager().getPublicRooms(category.getId()).values()) {
                    RoomData roomData = JavaPlusEnvironment.getGame().getRoomManager().getRoomData(publicRoom.getRoomId());
                    if (roomData != null) {
                        rooms.add(roomData);
                    }
                }
                break;

            case STAFF_PICKS:
                List<RoomData> staffPicks = Lists.newArrayList();
                for (StaffPick staffpick : JavaPlusEnvironment.getGame().getNavigatorManager().getStaffPicks().values()) {
                    RoomData roomData = JavaPlusEnvironment.getGame().getRoomManager().getRoomData(staffpick.getRoomId());
                    if (roomData != null) {
                        staffPicks.add(roomData);
                    }
                }
                rooms.addAll(order(staffPicks, expanded ? category.getRoomCountExpanded() : category.getRoomCount()));
                staffPicks.clear();
                break;

            case MY_GROUPS:
                /*List<RoomData> groupHomeRooms = Lists.newArrayList();

                for(int groupId : client.getHabbo().getGroups()) {
                    Group groupData = JavaPlusEnvironment.getGame().getGroupManager().getData(groupId);

                    if(groupData != null) {
                        RoomData roomData = RoomManager.getInstance().getRoomData(groupData.getRoomId());

                        if(roomData != null) {
                            groupHomeRooms.add(roomData);
                        }
                    }
                }

                rooms.addAll(order(groupHomeRooms, expanded ? category.getRoomCountExpanded() : category.getRoomCount()));
                groupHomeRooms.clear();*/
                break;

            case MY_FRIENDS_ROOMS:
                /*List<RoomData> friendsRooms = Lists.newArrayList();

                if(player.getMessenger() == null) {
                    return rooms;
                }

                for(MessengerFriend messengerFriend : player.getMessenger().getFriends().values()) {
                    if(messengerFriend.isInRoom()) {
                        PlayerEntity playerEntity = messengerFriend.getSession().getPlayer().getEntity();

                        if(playerEntity != null) {
                            if(!friendsRooms.contains(playerEntity.getRoom().getData())) {
                                friendsRooms.add(playerEntity.getRoom().getData());
                            }
                        }
                    }
                }

                rooms.addAll(order(friendsRooms, expanded ? category.getRoomCountExpanded() : category.getRoomCount()));
                friendsRooms.clear();*/
                break;
        }

        return rooms;
    }

    public static List<RoomData> getRoomsByQuery(String query) {
        List<RoomData> rooms = new ArrayList<>();

        if (query.length() > 0) {
            List<RoomData> list = RoomDao.getRoomsByQuery(query);
            for (RoomData roomData : list)
                rooms.add(roomData);
        }

        return rooms;
    }

    public static List<RoomData> order(List<RoomData> rooms, int limit) {
        try {
            Collections.sort(rooms, (room1, room2) -> {
                boolean is1Active = JavaPlusEnvironment.getGame().getRoomManager().isActive(room1.getId());
                boolean is2Active = JavaPlusEnvironment.getGame().getRoomManager().isActive(room2.getId());

                return ((!is2Active ? 0 : JavaPlusEnvironment.getGame().getRoomManager().getRoom(room2.getId()).getUsersNow()) -
                        (!is1Active ? 0 : JavaPlusEnvironment.getGame().getRoomManager().getRoom(room1.getId()).getUsersNow()));
            });
        } catch (Exception ignored) {
        }

        List<RoomData> returnRooms = new LinkedList<>();

        for (RoomData roomData : rooms) {
            if (returnRooms.size() >= limit) {
                break;
            }
            returnRooms.add(roomData);
        }
        return returnRooms;
    }
}
