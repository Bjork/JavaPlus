package me.bjork.javaplus.habbohotel.navigator.types.staffpicks;

/**
 * Created on 22/05/2017.
 */
public class StaffPick {
    private int roomId;
    private String image;

    public StaffPick(int roomId, String image)
    {
        this.roomId = roomId;
        this.image = image;
    }

    public int getRoomId() {
        return roomId;
    }

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
    	this.image = image;
    }
}
