package me.bjork.javaplus.habbohotel.navigator.types.categories;

/**
 * Created on 22/05/2017.
 */
public enum NavigatorCategoryType {
    CATEGORY,
    FEATURED,
    STAFF_PICKS,
    POPULAR,
    RECOMMENDED,
    QUERY,
    MY_ROOMS,
    MY_FAVORITES,
    MY_GROUPS,
    MY_HISTORY,
    MY_FRIENDS_ROOMS,
    MY_HISTORY_FREQ,
    TOP_PROMOTIONS,
    PROMOTION_CATEGORY,
    MY_RIGHTS
}
