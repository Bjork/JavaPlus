package me.bjork.javaplus.habbohotel.navigator.types.categories;

/**
 * Created on 22/05/2017.
 */
public enum NavigatorViewMode {
    REGULAR,
    THUMBNAIL
}

