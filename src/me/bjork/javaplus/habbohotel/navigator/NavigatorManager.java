package me.bjork.javaplus.habbohotel.navigator;

import com.google.common.collect.Lists;
import me.bjork.javaplus.core.Logging;
import me.bjork.javaplus.database.queries.navigator.NavigatorDao;
import me.bjork.javaplus.habbohotel.navigator.types.Category;
import me.bjork.javaplus.habbohotel.navigator.types.categories.NavigatorCategoryType;
import me.bjork.javaplus.habbohotel.navigator.types.publics.PublicRoom;
import me.bjork.javaplus.habbohotel.navigator.types.staffpicks.StaffPick;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created on 22/05/2017.
 */
public class NavigatorManager {
    private Map<Integer, Category> categories;
    private List<Category> userCategories;

    private Map<Integer, PublicRoom> publicRooms;
    private Map<Integer, StaffPick> staffPicks;

    public NavigatorManager() {
        this.categories = new HashMap<Integer, Category>();
        this.userCategories = new ArrayList<Category>();
        this.publicRooms = new HashMap<Integer, PublicRoom>();
        this.staffPicks = new HashMap<Integer, StaffPick>();
        this.initialize();
    }

    public void initialize() {
        this.loadCategories();
        this.loadPublicRooms();
        this.loadStaffPicks();

        Logging.println("Navigator Manager -> LOADED");
    }

    public void loadPublicRooms() {
        try {
            if (this.publicRooms != null && this.publicRooms.size() != 0) {
                this.publicRooms.clear();
            }

            this.publicRooms = NavigatorDao.getPublicRooms();

        } catch (Exception e) {
            Logging.printError("Error while loading public rooms / Error: " + e);
        }

        Logging.println("Loaded " + this.publicRooms.size() + " public rooms");
    }

    public void loadStaffPicks() {
        try {
            if (this.staffPicks != null && this.staffPicks.size() != 0) {
                this.staffPicks.clear();
            }

            this.staffPicks = NavigatorDao.getStaffPicks();

        } catch (Exception e) {
            Logging.printError("Error while loading staff picked rooms. Error: " + e);
        }

        Logging.println("Loaded " + this.staffPicks.size() + " staff picks");
    }

    public void loadCategories() {
        try {
            if (this.categories != null && this.categories.size() != 0) {
                this.categories.clear();
            }

            if (this.userCategories == null) {
                this.userCategories = Lists.newArrayList();
            } else {
                this.userCategories.clear();
            }

            this.categories = NavigatorDao.getCategories();

            for (Category category : this.categories.values()) {
                if (category.getCategoryType() == NavigatorCategoryType.CATEGORY) {
                    this.userCategories.add(category);
                }
            }
        } catch (Exception e) {
            Logging.printError("Error while loading navigator categories. Error: " + e);
        }

        Logging.println("Loaded " + (this.getCategories() == null ? 0 : this.getCategories().size()) + " room categories");
    }

    public Category getCategory(int id) {
        return this.categories.get(id);
    }

    public boolean isStaffPicked(int roomId) {
        return this.staffPicks.containsKey(roomId);
    }

    public PublicRoom getPublicRoom(int roomId) {
        return this.publicRooms.get(roomId);
    }

    public Map<Integer, Category> getCategories() {
        return this.categories;
    }

    public Map<Integer, PublicRoom> getPublicRooms(int CategoryId) {
        Map<Integer, PublicRoom> publicrooms = new HashMap<Integer, PublicRoom>();
        for (PublicRoom publicRoom : publicRooms.values())
            if (publicRoom.getCategoryId() == CategoryId)
                publicrooms.put(publicRoom.getRoomId(), publicRoom);

        return publicrooms;
    }

    public Map<Integer, StaffPick> getStaffPicks() {
        return staffPicks;
    }

    public List<Category> getUserCategories() {
        return userCategories;
    }
}
