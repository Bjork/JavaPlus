package me.bjork.javaplus.habbohotel.moderation;

import com.google.common.collect.Lists;
import me.bjork.javaplus.JavaPlusEnvironment;
import me.bjork.javaplus.core.Logging;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created on 29/05/2017.
 */
public class MuteManager {

    private Map<Integer, Double> mutedPlayers;

    public MuteManager()
    {
        this.mutedPlayers = new HashMap<Integer, Double>();
        Logging.println("Mute Manager -> LOADED");
    }

    public void processMute() {
        List<Integer> mutedPlayersToRemove = Lists.newArrayList();

        for(Map.Entry<Integer, Double> user : mutedPlayers.entrySet())
        {
            if (user.getValue() != 0 && user.getValue() <= (int) JavaPlusEnvironment.getUnixTimestamp())
                mutedPlayersToRemove.add(user.getKey());
        }

        for (int userId : mutedPlayersToRemove) {
            unmute(userId);
        }

        mutedPlayersToRemove.clear();
    }

    public boolean isMuted(int userId) {
        return this.mutedPlayers.containsKey(userId);
    }

    public void mute(int playerId, double muteTime) {
        this.mutedPlayers.put(playerId, muteTime);
    }

    public void unmute(int playerId) {
        this.mutedPlayers.remove(playerId);
    }

}
