package me.bjork.javaplus.core.language;

import me.bjork.javaplus.core.Logging;
import me.bjork.javaplus.database.queries.core.LanguageDao;

import java.util.HashMap;

/**
 * Created on 19/05/2017.
 */
public class LanguageManager {

    private static HashMap<String, String> values;

    public LanguageManager()
    {
        this.values = new HashMap<String, String>();
    }

    public void init()
    {
        if (this.values.size() > 0)
            this.values.clear();
        
        this.values = LanguageDao.loadItems();
        
        Logging.println("Loaded " + values.size() + " language locales.");
    }

    public static String TryGetValue(String value)
    {
        return values.containsKey(value) ? values.get(value) : "No language locale found for [" + value + "]";
    }
}
