package me.bjork.javaplus.core.settings;

import me.bjork.javaplus.core.Logging;
import me.bjork.javaplus.database.queries.core.SettingsDao;

import java.util.HashMap;

/**
 * Created on 19/05/2017.
 */
public class SettingsManager {

    private static HashMap<String, String> values;

    public SettingsManager()
    {
        this.values = new HashMap<String, String>();
    }

    public void init()
    {
        if (this.values.size() > 0)
            this.values.clear();
        
        this.values = SettingsDao.loadSettings();
        
        Logging.println("Loaded " + values.size() + " settings.");
    }

    public static String tryGetValue(String value)
    {
        return values.containsKey(value) ? values.get(value) : "0";
    }
}
