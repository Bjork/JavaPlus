package me.bjork.javaplus.core;

import me.bjork.javaplus.JavaPlusEnvironment;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Created on 19/05/2017.
 */
public class ConsoleCommands {

    public static void init() throws Exception
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            String line = reader.readLine();
            try {
                if (line != null) {
                    if (line.startsWith("/")) {
                        switch (line.split(" ")[0]) {
                            default:
                                System.out.print("Invalid command");
                                break;

                            case "/stop":
                                JavaPlusEnvironment.shutdownServer();
                                break;

                            case "/gc":
                                System.gc();
                                System.out.print("gc finished");
                                break;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }
}
