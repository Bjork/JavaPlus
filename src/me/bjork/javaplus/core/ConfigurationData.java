package me.bjork.javaplus.core;

import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Map;
import java.util.Properties;

/**
 * Created on 19/05/2017.
 */
public class ConfigurationData extends Properties {
    private static ConfigurationData configuration;

    public ConfigurationData(String file) {
        super();

        try {
            Reader stream = new InputStreamReader(new FileInputStream(file), "UTF-8");

            this.load(stream);
            stream.close();
        } catch (Exception e) {
            Logging.printError("Failed to fetch the server configuration (" + file + ")");
        }
    }

    /**
     * Override configuration
     *
     * @param config The config strings which you want to override
     */
    public void override(Map<String, String> config) {
        for (Map.Entry<String, String> configOverride : config.entrySet()) {
            if (this.containsKey(configOverride.getKey())) {
                this.remove(configOverride.getKey());
                this.put(configOverride.getKey(), configOverride.getValue());
            } else {
                this.put(configOverride.getKey(), configOverride.getValue());

            }
        }
    }

    /**
     * Get a string from the configuration
     *
     * @param key Retrieve a value from the config by the key
     * @return Value from the configuration
     */
    public String get(String key) {
        return this.getProperty(key);
    }

    public String get(String key, String fallback) {
        if (this.containsKey(key)) {
            return this.get(key);
        }

        return fallback;
    }

    public static ConfigurationData currentConfig() {
        return configuration;
    }

    public static void setConfiguration(ConfigurationData conf) {
        configuration = conf;
    }
}
