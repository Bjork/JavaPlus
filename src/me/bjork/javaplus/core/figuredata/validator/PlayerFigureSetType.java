package me.bjork.javaplus.core.figuredata.validator;

import java.util.Map;

/**
 * Created on 19/05/2017.
 */
public class PlayerFigureSetType {
    private String typeName;
    private int paletteId;
    private Map<Integer, PlayerFigureSet> sets;

    public PlayerFigureSetType(final String typeName, final int paletteId, final Map<Integer, PlayerFigureSet> sets) {
        this.typeName = typeName;
        this.paletteId = paletteId;
        this.sets = sets;
    }

    public Map<Integer, PlayerFigureSet> getSets() {
        return this.sets;
    }

    public int getPaletteId() {
        return this.paletteId;
    }

    public String getTypeName() {
        return this.typeName;
    }
}
