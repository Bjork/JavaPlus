package me.bjork.javaplus.core.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created on 21/05/2017.
 */
public class Util {
    public static boolean isParsable(String input){
        boolean parsable = true;
        try{
            Integer.parseInt(input);
        }catch(NumberFormatException e){
            parsable = false;
        }
        return parsable;
    }

    public static Boolean enumToBool(String value)
    {
        return (value == "1");
    }

    public static String completeDate()
    {
        return new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.FRANCE).format(new Date());
    }

    public static String statsDate()
    {
        return new SimpleDateFormat("MM/dd", Locale.FRANCE).format(new Date());
    }

    public static boolean isNumeric(String string)
            throws IllegalArgumentException
    {
        boolean isnumeric = false;
        if ((string != null) && (!string.equals("")))
        {
            isnumeric = true;
            char[] chars = string.toCharArray();
            for (char aChar : chars)
            {
                isnumeric = Character.isDigit(aChar);
                if (!isnumeric)
                {
                    break;
                }
            }
        }
        return isnumeric;
    }
}
