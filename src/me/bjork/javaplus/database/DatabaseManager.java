package me.bjork.javaplus.database;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import me.bjork.javaplus.core.ConfigurationData;
import me.bjork.javaplus.core.Logging;

/**
 * Created on 19/05/2017.
 */
public class DatabaseManager {
    private static DatabaseManager databaseManagerInstance;
    private HikariDataSource connections = null;

    public void initialize() {
        boolean isConnectionFailed = false;

        try {
            HikariConfig config = new HikariConfig();

            config.setJdbcUrl(
                    "jdbc:mysql://" + ConfigurationData.currentConfig().get("db.hostname", "127.0.0.1") +
                            ":" + ConfigurationData.currentConfig().get("db.port", "3306") + "/" + ConfigurationData.currentConfig().get("db.name", "javaplus") + "?tcpKeepAlive=" + ConfigurationData.currentConfig().get("db.pool.tcpKeepAlive") +
                            "&autoReconnect=" + ConfigurationData.currentConfig().get("db.pool.autoReconnect")
            );

            config.setUsername(ConfigurationData.currentConfig().get("db.username"));
            config.setPassword(ConfigurationData.currentConfig().get("db.password"));
            config.setMaximumPoolSize(Integer.valueOf(ConfigurationData.currentConfig().get("db.pool.max")));
            Logging.println("Connecting to the MySQL server");

            this.connections = new HikariDataSource(config);
        } catch (Exception e) {
            isConnectionFailed = true;
            Logging.printError("Failed to connect to MySQL server: " + e);
            System.exit(1);
        } finally {
            if (!isConnectionFailed) {
                Logging.println("Connection to MySQL server was successful");
            }
        }
        SqlHelper.init(this);
    }

    public static DatabaseManager getInstance() {
        if (databaseManagerInstance == null)
            databaseManagerInstance = new DatabaseManager();

        return databaseManagerInstance;
    }

    public HikariDataSource getConnections() {
        return this.connections;
    }
}
