package me.bjork.javaplus.database.queries.players;

import me.bjork.javaplus.JavaPlusEnvironment;
import me.bjork.javaplus.core.Logging;
import me.bjork.javaplus.database.SqlHelper;
import me.bjork.javaplus.habbohotel.users.Habbo;
import me.bjork.javaplus.habbohotel.users.HabboStats;

import java.sql.*;

/**
 * Created on 30/05/2017.
 */
public class HabboStatsDao {

    public static HabboStats getStats(int userId) {
        Connection sqlConnection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            sqlConnection = SqlHelper.getConnection();
            preparedStatement = SqlHelper.prepare("SELECT * FROM `user_stats` WHERE `id` = ? LIMIT 1", sqlConnection);
            preparedStatement.setInt(1, userId);
            resultSet = preparedStatement.executeQuery();
            resultSet.first();

            if (resultSet.getRow() != 0)
            {
                return new HabboStats(resultSet.getInt("id"), resultSet.getInt("RoomVisits"), resultSet.getDouble("OnlineTime"), resultSet.getInt("Respect"), resultSet.getInt("RespectGiven"), resultSet.getInt("GiftsGiven"), resultSet.getInt("GiftsReceived"), resultSet.getInt("DailyRespectPoints"), resultSet.getInt("DailyPetRespectPoints"), resultSet.getInt("AchievementScore"), resultSet.getInt("quest_id"), resultSet.getInt("quest_progress"), resultSet.getInt("groupid"), resultSet.getString("respectsTimestamp"), resultSet.getInt("forum_posts"), resultSet.getInt("rent_space_id"), resultSet.getInt("rent_space_endtime"));
            }
        } catch (Exception e) {
            Logging.exception(e);
        } finally {
            SqlHelper.closeSilently(resultSet);
            SqlHelper.closeSilently(preparedStatement);
            SqlHelper.closeSilently(sqlConnection);
        }
        return null;
    }

    public static void createUserStats(int userId) {
        try (Connection connection = SqlHelper.getConnection()) {
            Statement statement = connection.createStatement();
            statement.execute("INSERT INTO `user_stats` (`id`) VALUES ('" + userId + "')");
        } catch (SQLException e) {
            Logging.exception(e);
        }
    }

    public static void updateDailyRespects(Habbo habbo) {
        Connection sqlConnection = null;
        PreparedStatement preparedStatement = null;

        try {
            sqlConnection = SqlHelper.getConnection();
            preparedStatement = SqlHelper.prepare("UPDATE `user_stats` SET `dailyRespectPoints` = ?, `dailyPetRespectPoints` = ?, `respectsTimestamp` = ? WHERE id = ? LIMIT 1", sqlConnection);
            preparedStatement.setInt(1, habbo.getStats().getDailyRespectPoints());
            preparedStatement.setInt(2, habbo.getStats().getDailyRespectPoints());
            preparedStatement.setString(3, habbo.getStats().getRespectTimestamp());
            preparedStatement.setInt(4, habbo.getId());
            SqlHelper.executeStatementSilently(preparedStatement, false);
        } catch (SQLException e) {
            Logging.exception(e);
        } finally {
            SqlHelper.closeSilently(preparedStatement);
            SqlHelper.closeSilently(sqlConnection);
        }
    }

    public static void updatePlayerStatistics(HabboStats habboStats) {
        Connection sqlConnection = null;
        PreparedStatement preparedStatement = null;

        try {
            sqlConnection = SqlHelper.getConnection();

            preparedStatement = SqlHelper.prepare("UPDATE user_stats SET AchievementScore = ?, Respect = ?, RespectGiven = ?, DailyRespectPoints = ?, DailyPetRespectPoints = ? WHERE id = ? LIMIT 1", sqlConnection);
            preparedStatement.setInt(1, habboStats.getAchievementPoints());
            preparedStatement.setInt(2, habboStats.getRespects());
            preparedStatement.setInt(3, habboStats.getRespectGiven());
            preparedStatement.setInt(4, habboStats.getDailyRespectPoints());
            preparedStatement.setInt(5, habboStats.getDailyPetRespectPoints());
            preparedStatement.setInt(6, habboStats.getUserId());
            preparedStatement.execute();
        } catch (SQLException e) {
            SqlHelper.handleSqlException(e);
        } finally {
            SqlHelper.closeSilently(preparedStatement);
            SqlHelper.closeSilently(sqlConnection);
        }
    }

    public static void saveHabboStats(HabboStats habboStats) {
        Connection sqlConnection = null;
        PreparedStatement preparedStatement = null;

        try {
            sqlConnection = SqlHelper.getConnection();

            preparedStatement = SqlHelper.prepare("UPDATE user_stats SET RoomVisits = ?, OnlineTime = ?,  AchievementScore = ?, Respect = ?, RespectGiven = ?, GiftsGiven = ?, GiftsReceived = ?, DailyRespectPoints = ?, DailyPetRespectPoints = ?, quest_id = ?, quest_progress = ?, groupid = ?, forum_posts = ? WHERE id = ? LIMIT 1", sqlConnection);
            preparedStatement.setInt(1, habboStats.getRoomVisits());
            preparedStatement.setDouble(2, (JavaPlusEnvironment.getUnixTimestamp() - habboStats.getSessionStart() + habboStats.getOnlineTime()));
            preparedStatement.setInt(3, habboStats.getAchievementPoints());
            preparedStatement.setInt(4, habboStats.getRespects());
            preparedStatement.setInt(5, habboStats.getRespectGiven());
            preparedStatement.setInt(6, habboStats.getGiftsGiven());
            preparedStatement.setInt(7, habboStats.getGiftsReceived());
            preparedStatement.setInt(8, habboStats.getDailyRespectPoints());
            preparedStatement.setInt(9, habboStats.getDailyPetRespectPoints());
            preparedStatement.setInt(10, habboStats.getQuestId());
            preparedStatement.setInt(11, habboStats.getQuestProgress());
            preparedStatement.setInt(12, habboStats.getGroupId());
            preparedStatement.setInt(13, habboStats.getForumPosts());
            preparedStatement.setInt(14, habboStats.getUserId());
            preparedStatement.execute();
        } catch (SQLException e) {
            SqlHelper.handleSqlException(e);
        } finally {
            SqlHelper.closeSilently(preparedStatement);
            SqlHelper.closeSilently(sqlConnection);
        }
    }
}
