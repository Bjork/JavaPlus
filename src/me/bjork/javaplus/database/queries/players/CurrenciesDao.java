package me.bjork.javaplus.database.queries.players;

import me.bjork.javaplus.core.Logging;
import me.bjork.javaplus.core.settings.SettingsManager;
import me.bjork.javaplus.database.SqlHelper;
import me.bjork.javaplus.habbohotel.currencies.CurrencyDefinition;
import me.bjork.javaplus.habbohotel.users.currencies.type.CurrencyType;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created on 08/06/2017.
 */
public class CurrenciesDao {

    public static Map<Integer, CurrencyDefinition> getCurrenciesDefinitions() {
        Map<Integer, CurrencyDefinition> currencies = new HashMap<Integer, CurrencyDefinition>();

        Connection sqlConnection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            sqlConnection = SqlHelper.getConnection();
            preparedStatement = SqlHelper.prepare("SELECT `name`, `type_id`, `cycle_reward` FROM `currency_definitions`", sqlConnection);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                String currencyName = resultSet.getString("name").toUpperCase();
                if (!currencies.containsKey(currencyName))
                    currencies.put(resultSet.getInt("type_id"), new CurrencyDefinition(currencyName, resultSet.getInt("type_id"), resultSet.getInt("cycle_reward")));
            }
        } catch (Exception e) {
            Logging.exception(e);
        } finally {
            SqlHelper.closeSilently(resultSet);
            SqlHelper.closeSilently(preparedStatement);
            SqlHelper.closeSilently(sqlConnection);
        }

        return currencies;
    }


    public static Map<Integer, CurrencyType> getUserCurrencies(int userId) {

        Map<Integer, CurrencyType> userCurrencies = new HashMap<Integer, CurrencyType>();

        Connection sqlConnection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            sqlConnection = SqlHelper.getConnection();
            preparedStatement = SqlHelper.prepare("SELECT * FROM `user_currencies` WHERE `user_id` = ?", sqlConnection);
            preparedStatement.setInt(1, userId);
            resultSet = preparedStatement.executeQuery();

            if (!resultSet.next()) {
                return null;
            } else {
                do {
                    userCurrencies.put(resultSet.getInt("type"), new CurrencyType(resultSet.getInt("type"), resultSet.getInt("amount")));
                }
                while (resultSet.next());
            }
        } catch (Exception e) {
            Logging.exception(e);
        } finally {
            SqlHelper.closeSilently(resultSet);
            SqlHelper.closeSilently(preparedStatement);
            SqlHelper.closeSilently(sqlConnection);
        }
        return userCurrencies;
    }

    public static void createDefaultCurrency(int userId) {
        int duckets = Integer.valueOf(SettingsManager.tryGetValue("user.starting_duckets"));

        Connection sqlConnection = null;
        PreparedStatement preparedStatement = null;

        try {
            sqlConnection = SqlHelper.getConnection();
            preparedStatement = SqlHelper.prepare("INSERT INTO `user_currencies` (`type`, `amount`, `user_id`) VALUES (?, ?, ?)", sqlConnection);
            preparedStatement.setInt(1, 0);
            preparedStatement.setInt(2, duckets);
            preparedStatement.setInt(3, userId);
            preparedStatement.execute();

        } catch (SQLException e) {
            SqlHelper.handleSqlException(e);
        } finally {
            SqlHelper.closeSilently(preparedStatement);
            SqlHelper.closeSilently(sqlConnection);
        }
    }

    public static void createCurrency(int type, int userId) {
        Connection sqlConnection = null;
        PreparedStatement preparedStatement = null;

        try {
            sqlConnection = SqlHelper.getConnection();
            preparedStatement = SqlHelper.prepare("INSERT INTO `user_currencies` (`type`, `amount`, `user_id`) VALUES (?, ?, ?)", sqlConnection);
            preparedStatement.setInt(1, type);
            preparedStatement.setInt(2, 0);
            preparedStatement.setInt(3, userId);
            preparedStatement.execute();

        } catch (SQLException e) {
            SqlHelper.handleSqlException(e);
        } finally {
            SqlHelper.closeSilently(preparedStatement);
            SqlHelper.closeSilently(sqlConnection);
        }
    }

    public static void save(int amount, int type, int userId) {
        Connection sqlConnection = null;
        PreparedStatement preparedStatement = null;
        try {
            sqlConnection = SqlHelper.getConnection();
            preparedStatement = SqlHelper.prepare("UPDATE `user_currencies` SET `amount` = ? WHERE `type` = ? AND `user_id` = ? LIMIT 1", sqlConnection);
            preparedStatement.setInt(1, amount);
            preparedStatement.setInt(2, type);
            preparedStatement.setInt(3, userId);
            preparedStatement.execute();
        } catch (SQLException e) {
            SqlHelper.handleSqlException(e);
        } finally {
            SqlHelper.closeSilently(preparedStatement);
            SqlHelper.closeSilently(sqlConnection);
        }
    }
}
