package me.bjork.javaplus.database.queries.players;

import me.bjork.javaplus.JavaPlusEnvironment;
import me.bjork.javaplus.core.Logging;
import me.bjork.javaplus.core.utils.Util;
import me.bjork.javaplus.database.SqlHelper;
import me.bjork.javaplus.habbohotel.users.Habbo;
import me.bjork.javaplus.habbohotel.users.inventory.Badge;
import me.bjork.javaplus.habbohotel.users.messenger.friendbar.FriendBarStateUtility;

import java.sql.*;
import java.util.ArrayList;

/**
 * Created on 21/05/2017.
 */
public class HabboDao {
    public static Habbo getHabbo(String ssoTicket) {
        int userId;
        Connection sqlConnection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        ResultSet resultSet2 = null;

        try {
            sqlConnection = SqlHelper.getConnection();
            preparedStatement = SqlHelper.prepare("SELECT * FROM users WHERE auth_ticket = ? LIMIT 1", sqlConnection);
            preparedStatement.setString(1, ssoTicket);
            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                userId = resultSet.getInt("id");

                Double tradingLocked = 0.0;
                sqlConnection = SqlHelper.getConnection();
                preparedStatement = SqlHelper.prepare("SELECT * FROM `user_info` WHERE `user_id` = ? LIMIT 1", sqlConnection);
                preparedStatement.setInt(1, userId);
                resultSet2 = preparedStatement.executeQuery();

                if (resultSet2.next()) {
                    tradingLocked = resultSet.getDouble("trading_locked");
                }
                else
                {
                    SqlHelper.closeSilently(preparedStatement);

                    preparedStatement = SqlHelper.prepare("INSERT INTO `user_info` (`user_id`) VALUES (?)", sqlConnection);
                    preparedStatement.setInt(1, userId);
                    SqlHelper.executeStatementSilently(preparedStatement, false);
                }

                updateTicketUser(userId);
                return GenerateHabbo(resultSet, tradingLocked);
            }
            else
                return null;
        } catch (Exception e) {
            Logging.exception(e);
        } finally {
            SqlHelper.closeSilently(resultSet);
            SqlHelper.closeSilently(resultSet2);
            SqlHelper.closeSilently(preparedStatement);
            SqlHelper.closeSilently(sqlConnection);
        }
        return null;
    }

    private static Habbo GenerateHabbo(ResultSet row, Double tradingLocked) throws SQLException {
        return new Habbo(row.getInt("id"), row.getString("username"), row.getInt("rank"), row.getString("motto"), row.getString("look"), row.getString("gender"), row.getInt("credits"),
                row.getInt("home_room"), Util.enumToBool(row.getString("block_newfriends")), row.getInt("last_online"), Util.enumToBool(row.getString("hide_online")),
                Util.enumToBool(row.getString("hide_inroom")), row.getDouble("account_created"), row.getString("machine_id"), row.getString("volume"),
                Util.enumToBool(row.getString("chat_preference")), Util.enumToBool(row.getString("focus_preference")), Util.enumToBool(row.getString("pets_muted")),
                Util.enumToBool(row.getString("bots_muted")), row.getDouble("last_change"), Util.enumToBool(row.getString("ignore_invites")), row.getDouble("time_muted"),
                tradingLocked, Util.enumToBool(row.getString("allow_gifts")), row.getInt("friend_bar_state"), Util.enumToBool(row.getString("disable_forced_effects")),
                Util.enumToBool(row.getString("allow_mimic")), row.getInt("rank_vip"));

    }

    private static void updateTicketUser(int userId) {
        try (Connection connection = SqlHelper.getConnection())
        {
            Statement statement = connection.createStatement();
            statement.execute("UPDATE `users` SET `online` = '1', `auth_ticket` = '' WHERE `id` = '" + userId + "' LIMIT 1");
        }
        catch (SQLException e)
        {
            Logging.exception(e);
        }
    }

    public static String getUsernameById(int userId) throws SQLException
    {
        String Username = "";

        Connection sqlConnection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            sqlConnection = SqlHelper.getConnection();
            preparedStatement = SqlHelper.prepare("SELECT username FROM users WHERE id = ? LIMIT 1", sqlConnection);
            preparedStatement.setInt(1, userId);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                Username = resultSet.getString("username");
            }
            else
                return Username;
        } catch (Exception e) {
            Logging.exception(e);
        } finally {
            SqlHelper.closeSilently(resultSet);
            SqlHelper.closeSilently(preparedStatement);
            SqlHelper.closeSilently(sqlConnection);
        }
        return Username;
    }

    public static int getIdByUsername(String username) throws SQLException
    {
        int UserId = 0;
        Connection sqlConnection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            sqlConnection = SqlHelper.getConnection();
            preparedStatement = SqlHelper.prepare("SELECT id FROM users WHERE username = ? LIMIT 1", sqlConnection);
            preparedStatement.setString(1, username);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                UserId = resultSet.getInt("id");
            }
            else
                return UserId;
        } catch (Exception e) {
            Logging.exception(e);
        } finally {
            SqlHelper.closeSilently(resultSet);
            SqlHelper.closeSilently(preparedStatement);
            SqlHelper.closeSilently(sqlConnection);
        }
        return UserId;
    }

    public static int getIdBySSO(String ssoTicket) throws SQLException
    {
        int UserId = 0;
        Connection sqlConnection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            sqlConnection = SqlHelper.getConnection();
            preparedStatement = SqlHelper.prepare("SELECT id FROM users WHERE auth_ticket = ? LIMIT 1", sqlConnection);
            preparedStatement.setString(1, ssoTicket);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                UserId = resultSet.getInt("id");
            }
            else
                return UserId;
        } catch (Exception e) {
            Logging.exception(e);
        } finally {
            SqlHelper.closeSilently(resultSet);
            SqlHelper.closeSilently(preparedStatement);
            SqlHelper.closeSilently(sqlConnection);
        }
        return UserId;
    }

    public static String getIpAddress(int userId) {
        Connection sqlConnection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            sqlConnection = SqlHelper.getConnection();

            preparedStatement = SqlHelper.prepare("SELECT `ip_last` FROM users WHERE `id` = ?", sqlConnection);
            preparedStatement.setInt(1, userId);

            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                return resultSet.getString("ip_last");
            }
        } catch (SQLException e) {
            SqlHelper.handleSqlException(e);
        } finally {
            SqlHelper.closeSilently(resultSet);
            SqlHelper.closeSilently(preparedStatement);
            SqlHelper.closeSilently(sqlConnection);
        }
        return "";
    }

    public static ArrayList<Badge> getUserBadges(int userId) {

        ArrayList<Badge> badges = new ArrayList<Badge>();

        Connection sqlConnection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            sqlConnection = SqlHelper.getConnection();
            preparedStatement = SqlHelper.prepare("SELECT * FROM user_badges WHERE badge_slot > 0 AND user_id = ? ORDER BY badge_slot ASC", sqlConnection);
            preparedStatement.setInt(1, userId);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next())
                badges.add(new Badge(resultSet.getString("badge_id"), resultSet.getInt("badge_slot")));

        } catch (Exception e) {
            Logging.exception(e);
        } finally {
            SqlHelper.closeSilently(resultSet);
            SqlHelper.closeSilently(preparedStatement);
            SqlHelper.closeSilently(sqlConnection);
        }

        return badges;
    }

    public static void updateHabboData(int id, String username, String motto, String look, int credits, String gender) {
        Connection sqlConnection = null;
        PreparedStatement preparedStatement = null;
        try {
            sqlConnection = SqlHelper.getConnection();
            preparedStatement = SqlHelper.prepare("UPDATE users SET username = ?, motto = ?, look = ?, credits = ?, gender = ? WHERE id = ? LIMIT 1", sqlConnection);
            preparedStatement.setString(1, username);
            preparedStatement.setString(2, motto);
            preparedStatement.setString(3, look);
            preparedStatement.setInt(4, credits);
            preparedStatement.setString(5, gender);
            preparedStatement.setInt(6, id);
            SqlHelper.executeStatementSilently(preparedStatement, false);
        } catch (SQLException e) {
            SqlHelper.handleSqlException(e);
        } finally {
            SqlHelper.closeSilently(preparedStatement);
            SqlHelper.closeSilently(sqlConnection);
        }
    }

    public static void saveHabbo(Habbo habbo) {
        Connection sqlConnection = null;
        PreparedStatement preparedStatement = null;
        try {
            sqlConnection = SqlHelper.getConnection();
            preparedStatement = SqlHelper.prepare("UPDATE users SET online = ?, last_online = ?, credits = ?, home_room = ?, time_muted = ?, friend_bar_state = ? WHERE id = ? LIMIT 1", sqlConnection);
            preparedStatement.setString(1, "0");
            preparedStatement.setInt(2, JavaPlusEnvironment.getIntUnixTimestamp());
            preparedStatement.setInt(3, habbo.getCredits());
            preparedStatement.setInt(4, habbo.getHomeroom());
            preparedStatement.setDouble(5, habbo.getTimeMuted());
            preparedStatement.setString(6, String.valueOf(FriendBarStateUtility.getInt(habbo.getFriendBarState())));
            preparedStatement.setInt(7, habbo.getId());
            SqlHelper.executeStatementSilently(preparedStatement, false);
        } catch (SQLException e) {
            SqlHelper.handleSqlException(e);
        } finally {
            SqlHelper.closeSilently(preparedStatement);
            SqlHelper.closeSilently(sqlConnection);
        }
    }
}