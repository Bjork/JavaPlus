package me.bjork.javaplus.database.queries.navigator;

import me.bjork.javaplus.database.SqlHelper;
import me.bjork.javaplus.habbohotel.navigator.types.Category;
import me.bjork.javaplus.habbohotel.navigator.types.categories.NavigatorViewMode;
import me.bjork.javaplus.habbohotel.navigator.types.publics.PublicRoom;
import me.bjork.javaplus.habbohotel.navigator.types.staffpicks.StaffPick;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created on 22/05/2017.
 */
public class NavigatorDao {
    public static LinkedHashMap<Integer, PublicRoom> getPublicRooms() {
        Connection sqlConnection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        LinkedHashMap<Integer, PublicRoom> data = new LinkedHashMap<>();

        try {
            sqlConnection = SqlHelper.getConnection();
            preparedStatement = SqlHelper.prepare("SELECT * FROM `navigator_publics` WHERE `enabled` = '1' ORDER BY `order_num` ASC", sqlConnection);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                final int Id = resultSet.getInt("room_id");
                data.put(Id, new PublicRoom(resultSet.getInt("room_id"), resultSet.getString("caption"), resultSet.getString("description"), resultSet.getString("image_url"), resultSet.getInt("cat_id")));
            }
        } catch (SQLException e) {
            SqlHelper.handleSqlException(e);
        } finally {
            SqlHelper.closeSilently(resultSet);
            SqlHelper.closeSilently(preparedStatement);
            SqlHelper.closeSilently(sqlConnection);
        }

        return data;
    }

    public static LinkedHashMap<Integer, StaffPick> getStaffPicks() {
        Connection sqlConnection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        LinkedHashMap<Integer, StaffPick> data = new LinkedHashMap<>();

        try {
            sqlConnection = SqlHelper.getConnection();
            preparedStatement = SqlHelper.prepare("SELECT * FROM `navigator_staff_picks`", sqlConnection);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                final int roomId = resultSet.getInt("room_id");

                data.put(roomId, new StaffPick(roomId, resultSet.getString("image")));
            }
        } catch (SQLException e) {
            SqlHelper.handleSqlException(e);
        } finally {
            SqlHelper.closeSilently(resultSet);
            SqlHelper.closeSilently(preparedStatement);
            SqlHelper.closeSilently(sqlConnection);
        }

        return data;
    }

    public static Map<Integer, Category> getCategories() {
        Connection sqlConnection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        LinkedHashMap<Integer, Category> data = new LinkedHashMap<>();

        try {
            sqlConnection = SqlHelper.getConnection();
            preparedStatement = SqlHelper.prepare("SELECT * FROM `navigator_categories` WHERE `enabled` = '1' ORDER BY `id` ASC", sqlConnection);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                data.put(resultSet.getInt("id"), new Category(
                        resultSet.getInt("id"),
                        resultSet.getString("category"),
                        resultSet.getString("category_identifier"),
                        resultSet.getString("public_name"),
                        true,
                        -1,
                        resultSet.getInt("required_rank"),
                        NavigatorViewMode.valueOf(resultSet.getString("view_mode").toUpperCase()),
                        resultSet.getString("category_type"),
                        resultSet.getString("search_allowance"),
                        resultSet.getInt("order_id"),
                        resultSet.getString("visible").equals("1"),
                        resultSet.getInt("room_count"),
                        resultSet.getInt("room_count_expanded")));
            }
        } catch (SQLException e) {
            SqlHelper.handleSqlException(e);
        } finally {
            SqlHelper.closeSilently(resultSet);
            SqlHelper.closeSilently(preparedStatement);
            SqlHelper.closeSilently(sqlConnection);
        }

        return data;
    }

    public static void deleteStaffPick(int roomId) {
        Connection sqlConnection = null;
        PreparedStatement preparedStatement = null;

        try {
            sqlConnection = SqlHelper.getConnection();

            preparedStatement = SqlHelper.prepare("DELETE FROM `navigator_staff_picks` WHERE `room_id` = ? LIMIT 1", sqlConnection);
            preparedStatement.setInt(1, roomId);

            SqlHelper.executeStatementSilently(preparedStatement, false);
        } catch (SQLException e) {
            SqlHelper.handleSqlException(e);
        } finally {
            SqlHelper.closeSilently(preparedStatement);
            SqlHelper.closeSilently(sqlConnection);
        }
    }

    public static void addStaffPick(int roomId, int picker) {
        Connection sqlConnection = null;
        PreparedStatement preparedStatement = null;

        try {
            sqlConnection = SqlHelper.getConnection();
            preparedStatement = SqlHelper.prepare("INSERT into navigator_staff_picks VALUES(?, ?, ?)", sqlConnection);

            preparedStatement.setInt(1, roomId);
            preparedStatement.setString(2, null);
            preparedStatement.setInt(3, picker);

            preparedStatement.execute();
        } catch (SQLException e) {
            SqlHelper.handleSqlException(e);
        } finally {
            SqlHelper.closeSilently(preparedStatement);
            SqlHelper.closeSilently(sqlConnection);
        }
    }
}
