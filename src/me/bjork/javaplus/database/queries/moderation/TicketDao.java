package me.bjork.javaplus.database.queries.moderation;

import me.bjork.javaplus.database.SqlHelper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created on 08/06/2017.
 */
public class TicketDao {
    public static void updateTickets(int userId) {
        Connection sqlConnection = null;
        PreparedStatement preparedStatement = null;
        try {
            sqlConnection = SqlHelper.getConnection();
            preparedStatement = SqlHelper.prepare("UPDATE moderation_tickets SET status = ?, moderator_id = ? WHERE status = ? AND moderator_id = ?", sqlConnection);
            preparedStatement.setString(1, "open");
            preparedStatement.setInt(2, 0);
            preparedStatement.setString(3, "picked");
            preparedStatement.setInt(4, userId);
            SqlHelper.executeStatementSilently(preparedStatement, false);

        } catch (SQLException e) {
            SqlHelper.handleSqlException(e);
        } finally {
            SqlHelper.closeSilently(preparedStatement);
            SqlHelper.closeSilently(sqlConnection);
        }
    }
}
