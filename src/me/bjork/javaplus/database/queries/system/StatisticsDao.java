package me.bjork.javaplus.database.queries.system;

import me.bjork.javaplus.database.SqlHelper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created on 11/06/2017.
 */
public class StatisticsDao {
    public static void saveStatistics(int players, int rooms) {
        Connection sqlConnection = null;
        PreparedStatement preparedStatement = null;

        try {
            sqlConnection = SqlHelper.getConnection();
            preparedStatement = SqlHelper.prepare("UPDATE server_status SET users_online = ?, loaded_rooms = ? LIMIT 1", sqlConnection);
            preparedStatement.setInt(1, players);
            preparedStatement.setInt(2, rooms);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            SqlHelper.handleSqlException(e);
        } finally {
            SqlHelper.closeSilently(preparedStatement);
            SqlHelper.closeSilently(sqlConnection);
        }
    }
}
