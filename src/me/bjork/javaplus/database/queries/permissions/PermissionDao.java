package me.bjork.javaplus.database.queries.permissions;

import me.bjork.javaplus.database.SqlHelper;
import me.bjork.javaplus.habbohotel.permissions.types.Permission;
import me.bjork.javaplus.habbohotel.permissions.types.PermissionCommand;
import me.bjork.javaplus.habbohotel.permissions.types.PermissionGroup;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created on 24/05/2017.
 */
public class PermissionDao {

    public static Map<Integer,Permission> getPermissions() {
        Map<Integer, Permission> permissions = new HashMap<Integer, Permission>();

        Connection sqlConnection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            sqlConnection = SqlHelper.getConnection();
            preparedStatement = SqlHelper.prepare("SELECT * FROM `permissions`", sqlConnection);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                permissions.put(resultSet.getInt("id"), new Permission(resultSet.getInt("id"), resultSet.getString("permission"), resultSet.getString("description")));
            }
        } catch (SQLException e) {
            SqlHelper.handleSqlException(e);
        } finally {
            SqlHelper.closeSilently(resultSet);
            SqlHelper.closeSilently(preparedStatement);
            SqlHelper.closeSilently(sqlConnection);
        }

        return permissions;
    }

    public static Map<String, PermissionCommand> getCommands() {
        Map<String, PermissionCommand> commands = new HashMap<String, PermissionCommand>();

        Connection sqlConnection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            sqlConnection = SqlHelper.getConnection();
            preparedStatement = SqlHelper.prepare("SELECT * FROM `permissions_commands`", sqlConnection);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                commands.put(resultSet.getString("command"), new PermissionCommand(resultSet.getString("command"), resultSet.getInt("group_id"), resultSet.getInt("subscription_id")));
            }
        } catch (SQLException e) {
            SqlHelper.handleSqlException(e);
        } finally {
            SqlHelper.closeSilently(resultSet);
            SqlHelper.closeSilently(preparedStatement);
            SqlHelper.closeSilently(sqlConnection);
        }

        return commands;
    }

    public static Map<Integer,PermissionGroup> getPermissionGroups() {
        Map<Integer, PermissionGroup> permissionsGroups = new HashMap<Integer, PermissionGroup>();

        Connection sqlConnection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            sqlConnection = SqlHelper.getConnection();
            preparedStatement = SqlHelper.prepare("SELECT * FROM `permissions_groups`", sqlConnection);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                permissionsGroups.put(resultSet.getInt("id"), new PermissionGroup(resultSet.getInt("id"), resultSet.getString("name"), resultSet.getString("description"), resultSet.getString("badge_code"), resultSet.getInt("floodtime")));
            }
        } catch (SQLException e) {
            SqlHelper.handleSqlException(e);
        } finally {
            SqlHelper.closeSilently(resultSet);
            SqlHelper.closeSilently(preparedStatement);
            SqlHelper.closeSilently(sqlConnection);
        }

        return permissionsGroups;
    }
}
