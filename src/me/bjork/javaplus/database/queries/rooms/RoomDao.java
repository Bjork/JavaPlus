package me.bjork.javaplus.database.queries.rooms;

import com.google.common.collect.Lists;
import me.bjork.javaplus.JavaPlusEnvironment;
import me.bjork.javaplus.core.Logging;
import me.bjork.javaplus.database.SqlHelper;
import me.bjork.javaplus.database.queries.players.HabboDao;
import me.bjork.javaplus.habbohotel.gameclients.GameClient;
import me.bjork.javaplus.habbohotel.rooms.Room;
import me.bjork.javaplus.habbohotel.rooms.RoomData;
import me.bjork.javaplus.habbohotel.rooms.models.RoomModel;
import me.bjork.javaplus.habbohotel.rooms.promotions.RoomPromotion;
import me.bjork.javaplus.habbohotel.rooms.settings.RoomBanState;
import me.bjork.javaplus.habbohotel.rooms.settings.RoomKickState;
import me.bjork.javaplus.habbohotel.rooms.settings.RoomMuteState;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created on 22/05/2017.
 */
public class RoomDao {

    public static List<RoomData> getRoomDataByOwnerSortByName(int ownerId)
    {
        List<RoomData> data = Lists.newArrayList();

        Connection sqlConnection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            sqlConnection = SqlHelper.getConnection();
            preparedStatement = SqlHelper.prepare("SELECT * FROM `rooms` WHERE `owner` = ? ORDER BY `caption`", sqlConnection);
            preparedStatement.setInt(1, ownerId);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String modelName = resultSet.getString("model_name");

                if (JavaPlusEnvironment.getGame().getRoomManager().getActiveRooms().containsKey(id))
                    data.add(JavaPlusEnvironment.getGame().getRoomManager().getRoom(id));
                else
                {
                    if (!JavaPlusEnvironment.getGame().getRoomManager().tryGetModel(modelName))
                        continue;

                    String ownerName = HabboDao.getUsernameById(ownerId);
                    data.add(new RoomData(resultSet.getInt("id"), resultSet.getString("caption"), resultSet.getString("model_name"), ownerName, resultSet.getInt("owner"), resultSet.getString("password"),
                            resultSet.getInt("score"), resultSet.getString("roomtype"), resultSet.getString("state"), resultSet.getInt("users_now"), resultSet.getInt("users_max"),
                            resultSet.getInt("category"), resultSet.getString("description"), resultSet.getString("tags"), resultSet.getString("floor"), resultSet.getString("wallpaper"),
                            resultSet.getString("landscape"), resultSet.getInt("allow_pets"), resultSet.getInt("allow_pets_eat"), resultSet.getInt("room_blocking_disabled"),
                            resultSet.getInt("allow_hidewall"), resultSet.getInt("wallthick"), resultSet.getInt("floorthick"), RoomMuteState.valueOf(resultSet.getInt("mute_settings")), RoomBanState.valueOf(resultSet.getInt("ban_settings")),
                            RoomKickState.valueOf(resultSet.getInt("kick_settings")), resultSet.getInt("chat_mode"), resultSet.getInt("chat_size"), resultSet.getInt("chat_speed"), resultSet.getInt("chat_extra_flood"),
                            resultSet.getInt("chat_hearing_distance"), resultSet.getInt("trade_settings"), resultSet.getString("push_enabled") == "1", resultSet.getString("pull_enabled") == "1",
                            resultSet.getString("spush_enabled") == "1", resultSet.getString("spull_enabled") == "1", resultSet.getString("respect_notifications_enabled") == "1",
                            resultSet.getString("pet_morphs_allowed") == "1", resultSet.getInt("group_id")));
                }
            }

        } catch (Exception e) {
            Logging.exception(e);
        } finally {
            SqlHelper.closeSilently(resultSet);
            SqlHelper.closeSilently(preparedStatement);
            SqlHelper.closeSilently(sqlConnection);
        }
        return data;
    }

    public static Map<Integer, RoomData> getRoomsByOwnerId(int ownerId) {
        Connection sqlConnection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        Map<Integer, RoomData> rooms = new HashMap<>();

        try {
            sqlConnection = SqlHelper.getConnection();

            preparedStatement = SqlHelper.prepare("SELECT * FROM `rooms` WHERE `owner` = ? ORDER BY `caption`", sqlConnection);
            preparedStatement.setInt(1, ownerId);

            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                rooms.put(resultSet.getInt("id"), new RoomData(resultSet));
            }

        } catch (SQLException e) {
            SqlHelper.handleSqlException(e);
        } finally {
            SqlHelper.closeSilently(resultSet);
            SqlHelper.closeSilently(preparedStatement);
            SqlHelper.closeSilently(sqlConnection);
        }

        return rooms;
    }

    public static RoomData getRoomDataByRoomId(int roomId)
    {
        RoomData data = null;
        Connection sqlConnection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            sqlConnection = SqlHelper.getConnection();
            preparedStatement = SqlHelper.prepare("SELECT * FROM `rooms` WHERE `id` = ? LIMIT 1", sqlConnection);
            preparedStatement.setInt(1, roomId);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                int owner = resultSet.getInt("owner");
                if (JavaPlusEnvironment.getGame().getRoomManager().getActiveRooms().containsKey(id))
                {
                    data = JavaPlusEnvironment.getGame().getRoomManager().getRoom(id);
                }
                else
                {
                    String ownerName = HabboDao.getUsernameById(owner);
                    data = new RoomData(resultSet.getInt("id"), resultSet.getString("caption"), resultSet.getString("model_name"), ownerName, resultSet.getInt("owner"), resultSet.getString("password"),
                            resultSet.getInt("score"), resultSet.getString("roomtype"), resultSet.getString("state"), resultSet.getInt("users_now"), resultSet.getInt("users_max"),
                            resultSet.getInt("category"), resultSet.getString("description"), resultSet.getString("tags"), resultSet.getString("floor"), resultSet.getString("wallpaper"),
                            resultSet.getString("landscape"), resultSet.getInt("allow_pets"), resultSet.getInt("allow_pets_eat"), resultSet.getInt("room_blocking_disabled"),
                            resultSet.getInt("allow_hidewall"), resultSet.getInt("wallthick"), resultSet.getInt("floorthick"), RoomMuteState.valueOf(resultSet.getInt("mute_settings")), RoomBanState.valueOf(resultSet.getInt("ban_settings")),
                            RoomKickState.valueOf(resultSet.getInt("kick_settings")), resultSet.getInt("chat_mode"), resultSet.getInt("chat_size"), resultSet.getInt("chat_speed"), resultSet.getInt("chat_extra_flood"),
                            resultSet.getInt("chat_hearing_distance"), resultSet.getInt("trade_settings"), resultSet.getString("push_enabled") == "1", resultSet.getString("pull_enabled") == "1",
                            resultSet.getString("spush_enabled") == "1", resultSet.getString("spull_enabled") == "1", resultSet.getString("respect_notifications_enabled") == "1",
                            resultSet.getString("pet_morphs_allowed") == "1", resultSet.getInt("group_id"));
                }
            }

        } catch (Exception e) {
            Logging.exception(e);
        } finally {
            SqlHelper.closeSilently(resultSet);
            SqlHelper.closeSilently(preparedStatement);
            SqlHelper.closeSilently(sqlConnection);
        }
        return data;
    }

    public static void getActivePromotions(Map<Integer, RoomPromotion> roomPromotions) {
        Connection sqlConnection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            sqlConnection = SqlHelper.getConnection();
            preparedStatement = SqlHelper.prepare("SELECT * FROM `room_promotions` WHERE timestamp_expire > " + JavaPlusEnvironment.getUnixTimestamp(), sqlConnection);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                roomPromotions.put(resultSet.getInt("room_id"), new RoomPromotion(resultSet.getInt("room_id"), resultSet.getString("title"), resultSet.getString("description"), resultSet.getLong("timestamp_start"), resultSet.getLong("time_expire"), resultSet.getInt("category_id")));
            }

        } catch (SQLException e) {
            SqlHelper.handleSqlException(e);
        } finally {
            SqlHelper.closeSilently(resultSet);
            SqlHelper.closeSilently(preparedStatement);
            SqlHelper.closeSilently(sqlConnection);
        }
    }

    public static void deleteExpiredRoomPromotions() {
        Connection sqlConnection = null;
        PreparedStatement preparedStatement = null;

        try {
            sqlConnection = SqlHelper.getConnection();
            preparedStatement = SqlHelper.prepare("DELETE FROM `room_promotions` WHERE timestamp_expire < " + JavaPlusEnvironment.getUnixTimestamp(), sqlConnection);
            preparedStatement.execute();
        } catch (SQLException e) {
            SqlHelper.handleSqlException(e);
        } finally {
            SqlHelper.closeSilently(preparedStatement);
            SqlHelper.closeSilently(sqlConnection);
        }
    }

    public static List<RoomData> getRoomsByQuery(String query) {
        Connection sqlConnection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        List<RoomData> rooms = new ArrayList<>();

        try {
            sqlConnection = SqlHelper.getConnection();
            if (query.startsWith("owner:")) {
                int UserId = HabboDao.getIdByUsername(query.split("owner:")[1]);
                preparedStatement = SqlHelper.prepare("SELECT * FROM rooms WHERE owner = ?", sqlConnection);
                preparedStatement.setInt(1, UserId);
            } else if (query.startsWith("tag:")) {
                preparedStatement = SqlHelper.prepare("SELECT * FROM rooms WHERE tags LIKE ? LIMIT 50", sqlConnection);

                String tagName = SqlHelper.escapeWildcards(query.split("tag:")[1]);
                preparedStatement.setString(1, "%" + tagName + "%");
            } else {
                // escape wildcard characters
                query = SqlHelper.escapeWildcards(query);

                preparedStatement = SqlHelper.prepare("SELECT * FROM rooms WHERE caption LIKE ? LIMIT 50", sqlConnection);
                preparedStatement.setString(1, query + "%");
            }

            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                rooms.add(new RoomData(resultSet));
            }
        } catch (SQLException e) {
            SqlHelper.handleSqlException(e);
        } finally {
            SqlHelper.closeSilently(resultSet);
            SqlHelper.closeSilently(preparedStatement);
            SqlHelper.closeSilently(sqlConnection);
        }
        return rooms;
    }

    public static Map<String, RoomModel> getModels() {
        Connection sqlConnection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        Map<String, RoomModel> data = new HashMap<>();

        try {
            sqlConnection = SqlHelper.getConnection();

            preparedStatement = SqlHelper.prepare("SELECT * FROM room_models_new", sqlConnection);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                data.put(resultSet.getString("id"), new RoomModel(resultSet.getString("id"), resultSet.getInt("door_x"), resultSet.getInt("door_y"), resultSet.getInt("door_z"), resultSet.getInt("door_dir"), resultSet.getString("heightmap"), resultSet.getInt("wall_height")));
            }

        } catch (Exception e) {
            if (e instanceof SQLException)
                SqlHelper.handleSqlException(((SQLException) e));
        } finally {
            SqlHelper.closeSilently(resultSet);
            SqlHelper.closeSilently(preparedStatement);
            SqlHelper.closeSilently(sqlConnection);
        }

        return data;
    }

    public static void updateSettings(GameClient client, Room room) {
        Connection sqlConnection = null;
        PreparedStatement preparedStatement = null;
        try {
            sqlConnection = SqlHelper.getConnection();
            preparedStatement = SqlHelper.prepare("UPDATE user_roomvisits SET exit_timestamp = ? WHERE room_id = ? AND user_id = ? ORDER BY exit_timestamp DESC LIMIT 1", sqlConnection);
            preparedStatement.setInt(1, (int)JavaPlusEnvironment.getUnixTimestamp());
            preparedStatement.setInt(2, room.getId());
            preparedStatement.setInt(3, client.getHabbo().getId());
            SqlHelper.executeStatementSilently(preparedStatement, false);
        } catch (SQLException e) {
            SqlHelper.handleSqlException(e);
        } finally {
            SqlHelper.closeSilently(preparedStatement);
            SqlHelper.closeSilently(sqlConnection);
        }
    }

    public static void updateUserCount(int roomId, int usersNow) {
        Connection sqlConnection = null;
        PreparedStatement preparedStatement = null;
        try {
            sqlConnection = SqlHelper.getConnection();
            preparedStatement = SqlHelper.prepare("UPDATE `rooms` SET `users_now` = ? WHERE `id` = ? LIMIT 1", sqlConnection);
            preparedStatement.setInt(1, usersNow);
            preparedStatement.setInt(2, roomId);
            SqlHelper.executeStatementSilently(preparedStatement, false);
        } catch (SQLException e) {
            SqlHelper.handleSqlException(e);
        } finally {
            SqlHelper.closeSilently(preparedStatement);
            SqlHelper.closeSilently(sqlConnection);
        }
    }

    public static void saveScore(int score, int roomId) {
        Connection sqlConnection = null;
        PreparedStatement preparedStatement = null;
        try {
            sqlConnection = SqlHelper.getConnection();
            preparedStatement = SqlHelper.prepare("UPDATE `rooms` SET `score` = ? WHERE `id` = ? LIMIT 1", sqlConnection);
            preparedStatement.setInt(1, score);
            preparedStatement.setInt(2, roomId);
            SqlHelper.executeStatementSilently(preparedStatement, false);
        } catch (SQLException e) {
            SqlHelper.handleSqlException(e);
        } finally {
            SqlHelper.closeSilently(preparedStatement);
            SqlHelper.closeSilently(sqlConnection);
        }
    }
}
