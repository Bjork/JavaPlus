package me.bjork.javaplus.database.queries.rooms;

import me.bjork.javaplus.database.SqlHelper;
import me.bjork.javaplus.habbohotel.rooms.components.types.RoomBan;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created on 24/05/2017.
 */
public class RightsDao {
    public static List<Integer> getRightsByRoomId(int roomId) {
        Connection sqlConnection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        List<Integer> data = new CopyOnWriteArrayList<>();

        try {
            sqlConnection = SqlHelper.getConnection();

            preparedStatement = SqlHelper.prepare("SELECT user_id FROM room_rights WHERE room_id = ?", sqlConnection);
            preparedStatement.setInt(1, roomId);

            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                data.add(resultSet.getInt("user_id"));
            }
        } catch (SQLException e) {
            SqlHelper.handleSqlException(e);
        } finally {
            SqlHelper.closeSilently(resultSet);
            SqlHelper.closeSilently(preparedStatement);
            SqlHelper.closeSilently(sqlConnection);
        }
        return data;
    }

    public static Map<Integer, RoomBan> getRoomBansByRoomId(int roomId) {
        Connection sqlConnection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        Map<Integer, RoomBan> data = new ConcurrentHashMap<>();

        try {
            sqlConnection = SqlHelper.getConnection();

            preparedStatement = SqlHelper.prepare("SELECT b.`room_id`, b.`user_id`, b.`expire`, p.`username` AS player_name FROM `room_bans` b LEFT JOIN `users` AS p ON p.`id` = b.`user_id` WHERE b.`room_id` = ? AND b.`expire` >= UNIX_TIMESTAMP() OR b.`room_id` = ? AND b.`expire` = -1", sqlConnection);
            preparedStatement.setInt(1, roomId);
            preparedStatement.setInt(2, roomId);

            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                data.put(resultSet.getInt("user_id"), new RoomBan(resultSet.getInt("user_id"), resultSet.getString("username"), resultSet.getInt("expire")));
            }
        } catch (SQLException e) {
            SqlHelper.handleSqlException(e);
        } finally {
            SqlHelper.closeSilently(resultSet);
            SqlHelper.closeSilently(preparedStatement);
            SqlHelper.closeSilently(sqlConnection);
        }

        return data;
    }
}
