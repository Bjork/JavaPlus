ALTER TABLE `navigator_categories`
ADD COLUMN `room_count`  int(11) NOT NULL DEFAULT 15,
ADD COLUMN `room_count_expanded`  int(11) NOT NULL DEFAULT 50,
ADD COLUMN `visible`  enum('0','1') NOT NULL DEFAULT '1';

ALTER TABLE `navigator_staff_picks`
ADD COLUMN `picker_id`  int(11) NOT NULL DEFAULT 0;

INSERT INTO `server_settings` (`key`, `value`, `description`) VALUES ('room.idle_time', '15', 'time of a user being idle');
INSERT INTO `server_settings` (`key`, `value`, `description`) VALUES ('word.filter_mode', 'default', 'filter of the wordfilter');
INSERT INTO `server_settings` (`key`, `value`, `description`) VALUES ('word.filter_strict_char', '', 'Character to filter');

ALTER TABLE `permissions_groups`
ADD COLUMN `floodtime`  int(11) NOT NULL DEFAULT 30;
INSERT INTO `server_settings` (`key`, `value`, `description`) VALUES ('use.ip_last', 'true', 'Use the ipLast instead of Client Ip');
INSERT INTO `server_settings` (`key`, `value`, `description`) VALUES ('minrank.see_whispers', '2', 'Min rank for user can see whispers');